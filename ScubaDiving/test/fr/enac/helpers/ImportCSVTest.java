package fr.enac.helpers;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Test;

import fr.enac.utils.ImportCSV;

public class ImportCSVTest {

	@Test
	public void testGetResource() {
		final String fileName = "test/ressources/test_email_plongeurs.csv";
		final File file = ImportCSV.getResource(fileName);
		assertTrue(file.exists());
	}

	
	@Test
	public void testReadFile() {
		final String fileName = "test/ressources/test_100plongeurs_18-02-2014.csv";
		final int nombreLigne = 100;
	    long begin = System.currentTimeMillis();
		final File file = ImportCSV.getResource(fileName);
		try{
			List<String> lines = ImportCSV.readFile(file);
			
			long end = System.currentTimeMillis();
		    System.out.println("Temps d'ex�cution des 100plongeurs :"+ ((float) (end-begin)) / 1000f);
		    System.out.println("Nombres de lignes :"+ (lines.size()-1));
		    System.out.println(lines.toString());
			assertEquals(nombreLigne, lines.size()-1);	
		}
		catch(IOException e){
			fail("File not found");
		}	    
	}
}


