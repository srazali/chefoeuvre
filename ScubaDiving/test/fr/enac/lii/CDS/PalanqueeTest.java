/*
 * Copyright Sébastien LERICHE, 2014
 * 
 * sebastien.leriche@flabelline.com
 * 
 * Ce logiciel est un programme informatique servant à manipuler les concepts de
 * plongée sous-marine exprimés dans le Code du Sport.
 * 
 * Ce logiciel est régi par la licence CeCILL-B soumise au
 * droit français et respectant les principes de diffusion des logiciels libres.
 * Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les
 * conditions de la licence CeCILL-B telle que diffusée par le
 * CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie, de
 * modification et de redistribution accordés par cette licence, il n'est offert
 * aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une
 * responsabilité restreinte pèse sur l'auteur du programme, le titulaire des
 * droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard l'attention de l'utilisateur est attirée sur les risques associés
 * au chargement, à l'utilisation, à la modification et/ou au développement et à
 * la reproduction du logiciel par l'utilisateur étant donné sa spécificité de
 * logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve
 * donc à des développeurs et des professionnels avertis possédant des
 * connaissances informatiques approfondies. Les utilisateurs sont donc invités
 * à charger et tester l'adéquation du logiciel à leurs besoins dans des
 * conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs
 * données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes
 * conditions de sécurité.
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL-B, et que vous en avez
 * accepté les termes.
 */

/**
 * @author leriche
 */

package fr.enac.lii.CDS;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.enac.lii.CDS.Palanquee;
import fr.enac.lii.CDS.Plongeur;
import fr.enac.lii.CDS.TypeGaz;
import fr.enac.lii.CDS.TypePlongee;
import static org.junit.Assert.*;

public class PalanqueeTest {
	public boolean verbose = true;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		Palanquee p = new Palanquee();
		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur seb = new Plongeur();
		seb.specifierNiveauCDS("E4 PTH-120");

		Plongeur racim = new Plongeur(); // baptême
		p.ajouterEncadrant(seb);
		p.ajouterPlongeur(racim, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(6, p.profondeurMax());
		assertEquals(true, p.peutPlonger());
	}

	@Test
	public void test2() {
		Palanquee p = new Palanquee();

		Plongeur racim = new Plongeur();
		p.ajouterPlongeur(racim, 0);

		if (verbose)
			System.out.println(p);

		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test3() {
		Palanquee p = new Palanquee();

		Plongeur racim = new Plongeur();
		racim.specifierNiveauCDS("DEB");
		p.ajouterPlongeur(racim, 0);
		Plongeur john = new Plongeur();
		p.ajouterPlongeur(john, 1);

		if (verbose)
			System.out.println(p);

		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test4() {
		Palanquee p = new Palanquee();

		Plongeur seb = new Plongeur();
		seb.specifierNiveauCDS("E4 PTH-120");
		p.ajouterEncadrant(seb);
		Plongeur racim = new Plongeur();
		racim.specifierNiveauCDS("DEB");
		p.ajouterPlongeur(racim, 0);
		Plongeur john = new Plongeur();
		p.ajouterPlongeur(john, 1);

		if (verbose)
			System.out.println(p);

		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test5() {
		Palanquee p = new Palanquee();

		Plongeur seb = new Plongeur();
		seb.specifierNiveauCDS("E4 PTH-120");
		p.ajouterEncadrant(seb);
		Plongeur racim = new Plongeur();
		racim.specifierNiveauCDS("DEB");
		p.ajouterPlongeur(racim, 0);
		Plongeur audrey = new Plongeur();
		audrey.specifierNiveauCDS("P3 PN");
		p.ajouterPlongeur(audrey, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(6, p.profondeurMax());
		assertEquals(true, p.peutPlonger());
	}

	@Test
	public void test6() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.TRIMIX);

		Plongeur seb = new Plongeur();
		seb.specifierNiveauCDS("E4 PTH-120");
		p.ajouterEncadrant(seb);
		Plongeur racim = new Plongeur();
		racim.specifierNiveauCDS("DEB");
		p.ajouterPlongeur(racim, 0);
		Plongeur audrey = new Plongeur();
		audrey.specifierNiveauCDS("P3 PN");
		p.ajouterPlongeur(audrey, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test7() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.TRIMIX);
		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur seb = new Plongeur();
		seb.specifierNiveauCDS("E4 PTH-120");
		p.ajouterEncadrant(seb);
		Plongeur racim = new Plongeur();
		racim.specifierNiveauCDS("DEB");
		p.ajouterPlongeur(racim, 0);
		Plongeur audrey = new Plongeur();
		audrey.specifierNiveauCDS("P3 PN");
		p.ajouterPlongeur(audrey, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test8() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.NITROX);
		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur seb = new Plongeur();
		seb.specifierNiveauCDS("E4 PTH-120");
		p.ajouterEncadrant(seb);
		Plongeur racim = new Plongeur();
		racim.specifierNiveauCDS("DEB");
		p.ajouterPlongeur(racim, 0);
		Plongeur audrey = new Plongeur();
		audrey.specifierNiveauCDS("P3 PN");
		p.ajouterPlongeur(audrey, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(6, p.profondeurMax());
		assertEquals(true, p.peutPlonger());
	}

	@Test
	public void test9() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.TRIMIX);
		p.setTypePlongee(TypePlongee.EXPLORATION);

		Plongeur seb = new Plongeur();
		seb.specifierNiveauCDS("E4 PTH-120");
		p.ajouterEncadrant(seb);
		Plongeur rachid = new Plongeur();
		rachid.specifierNiveauCDS("E4 PTH-120");
		p.ajouterPlongeur(rachid, 0);
		Plongeur domi = new Plongeur();
		domi.specifierNiveauCDS("E3 PTH-70");
		p.ajouterPlongeur(domi, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(70, p.profondeurMax());
		assertEquals(true, p.peutPlonger());
	}

	@Test
	public void test10() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.TRIMIX);
		p.setTypePlongee(TypePlongee.EXPLORATION);

		Plongeur rachid = new Plongeur();
		rachid.specifierNiveauCDS("E4 PTH-120");
		p.ajouterPlongeur(rachid, 0);
		Plongeur domi = new Plongeur();
		domi.specifierNiveauCDS("E3 PTH-70");
		p.ajouterPlongeur(domi, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(70, p.profondeurMax());
		assertEquals(true, p.peutPlonger());
	}

	@Test
	public void test11() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.TRIMIX);
		p.setTypePlongee(TypePlongee.EXPLORATION);

		Plongeur seb = new Plongeur();
		seb.specifierNiveauCDS("E4 PTH-120");
		p.ajouterEncadrant(seb);
		Plongeur rachid = new Plongeur();
		rachid.specifierNiveauCDS("E4 PTH-120");
		p.ajouterPlongeur(rachid, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(80, p.profondeurMax());
		assertEquals(true, p.peutPlonger());
	}

	@Test
	public void test12() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.TRIMIX);
		p.setTypePlongee(TypePlongee.EXPLORATION);

		Plongeur seb = new Plongeur();
		seb.specifierNiveauCDS("E4 PTH-120");
		p.ajouterPlongeur(seb, 0);
		Plongeur rachid = new Plongeur();
		rachid.specifierNiveauCDS("E4 PTH-120");
		p.ajouterPlongeur(rachid, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(120, p.profondeurMax());
		assertEquals(true, p.peutPlonger());
	}

	@Test
	public void test13() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.TRIMIX);
		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur seb = new Plongeur();
		seb.specifierNiveauCDS("E4 PTH-120");
		p.ajouterEncadrant(seb);
		Plongeur domi = new Plongeur();
		domi.specifierNiveauCDS("E3 PTH-70");
		p.ajouterPlongeur(domi, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(80, p.profondeurMax());
		assertEquals(true, p.peutPlonger());
	}

	@Test
	public void test14() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.TRIMIX);
		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur seb = new Plongeur();
		seb.specifierNiveauCDS("E4 PTH-120");
		p.ajouterEncadrant(seb);
		Plongeur gaetan = new Plongeur();
		gaetan.specifierNiveauCDS("E4 PN-C");
		p.ajouterPlongeur(gaetan, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(40, p.profondeurMax());
		assertEquals(true, p.peutPlonger());
	}

	@Test
	public void test15() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.NITROX);
		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur seb = new Plongeur();
		seb.specifierNiveauCDS("E4 PTH-120");
		p.ajouterEncadrant(seb);
		Plongeur gaetan = new Plongeur();
		gaetan.specifierNiveauCDS("E4 PN-C");
		p.ajouterPlongeur(gaetan, 0);
		Plongeur audrey = new Plongeur();
		audrey.specifierNiveauCDS("P3 PN");
		p.ajouterPlongeur(audrey, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(60, p.profondeurMax());
		assertEquals(true, p.peutPlonger());
	}

	@Test
	public void test16() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.NITROX);
		p.setTypePlongee(TypePlongee.EXPLORATION);

		Plongeur seb = new Plongeur();
		seb.specifierNiveauCDS("E4 PTH-120");
		p.ajouterEncadrant(seb);
		Plongeur gaetan = new Plongeur();
		gaetan.specifierNiveauCDS("E4 PN-C");
		p.ajouterPlongeur(gaetan, 0);
		Plongeur audrey = new Plongeur();
		audrey.specifierNiveauCDS("P3 PN");
		p.ajouterPlongeur(audrey, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(60, p.profondeurMax());
		assertEquals(true, p.peutPlonger());
	}

	@Test
	public void test17() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.NITROX);
		p.setTypePlongee(TypePlongee.EXPLORATION);

		Plongeur gaetan = new Plongeur();
		gaetan.specifierNiveauCDS("E4 PN-C");
		p.ajouterPlongeur(gaetan, 0);
		Plongeur audrey = new Plongeur();
		audrey.specifierNiveauCDS("P3 PN");
		p.ajouterPlongeur(audrey, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(60, p.profondeurMax());
		assertEquals(true, p.peutPlonger());
	}

	@Test
	public void test18() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.NITROX);
		p.setTypePlongee(TypePlongee.EXPLORATION);

		Plongeur gaetan = new Plongeur();
		gaetan.specifierNiveauCDS("E4 PN-C");
		p.ajouterPlongeur(gaetan, 0);
		Plongeur cedric = new Plongeur();
		cedric.specifierNiveauCDS("P2+E1 PN");
		p.ajouterPlongeur(cedric, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(20, p.profondeurMax());
		assertEquals(true, p.peutPlonger());
	}

	@Test
	public void test19() {
		Palanquee p = new Palanquee();

		Plongeur alexandra = new Plongeur();
		alexandra.specifierNiveauCDS("P4 PN");
		p.ajouterEncadrant(alexandra);

		Plongeur racim = new Plongeur(); // baptême
		p.ajouterPlongeur(racim, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test20() {
		Palanquee p = new Palanquee();

		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur domi = new Plongeur();
		domi.specifierNiveauCDS("E3 PTH-70");
		p.ajouterEncadrant(domi);
		Plongeur deb = new Plongeur();
		deb.specifierNiveauCDS("DEB");
		p.ajouterPlongeur(deb, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
		assertEquals(20, p.profondeurMax());
	}

	@Test
	public void test21() {
		Palanquee p = new Palanquee();

		Plongeur domi = new Plongeur();
		domi.specifierNiveauCDS("E3 PTH-70");
		p.ajouterEncadrant(domi);
		Plongeur deb = new Plongeur();
		deb.specifierNiveauCDS("DEB");
		p.ajouterPlongeur(deb, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
		assertEquals(6, p.profondeurMax());
	}

	@Test
	public void test22() {
		Palanquee p = new Palanquee();

		Plongeur domi = new Plongeur();
		domi.specifierNiveauCDS("E3 PTH-70");
		p.ajouterEncadrant(domi);
		Plongeur deb = new Plongeur();
		deb.specifierNiveauCDS("PE-12");
		p.ajouterPlongeur(deb, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
		assertEquals(12, p.profondeurMax());
	}

	@Test
	public void test23() {
		Palanquee p = new Palanquee();

		Plongeur padi1 = new Plongeur();
		padi1.specifierNiveauCDS("PA-12");
		p.ajouterPlongeur(padi1, 0);
		Plongeur padi2 = new Plongeur();
		padi2.specifierNiveauCDS("PA-20");
		p.ajouterPlongeur(padi2, 1);
		Plongeur padi3 = new Plongeur();
		padi3.specifierNiveauCDS("PA-40");
		p.ajouterPlongeur(padi3, 2);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
		assertEquals(12, p.profondeurMax());
	}

	@Test
	public void test24() {
		Palanquee p = new Palanquee();

		Plongeur padi1 = new Plongeur();
		padi1.specifierNiveauCDS("PE-12");
		p.ajouterPlongeur(padi1, 0);
		Plongeur padi2 = new Plongeur();
		padi2.specifierNiveauCDS("PE-20");
		p.ajouterPlongeur(padi2, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test25() {
		Palanquee p = new Palanquee();

		Plongeur domi = new Plongeur();
		domi.specifierNiveauCDS("E3 PTH-70");
		p.ajouterEncadrant(domi);

		Plongeur padi2 = new Plongeur();
		padi2.specifierNiveauCDS("PA-20");
		p.ajouterPlongeur(padi2, 1);
		Plongeur padi3 = new Plongeur();
		padi3.specifierNiveauCDS("PE-40");
		p.ajouterPlongeur(padi3, 2);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
		assertEquals(20, p.profondeurMax());
	}

	@Test
	public void test26() {
		Palanquee p = new Palanquee();

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E4 PTH-120");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("PE-20+PA-12 PN");
		p.ajouterPlongeur(a, 0);

		Plongeur c = new Plongeur();
		c.specifierNiveauCDS("GP+E1 PN");
		p.ajouterPlongeur(c, 1);

		Plongeur d = new Plongeur();
		d.specifierNiveauCDS("P5+E2 PN-C");
		p.ajouterSerreFile(d);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
		assertEquals(20, p.profondeurMax());
	}

	@Test
	public void test27() {
		Palanquee p = new Palanquee();

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("GP PTH-40");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("PE-12");
		p.ajouterPlongeur(a, 0);

		Plongeur c = new Plongeur();
		c.specifierNiveauCDS("P1");
		p.ajouterPlongeur(c, 1);

		Plongeur d = new Plongeur();
		d.specifierNiveauCDS("P1");
		p.ajouterSerreFile(d);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test28() {
		Palanquee p = new Palanquee();

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("GP PTH-40");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("PE-12");
		p.ajouterPlongeur(a, 0);

		Plongeur c = new Plongeur();
		c.specifierNiveauCDS("P1");
		p.ajouterPlongeur(c, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
		assertEquals(12, p.profondeurMax());
	}

	@Test
	public void test29() {
		Palanquee p = new Palanquee();

		p.setTypeGaz(TypeGaz.TRIMIX);
		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E3 PTH-40");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("E4 PN-C");
		p.ajouterPlongeur(a, 0);

		Plongeur c = new Plongeur();
		c.specifierNiveauCDS("E4 PN-C");
		p.ajouterPlongeur(c, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test30() {
		Palanquee p = new Palanquee();

		p.setTypeGaz(TypeGaz.TRIMIX);
		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E3 PTH-70");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("E4 PTH-40");
		p.ajouterPlongeur(a, 0);

		Plongeur c = new Plongeur();
		c.specifierNiveauCDS("E4 PTH-40");
		p.ajouterPlongeur(c, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
		assertEquals(40, p.profondeurMax());
	}

	@Test
	public void test31() {
		Palanquee p = new Palanquee();

		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E3 PTH-70");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("P2");
		p.ajouterPlongeur(a, 0);

		Plongeur c = new Plongeur();
		c.specifierNiveauCDS("P2");
		p.ajouterPlongeur(c, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
		assertEquals(40, p.profondeurMax());
	}

	@Test
	public void test32() {
		Palanquee p = new Palanquee();

		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);
		p.setTypeGaz(TypeGaz.NITROX);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E2 PN-C");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("P3 PN");
		p.ajouterPlongeur(a, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
		assertEquals(20, p.profondeurMax());
	}

	@Test
	public void test34() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.NITROX);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E2 PN-C");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("P3 PN");
		p.ajouterPlongeur(a, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
		assertEquals(40, p.profondeurMax());
	}

	@Test
	public void test35() {
		Palanquee p = new Palanquee();

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E3");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("P3");
		p.ajouterPlongeur(a, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
		assertEquals(40, p.profondeurMax());
	}

	@Test
	public void test33() {
		Palanquee p = new Palanquee();

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E2 PN-C");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("P3 PN");
		p.ajouterPlongeur(a, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
		assertEquals(40, p.profondeurMax());
	}

	@Test
	public void test36() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.NITROX);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E4 PTH-70");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("P3 PN");
		p.ajouterPlongeur(a, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
		assertEquals(60, p.profondeurMax());
	}

	@Test
	public void test37() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.NITROX);
		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E4 PTH-70");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("P3 PN");
		p.ajouterPlongeur(a, 0);

		Plongeur c = new Plongeur();
		c.specifierNiveauCDS("P4 PN-C");
		p.ajouterSerreFile(c);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
		assertEquals(40, p.profondeurMax());
	}

	@Test
	public void test38() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.TRIMIX);
		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E3 PN-C");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("P3 PN-C");
		p.ajouterPlongeur(a, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test39() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.TRIMIX);
		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E4 PTH-70");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("P3 PN-C");
		p.ajouterPlongeur(a, 0);

		Plongeur c = new Plongeur();
		c.specifierNiveauCDS("P4 PN-C");
		p.ajouterSerreFile(c);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test40() {
		Palanquee p = new Palanquee();

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("P3");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("P2 PN-C");
		p.ajouterPlongeur(a, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test41() {
		Palanquee p = new Palanquee();
		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("P3");
		p.ajouterPlongeur(b, 1);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("P2 PN-C");
		p.ajouterPlongeur(a, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test42() {
		Palanquee p = new Palanquee();

		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E4 PTH-70");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("BAT");
		p.ajouterPlongeur(a, 0);

		Plongeur c = new Plongeur();
		c.specifierNiveauCDS("P4 PN-C");
		p.ajouterSerreFile(c);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
	}

	@Test
	public void test43() {
		Palanquee p = new Palanquee();

		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E5 PTH-70");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("BAT");
		p.ajouterPlongeur(a, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test44() {
		Palanquee p = new Palanquee();

		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E4 PTH-70");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("PA-42");
		p.ajouterPlongeur(a, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
	}

	@Test
	public void test45() {
		Palanquee p = new Palanquee();

		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);
		p.setTypeGaz(TypeGaz.NITROX);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E1 PN-C");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("PA-12");
		p.ajouterPlongeur(a, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test46() {
		Palanquee p = new Palanquee();

		p.setTypeGaz(TypeGaz.NITROX);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E1 PN-C");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("PE-40 PN");
		p.ajouterPlongeur(a, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test47() {
		Palanquee p = new Palanquee();

		p.setTypeGaz(TypeGaz.NITROX);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E2 PN-C");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("PE-40 PN");
		p.ajouterPlongeur(a, 0);

		Plongeur c = new Plongeur();
		c.specifierNiveauCDS("P4 PN");
		p.ajouterSerreFile(c);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test48() {
		Palanquee p = new Palanquee();

		p.setTypeGaz(TypeGaz.NITROX);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("PE-40 PN");
		p.ajouterPlongeur(a, 0);

		Plongeur c = new Plongeur();
		c.specifierNiveauCDS("P4 PN");
		p.ajouterSerreFile(c);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test49() {
		Palanquee p = new Palanquee();

		p.setTypeGaz(TypeGaz.NITROX);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("PA-40 PN");
		p.ajouterPlongeur(a, 0);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E4");
		p.ajouterPlongeur(b, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test50() {
		Palanquee p = new Palanquee();

		p.setTypeGaz(TypeGaz.TRIMIX);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("E2 PTH-70");
		p.ajouterEncadrant(a);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E2 PTH-40");
		p.ajouterPlongeur(b, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test51() {
		Palanquee p = new Palanquee();

		p.setTypeGaz(TypeGaz.TRIMIX);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("E3 PTH-70");
		p.ajouterEncadrant(a);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E4 PTH-40");
		p.ajouterPlongeur(b, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
		assertEquals(40, p.profondeurMax());
	}

	@Test
	public void test52() {
		Palanquee p = new Palanquee();

		p.setTypeGaz(TypeGaz.TRIMIX);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("E4 PTH-70");
		p.ajouterEncadrant(a);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E4 PTH-70");
		p.ajouterPlongeur(b, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
		assertEquals(70, p.profondeurMax());
	}

	@Test
	public void test53() {
		Palanquee p = new Palanquee();

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("P1+PA-12");
		p.ajouterPlongeur(a, 0);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("P1+PA-12");
		p.ajouterPlongeur(b, 1);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
		assertEquals(12, p.profondeurMax());
	}

	@Test
	public void test54() {
		Palanquee p = new Palanquee();

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("P1+PA-12");
		p.ajouterPlongeur(a, 0);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("P1+PA-12");
		p.ajouterPlongeur(b, 1);

		Plongeur c = new Plongeur();
		c.specifierNiveauCDS("P1+PA-12");
		p.ajouterPlongeur(c, 2);

		Plongeur d = new Plongeur();
		d.specifierNiveauCDS("P1+PA-12");
		p.ajouterPlongeur(d, 3);

		p.ajouterPlongeur(d, 4);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test55() {
		Palanquee p = new Palanquee();

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E2 PN-C");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("PA-40");
		p.ajouterPlongeur(a, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(true, p.peutPlonger());
		assertEquals(40, p.profondeurMax());
	}

	@Test
	public void test56() {
		Palanquee p = new Palanquee();

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E2 PN-C");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("PA-40");
		p.ajouterPlongeur(a, 0);

		Plongeur c = new Plongeur();
		c.specifierNiveauCDS("E1");
		p.ajouterSerreFile(c);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test57() {
		Palanquee p = new Palanquee();

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("P4");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		p.ajouterPlongeur(a, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test58() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.NITROX);
		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E2 PN");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("P4");
		p.ajouterPlongeur(a, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test59() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.NITROX);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("P4");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("P4 PN");
		p.ajouterPlongeur(a, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test60() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.NITROX);
		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur seb = new Plongeur();
		seb.specifierNiveauCDS("E4 PTH-120");
		p.ajouterEncadrant(seb);
		Plongeur gaetan = new Plongeur();
		gaetan.specifierNiveauCDS("E4 PN-C");
		p.ajouterPlongeur(gaetan, 0);
		Plongeur audrey = new Plongeur();
		audrey.specifierNiveauCDS("P3 PN");
		p.ajouterPlongeur(audrey, 1);

		Plongeur domi = new Plongeur();
		domi.specifierNiveauCDS("E3");
		p.ajouterSerreFile(domi);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test61() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.TRIMIX);
		p.setTypePlongee(TypePlongee.ENSEIGNEMENT);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E2 PTH-120");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("P4 PN");
		p.ajouterPlongeur(a, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test62() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.TRIMIX);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E3");
		p.ajouterEncadrant(b);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("P4 PN");
		p.ajouterPlongeur(a, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test63() {
		Palanquee p = new Palanquee();
		p.setTypeGaz(TypeGaz.TRIMIX);

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("E4 PTH-70");
		p.ajouterPlongeur(b, 0);

		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("P3");
		p.ajouterPlongeur(a, 0);

		if (verbose)
			System.out.println(p);
		assertEquals(false, p.peutPlonger());
	}

	@Test
	public void test64() {
		Plongeur a = new Plongeur();
		a.specifierNiveauCDS("P2 PA-40");
		assertEquals(40,
				a.profondeurMax(TypeGaz.AIR, TypePlongee.EXPLORATION, true));

		Plongeur b = new Plongeur();
		b.specifierNiveauCDS("P1 PE-40");
		assertEquals(40,
				b.profondeurMax(TypeGaz.AIR, TypePlongee.EXPLORATION, false));

		System.out.println(a + " - " + b + "\n");
		assertEquals("P2+PA-40",a.toString());
		assertEquals("P1+PE-40",b.toString());
	}
	
	@Test
	public void test65() {
		String[] s3 = Plongeur.toSpinner("P5+E2 PN-C");
		System.out.println(s3[0]+" . "+s3[1]+" . "+s3[2]);
		
		s3 = Plongeur.toSpinner("P1+PA12 PN");
		System.out.println(s3[0]+" . "+s3[1]+" . "+s3[2]);
		
		s3 = Plongeur.toSpinner("GP+E1 PN-C");
		System.out.println(s3[0]+" . "+s3[1]+" . "+s3[2]);
		
		s3 = Plongeur.toSpinner("P1+PE-40 PN");
		System.out.println(s3[0]+" . "+s3[1]+" . "+s3[2]);
	}
}
