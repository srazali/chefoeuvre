package fr.enac.models;

/**
 * Enum PlongeeHoraireEnum représentant les différents horaires d'une plongée.
 * 
 * @author Sophien Razali
 *
 */
public enum PlongeeHoraireEnum {
	MATIN,				/* Plongée le matin */
	APRES_MIDI,			/* Plongée l'après-midi */
	SOIR;				/* Plongée nocturne */
}
