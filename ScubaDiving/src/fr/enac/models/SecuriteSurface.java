package fr.enac.models;

import java.io.Serializable;

/**
 * Classe SecuriteSurface représentant un membre assurant 
 * la sécurité surface pour un tour.
 * 
 * @author Sophien Razali
 *
 */
public class SecuriteSurface implements Serializable {
	/**
	 * Pour la sérialisation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * L'id de la sécurité surface.
	 */
	private long mId;
	
	/**
	 * L'id du tour auquel appartient la sécurité surface.
	 */
	private long mIdTour;
	
	/**
	 * Le nom de la sécurité surface.
	 */
	private String mName;
	
	public SecuriteSurface(long mId, long mIdTour, String mName) {
		super();
		this.mId = mId;
		this.mIdTour = mIdTour;
		this.mName = mName;
	}

	//***** GETTERS AND SETTERS *****
	
	public long getmId() {
		return mId;
	}

	public void setmId(long mId) {
		this.mId = mId;
	}
	
	public long getmIdTour() {
		return mIdTour;
	}

	public void setmIdTour(long mIdTour) {
		this.mIdTour = mIdTour;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}
	
}
