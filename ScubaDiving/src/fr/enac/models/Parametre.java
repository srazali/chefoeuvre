package fr.enac.models;

import java.io.Serializable;

/**
 * Classe Parametre représentant un paramètre général.
 * 
 * @author Sophien Razali
 *
 */
public abstract class Parametre implements Serializable {
	/**
	 * Pour la sérialisation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * L'id du paramètre.
	 */
	private long mId;
	
	/**
	 * Le nom du paramètre.
	 */
	private String mNom;
	
	/**
	 * L'id de l'icone du paramètre, pour l'affichage visuel.
	 */
	private long mIconId;
	
	public Parametre(long mId, String mNom, long mIconId) {
		this.mId = mId;
		this.mNom = mNom;
		this.mIconId = mIconId;
	}
	
	//***** GETTERS AND SETTERS *****


	public long getmId() {
		return mId;
	}

	public void setmId(long mId) {
		this.mId = mId;
	}

	public String getmNom() {
		return mNom;
	}

	public void setmNom(String mNom) {
		this.mNom = mNom;
	}

	public long getmIconId() {
		return mIconId;
	}

	public void setmIconId(long mIconId) {
		this.mIconId = mIconId;
	}

}
