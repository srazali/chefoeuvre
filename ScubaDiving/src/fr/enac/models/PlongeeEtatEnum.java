package fr.enac.models;

/**
 * Enum PlongeeEtatEnum représentant les différents états d'une plongée.
 * 
 * @author Sophien Razali
 *
 */
public enum PlongeeEtatEnum {
	EN_COURS,			/* La plongée est en cours, des palanquées doivent encore plonger ou sortir de l'eau */
	TERMINEE;			/* La plongée est terminée, toutes les plalanquées sont sorties de l'eau */
}
