package fr.enac.models;

/**
 * Enum ParametrePlongeeCategorieEnum représentant les différentes 
 * catégories d'un paramètre de plongée.
 * 
 * @author Sophien Razali
 *
 */
public enum ParametrePlongeeCategorieEnum {
	MATERIEL_SECURITE,			/* La catégorie des paramètres liés au matériel de sécurité */
	SIGNALISATION;				/* La catégorie des paramètres liés à la signalisation */
}
