package fr.enac.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.enac.lii.CDS.Plongeur;
import fr.enac.models.Plongeur.Participe;

/**
 * Classe Palanquee issue de la classe Planquee associée au CDS.
 * 
 * @see fr.enac.lii.CDS.Palanquee
 * @author Sophien Razali
 *
 */
public class Palanquee extends fr.enac.lii.CDS.Palanquee implements Serializable {
	/**
	 * Pour la sérialisation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * L'id de la palanquée.
	 */
	private long mId;
	
	/**
	 * L'id du tour auquel appartient la palanquée.
	 */
	private long mIdTour;
	
	/**
	 * Si la palanquee est en attente de plongée, entrain de plongée, ou a fini de plongée
	 */
	public enum Etat{
		ATTENTE(0),
		ENCOURS(1),
		FINIE(2);

		private final long id;

		private Etat(long id){
			this.id=id;
		}

		public long getId(){
			return id;
		}
	}
	private Etat mEtat;
	
	/**
	 * La liste des paramètres de la palanquée.
	 */
	private List<ParametrePalanquee> mListeParametresPalanquee;

	public Palanquee(long mId, long mIdTour) {
		super();
		this.mId = mId;
		this.mIdTour = mIdTour;
		this.mListeParametresPalanquee = new ArrayList<ParametrePalanquee>();
		this.mEtat = Etat.ATTENTE;
	}

	//***** GETTERS AND SETTERS *****
	
	public long getmId() {
		return mId;
	}

	public void setmId(long mId) {
		this.mId = mId;
	}
	
	public long getmIdTour() {
		return mIdTour;
	}

	public void setmIdTour(long mIdTour) {
		this.mIdTour = mIdTour;
	}

	public List<ParametrePalanquee> getmListeParametresPalanquee() {
		return mListeParametresPalanquee;
	}

	public void setmListeParametresPalanquee(
			List<ParametrePalanquee> mListeParametresPalanquee) {
		this.mListeParametresPalanquee = mListeParametresPalanquee;
	}
	
	public void ajouterParametre(ParametrePalanquee p){
		this.mListeParametresPalanquee.add(p);
	}
	
	public void supprimerParametre(ParametrePalanquee p){
		if (mListeParametresPalanquee.contains(p)){
			mListeParametresPalanquee.remove(p);
		}
	}
	
	public Etat getmEtat(){
		return this.mEtat;
	}
	
	public void setmEtat(Etat etat){
		this.mEtat = etat;
	}
	
	/**
	 * utilisé par palanquée DAO
	 */
	public void setmEtat(long idEtat){
		switch((int)idEtat){
		case 0 :
			this.mEtat = Etat.ATTENTE;
			break;
		case 1 :
			this.mEtat = Etat.ENCOURS;
			break;
		case 2 :
			this.mEtat = Etat.FINIE;
			break;
		}
	}
	
	/**
	 * Ce n'est pas moi (Sophien Razali), qui ai écrit cette méthode).
	 * 
	 * @param p
	 * @return
	 */
	public boolean contientPlongeur(Plongeur p){
		boolean trouver = false;
		if (getEncadrant() != null){
			if (getEncadrant().equals(p)){
				trouver = true;
			}
		}
		
		if (getPlongeur(0) != null){
			if (getPlongeur(0).equals(p)){
				trouver = true;
			}
		}
		if (getPlongeur(1) != null){
			if (getPlongeur(1).equals(p)){
				trouver = true;
			}
		}
		if (getPlongeur(2) != null){
			if (getPlongeur(2).equals(p)){
				trouver = true;
			}
		}
		if (getPlongeur(3) != null){
			if (getPlongeur(3).equals(p)){
				trouver = true;
			}
		}
		if (getSerreFile() != null){
			if (getSerreFile().equals(p)){
				trouver = true;
			}
		}
		return trouver;
		
	}
	
}
