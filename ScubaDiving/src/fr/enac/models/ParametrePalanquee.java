package fr.enac.models;

/**
 * Classe ParametrePalanquee représentant un paramètre associé à une palanquée.
 * Exemple : profondeur prévue/réelle.
 * 
 * @author Sophien Razali
 *
 */
public class ParametrePalanquee extends Parametre {
	/**
	 * Pour la sérialisation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * L'id de la palanquée à laquelle appartient le paramètre.
	 */
	private long mIdPalanquee;
	
	/**
	 * La valeur du paramètre.
	 */
	private String mValeur;
	
	/**
	 * Le type du paramètre.
	 * @see ParametrePalanqueeTypeEnum
	 */
	private ParametrePalanqueeTypeEnum mtype;

	public ParametrePalanquee(long mId, String mNom, long mIconId, long mIdPalanquee, String mValeur, ParametrePalanqueeTypeEnum mtype) {
		super(mId, mNom, mIconId);
		this.mIdPalanquee = mIdPalanquee;
		this.mValeur = mValeur;
		this.mtype = mtype;
	}
	
	//***** GETTERS AND SETTERS *****

	public String getmValeur() {
		return mValeur;
	}

	public void setmValeur(String mValeur) {
		this.mValeur = mValeur;
	}

	public long getmIdPalanquee() {
		return mIdPalanquee;
	}

	public void setmIdPalanquee(long mIdPalanquee) {
		this.mIdPalanquee = mIdPalanquee;
	}
	
	public ParametrePalanqueeTypeEnum getmType() {
		return mtype;
	}
	
	public void setmType(ParametrePalanqueeTypeEnum mtype) {
		this.mtype = mtype;
	}
	
}
