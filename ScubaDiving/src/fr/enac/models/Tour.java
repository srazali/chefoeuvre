package fr.enac.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe Tour représentant un tour de plongée.
 * 
 * @author Sophien Razali
 *
 */
public class Tour implements Serializable {
	/**
	 * Pour la sérialisation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * L'id du tour.
	 */
	private long mId;
	
	/**
	 * L'id du la plongée à laquelle appartient le tour.
	 */
	private long mIdPlongee;
	
	/**
	 * La position du tour (tour 1, tour 2, etc.).
	 */
	private long mPosition;
	
	/**
	 * La liste des palanquées présentes du tour.
	 */
	private List<Palanquee> mListePalanquees;
	
	/**
	 * La liste des sécurité surface du tour.
	 */
	private List<SecuriteSurface> mListeSecuritesSurface;

	public Tour(long mId, long mIdPlongee, long mPosition) {
		this.mId = mId;
		this.mIdPlongee = mIdPlongee;
		this.mPosition = mPosition;
		this.mListePalanquees = new ArrayList<Palanquee>();
		this.mListeSecuritesSurface = new ArrayList<SecuriteSurface>();
	}

	//***** GETTERS AND SETTERS *****
	
	public long getmId() {
		return mId;
	}

	public void setmId(long mId) {
		this.mId = mId;
	}
	
	public long getmIdPlongee() {
		return mIdPlongee;
	}

	public void setmIdPlongee(long mIdPlongee) {
		this.mIdPlongee = mIdPlongee;
	}

	public long getmPosition() {
		return mPosition;
	}

	public void setmPosition(long mPosition) {
		this.mPosition = mPosition;
	}

	public List<Palanquee> getmListePalanquees() {
		return mListePalanquees;
	}

	public void setmListePalanquees(List<Palanquee> mListePalanquees) {
		this.mListePalanquees = mListePalanquees;
	}

	public List<SecuriteSurface> getmListeSecuritesSurface() {
		return mListeSecuritesSurface;
	}

	public void setmListeSecuritesSurface(List<SecuriteSurface> mListeSecuritesSurface) {
		this.mListeSecuritesSurface = mListeSecuritesSurface;
	}
	
	
	public void ajouterPalanquee(Palanquee p){
		mListePalanquees.add(p);
	}
}
