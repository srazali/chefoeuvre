package fr.enac.models;

/**
 * Enum PlongeurTypeEnum représentant les différents types de plongeur
 * dans une palanquée.
 * 
 * @author Sophien Razali
 *
 */
public enum PlongeurTypeEnum {
	ENCADRANT,			/* Le plongeur est encadrant de la palanquee */
	PLONGEUR,			/* Le plongeur est un plongeur de la palanquee */
	SERRE_FILE;			/* Le plongeur est serre file de la palanquee */
}
