package fr.enac.models;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import android.util.Log;

/**
 * Classe Plongee représentant une séance de plongée sous-marine.
 * 
 * @author Sophien Razali
 *
 */
public class Plongee implements Serializable {
	/**
	 * Pour la sérialisation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * L'id de la plongée.
	 */
	private long mId;
	
	/**
	 * L'état de la plongée.
	 * @see PlongeeEtatEnum
	 */
	private PlongeeEtatEnum mEtat;
	
	/**
	 * La date de création de la plongée, lorsque l'utilisateur créé la fiche de sécurité.
	 */
	private long mDateCreation;
	
	/**
	 * La date de la plongée.
	 */
	private long mDatePlongee;
	
	/**
	 * L'horaire de la plongée.
	 * @see PlongeeHoraireEnum
	 */
	private PlongeeHoraireEnum mHoraire;
	
	/**
	 * Le directeur de plongée.
	 */
	private String mDirecteurPlongee;
	
	/**
	 * Le lieu de la plongée.
	 */
	private String mLieu;
	
	/**
	 * Les notes de la plongée.
	 */
	private String mNote;
	
	/**
	 * Les tours de la plongée.
	 */
	private List<Tour> mListeTours;
	
	/**
	 * Les paramètres de la plongée.
	 */
	private List<ParametrePlongee> mListeParametres;
	
	/**
	 * Les plongeurs participants à la plongée.
	 */
	private List<Plongeur> mListePlongeurs;

	/**
	 * La dernière activité reliée à la plongée.
	 */
	private String mDerniereActivite;
	
	public Plongee(long mId, PlongeeEtatEnum mEtat, long mDateCreation,
			long mDatePlongee, PlongeeHoraireEnum mHoraire,
			String mDirecteurPlongee, String mLieu, String mNote, String mDerniereActivite) {
		super();
		this.mId = mId;
		this.mEtat = mEtat;
		this.mDateCreation = mDateCreation;
		this.mDatePlongee = mDatePlongee;
		this.mHoraire = mHoraire;
		this.mDirecteurPlongee = mDirecteurPlongee;
		this.mLieu = mLieu;
		this.mNote = mNote;
		this.mDerniereActivite = mDerniereActivite;
		this.mListeTours = new ArrayList<Tour>();
		this.mListeParametres = new ArrayList<ParametrePlongee>();
		this.mListePlongeurs = new ArrayList<Plongeur>();
	}
	
	/**
	 * TODO: So - changer le texte pour qu'il ne soit pas static.
	 */
	@Override
	public String toString() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTimeInMillis(this.mDatePlongee);
		return "Lieu : " + (this.getmLieu().isEmpty() ? "non défini" : this.getmLieu()) + " -- Date : " + (this.mDatePlongee == 0 ? "non définie" : formatter.format(calendar.getTime()));
	}

	//***** GETTERS AND SETTERS *****

	public long getmId() {
		return mId;
	}

	public void setmId(long mId) {
		this.mId = mId;
	}

	public PlongeeEtatEnum getmEtat() {
		return mEtat;
	}

	public void setmEtat(PlongeeEtatEnum mEtat) {
		this.mEtat = mEtat;
	}

	public long getmDateCreation() {
		return mDateCreation;
	}

	public void setmDateCreation(long mDateCreation) {
		this.mDateCreation = mDateCreation;
	}

	public long getmDatePlongee() {
		return mDatePlongee;
	}

	public void setmDatePlongee(long mDatePlongee) {
		this.mDatePlongee = mDatePlongee;
	}

	public PlongeeHoraireEnum getmHoraire() {
		return mHoraire;
	}

	public void setmHoraire(PlongeeHoraireEnum mHoraire) {
		this.mHoraire = mHoraire;
	}

	public String getmDirecteurPlongee() {
		return mDirecteurPlongee;
	}

	public void setmDirecteurPlongee(String mDirecteurPlongee) {
		this.mDirecteurPlongee = mDirecteurPlongee;
	}

	public String getmLieu() {
		return mLieu;
	}

	public void setmLieu(String mLieu) {
		this.mLieu = mLieu;
	}

	public String getmNote() {
		return mNote;
	}

	public void setmNote(String mNote) {
		this.mNote = mNote;
	}

	public String getmDerniereActivite() {
		return mDerniereActivite;
	}

	public void setmDerniereActivite(String mDerniereActivite) {
		this.mDerniereActivite = mDerniereActivite;
	}

	public List<Tour> getmListeTours() {
		return mListeTours;
	}
	
	public Tour getmTour(int position) {
		for (Tour t:mListeTours){
			if (t.getmPosition() == position){
				return t;
			}
		}
		return null;
	}

	public void setmListeTours(List<Tour> mListeTours) {
		this.mListeTours = mListeTours;
	}

	public List<ParametrePlongee> getmListeParametres() {
		return mListeParametres;
	}

	public void setmListeParametres(List<ParametrePlongee> mListeParametres) {
		this.mListeParametres = mListeParametres;
	}

	public List<Plongeur> getmListePlongeurs() {
		long start = System.currentTimeMillis();
		Collections.sort(mListePlongeurs, Collections.reverseOrder());
		Log.e("TRI","tri : " + (System.currentTimeMillis() - start)+"ms");
		return mListePlongeurs;
	}

	public void setmListePlongeurs(List<Plongeur> mListePlongeurs) {
		this.mListePlongeurs = mListePlongeurs;
	}
	
	public void ajouterTour(Tour t){
		mListeTours.add(t);
	}

}
