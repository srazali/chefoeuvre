package fr.enac.models;

import java.io.Serializable;

/**
 * Classe Plongeur issue de la classe Plongeur associée au CDS.
 * 
 * @see fr.enac.lii.CDS.Plongeur
 * @author Sophien Razali
 *
 */
public class Plongeur extends fr.enac.lii.CDS.Plongeur implements Serializable {

	/**
	 * Pour la sérialisation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * L'id du plongeur.
	 */
	private long mId;

	/**
	 * L'id de la plongée à laquelle appartient le plongeur.
	 */
	private long mIdPlongee;

	/**
	 * Le nom du plongeur.
	 */
	private String mNom;

	/**
	 * Le prénom du plongeur.
	 */
	private String mPrenom;

	/**
	 * Si le plongeur participe ou non
	 */
	public enum Participe{
		NON(0),
		OUI(1);

		private final long id;

		private Participe(long id){
			this.id=id;
		}

		public long getId(){
			return id;
		}
	}
	private Participe mParticipe;
	
	/**
	 * Utiliser pour les certificats, licences et diplomes
	 */
	public enum Papier{
		NON(0),
		OUI(1);

		private final long id;

		private Papier(long id){
			this.id=id;
		}

		public long getId(){
			return id;
		}
	}
	private Papier mCertificat;
	private Papier mDiplome;
	private Papier mLicence;

	/**
	 * Le type du plongeur (encadrant, plongeur ou serre file).
	 * @see PlongeurTypeEnum
	 */
	private PlongeurTypeEnum mTypePlongeur;

	/*
	 * pour le prénom
	 */
	private static String capitalizeFirstLetter(String value) {
		if (value == null) {
			return null;
		}
		if (value.length() == 0) {
			return value;
		}
		StringBuilder result = new StringBuilder(value.toLowerCase());
		result.replace(0, 1, result.substring(0, 1).toUpperCase());
		return result.toString();
	}
	
	/**@author Jonathan
	 * Est utilisé ?
	 * @param mId
	 * @param mIdPlongee
	 * @param mNom
	 * @param mPrenom
	 */
	public Plongeur(long mId, long mIdPlongee, String mNom, String mPrenom) {
		super();
		this.mId = mId;
		this.mIdPlongee = mIdPlongee;
		this.mNom = mNom.toUpperCase();
		this.mPrenom = capitalizeFirstLetter(mPrenom);
		this.mParticipe = Participe.OUI;
		this.mCertificat = Papier.NON;
		this.mDiplome = Papier.NON;
		this.mLicence = Papier.NON;
	}

	/** @author : Jonathan
	 * Constructeur utilisé dans PlongeurActivity
	 * @param getmId
	 * @param string
	 * @param string2
	 * @param string3
	 */
	public Plongeur(long idPlongee, String nom, String prenom) {
		this(-1,idPlongee,nom,prenom);
	}

	//***** GETTERS AND SETTERS *****
	public long getmId() {
		return mId;
	}

	public void setmId(long mId) {
		this.mId = mId;
	}

	public long getmIdPlongee() {
		return mIdPlongee;
	}

	public void setmIdPlongee(long mIdPlongee) {
		this.mIdPlongee = mIdPlongee;
	}

	public String getmNom() {
		return mNom;
	}

	public void setmNom(String mNom) {
		this.mNom = mNom.toUpperCase();
	}

	public PlongeurTypeEnum getmTypePlongeur() {
		return mTypePlongeur;
	}

	public void setmTypePlongeur(PlongeurTypeEnum mTypePlongeur) {
		this.mTypePlongeur = mTypePlongeur;
	}

	public String getmPrenom() {
		return mPrenom;
	}

	public void setmPrenom(String mPrenom) {
		this.mPrenom = capitalizeFirstLetter(mPrenom);
	}

	public Participe getmParticipe() {
		return mParticipe;
	}
	
	public void setmParticipe(Participe mParticipe){
		this.mParticipe = mParticipe;
	}
	
	/**
	 * utilisé par plongeur DAO
	 * @param mParticipe
	 */
	public void setmParticipe(long mParticipe) {
		if (mParticipe ==0)
			this.mParticipe = Participe.NON;
		else
			this.mParticipe = Participe.OUI;
	}

	public Papier getmCertificat() {
		return mCertificat;
	}

	public Papier getmDiplome() {
		return mDiplome;
	}

	public Papier getmLicence() {
		return mLicence;
	}

	public void setmCertificat(Papier mCertificat) {
		this.mCertificat = mCertificat;
	}

	public void setmDiplome(Papier mDiplome) {
		this.mDiplome = mDiplome;
	}

	public void setmLicence(Papier mLicence) {
		this.mLicence = mLicence;
	}	
	
	/**
	 * utilisé par plongeur DAO
	 * @param mCertificat
	 */
	public void setmCertificat(long mCertificat) {
		if (mCertificat ==0)
			this.mCertificat = Papier.NON;
		else
			this.mCertificat = Papier.OUI;
	}
	
	/**
	 * utilisé par plongeur DAO
	 * @param mParticipe
	 */
	public void setmDiplome(long mDiplome) {
		if (mDiplome ==0)
			this.mDiplome = Papier.NON;
		else
			this.mDiplome = Papier.OUI;
	}
	
	/**
	 * utilisé par plongeur DAO
	 * @param mParticipe
	 */
	public void setmLicence(long mLicence) {
		if (mLicence ==0)
			this.mLicence = Papier.NON;
		else
			this.mLicence = Papier.OUI;
	}
	
	/**
	 * Convertir un boolean en Papier enum
	 * @param bool
	 * @return
	 */
	public static Papier convertBooleanToPapier(boolean bool){
		return (bool == true) ? Papier.OUI : Papier.NON;
	}
	
	/**
	 * Convertir un Papier enum en boolean
	 * @param bool
	 * @return
	 */
	public static boolean convertPapierToBoolean(Papier p){
		return (p == Papier.OUI) ? true : false;
	}
}
