package fr.enac.models;

/**
 * Classe ParametrePlongee représentant un paramètre associé à une plongée.
 * Exemple : oxygénothérapie.
 * 
 * @author Sophien Razali
 *
 */
public class ParametrePlongee extends Parametre {
	/**
	 * Pour la sérialisation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * L'id du la plongée à laquelle appartient le paramètre.
	 */
	private long mIdPlongee;
	
	/**
	 * Les details d'un paramètre.
	 */
	private String mDetails;
	
	/**
	 * La catégorie d'un paramètre.
	 */
	private ParametrePlongeeCategorieEnum mCategorie;
	
	public ParametrePlongee(long mId, String mNom, long mIconId, long mIdPlongee, String mDetails, ParametrePlongeeCategorieEnum mCategorie) {
		super(mId, mNom, mIconId);
		this.mIdPlongee = mIdPlongee;
		this.mDetails = mDetails;
		this.mCategorie = mCategorie;
	}

	//***** GETTERS AND SETTERS *****
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((mCategorie == null) ? 0 : mCategorie.hashCode())
				+ ((getmNom() == null) ? 0 : getmNom().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParametrePlongee other = (ParametrePlongee) obj;
		if (!mCategorie.toString().equals(other.mCategorie.toString()))
			return false;
		if (!getmNom().equals(other.getmNom()))
			return false;
		return true;
	}

	public String getmDetails() {
		return mDetails;
	}

	public void setmDetails(String mDetails) {
		this.mDetails = mDetails;
	}

	public long getmIdPlongee() {
		return mIdPlongee;
	}

	public void setmIdPlongee(long mIdPlongee) {
		this.mIdPlongee = mIdPlongee;
	}

	public ParametrePlongeeCategorieEnum getmCategorie() {
		return mCategorie;
	}

	public void setmCategorie(ParametrePlongeeCategorieEnum mCategorie) {
		this.mCategorie = mCategorie;
	}

}
