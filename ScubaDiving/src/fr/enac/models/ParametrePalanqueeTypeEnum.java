package fr.enac.models;

/**
 * Enum ParametrePalanqueeTypeEnum représentant les différents 
 * type d'un paramètre de palanquée.
 * 
 * @author Sophien Razali
 *
 */
public enum ParametrePalanqueeTypeEnum {
	PREVU,					/* Paramètre prévu (ex: profondeur prévue) */
	REEL,					/* Paramètre réel (ex: profondeur réelle) */
	NEUTRE;					/* Paramètre créé par l'utilisateur */
}