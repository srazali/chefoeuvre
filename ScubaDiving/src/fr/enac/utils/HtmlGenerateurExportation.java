package fr.enac.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import fr.enac.models.Palanquee;
import fr.enac.models.ParametrePalanquee;
import fr.enac.models.ParametrePalanqueeTypeEnum;
import fr.enac.models.ParametrePlongee;
import fr.enac.models.Plongee;
import fr.enac.models.Plongeur;
import fr.enac.models.SecuriteSurface;
import fr.enac.models.Tour;

/**
 * Classe permettant d'exporter une plongée.
 * 
 * La plongée est exportée au format HTML, la convention de nommage est la suivante :
 * [id_plongee]plongee_du_"date_de_la_plongee"_"lieu_de_la_plongee".html
 * L'id_plongee permet de sauvegarder plusieurs plongées effectuées 
 * le même jour, sans réécrire le fichier.
 * 
 * @author Sophien Razali
 *
 */
public class HtmlGenerateurExportation {
	
	/**
	 * Cette méthode permet de créer un fichier html contenant
	 * les informations sur la plongée passée en paramètre.
	 * 
	 * @param plongee la plongee a exporter
	 * @throws IOException
	 */
	public static void exporterPlongee(Plongee plongee) throws IOException {
		File file = new File(DossiersApplication.DOSSIER_EXPORT	+ File.separator 
				+ "[" + plongee.getmId() + "]plongee_du_" + (plongee.getmDatePlongee() == 0 ? "date_non_specifiee" : parseDate(plongee.getmDatePlongee()))  + "_" + (plongee.getmLieu().isEmpty() == true ? "lieu_non_specifie" : plongee.getmLieu().replace(" ", "_")) + ".html");
		
		/* Création du fichier */
		file.createNewFile();

		/* Objet permettant d'écrire dans le fichier */
		FileWriter writer = new FileWriter(file);
		writer.write(HtmlGenerateurExportation.generateHtmlString(plongee));
		
		/* Flush du writter permettant de s'assurer que tout le contenu a été écrit dans le fichier */
		writer.flush();
		writer.close();
	}
	
	/**
	 * Cette méthode génère une chaine de caractère contenant le code
	 * html du fichier à exporter pour la plongee passée en parametre.
	 * 
	 * @param p la plongee a exporter
	 * @return le fichier html sous forme de string
	 */
	public static String generateHtmlString(Plongee p) {
		String toReturn = "";
		toReturn += "<!DOCTYPE html>";
		toReturn += "<html>";
		toReturn += 	"<head>";
		toReturn += 		"<meta charset=\"utf-8\" />";
		toReturn += 		"<link rel=\"stylesheet\" href=\"style.css\" />";
		toReturn += 		"<title>Export fiche de sécuritée Amis de la Mer</title>";
		
		toReturn += 		"<style>";
		toReturn += 			"body";
		toReturn += 			"{";
		toReturn += 				"font-family:\"Arial\";";
		toReturn += 				"font-size:13px;";
		toReturn += 				"width: 1280px;";
		toReturn += 				"margin-left: auto;";
		toReturn += 				"margin-right: auto;";
		toReturn += 			"}";
		
		toReturn += 			"header";
		toReturn += 			"{";
		toReturn += 				"margin-top: 50px;";
		toReturn += 				"text-align: center;";
		toReturn += 			"}";
		
		toReturn +=				"h2";
		toReturn += 			"{";
		toReturn += 				"padding-top: 0px;";
		toReturn += 				"margin-top: 0px;";
		toReturn += 			"}";
		
		toReturn += 			"table";
		toReturn += 			"{";
		toReturn += 				"margin-bottom: 40px;";
		toReturn += 				"border-collapse: collapse;";
		toReturn += 			"}";
		
		toReturn += 			"table td, th";
		toReturn += 			"{";
		toReturn += 				"border: 1px solid black;";
		toReturn += 			"}";
		
		toReturn += 			"#info_plongee_table";
		toReturn += 			"{";
		toReturn += 				"min-width: 300px;";
		toReturn += 				"display: inline-block;";
		toReturn += 			"}";
		
		toReturn += 			"#parametres_plongee_div";
		toReturn += 			"{";
		toReturn += 				"width: 700px;";
		toReturn += 				"display: inline-block;";
		toReturn += 				"vertical-align: top;";
		toReturn += 			"}";
	
		toReturn += 			"#plongeurs_table";
		toReturn += 			"{";
		toReturn += 				"min-width: 500px;";
		toReturn += 			"}";
	
		toReturn += 			"#secu_surface_table";
		toReturn += 			"{";
		toReturn += 				"min-width: 300px;";
		toReturn += 			"}";
	
		toReturn += 			"#palanquees_table";
		toReturn += 			"{";
		toReturn += 				"width: 100%;";
		toReturn += 			"}";
	
		toReturn += 			"#separator";
		toReturn += 			"{";
		toReturn += 				"background-color:#248547;";
		toReturn += 			"}";
		toReturn += 		"</style>";
		toReturn += 	"</head>";
		
		toReturn += 	"<body>";
		toReturn += 		"<header>";
		toReturn += 			"<h1>Fiche de sécurité n°" + p.getmId();
		toReturn += 		"</header>";
		
		toReturn += 		"<div>";
		toReturn += 			"<table id=\"info_plongee_table\">";
		toReturn +=					"<thead>";
		toReturn += 					"<tr>";
		toReturn += 						"<th>Date</th>";
		toReturn += 						"<th>Horaire</th>";
		toReturn += 					"</tr>";
		toReturn += 				"</thead>";
		toReturn += 				"<tr>";
		toReturn += 					"<td>" + parseDate(p.getmDatePlongee()) + "</td>";
		toReturn += 					"<td>" +  (p.getmHoraire() == null ? "non spécifié" : p.getmHoraire().name()) + "</td>";
		toReturn += 				"</tr>";
		toReturn +=					"<tr id=\"separator\">";
		toReturn +=						"<th colspan=\"2\"></th>";
		toReturn +=					"</tr>";
		toReturn +=					"<thead>";
		toReturn += 					"<tr>";
		toReturn += 						"<th>Directeur de plongée</th>";
		toReturn += 						"<th>Site</th>";
		toReturn += 					"</tr>";
		toReturn += 				"</thead>";
		toReturn += 				"<tr>";
		toReturn += 					"<td>" + (p.getmDirecteurPlongee().isEmpty() == true ? "-" : p.getmDirecteurPlongee()) + "</td>";
		toReturn += 					"<td>" + (p.getmLieu().isEmpty() == true ? "-" : p.getmLieu()) + "</td>";
		toReturn += 				"</tr>";
		toReturn += 			"</table>";
		
		toReturn += 			"<div id=\"parametres_plongee_div\">";
		toReturn +=					parseParametrePlongee(p.getmListeParametres());
		toReturn += 			"</div>";
		toReturn += 		"</div>";
		
		toReturn +=			"<h2>Liste des plongeurs</h2>";
		toReturn += 		"<table id=\"plongeurs_table\">";
		toReturn += 			"<thead>";
		toReturn += 				"<tr>";
		toReturn += 					"<th>Nom</th>";
		toReturn += 					"<th>Prénom</th>";
		toReturn += 					"<th>Niveau</th>";
		toReturn += 				"</tr>";
		toReturn += 			"</thead>";
		
		toReturn += 			"<tr id=\"separator\">";
		toReturn += 				"<th colspan=\"3\"></th>";
		toReturn += 			"</tr>";
		
		toReturn += 			parsePlongeurs(p.getmListePlongeurs());
		toReturn += 		"</table>";
		
		toReturn += 		"<h2>Sécurités surface</h2>";
		toReturn += 		"<table id=\"secu_surface_table\">";
		toReturn += 			"<thead>";
		toReturn += 				"<tr>";
		toReturn += 					"<th>Tour</th>";
		toReturn += 					"<th>Sécurité Surface</th>";
		toReturn += 				"</tr>";
		toReturn += 			"</thead>";
		toReturn += 			"<tr id=\"separator\">";
		toReturn += 				"<th colspan=\"2\"></th>";
		toReturn += 			"</tr>";
		toReturn += 			parseSecuriteSurface(p.getmListeTours());
		toReturn += 		"</table>";

		toReturn += 		"<h2>Liste des palanquées</h2>";
		toReturn += 		"<table id=\"palanquees_table\">";
		toReturn += 			"<thead>";
		toReturn += 				"<tr>";
		toReturn += 					"<th>Nom</th>";
		toReturn += 					"<th>Prénom</th>";
		toReturn += 					"<th>Niveau</th>";
		toReturn += 					"<th>Fonction</th>";
		toReturn += 					"<th colspan=\"2\">Parametres prévus</th>";
		toReturn += 					"<th colspan=\"3\">Parametres réalisés</th>";
		toReturn += 					"<th>Autres paramètres</th>";
		toReturn += 					"<th>Tour</th>";
		toReturn += 				"</tr>";
		toReturn += 			"</thead>";
		toReturn += 			"<tr id=\"separator\">";
		toReturn += 				"<th colspan=\"11\"></th>";
		toReturn += 			"</tr>";
		toReturn += 			parsePalanquees(p.getmListeTours());
		toReturn += 		"</table>";
		
		toReturn += 	"</body>";
		toReturn += "</html>";
		
		return toReturn;
	}
	
	/**
	 * Cette méthode créé les lignes de tableau en html
	 * pour chaque palanquée contenue dans 
	 * liste des tour passée en paramètre.
	 * 
	 * @param listeTours la liste des tours
	 * @return le code html sous forme de string
	 */
	private static String parsePalanquees(List<Tour> listeTours) {
		String listePalanqueesString = "";
		for (Tour t : listeTours) {
			for (Palanquee palanquee : t.getmListePalanquees()) {
				Plongeur encadrant = (Plongeur) palanquee.getEncadrant();
				List<Plongeur> listePlongeursDeLaPalanquee = new ArrayList<Plongeur>();
				for (int i = 0; i < 4; i ++) {
					listePlongeursDeLaPalanquee.add((Plongeur) palanquee.getPlongeur(i));
				}
				Plongeur serreFile = (Plongeur) palanquee.getSerreFile();
				
				listePalanqueesString += 	"<tr>";
				listePalanqueesString += 		"<td>" + (encadrant == null ? "-" : encadrant.getmNom()) + "</td>";
				listePalanqueesString += 		"<td>" + (encadrant == null ? "-" : encadrant.getmPrenom()) + "</td>";
				listePalanqueesString += 		"<td>" + (encadrant == null ? "-" : encadrant.getNiveauPlongeurCDS()) + "</td>";
				listePalanqueesString += 		"<td>" + (encadrant == null ? "-" : "Encadrant") + "</td>";
				listePalanqueesString += 		"<td colspan=\"2\" rowspan=\"6\">" + parseParamPalanqueePrevus(palanquee) + "</td>";
				listePalanqueesString += 		"<td colspan=\"3\" rowspan=\"5\">" + parseParamPalanqueeReels(palanquee) + "</td>";
				listePalanqueesString += 		"<td rowspan=\"6\">" + parseParamPalanqueeNeutres(palanquee) + "</td>";
				listePalanqueesString += 		"<td rowspan=\"6\">" + t.getmPosition() + "</td>";
				listePalanqueesString += 	"</tr>";
				for (Plongeur plongeur : listePlongeursDeLaPalanquee) {
					listePalanqueesString += 	"<tr>";
					listePalanqueesString += 		"<td>" + (plongeur == null ? "-" : plongeur.getmNom()) + "</td>";
					listePalanqueesString += 		"<td>" + (plongeur == null ? "-" : plongeur.getmPrenom()) + "</td>";
					listePalanqueesString += 		"<td>" + (plongeur == null ? "-" : plongeur.getNiveauPlongeurCDS()) + "</td>";
					if (encadrant != null) {
						listePalanqueesString += 	"<td>" + (plongeur == null ? "-" : "Encadré") + "</td>";
					}
					else {
						listePalanqueesString += 	"<td>" + (plongeur == null ? "-" : "Autonome") + "</td>";
					}
					listePalanqueesString += 	"</tr>";
				}
				listePalanqueesString += 	"<tr>";
				listePalanqueesString += 		"<td>" + (serreFile == null ? "-" : serreFile.getmNom()) + "</td>";
				listePalanqueesString += 		"<td>" + (serreFile == null ? "-" : serreFile.getmPrenom()) + "</td>";
				listePalanqueesString += 		"<td>" + (serreFile == null ? "-" : serreFile.getNiveauPlongeurCDS()) + "</td>";
				listePalanqueesString += 		"<td>" + (serreFile == null ? "-" : "Serre file") + "</td>";
				listePalanqueesString += 		"<td colspan=\"3\">Dép : " + " " + " Fin : " + " " + "</td>";
				listePalanqueesString += 	"</tr>";
				listePalanqueesString += 	"<tr id=\"separator\">";
				listePalanqueesString += 		"<th colspan=\"11\"></th>";
				listePalanqueesString += 	"</tr>";
			}
		}
		
		return listePalanqueesString;
	}
	
	/**
	 * Cette méthode créé des paragraphe html contenant la liste
	 * des parametres réels de la palanquée passée en paramètre.
	 * 
	 * @param palanquee la palanque
	 * @return
	 */
	private static String parseParamPalanqueeReels(Palanquee palanquee) {
		String paramReelsString = "";
		for (ParametrePalanquee param : palanquee.getmListeParametresPalanquee()) {
			if (param.getmType().equals(ParametrePalanqueeTypeEnum.REEL)) {
				paramReelsString += "<p><b>" + param.getmNom() + "</b> : " + param.getmValeur() + "</p>"; 
			}
		}
		return paramReelsString;
	}

	/**
	 * Cette méthode créé des paragraphe html contenant la liste
	 * des parametres neutres de la palanquée passée en paramètre.
	 * 
	 * @param palanquee la palanque
	 * @return
	 */
	private static String parseParamPalanqueeNeutres(Palanquee palanquee) {
		String paramNeutresString = "";
		for (ParametrePalanquee param : palanquee.getmListeParametresPalanquee()) {
			if (param.getmType().equals(ParametrePalanqueeTypeEnum.NEUTRE)) {
				paramNeutresString += "<p><b>" + param.getmNom() + "</b> : " + param.getmValeur() + "</p>"; 
			}
		}
		return paramNeutresString;
	}

	/**
	 * Cette méthode créé des paragraphe html contenant la liste
	 * des parametres prévus de la palanquée passée en paramètre.
	 * 
	 * @param palanquee la palanque
	 * @return
	 */
	private static String parseParamPalanqueePrevus(Palanquee palanquee) {
		String paramPrevusString = "";
		for (ParametrePalanquee param : palanquee.getmListeParametresPalanquee()) {
			if (param.getmType().equals(ParametrePalanqueeTypeEnum.PREVU)) {
				paramPrevusString += "<p><b>" + param.getmNom() + "</b> : " + param.getmValeur() + "</p>"; 
			}
		}
		return paramPrevusString;
	}

	/**
	 * Cette méthode créé une ligne de tableau en html
	 * pour chaque securité surface contenue dans la 
	 * liste des tours passée en paramètre.
	 * 
	 * @param listeTours la liste des tours
	 * @return le code html sous forme de string
	 */
	private static String parseSecuriteSurface(List<Tour> listeTours) {
		String listeToursString = "";
		for (Tour t : listeTours) {
			listeToursString += 	"<tr>";
			listeToursString += 		"<td>" + t.getmPosition() + "</td>";
			listeToursString += 		"<td>";
			for (SecuriteSurface ss : t.getmListeSecuritesSurface()) {
				listeToursString +=			ss.getmName() + " -- ";
			}
			listeToursString += 		"</td>";
			listeToursString += 	"</tr>";
		}
		return listeToursString;
	}

	/**
	 * Cette méthode créé un paragraphe en html
	 * pour la liste des paramètres de plongée passés en paramètre.
	 * 
	 * @param listeParametresPlongee la liste des paramètres
	 * @return le code html sous forme de string
	 */
	private static String parseParametrePlongee(List<ParametrePlongee> listeParametresPlongee) {
		String listeParamString = "";
		for (ParametrePlongee pp : listeParametresPlongee) {
			listeParamString += 		"<p>";
			listeParamString += 			"<b>" +  pp.getmCategorie() == null ? "Pas de catégorie" : pp.getmCategorie().name() + " : " + pp.getmNom() + " - </b>" + pp.getmDetails();
			listeParamString +=		"</p>";
		}
		return listeParamString;
	}

	/**
	 * Cette méthode créé une ligne de tableau en html
	 * pour la liste des plongeurs passée en paramètre.
	 * 
	 * @param listePlongeurs la liste des plongeurs
	 * @return le code html sous forme de string
	 */
	private static String parsePlongeurs(List<Plongeur> listePlongeurs) {
		String lignesPlongeurs = "";
		for (Plongeur p : listePlongeurs) {
			if (p.getmParticipe().getId() == 1) {
				lignesPlongeurs += "<tr>";
				lignesPlongeurs += 		"<td>" + p.getmNom() + "</td>";
				lignesPlongeurs += 		"<td>" + p.getmPrenom() + "</td>";
				lignesPlongeurs += 		"<td>" + p.getNiveauPlongeurCDS() + "</td>";
				lignesPlongeurs += "</tr>";
			}
		}
		return lignesPlongeurs;
	}

	/**
	 * Cette méthode renvoie la date donnée en ms en string.
	 * 
	 * @param datePlongee la date de la plongée
	 * @return le code html sous forme de string
	 */
	private static String parseDate(long datePlongee) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd_MM_yyyy");
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTimeInMillis(datePlongee);
		return formatter.format(calendar.getTime());
	}
	
}
