package fr.enac.utils.dragndrop;

import android.content.ClipData;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnLongClickListener;
import fr.enac.activities.PalanqueeActivity;

public class PalanqueeOnLongClickTouchListener implements OnLongClickListener {

	private long idPlongeur;

	public PalanqueeOnLongClickTouchListener(long idPlongeur) {
		this.idPlongeur = idPlongeur;
	}

	public long getIdPlongeur() {
		return idPlongeur;
	}

	@Override
	public boolean onLongClick(View view) {
		ClipData data = ClipData.newPlainText("", "");
		DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
		view.startDrag(data, shadowBuilder, view, 0);
		view.setVisibility(View.INVISIBLE);
		PalanqueeActivity.idDragPlongeur = idPlongeur;
		return true;
	}

	/*
	 * public boolean onTouch(View view, MotionEvent motionEvent) { if
	 * (motionEvent.getAction() == MotionEvent.ACTION_DOWN) { ClipData data =
	 * ClipData.newPlainText("", ""); DragShadowBuilder shadowBuilder = new
	 * View.DragShadowBuilder(view); view.startDrag(data, shadowBuilder, view,
	 * 0); view.setVisibility(View.INVISIBLE); PalanqueeActivity.idDragPlongeur
	 * = idPlongeur; return true; } else { return false; } }
	 */
}
