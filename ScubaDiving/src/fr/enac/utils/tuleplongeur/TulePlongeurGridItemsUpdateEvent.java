package fr.enac.utils.tuleplongeur;

import java.util.ArrayList;
import java.util.EventObject;

/**
 * Event qui permet de dialoguer avec l'activité plongeurActivity Utilisé
 * TuleGridPlongeurOnItemClickListener
 * 
 * @author Jonathan Tolle
 */
public class TulePlongeurGridItemsUpdateEvent extends EventObject {

	/**
	 * Enleve le warning
	 */
	private static final long serialVersionUID = 1L;
	
	// ------------------------------------------------------------------//
	// Variables de classe //
	// ------------------------------------------------------------------//
	/**
	 * Liste des tules à envoyer à l'activité
	 */
	private final ArrayList<TulePlongeurObjetAdapter> mtules;

	// ------------------------------------------------------------------//
	// Constructeurs //
	// ------------------------------------------------------------------//
	/**
	 * Constructeur par défaut
	 * 
	 * @param source
	 */
	public TulePlongeurGridItemsUpdateEvent(Object source,
			ArrayList<TulePlongeurObjetAdapter> tules) {
		super(source);
		this.mtules = tules;
	}

	// ------------------------------------------------------------------//
	// Getters & Setters //
	// ------------------------------------------------------------------//
	public ArrayList<TulePlongeurObjetAdapter> getTules() {
		return mtules;
	}
}
