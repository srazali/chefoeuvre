package fr.enac.utils.tuleplongeur;

import android.util.Log;
import android.view.View;
import android.view.View.OnLongClickListener;

/**
 * Long click event
 * @author Jonathan Tolle
 *
 */
public class TulePlongeurObjetAdapterOnLongClickListener implements OnLongClickListener{
	private final static String LOG_TAG = TulePlongeurObjetAdapterOnLongClickListener.class.getName();
	
	private TulePlongeurObjetAdapter tule;
	private int position;
	
	public TulePlongeurObjetAdapterOnLongClickListener(TulePlongeurObjetAdapter tule, int position){
		this.tule = tule;
		this.position = position;
	}
	
	@Override
	public boolean onLongClick(View v) {
		Log.d(LOG_TAG, "[LONG CLICK] Tule n°"+position);
		return false;
	}

}
