package fr.enac.utils.tuleplongeur;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.scubadiving.R;


/**
 * Redéfini l'adapteur de la gridview 
 * pour afficher les plongeurs dans PlongeurActivity
 * @author Jonathan Tolle
 */
public class TulePlongeurGridAdapter extends ArrayAdapter<TulePlongeurObjetAdapter>{

	private static final String LOG_TAG = TulePlongeurGridAdapter.class.getName();

	//------------------------------------------------------------------//
	//                     Variables de classe                          //
	//------------------------------------------------------------------//
	private Context context;
	private int layoutResourceId;
	private ArrayList<TulePlongeurObjetAdapter> mtules;
	private ArrayList<TulePlongeurGridItemsUpdateListener> listeners;

	//------------------------------------------------------------------//
	//                          Constructeurs                           //
	//------------------------------------------------------------------//
	/**
	 * Constructeur
	 * @param context
	 * @param layoutResourceId
	 * @param tules
	 */
	public TulePlongeurGridAdapter(Context context, int layoutResourceId,
			ArrayList<TulePlongeurObjetAdapter> tules) {
		super(context, layoutResourceId, tules);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.mtules = tules;
		this.listeners = new ArrayList<TulePlongeurGridItemsUpdateListener>();
	}
	
	//------------------------------------------------------------------//
	//                      Getters & Setters                           //
	//------------------------------------------------------------------//

	public void setTules(ArrayList<TulePlongeurObjetAdapter> mtules) {
		this.mtules = mtules;
	}
	
	public ArrayList<TulePlongeurObjetAdapter> getMtules() {
		return mtules;
	}
	
	//------------------------------------------------------------------//
	//                          Méthodes                                //
	//------------------------------------------------------------------//
	/**
	 * Redéfini l'affichage de la gridview
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			view = inflater.inflate(layoutResourceId, parent, false);						
		}		
		
		TextView tv_niveau = (TextView) view.findViewById(R.id.tv_tule_soustitre);			
		TextView tv_titre = (TextView) view.findViewById(R.id.tv_tule_titre);
		ImageView imv_icon = (ImageView) view.findViewById(R.id.imv_tule_parametre);
		
		TulePlongeurObjetAdapter item = mtules.get(position);
		
		tv_niveau.setText(item.getNiveau());
		tv_titre.setText(item.getNom());
		imv_icon.setImageBitmap(item.getIcone());

		setTuleStyle(view, tv_titre, tv_niveau, item.getNiveau(), item.getEtatCourant());
		
		view.setOnClickListener(new TulePlongeurObjetAdapterOnClickListener(position, this));
		view.setOnLongClickListener(new TulePlongeurObjetAdapterOnLongClickListener(item,position));	
		return view;
	}
	

	/**
	 * Gere l'affichage des etat des tules
	 * @param view
	 * @param tv_titre
	 * @param tv_niveau
	 * @param niveau
	 * @param etat
	 */
	private void setTuleStyle(View view, TextView tv_titre, TextView tv_niveau, String niveau, TulePlongeurObjetAdapter.TuleEtat etat){

		RelativeLayout rl_base_tule = (RelativeLayout) view.findViewById(R.id.rl_base_tule);
		LinearLayout llh_tule = (LinearLayout) view.findViewById(R.id.llh_tule);
		Resources resources = view.getContext().getResources();
		//Log.d(LOG_TAG, "Plongeur de niveau :"+niveau);

		switch(etat){
		case NON_SELECTIONNE_ET_VALIDE:
			tuleNonSelectionne(resources, rl_base_tule);
			tuleValide(resources, llh_tule, tv_titre, tv_niveau);
			break;
		case SELECTIONNE_ET_VALIDE:
			tuleSelectione(resources, rl_base_tule);
			tuleValide(resources, llh_tule, tv_titre, tv_niveau);
			break;
		case SELECTIONNE_ET_NON_VALIDE:
			tuleSelectione(resources, rl_base_tule);
			tuleNonValide(resources, llh_tule, tv_titre, tv_niveau);
			break;
		case NON_SELECTIONNE_ET_NON_VALIDE:
			tuleNonSelectionne(resources, rl_base_tule);
			tuleNonValide(resources, llh_tule, tv_titre, tv_niveau);
			break;
		}
	}

	private void tuleValide(Resources resources, LinearLayout llh_tule, TextView tv_titre, TextView tv_niveau){
		llh_tule.setBackground(resources.getDrawable(R.drawable.tule_plongeur_valide_background));
		tv_niveau.setTextColor(resources.getColor(R.color.blanc_primaire));
		tv_titre.setTextColor(resources.getColor(R.color.blanc_primaire));
	}

	private void tuleNonValide(Resources resources, LinearLayout llh_tule, TextView tv_titre, TextView tv_niveau){
		llh_tule.setBackground(resources.getDrawable(R.drawable.tule_plongeur_nonvalide_background));
		tv_niveau.setTextColor(resources.getColor(R.color.noir_primaire));
		tv_titre.setTextColor(resources.getColor(R.color.noir_primaire));
	}

	private void tuleSelectione(Resources resources, RelativeLayout rl_base_tule){
		rl_base_tule.setBackground(resources.getDrawable(R.drawable.tule_plongeur_selectionne));		
	}

	private void tuleNonSelectionne(Resources resources, RelativeLayout rl_base_tule){
		rl_base_tule.setBackground(resources.getDrawable(R.drawable.tule_plongeur_nonselectionne));		
	}
	
	/**
	 * Notifie les listeners du changement des tules
	 */
	public void notifierListener() {
		for (TulePlongeurGridItemsUpdateListener hl : listeners)
			hl.tulesUpdated(new TulePlongeurGridItemsUpdateEvent(this, mtules));	
	}

	/**
	 * Ajoute les listeners utilisé pour fire 
	 * TuleGridPlongeurItemUpdateEvent
	 * à l'objet plongeurActivity
	 * @param toAdd
	 */
	public void addListener(TulePlongeurGridItemsUpdateListener toAdd) {
		listeners.add(toAdd);
	}
}


