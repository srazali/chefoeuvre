package fr.enac.utils.tuleplongeur;

import android.graphics.Bitmap;

/**
 * 
 * @author Jonathan Tolle
 */

public class TulePlongeurObjetAdapter {

	//------------------------------------------------------------------//
	//                     Variables de classe                          //
	//------------------------------------------------------------------//
	private Bitmap micone;
	private String mnom;
	private String mniveau;
	private TuleEtat metatCourant;
	private long idPlongeur;

	public enum TuleEtat{
		NON_SELECTIONNE_ET_NON_VALIDE,
		NON_SELECTIONNE_ET_VALIDE,
		SELECTIONNE_ET_VALIDE,
		SELECTIONNE_ET_NON_VALIDE,		
	}

	//------------------------------------------------------------------//
	//                          Constructeurs                           //
	//------------------------------------------------------------------//
	/**
	 * Constructeur
	 * @param etatCourant
	 * @param micone
	 * @param mnom
	 * @param mniveau
	 */
	public TulePlongeurObjetAdapter(TuleEtat etatCourant, Bitmap micone, String mnom,
			String mniveau) {
		super();
		this.metatCourant = etatCourant;
		this.micone = micone;
		this.mnom = mnom;
		this.mniveau = mniveau;
	}	
	
	public TulePlongeurObjetAdapter(TuleEtat etatCourant, Bitmap micone, String mnom,
			String mniveau, long idPlongeur) {
		this(etatCourant,micone,mnom,mniveau);
		this.idPlongeur = idPlongeur;
	}	

	//------------------------------------------------------------------//
	//                      Getters & Setters                           //
	//------------------------------------------------------------------//	
	public TuleEtat getEtatCourant() {
		return metatCourant;
	}

	public void setEtatCourant(TuleEtat etatCourant) {
		this.metatCourant = etatCourant;
	}

	public Bitmap getIcone() {
		return micone;
	}

	public String getNom() {
		return mnom;
	}

	public String getNiveau() {
		return mniveau;
	}

	public void setMicone(Bitmap micone) {
		this.micone = micone;
	}

	public void setMnom(String mnom) {
		this.mnom = mnom;
	}

	public void setMniveau(String mniveau) {
		this.mniveau = mniveau;
	}
	
	public long getIdPlongeur() {
		return idPlongeur;
	}

	//------------------------------------------------------------------//
	//                          Méthodes                                //
	//------------------------------------------------------------------//
	/**
	 * Etat suivant apres chaque clic de l'utilisateur
	 */
	public void etatSuivant() {
		switch (metatCourant) {
		case NON_SELECTIONNE_ET_VALIDE:
			metatCourant = TuleEtat.SELECTIONNE_ET_VALIDE;
			break;
		case SELECTIONNE_ET_VALIDE:
			metatCourant = TuleEtat.SELECTIONNE_ET_NON_VALIDE;
			break;
		case SELECTIONNE_ET_NON_VALIDE:
			metatCourant = TuleEtat.NON_SELECTIONNE_ET_NON_VALIDE;
			break;
		case NON_SELECTIONNE_ET_NON_VALIDE:
			metatCourant = TuleEtat.SELECTIONNE_ET_VALIDE;			
			break;
		default:
			break;
		}		
	}

	/**
	 * Déselectionne la tule
	 */
	public void deselectionne(){
		switch (metatCourant) {
		case NON_SELECTIONNE_ET_VALIDE:
			//on ne fait rien
			break;
		case SELECTIONNE_ET_VALIDE:
			metatCourant = TuleEtat.NON_SELECTIONNE_ET_VALIDE;
			break;
		case SELECTIONNE_ET_NON_VALIDE:
			metatCourant = TuleEtat.NON_SELECTIONNE_ET_NON_VALIDE;
			break;
		case NON_SELECTIONNE_ET_NON_VALIDE:
			//on ne fait rien
			break;
		default:
			break;
		}
	}	
}
