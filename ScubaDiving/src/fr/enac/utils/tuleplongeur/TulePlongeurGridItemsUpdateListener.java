package fr.enac.utils.tuleplongeur;

/**
 * Interface pour les listeners
 * Utilisé dans plongeurActivity
 * @author Jonathan
 */
public interface TulePlongeurGridItemsUpdateListener {
	public void tulesUpdated(TulePlongeurGridItemsUpdateEvent event);
}
