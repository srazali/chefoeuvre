package fr.enac.utils.tuleplongeur;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

/**
 * Clic event sur les tules
 * Notifie les listeners de l'adapter @see TuleGridAdapter
 * Pour mettre à jour l'activité (listener)
 * @author Jonathan Tolle
 *
 */
public class TulePlongeurObjetAdapterOnClickListener implements OnClickListener{
	private final static String LOG_TAG = TulePlongeurObjetAdapterOnClickListener.class.getName();
	
	private int position;
	private TulePlongeurGridAdapter adapter;
	
	public TulePlongeurObjetAdapterOnClickListener(int position, TulePlongeurGridAdapter adapter){
		this.position = position;
		this.adapter = adapter;
	}
	
	@Override
	public void onClick(View v) {
			Log.d(LOG_TAG, "[CLICK] Tule n°"+position);	
			
			TulePlongeurObjetAdapter tule = adapter.getMtules().get(position);

			// si clic sur un item on déselectionne toutes les autres tules
			for(TulePlongeurObjetAdapter t: adapter.getMtules()){
				if(t!=tule){
					t.deselectionne();
				}
			}

			// On fait passer la tule a l'état suivant
			tule.etatSuivant();					

			// On refresh la vue
			adapter.notifyDataSetChanged();

			// On notifie l'activité
			adapter.notifierListener();
	}
}
