package fr.enac.utils;

import android.util.Log;

/**
 * Classe qui permet de convertir le temps rentré par l'utilisateur en minute et inversement
 * @author Jonathan Tolle
 *
 */
public class TempsFormat {
	private static final String LOG_TAG = TempsFormat.class.getName();

	/**
	 * Converti un string du temps (ex : 1h20, 1 heure 20 minutes, 1h20m, 1h, 20m, 40) en nombre de minutes 
	 * @param temps
	 * @return nb minutes
	 */
	public static int convertStringTimeToMinute(String temps){ 
		int minutes = 0;	
		int heures = 0;
		if(temps.matches(".*[hH].*")){ //si heures
			String[] tmp = temps.split("[^0-9]+");
			Log.d(LOG_TAG ," String[] tmp : ");
			for(int i=0; i<tmp.length; i++)
				Log.d(LOG_TAG," "+(tmp[i]));
			heures = (tmp.length>=1 ? Integer.parseInt(tmp[0].replaceAll("[^0-9]+", " ")) : 0);
			minutes = (tmp.length>=2 ? Integer.parseInt(tmp[1].replaceAll("[^0-9]+", " ")) : 0);
		}
		else if(temps.matches("( )*[0-9]+[ a-zA-Z]*")){ // si que minutes
			String[] tmp = temps.split("[^0-9]+");
			Log.d(LOG_TAG," String[] tmp : ");
			for(int i=0; i<tmp.length; i++)
				Log.d(LOG_TAG," "+(tmp[i]));
			minutes = (tmp.length>=1 ? Integer.parseInt(tmp[0].replaceAll("[^0-9]+", " ")) : 0);
		}
		else{
			Log.d(LOG_TAG,"Erreur synthaxe temps non reconnue : "+temps);
		}		
		Log.d(LOG_TAG,"Convert : "+ temps +" to "+(heures*60 + minutes)+"min ["+heures+"h"+minutes+"]");
		return heures*60 + minutes;		
	}
	
	/**
	 * Converti les minutes en format [nombre d'heure] nombre de minutes
	 * @param temps
	 * @return String
	 */
	public static String convertMinuteToStringTime(int temps){
		StringBuilder s = new StringBuilder();
		if(temps%60>0){
			s.append(temps%60+"h "+(temps-(temps%60*60))+"min");
		}
		else{
			s.append(temps+"min");
		}
		return s.toString();
	}
}
