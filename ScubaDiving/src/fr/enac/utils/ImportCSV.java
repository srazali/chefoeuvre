package fr.enac.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;

/**
 *
 * @author Jonathan Tolle tuto :
 *         http://thierry-leriche-dessirier.developpez.com/
 *         tutoriels/java/csv-avec-java/
 *         
 * @author Leriche        
 */
public class ImportCSV {

	public static String getResourcePath(String fileName) {
		final File f = new File("");
		final String dossierPath = f.getAbsolutePath() + File.separator
				+ fileName;
		return dossierPath;
	}

	public static File getResource(String fileName) {
		final String completeFileName = getResourcePath(fileName);
		File file = new File(completeFileName);
		return file;
	}

	/**
	 * Méthode qui lit ligne par ligne le fichier passé en paramètre
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static List<String> readFile(File file) throws IOException {

		List<String> result = new ArrayList<String>();

		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);

		boolean aEnregistrer = false;

		// On lit ligne par ligne tant que le fichier ne renvoi pas null
		for (String line = br.readLine(); line != null; line = br.readLine()) {

			// Enregistre seulement si le header est passé
			if (aEnregistrer) {
				result.add(line);
			}

			// Skip the header
			if (!aEnregistrer && line.toLowerCase().contains("niveau")) {
				aEnregistrer = true;
			}
		}

		br.close();
		fr.close();

		return result;
	}
/**
 * Lit un fichier CSV depuis le dossier assets
 * @param ctx contexte de l'activité
 * @param s nom du fichier
 * @return une liste de lignes au format texte
 */
	public static List<String> readCSVfromAssets(Context ctx, String s) {
		String line;
		List<String> result = new ArrayList<String>();
		BufferedReader reader = null;

		try {
			reader = new BufferedReader(new InputStreamReader(ctx.getAssets()
					.open(s)));
			while ((line = reader.readLine()) != null) {
				result.add(line);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (reader != null)
					reader.close();
			} catch (IOException e) {
				// handle exception
			}
		}
		return result;
	}
}
