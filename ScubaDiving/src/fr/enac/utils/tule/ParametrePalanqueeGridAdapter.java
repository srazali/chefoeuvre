package fr.enac.utils.tule;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.scubadiving.R;

import fr.enac.activities.PalanqueeActivity;
import fr.enac.models.ParametrePalanquee;
import fr.enac.utils.tuleplongeur.TulePlongeurObjetAdapter;
import fr.enac.utils.tuleplongeur.TulePlongeurObjetAdapter.TuleEtat;

/**
 * Redéfini l'adapteur de la gridview pour afficher les plongeurs dans
 * PlongeurActivity
 * 
 * @author Jonathan Tolle
 */
public class ParametrePalanqueeGridAdapter extends
		ArrayAdapter<ParametrePalanquee> {

	private static final String LOG_TAG = ParametrePalanqueeGridAdapter.class
			.getName();

	// ------------------------------------------------------------------//
	// Variables de classe //
	// ------------------------------------------------------------------//
	private Context context;
	private int layoutResourceId;
	private ArrayList<ParametrePalanquee> lParam;

	// ------------------------------------------------------------------//
	// Constructeurs //
	// ------------------------------------------------------------------//
	/**
	 * Constructeur
	 */
	public ParametrePalanqueeGridAdapter(Context context, int layoutResourceId,
			ArrayList<ParametrePalanquee> lParam) {
		super(context, layoutResourceId, lParam);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.lParam = lParam;
	}

	// ------------------------------------------------------------------//
	// Méthodes //
	// ------------------------------------------------------------------//
	/**
	 * Redéfini l'affichage de la gridview
	 */
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;

		if (view == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			view = inflater.inflate(layoutResourceId, parent, false);
		}

		TextView tv_value = (TextView) view
				.findViewById(R.id.tv_palanquee_parametre_value);
		TextView tv_indice = (TextView) view
				.findViewById(R.id.tv_palanquee_parametre_indice);
		ImageView imv_icon = (ImageView) view
				.findViewById(R.id.imv_tule_parametre_palanquee);

		ParametrePalanquee item = lParam.get(position);
		tv_value.setText(item.getmValeur());
		tv_indice.setText(item.getmType().name());
		imv_icon.setImageBitmap(BitmapFactory.decodeResource(
				context.getResources(), (int) item.getmIconId()));

		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				PalanqueeActivity.setSelectedParametre(lParam.get(position));
				EditText etNom = (EditText) ((Activity) context)
						.findViewById(R.id.et_palanquee_nom);
				etNom.setText(lParam.get(position).getmNom());

				EditText etValue = (EditText) ((Activity) context)
						.findViewById(R.id.et_palanquee_value);
				etValue.setText(lParam.get(position).getmValeur());
			}
		});
		return view;
	}

	/**
	 * Gere l'affichage des etat des tules
	 * 
	 * @param view
	 * @param tv_titre
	 * @param tv_niveau
	 * @param niveau
	 * @param etat
	 */
	private void setTuleStyle(View view, TextView tv_titre, TextView tv_niveau,
			String niveau, TulePlongeurObjetAdapter.TuleEtat etat) {

		RelativeLayout rl_base_tule = (RelativeLayout) view
				.findViewById(R.id.rl_base_tule);
		LinearLayout llh_tule = (LinearLayout) view.findViewById(R.id.llh_tule);
		Resources resources = view.getContext().getResources();
		// Log.d(LOG_TAG, "Plongeur de niveau :"+niveau);

		switch (etat) {
		case NON_SELECTIONNE_ET_VALIDE:
			tuleNonSelectionne(resources, rl_base_tule);
			tuleValide(resources, llh_tule, tv_titre, tv_niveau);
			break;
		case SELECTIONNE_ET_VALIDE:
			tuleSelectione(resources, rl_base_tule);
			tuleValide(resources, llh_tule, tv_titre, tv_niveau);
			break;
		case SELECTIONNE_ET_NON_VALIDE:
			tuleSelectione(resources, rl_base_tule);
			tuleNonValide(resources, llh_tule, tv_titre, tv_niveau);
			break;
		case NON_SELECTIONNE_ET_NON_VALIDE:
			tuleNonSelectionne(resources, rl_base_tule);
			tuleNonValide(resources, llh_tule, tv_titre, tv_niveau);
			break;
		}
	}

	private void tuleValide(Resources resources, LinearLayout llh_tule,
			TextView tv_titre, TextView tv_niveau) {
		llh_tule.setBackground(resources
				.getDrawable(R.drawable.tule_plongeur_valide_background));
		tv_niveau.setTextColor(resources.getColor(R.color.blanc_primaire));
		tv_titre.setTextColor(resources.getColor(R.color.blanc_primaire));
	}

	private void tuleNonValide(Resources resources, LinearLayout llh_tule,
			TextView tv_titre, TextView tv_niveau) {
		llh_tule.setBackground(resources
				.getDrawable(R.drawable.tule_plongeur_nonvalide_background));
		tv_niveau.setTextColor(resources.getColor(R.color.noir_primaire));
		tv_titre.setTextColor(resources.getColor(R.color.noir_primaire));
	}

	private void tuleSelectione(Resources resources, RelativeLayout rl_base_tule) {
		rl_base_tule.setBackground(resources
				.getDrawable(R.drawable.tule_plongeur_selectionne));
	}

	private void tuleNonSelectionne(Resources resources,
			RelativeLayout rl_base_tule) {
		rl_base_tule.setBackground(resources
				.getDrawable(R.drawable.tule_plongeur_nonselectionne));
	}
}
