package fr.enac.utils.tule;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import fr.enac.utils.clicclic.PalanqueeTuleTouchListener;
import fr.enac.utils.dragndrop.PalanqueeOnLongClickTouchListener;
import fr.enac.utils.tuleplongeur.TulePlongeurGridAdapter;
import fr.enac.utils.tuleplongeur.TulePlongeurObjetAdapter;

/**
 * 
 * @author Racim Fahssi & Jonathan Tolle
 */
public class TuleGridAdapterForDrag extends TulePlongeurGridAdapter {
	/**
	 * LOG TAG
	 */
	private static final String LOG_TAG = TuleGridAdapterForDrag.class
			.getName();

	public TuleGridAdapterForDrag(Context context, int layoutResourceId,
			ArrayList<TulePlongeurObjetAdapter> tules) {
		super(context, layoutResourceId, tules);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = super.getView(position, convertView, parent);
		view.setOnLongClickListener(new PalanqueeOnLongClickTouchListener(super
				.getMtules().get(position).getIdPlongeur()));
		view.setOnClickListener(new PalanqueeTuleTouchListener((super
				.getMtules().get(position).getIdPlongeur())));

		return view;
	}

}
