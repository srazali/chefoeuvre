package fr.enac.utils;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.scubadiving.R;

import fr.enac.lii.CDS.Niveau;


public class IconNiveauPlongeur {
	
	private static Bitmap iconeP1 ;
	private static Bitmap iconeP2 ;
	private static Bitmap iconeP3 ;
	private static Bitmap iconeP4 ;
	private static Bitmap iconeGP ;
	private static Bitmap iconeE1 ;
	private static Bitmap iconeDEB ;
	
	private static Bitmap iconeP1_dark ;
	private static Bitmap iconeP2_dark ;
	private static Bitmap iconeP3_dark ;
	private static Bitmap iconeP4_dark ;
	private static Bitmap iconeGP_dark ;
	private static Bitmap iconeE1_dark ;
	private static Bitmap iconeDEB_dark ;
	/**
	 * Retourne l'icone en fonction du niveau du plongeur
	 * 
	 * @param niveauPlongeurCDS
	 * @return
	 */
	public static Bitmap getBitmapFromNiveau(Resources res, String niveauPlongeurCDS) {
		
		if (niveauPlongeurCDS == null){
			// evite les erreurs si on a pas affecté de niveau CDS
			if (iconeDEB == null){
				iconeDEB = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_deb_light);
			}
			return iconeDEB;
		}
			

		if(niveauPlongeurCDS.contains(Niveau.P1.toString())){
			if (iconeP1 == null){
				iconeP1 = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_p1_light);
			}
			return iconeP1;
		}
		else if(niveauPlongeurCDS.contains(Niveau.P2.toString())){
			if (iconeP2 == null){
				iconeP2 = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_p1_light);
			}
			return iconeP2;
		}
		else if(niveauPlongeurCDS.contains(Niveau.P3.toString())){
			if (iconeP3 == null){
				iconeP3 = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_p3_light);
			}
			return iconeP3;
		}
		else if(niveauPlongeurCDS.contains(Niveau.P4.toString())){
			if (iconeP4 == null){
				iconeP4 = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_p4_light);
			}
			return iconeP4;
		}
		else if(niveauPlongeurCDS.contains(Niveau.P5.toString())){
			if (iconeP4 == null){
				iconeP4 = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_p4_light);
			}
			return iconeP4;
		}
		else if(niveauPlongeurCDS.contains(Niveau.E1.toString())){
			if (iconeE1 == null){
				iconeE1 = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_e1_light);
			}
			return iconeE1;
		}
		else if(niveauPlongeurCDS.contains(Niveau.E2.toString())){
			if (iconeE1 == null){
				iconeE1 = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_e1_light);
			}
			return iconeE1;
		}
		else if(niveauPlongeurCDS.contains(Niveau.E3.toString())){
			if (iconeE1 == null){
				iconeE1 = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_e1_light);
			}
			return iconeE1;
		}
		else if(niveauPlongeurCDS.contains(Niveau.E4.toString())){
			if (iconeE1 == null){
				iconeE1 = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_e1_light);
			}
			return iconeE1;
		}
		else if(niveauPlongeurCDS.contains(Niveau.GP.toString())){
			if (iconeE1 == null){
				iconeE1 = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_gp_light);
			}
			return iconeE1;
		}
		else{			
			if (iconeDEB == null){
				iconeDEB = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_deb_light);
			}
			return iconeDEB;
		}
	}
	
	/**
	 * Retourne l'icone dark en fonction du niveau du plongeur
	 * 
	 * @param niveauPlongeurCDS
	 * @return
	 */
	public static Bitmap getDarkBitmapFromNiveau(Resources res, String niveauPlongeurCDS) {
		
		if (niveauPlongeurCDS == null){
			// evite les erreurs si on a pas affecté de niveau CDS
			if (iconeDEB_dark == null){
				iconeDEB_dark = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_deb_dark);
			}
			return iconeDEB_dark;
		}
			

		if(niveauPlongeurCDS.contains(Niveau.P1.toString())){
			if (iconeP1_dark == null){
				iconeP1_dark = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_p1_dark);
			}
			return iconeP1_dark;
		}
		else if(niveauPlongeurCDS.contains(Niveau.P2.toString())){
			if (iconeP2_dark == null){
				iconeP2_dark = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_p2_dark);
			}
			return iconeP2_dark;
		}
		else if(niveauPlongeurCDS.contains(Niveau.P3.toString())){
			if (iconeP3_dark == null){
				iconeP3_dark = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_p3_dark);
			}
			return iconeP3_dark;
		}
		else if(niveauPlongeurCDS.contains(Niveau.P4.toString())){
			if (iconeP4_dark == null){
				iconeP4_dark = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_p4_dark);
			}
			return iconeP4_dark;
		}
		else if(niveauPlongeurCDS.contains(Niveau.P5.toString())){
			if (iconeP4_dark == null){
				iconeP4_dark = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_p4_dark);
			}
			return iconeP4_dark;
		}
		else if(niveauPlongeurCDS.contains(Niveau.E1.toString())){
			if (iconeE1_dark == null){
				iconeE1_dark = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_e1_dark);
			}
			return iconeE1_dark;
		}
		else if(niveauPlongeurCDS.contains(Niveau.E2.toString())){
			if (iconeE1_dark == null){
				iconeE1_dark = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_e1_dark);
			}
			return iconeE1_dark;
		}
		else if(niveauPlongeurCDS.contains(Niveau.E3.toString())){
			if (iconeE1_dark == null){
				iconeE1_dark = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_e1_dark);
			}
			return iconeE1_dark;
		}
		else if(niveauPlongeurCDS.contains(Niveau.E4.toString())){
			if (iconeE1_dark == null){
				iconeE1_dark = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_e1_dark);
			}
			return iconeE1_dark;
		}
		else if(niveauPlongeurCDS.contains(Niveau.GP.toString())){
			if (iconeGP_dark == null){
				iconeGP_dark = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_gp_dark);
			}
			return iconeGP_dark;
		}
		else{			
			if (iconeDEB_dark == null){
				iconeDEB_dark = BitmapFactory.decodeResource(res, R.drawable.ic_plongeur_deb_dark);
			}
			return iconeDEB_dark;
		}
	}

}
