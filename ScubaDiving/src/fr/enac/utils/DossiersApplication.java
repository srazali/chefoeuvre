package fr.enac.utils;

import java.io.File;

import android.os.Environment;
import android.util.Log;

/**
 * Classe servant a générer les dossiers utiles à l'application
 * @author Jonathan Tolle
 *
 */
public class DossiersApplication {
	
	/**
	 * Chemin par défaut du stockage
	 */
	public final static String CHEMIN_PAR_DEFAUT = Environment.getExternalStorageDirectory().getAbsolutePath()+""+File.separator;
	
	/**
	 * Dossiers 
	 */
	public final static String DOSSIER_ADLM = CHEMIN_PAR_DEFAUT + "ADLM"+""+File.separator;
	public final static String DOSSIER_IMPORT = DOSSIER_ADLM+""+File.separator+"Import"+""+File.separator;
	public final static String DOSSIER_EXPORT = DOSSIER_ADLM+""+File.separator+"Export"+""+File.separator;
	public final static String DOSSIER_SECOURS = DOSSIER_ADLM+""+File.separator+"Plan de secours"+""+File.separator;

	private static final String LOG_TAG = DossiersApplication.class.getName();
	
	/*
	 * Impossible d'instancier la classe
	 */
	private DossiersApplication(){}

	public static void genererDossiers(){
		creationDossier(DOSSIER_ADLM);
		creationDossier(DOSSIER_IMPORT);
		creationDossier(DOSSIER_EXPORT);
		creationDossier(DOSSIER_SECOURS);
	}
	
	private static void creationDossier(String chemin){
		File dir = new File(chemin);
		if(!dir.exists()){
			Log.d(LOG_TAG,"Création du dossier : "+chemin);			
			dir.mkdir();
		}
	}	
}
