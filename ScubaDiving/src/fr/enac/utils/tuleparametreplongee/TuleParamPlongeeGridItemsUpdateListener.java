package fr.enac.utils.tuleparametreplongee;

/**
 * Interface pour les listeners
 * Utilisé dans plongeurActivity
 * @author Jonathan
 */
public interface TuleParamPlongeeGridItemsUpdateListener {
	public void tulesUpdated(TuleParamPlongeeGridItemsUpdateEvent event);
}
