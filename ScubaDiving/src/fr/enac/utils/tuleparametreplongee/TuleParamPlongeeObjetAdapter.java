package fr.enac.utils.tuleparametreplongee;

import fr.enac.models.ParametrePlongeeCategorieEnum;
import android.graphics.Bitmap;

/**
 * 
 * @author Racim Fahssi & Jonathan Tolle
 */

public class TuleParamPlongeeObjetAdapter {
	private Bitmap micone;
	private String mnom;
	private String mdetails;
	private ParametrePlongeeCategorieEnum mcategorie;
	private TuleParamPlongeeEtat metatCourant;

	public enum TuleParamPlongeeEtat{
		NON_SELECTIONNE_ET_NON_VALIDE,
		NON_SELECTIONNE_ET_VALIDE,
		SELECTIONNE_ET_VALIDE,
		SELECTIONNE_ET_NON_VALIDE,		
	}

	public TuleParamPlongeeObjetAdapter(TuleParamPlongeeEtat etatCourant, Bitmap micone, String mnom,
			String mdetails, ParametrePlongeeCategorieEnum categorie) {
		super();
		this.metatCourant = etatCourant;
		this.micone = micone;
		this.mnom = mnom;
		this.mdetails = mdetails;
		this.mcategorie = categorie;
	}

	public TuleParamPlongeeEtat getEtatCourant() {
		return metatCourant;
	}

	public void setEtatCourant(TuleParamPlongeeEtat etatCourant) {
		this.metatCourant = etatCourant;
	}

	public Bitmap getIcone() {
		return micone;
	}

	public String getNom() {
		return mnom;
	}

	public void setMicone(Bitmap micone) {
		this.micone = micone;
	}

	public void setMnom(String mnom) {
		this.mnom = mnom;
	}
	
	public String getMdetails() {
		return mdetails;
	}

	public void setMdetails(String mdetails) {
		this.mdetails = mdetails;
	}

	public ParametrePlongeeCategorieEnum getMcategorie() {
		return mcategorie;
	}

	public void setMcategorie(ParametrePlongeeCategorieEnum mcategorie) {
		this.mcategorie = mcategorie;
	}

	/*
	 * Etat suivant apres chaque clic de l'utilisateur
	 */
	public void etatSuivant() {
		switch (metatCourant) {
		case NON_SELECTIONNE_ET_VALIDE:
			metatCourant = TuleParamPlongeeEtat.SELECTIONNE_ET_VALIDE;
			break;
		case SELECTIONNE_ET_VALIDE:
			metatCourant = TuleParamPlongeeEtat.SELECTIONNE_ET_NON_VALIDE;
			break;
		case SELECTIONNE_ET_NON_VALIDE:
			metatCourant = TuleParamPlongeeEtat.NON_SELECTIONNE_ET_NON_VALIDE;
			break;
		case NON_SELECTIONNE_ET_NON_VALIDE:
			metatCourant = TuleParamPlongeeEtat.SELECTIONNE_ET_VALIDE;			
			break;
		default:
			break;
		}		
	}
	
	public void deselectionne(){
		switch (metatCourant) {
		case NON_SELECTIONNE_ET_VALIDE:
			//on ne fait rien
			break;
		case SELECTIONNE_ET_VALIDE:
			metatCourant = TuleParamPlongeeEtat.NON_SELECTIONNE_ET_VALIDE;
			break;
		case SELECTIONNE_ET_NON_VALIDE:
			metatCourant = TuleParamPlongeeEtat.NON_SELECTIONNE_ET_NON_VALIDE;
			break;
		case NON_SELECTIONNE_ET_NON_VALIDE:
			//on ne fait rien
			break;
		default:
			break;
		}
	}	
}
