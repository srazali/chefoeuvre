package fr.enac.utils.tuleparametreplongee;

import java.util.ArrayList;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class TuleParamPlongeeGridOnItemClickListener implements
		OnItemClickListener {

	private static final String LOG_TAG = TuleParamPlongeeGridOnItemClickListener.class
			.getName();

	private ArrayList<TuleParamPlongeeObjetAdapter> mtules;
	private TuleParamPlongeeGridAdapter mtuleGrilleAdapter;
	private ArrayList<TuleParamPlongeeObjetAdapter> mOtherCatTules;
	private TuleParamPlongeeGridAdapter mOtherCatTuleGrilleAdapter;

	private ArrayList<TuleParamPlongeeGridItemsUpdateListener> listeners;

	public TuleParamPlongeeGridOnItemClickListener(
			ArrayList<TuleParamPlongeeObjetAdapter> tules,
			TuleParamPlongeeGridAdapter tuleGrilleAdapter,
			ArrayList<TuleParamPlongeeObjetAdapter> otherCatTules,
			TuleParamPlongeeGridAdapter otherCatTulesGrilleAdapter) {
		mtules = tules;
		mtuleGrilleAdapter = tuleGrilleAdapter;
		mOtherCatTules = otherCatTules;
		mOtherCatTuleGrilleAdapter = otherCatTulesGrilleAdapter;
		listeners = new ArrayList<TuleParamPlongeeGridItemsUpdateListener>();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {

		TuleParamPlongeeObjetAdapter tule = mtules.get(position);

		// si clic sur un item on déselectionne toutes les autres tules
		for (TuleParamPlongeeObjetAdapter t : mtules) {
			if (t != tule) {
				t.deselectionne();
			}
		}
		// Comme on a cliqu� sur l'item dans la grille qui contient les mTules,
		// on deselectionne tous les autres de l'autre grille de tules
		for (TuleParamPlongeeObjetAdapter t : mOtherCatTules) {
			t.deselectionne();
		}
		// On fait passer la tule a l'état suivant
		tule.etatSuivant();

		// On refresh la vue
		mtuleGrilleAdapter.notifyDataSetChanged();
		mOtherCatTuleGrilleAdapter.notifyDataSetChanged();

		// On notifie l'activité
		notifierListener();

	}

	/**
	 * Notifie les listeners du changement des tules
	 */
	private void notifierListener() {
		for (TuleParamPlongeeGridItemsUpdateListener hl : listeners)
			hl.tulesUpdated(new TuleParamPlongeeGridItemsUpdateEvent(this,
					mtules));
	}

	/**
	 * Ajoute les listeners utilisé pour fire TuleGridPlongeurItemUpdateEvent à
	 * l'objet plongeurActivity
	 * 
	 * @param toAdd
	 */
	public void addListener(TuleParamPlongeeGridItemsUpdateListener toAdd) {
		listeners.add(toAdd);
	}
}
