package fr.enac.utils.tuleparametreplongee;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.scubadiving.R;

/**
 * 
 * @author Romain Lechien
 */
public class TuleParamPlongeeGridAdapter extends
		ArrayAdapter<TuleParamPlongeeObjetAdapter> {
	/**
	 * LOG TAG
	 */
	private static final String LOG_TAG = TuleParamPlongeeGridAdapter.class
			.getName();

	private Context context;
	private int layoutResourceId;
	private ArrayList<TuleParamPlongeeObjetAdapter> mtules;

	public TuleParamPlongeeGridAdapter(Context context, int layoutResourceId,
			ArrayList<TuleParamPlongeeObjetAdapter> tules) {
		super(context, layoutResourceId, tules);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.mtules = tules;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;

		if (view == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			view = inflater.inflate(layoutResourceId, parent, false);
		}

		TextView tv_details = (TextView) view
				.findViewById(R.id.tv_tule_soustitre);
		TextView tv_titre = (TextView) view.findViewById(R.id.tv_tule_titre);
		ImageView imv_icon = (ImageView) view
				.findViewById(R.id.imv_tule_parametre);

		TuleParamPlongeeObjetAdapter item = mtules.get(position);
		tv_details.setText(item.getMdetails());
		tv_titre.setText(item.getNom());
		imv_icon.setImageBitmap(item.getIcone());

		setTuleStyle(view, tv_titre, tv_details, item.getMdetails(),
				item.getEtatCourant());

		return view;
	}

	/**
	 * Gere l'affichage des etat des tules
	 * 
	 * @param view
	 * @param tv_titre
	 * @param tv_details
	 * @param niveau
	 * @param etat
	 */
	private void setTuleStyle(View view, TextView tv_titre,
			TextView tv_details, String niveau,
			TuleParamPlongeeObjetAdapter.TuleParamPlongeeEtat etat) {

		RelativeLayout rl_base_tule = (RelativeLayout) view
				.findViewById(R.id.rl_base_tule);
		LinearLayout llh_tule = (LinearLayout) view.findViewById(R.id.llh_tule);
		Resources resources = view.getContext().getResources();

		switch (etat) {
		case NON_SELECTIONNE_ET_VALIDE:
			tuleNonSelectionne(resources, rl_base_tule);
			tuleValide(resources, llh_tule, tv_titre, tv_details);
			break;
		case SELECTIONNE_ET_VALIDE:
			tuleSelectione(resources, rl_base_tule);
			tuleValide(resources, llh_tule, tv_titre, tv_details);
			break;
		case SELECTIONNE_ET_NON_VALIDE:
			tuleSelectione(resources, rl_base_tule);
			tuleNonValide(resources, llh_tule, tv_titre, tv_details);
			break;
		case NON_SELECTIONNE_ET_NON_VALIDE:
			tuleNonSelectionne(resources, rl_base_tule);
			tuleNonValide(resources, llh_tule, tv_titre, tv_details);
			break;
		}
	}

	private void tuleValide(Resources resources, LinearLayout llh_tule,
			TextView tv_titre, TextView tv_details) {
		llh_tule.setBackground(resources
				.getDrawable(R.drawable.tule_plongeur_valide_background));
		tv_details.setTextColor(resources.getColor(R.color.blanc_primaire));
		tv_titre.setTextColor(resources.getColor(R.color.blanc_primaire));
	}

	private void tuleNonValide(Resources resources, LinearLayout llh_tule,
			TextView tv_titre, TextView tv_details) {
		llh_tule.setBackground(resources
				.getDrawable(R.drawable.tule_plongeur_nonvalide_background));
		tv_details.setTextColor(resources.getColor(R.color.noir_primaire));
		tv_titre.setTextColor(resources.getColor(R.color.noir_primaire));
	}

	private void tuleSelectione(Resources resources, RelativeLayout rl_base_tule) {
		rl_base_tule.setBackground(resources
				.getDrawable(R.drawable.tule_plongeur_selectionne));
	}

	private void tuleNonSelectionne(Resources resources,
			RelativeLayout rl_base_tule) {
		rl_base_tule.setBackground(resources
				.getDrawable(R.drawable.tule_plongeur_nonselectionne));
	}
}
