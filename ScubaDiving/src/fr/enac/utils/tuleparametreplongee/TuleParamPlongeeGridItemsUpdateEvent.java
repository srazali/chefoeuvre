package fr.enac.utils.tuleparametreplongee;

import java.util.ArrayList;
import java.util.EventObject;

/**
 * Event qui permet de dialoguer avec l'activité plongeurActivity Utilisé
 * TuleGridPlongeurOnItemClickListener
 * 
 * @author Jonathan Tolle
 */
public class TuleParamPlongeeGridItemsUpdateEvent extends EventObject {

	// ------------------------------------------------------------------//
	// Variables de classe //
	// ------------------------------------------------------------------//
	/**
	 * Liste des tules à envoyer à l'activité
	 */
	private final ArrayList<TuleParamPlongeeObjetAdapter> mtules;

	// private final ArrayList<TuleParamPlongeeObjetAdapter> mtulesOtherCate;

	// ------------------------------------------------------------------//
	// Constructeurs //
	// ------------------------------------------------------------------//
	/**
	 * Constructeur par défaut
	 * 
	 * @param source
	 */
	public TuleParamPlongeeGridItemsUpdateEvent(Object source,
			ArrayList<TuleParamPlongeeObjetAdapter> tules) {
		super(source);
		this.mtules = tules;
		// this.mtulesOtherCate = tulesOC;
	}

	// ------------------------------------------------------------------//
	// Getters & Setters //
	// ------------------------------------------------------------------//
	public ArrayList<TuleParamPlongeeObjetAdapter> getTules() {
		return mtules;
	}
}
