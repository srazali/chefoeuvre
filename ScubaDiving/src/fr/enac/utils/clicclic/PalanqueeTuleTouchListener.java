package fr.enac.utils.clicclic;

import android.view.View;
import android.view.View.OnClickListener;
import fr.enac.activities.PalanqueeActivity;

public class PalanqueeTuleTouchListener implements OnClickListener {

	private long idPlongeur;

	public PalanqueeTuleTouchListener(long idPlongeur) {
		this.idPlongeur = idPlongeur;
	}

	public long getIdPlongeur() {
		return idPlongeur;
	}

	@Override
	public void onClick(View v) {
		PalanqueeActivity.idDragPlongeur = idPlongeur;
	}
}
