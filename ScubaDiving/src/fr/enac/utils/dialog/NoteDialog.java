package fr.enac.utils.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.example.scubadiving.R;

import fr.enac.database.DatabaseHelper;
import fr.enac.database.PlongeeDAO;
import fr.enac.models.Plongee;

/**
 * Classe NouvellePlongeeDialog qui définit la fenêtre de dialog
 * qui est affichée lorsqu'une plongée non terminée existe.
 * 
 * @author Sophien Razali
 *
 */
public class NoteDialog extends DialogFragment {
	
	/**
	 * Méthode newInstance appelée lors de la création du dialog.
	 * 
	 * @param plongee la plongée en cours
	 * @return le dialogFragment
	 */
	public static NoteDialog newInstance(Plongee plongee) {
		NoteDialog noteDialog = new NoteDialog();
        Bundle args = new Bundle();
        args.putSerializable("plongee", plongee);
        noteDialog.setArguments(args);
        return noteDialog;
    }
	
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	final Plongee plongee = (Plongee) getArguments().getSerializable("plongee");    	
    	final EditText edt_note = new EditText(getActivity());    	
    	edt_note.setText(plongee.getmNote());
    	edt_note.setSelection(plongee.getmNote().length());
    	
        /* Utilisation de la classe Builder pour construire le dialog */
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_LIGHT);
        builder.setTitle(getResources().getString(R.string.alrtDial_note_titre))
        	   .setIcon(android.R.drawable.ic_dialog_alert)
        	   .setView(edt_note)
        	   .setPositiveButton(getResources().getString(R.string.alrtDial_note_bouton), new DialogInterface.OnClickListener() {
        		   public void onClick(DialogInterface dialog, int id) {
                	   DatabaseHelper databaseHelper = DatabaseHelper.getInstance(getActivity());
                	   PlongeeDAO plongeeDAO = new PlongeeDAO(databaseHelper.getWritableDatabase());
                	   plongee.setmNote(edt_note.getEditableText().toString());
                	   plongeeDAO.update(plongee);
                	   //cache le clavier
           			   InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
           			   imm.hideSoftInputFromWindow(edt_note.getWindowToken(), 0);
                   }
               });
        return builder.create();
    }
}

