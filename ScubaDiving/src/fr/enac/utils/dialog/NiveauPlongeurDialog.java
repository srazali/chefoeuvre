package fr.enac.utils.dialog;

import java.util.ArrayList;
import java.util.Arrays;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.scubadiving.R;

import fr.enac.activities.PlongeurActivity;
import fr.enac.lii.CDS.AptitudeAir;
import fr.enac.lii.CDS.AptitudeNitrox;
import fr.enac.lii.CDS.AptitudeTrimix;
import fr.enac.lii.CDS.Niveau;

/**
 * Classe NiveauPlongeurDialog qui définit la fenêtre de dialog qui est affichée
 * lorsque l'utilisateur souhaite changé les niveaux
 * 
 * @author Jonathan Tolle
 * 
 */
public class NiveauPlongeurDialog extends DialogFragment {

	private static final String LOG_TAG = NiveauPlongeurDialog.class.getName();

	public static int NB_CLICK_NIVEAUX_AUTORISES = 2;
	private static int NB_CLICK_APTITUDES_AUTORISEES = 2;
	private static int NB_CLICK_MELANGES_AUTORISES = 1;

	/**
	 * Méthode newInstance appelée lors de la création du dialog. Sert a passer
	 * des paramètres au dialog
	 * 
	 * @param niveauCDS
	 *            (niveau, aptitude, melange)
	 * @return
	 */
	public static NiveauPlongeurDialog newInstance(String[] niveauCDS) {
		NiveauPlongeurDialog dialog = new NiveauPlongeurDialog();
		Bundle args = new Bundle();
		args.putStringArray("niveauPlongeur", niveauCDS);
		dialog.setArguments(args);
		return dialog;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.onCreateDialog(savedInstanceState);

		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.plongeur_niveau_listeviews, null);

		/* récupération des valeurs pré-selectionnées */
		String niveauCDS[] = getArguments().getStringArray("niveauPlongeur");
		ArrayList<String> selectedNiveau = null;
		ArrayList<String> selectedAptitude = null;
		ArrayList<String> selectedMelange = null;
		if (!"Niveau".equals(niveauCDS[0])) {
			String[] aux = niveauCDS[0].split("[ \\+]");
			selectedNiveau = new ArrayList(Arrays.asList(aux));
		}
		if (!"Aptitude".equals(niveauCDS[1])) {
			String[] aux = niveauCDS[1].split("[ \\+]");
			selectedAptitude = new ArrayList(Arrays.asList(aux));
		}
		if (!"Mélange".equals(niveauCDS[2])) {
			String[] aux = niveauCDS[2].split("[ \\+]");
			selectedMelange = new ArrayList(Arrays.asList(aux));
		}

		/* Vues */
		ListView niveauListe = (ListView) view
				.findViewById(R.id.plongeur_niveau);
		ListView aptitudeListe = (ListView) view
				.findViewById(R.id.plongeur_aptitude);
		ListView melangeListe = (ListView) view
				.findViewById(R.id.plongeur_complement);

		/* Création des listes de niveaux, aptitudes, mélanges */
		String[] niveau = new String[Niveau.values().length];
		for (int i = 0; i < Niveau.values().length; i++) {
			niveau[i] = Niveau.values()[i].toString();
		}

		String[] aptitude = new String[AptitudeAir.values().length];
		for (int i = 0; i < AptitudeAir.values().length; i++) {
			aptitude[i] = AptitudeAir.values()[i].toString();
		}

		String[] melange = new String[AptitudeNitrox.values().length
				+ AptitudeTrimix.values().length];
		int i = 0;
		for (AptitudeNitrox apt : AptitudeNitrox.values()) {
			melange[i++] = apt.toString();
		}
		for (AptitudeTrimix apt : AptitudeTrimix.values()) {
			melange[i++] = apt.toString();
		}

		/* Affectation des tableaux aux listes */
		SpecialAdapter<String> niveauListeAdapter = new SpecialAdapter<String>(
				getActivity(), R.layout.plongeur_niveau_listeitem,
				R.id.tv_plongeur_niveau_item, niveau, selectedNiveau);
		niveauListe.setAdapter(niveauListeAdapter);
		niveauListe.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		final NiveauPlongeurOnItemClickListener niveauListener = new NiveauPlongeurOnItemClickListener(
				getActivity(), NB_CLICK_NIVEAUX_AUTORISES, selectedNiveau);
		niveauListe.setOnItemClickListener(niveauListener);

		SpecialAdapter<String> aptitudeListeAdapter = new SpecialAdapter<String>(
				getActivity(), R.layout.plongeur_niveau_listeitem,
				R.id.tv_plongeur_niveau_item, aptitude, selectedAptitude);
		aptitudeListe.setAdapter(aptitudeListeAdapter);
		aptitudeListe.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		final NiveauPlongeurOnItemClickListener aptitudeListener = new NiveauPlongeurOnItemClickListener(
				getActivity(), NB_CLICK_APTITUDES_AUTORISEES, selectedAptitude);
		aptitudeListe.setOnItemClickListener(aptitudeListener);

		SpecialAdapter<String> melangeListeAdapter = new SpecialAdapter<String>(
				getActivity(), R.layout.plongeur_niveau_listeitem,
				R.id.tv_plongeur_niveau_item, melange, selectedMelange);
		melangeListe.setAdapter(melangeListeAdapter);
		melangeListe.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		final NiveauPlongeurOnItemClickListener melangeListener = new NiveauPlongeurOnItemClickListener(
				getActivity(), NB_CLICK_MELANGES_AUTORISES, selectedMelange);
		melangeListe.setOnItemClickListener(melangeListener);

		/* Utilisation de la classe Builder pour construire le dialog */
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),
				AlertDialog.THEME_HOLO_DARK);
		builder.setTitle(
				getResources().getString(R.string.act_plongeur_niveauPicker))
				.setIcon(android.R.drawable.ic_dialog_dialer)
				.setView(view)
				.setPositiveButton(
						getResources().getString(
								R.string.btn_plongeur_niveauSpinners_valider),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								((PlongeurActivity) getActivity())
										.changeNiveauPickerFormulaire(
												formatListeToTextView(niveauListener
														.getNiveaux()),
												formatListeToTextView(aptitudeListener
														.getNiveaux()),
												formatListeToTextView(melangeListener
														.getNiveaux()));
							}
						})
				.setNegativeButton(
						getResources().getString(
								R.string.btn_plongeur_niveauSpinners_annuler),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// on ne fait rien
							}
						});
		return builder.create();
	}

	/**
	 * Concaténation de l'arraylist en string pour être afficher dans les
	 * textViews du niveauPicker
	 * 
	 * @param strings
	 * @return
	 */
	private String formatListeToTextView(ArrayList<String> strings) {
		if (strings.size() == 0)
			return "";
		else if (strings.size() == 1)
			return strings.get(0);
		else {
			return strings.get(0)
					+ ""
					+ getActivity().getResources().getString(
							R.string.tv_plongeur_details_plus) + ""
					+ strings.get(1);
		}
	}
}

/**
 * Listener de la listevieew Niveau
 * 
 * @author Jonathan Tolle
 * 
 */
class NiveauPlongeurOnItemClickListener implements OnItemClickListener {
	private static final String LOG_TAG = NiveauPlongeurOnItemClickListener.class
			.getName();
	private ArrayList<String> niveauxSelectionnes;
	private int limite;
	private Context context;

	public NiveauPlongeurOnItemClickListener(Context context, int limite,
			ArrayList<String> niveauxSelectionnes) {
		this.context = context;
		this.niveauxSelectionnes = (niveauxSelectionnes != null) ? niveauxSelectionnes
				: new ArrayList<String>();
		this.limite = limite;
	}

	public ArrayList<String> getNiveaux() {
		return niveauxSelectionnes;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position,
			long arg3) {

		TextView text = (TextView) view
				.findViewById(R.id.tv_plongeur_niveau_item);
		String item = text.getText().toString();

		Log.d(LOG_TAG, "Nombre de niveaux déjà sélectionné :"
				+ niveauxSelectionnes.size());

		if (niveauxSelectionnes.size() == 0) { // O item
			Log.d(LOG_TAG, "Ajout niveau : [" + item + "]");
			view.setBackgroundColor(context.getResources().getColor(
					R.color.rouge_2));
			niveauxSelectionnes.add(item);
		} else { // 1 ou plus item
			boolean dejaSelectionne = false;
			for (int i = 0; i < niveauxSelectionnes.size(); i++) {
				if (item.compareTo(niveauxSelectionnes.get(i)) == 0) {
					Log.d(LOG_TAG, "Item déjà selectionné");
					view.setBackgroundColor(context.getResources().getColor(
							android.R.color.transparent));
					niveauxSelectionnes.remove(i);
					dejaSelectionne = true;
				}
			}
			/*
			 * si on a atteint la limite d'item sélectionné et que l'item ne se
			 * trouver pas dans les items déjà sélectionné
			 */
			if (!dejaSelectionne && niveauxSelectionnes.size() == limite - 1) {
				Log.d(LOG_TAG, "Ajout niveau : [" + item + "]");
				view.setBackgroundColor(context.getResources().getColor(
						R.color.rouge_2));
				niveauxSelectionnes.add(item);
			}
		}
	}
}
