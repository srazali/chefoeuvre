package fr.enac.utils.dialog;

import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import com.example.scubadiving.R;

import fr.enac.activities.AccueilActivity;
import fr.enac.database.DatabaseHelper;
import fr.enac.database.PlongeeDAO;
import fr.enac.models.Plongee;
import fr.enac.models.PlongeeEtatEnum;

/**
 * Classe NouvellePlongeeDialog qui définit la fenêtre de dialog
 * qui est affichée lorsqu'une plongée non terminée existe.
 * 
 * @author Sophien Razali
 *
 */
public class DupliquerPlongeeDialog extends DialogFragment {
	
	private DatabaseHelper mDatabaseHelper;
	private PlongeeDAO mPlongeeDAO;
	
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	
    	mDatabaseHelper = DatabaseHelper.getInstance(this.getActivity());
    	mPlongeeDAO = new PlongeeDAO(mDatabaseHelper.getWritableDatabase());
    	
    	List<Plongee> listePlongeesTerminees = mPlongeeDAO.selectPlongeesAvecEtat(PlongeeEtatEnum.TERMINEE);
    	final ArrayAdapter<Plongee> arrayAdapter = new ArrayAdapter<Plongee>(getActivity(), android.R.layout.select_dialog_singlechoice, listePlongeesTerminees);
    	     
        /* Utilisation de la classe Builder pour construire le dialog */
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_LIGHT);
        builder.setTitle(getResources().getString(R.string.alrtDial_accueil_dupliquerPlongeeTitre))
        	   .setIcon(android.R.drawable.ic_dialog_info)
        	   .setAdapter(arrayAdapter, new OnClickListener() {				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					((AccueilActivity) getActivity()).dupliquerPlongee(mPlongeeDAO.select(arrayAdapter.getItem(which).getmId()));
				}
        	   })
               .setNegativeButton(getResources().getString(R.string.alrtDial_accueil_dupliquerPlongeeAnuler), null);
        
        return builder.create();
    }
}

