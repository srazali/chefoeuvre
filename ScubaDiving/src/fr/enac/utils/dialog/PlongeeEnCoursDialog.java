package fr.enac.utils.dialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.example.scubadiving.R;

import fr.enac.activities.AccueilActivity;
import fr.enac.models.Plongee;

/**
 * Classe NouvellePlongeeDialog qui définit la fenêtre de dialog
 * qui est affichée lorsqu'une plongée non terminée existe.
 * 
 * @author Sophien Razali
 *
 */
public class PlongeeEnCoursDialog extends DialogFragment {
	
	/**
	 * Méthode newInstance appelée lors de la création du dialog.
	 * 
	 * @param plongee la plongée en cours
	 * @return le dialogFragment
	 */
	public static PlongeeEnCoursDialog newInstance(Plongee plongee) {
    	PlongeeEnCoursDialog nouvellePlongeeDialog = new PlongeeEnCoursDialog();
        Bundle args = new Bundle();
        args.putSerializable("plongee_en_cours", plongee);
        nouvellePlongeeDialog.setArguments(args);
        return nouvellePlongeeDialog;
    }
	
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	Plongee plongee = (Plongee) getArguments().getSerializable("plongee_en_cours");
    	
    	/* Convertion de la date en string */
    	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTimeInMillis(plongee.getmDatePlongee());
    	     
        /* Utilisation de la classe Builder pour construire le dialog */
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), AlertDialog.THEME_HOLO_LIGHT);
        builder.setTitle(getResources().getString(R.string.alrtDial_accueil_titre))
        	   .setIcon(android.R.drawable.ic_dialog_alert)
        	   .setMessage(getResources().getString(R.string.alrtDial_accueil_messagePartie1) + " " + formatter.format(calendar.getTime()) + " " + getResources().getString(R.string.alrtDial_accueil_messagePartie2) + " " + plongee.getmLieu() + " " + getResources().getString(R.string.alrtDial_accueil_messagePartie3))
               .setPositiveButton(getResources().getString(R.string.alrtDial_accueil_continuerPlongee), new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   ((AccueilActivity) getActivity()).continuerPlongeeEnCours();
                   }
               })
               .setNegativeButton(getResources().getString(R.string.alrtDial_accueil_fermerPlongee), new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   ((AccueilActivity) getActivity()).fermerPlongeeEnCours(null);
                   }
               });
        
        return builder.create();
    }
}

