package fr.enac.utils.dialog;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.scubadiving.R;

public class SpecialAdapter<T> extends ArrayAdapter<T> {

	private ArrayList<T> alreadySelectedObjects;

	public SpecialAdapter(Context context, int resource,
			int textViewResourceId, T[] objects, ArrayList<T> selectedObjects) {
		super(context, resource, textViewResourceId, objects);
		this.alreadySelectedObjects = selectedObjects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = super.getView(position, convertView, parent);
		if (isAlreadySelectedObject(position)) {
			view.setBackgroundColor(super.getContext().getResources()
					.getColor(R.color.rouge_2));
		} else {
			view.setBackgroundColor(super.getContext().getResources()
					.getColor(android.R.color.transparent));
		}
		return view;
	}

	private boolean isAlreadySelectedObject(int position) {
		if (alreadySelectedObjects == null) {
			return false;
		}

		for (T obj : alreadySelectedObjects) {
			if (super.getItem(position).equals(obj)) {
				return true;
			}
		}
		return false;
	}
}