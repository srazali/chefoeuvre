package fr.enac.utils.selecteurfichier;

import java.util.ArrayList;

import com.example.scubadiving.R;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

/**
 * Permet d'afficher les FichierObjetAdapter dans le layout selecteurfichier_objetliste
 * @author Jonathan Tolle
 */
public class SelecteurFichierListeAdapter extends ArrayAdapter<SelecteurFichierObjetAdapter> implements ListAdapter{

	private static final String LOG_TAG = SelecteurFichierListeAdapter.class.getName();

	private Context mContext;
	private ArrayList<SelecteurFichierObjetAdapter> mListe;
	private int mResource;

	/**
	 * Constructeur
	 * @param context
	 * @param resource
	 * @param list
	 */
	public SelecteurFichierListeAdapter(Context context, int resource,
			ArrayList<SelecteurFichierObjetAdapter> list) {
		super(context, resource, list);
		this.mContext = context;
		this.mListe = list;
		this.mResource = resource;
	}

	/**
	 * Affichage de la liste
	 */
	@Override
	public View getView(int position, View convertView,
			ViewGroup parent) {

		// On r�cup�re la vue
		if(convertView==null){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(mResource, parent, false);
		}

		// On affiche les donn�es
		SelecteurFichierObjetAdapter item = mListe.get(position);
		Log.d(LOG_TAG,"FichierObjetAdapter : "+item.getmFile().getName());

		if (item != null) {
			ImageView iconeFichier = (ImageView) convertView.findViewById(R.id.imv_icone);
			TextView nomFichier = (TextView) convertView.findViewById(R.id.tv_nom);

			// L'icone
			if (iconeFichier != null) {
				iconeFichier.setImageDrawable(item.getmDrawable());
			}

			// Le nom du fichier
			if (nomFichier != null) {
				nomFichier.setText(item.getmFile().getName());
			}
		}
		return convertView;
	}


}
