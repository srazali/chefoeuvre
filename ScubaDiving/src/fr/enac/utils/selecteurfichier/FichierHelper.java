package fr.enac.utils.selecteurfichier;

import java.io.File;
import java.util.ArrayList;

import android.util.Log;

/**
 * Classe utilis�e par le FichierListeAdapter pour r�cup�rer la liste des fichiers
 * @author Jonathan Tolle
 */
public class FichierHelper {
	/**
	 * LOG TAG
	 */
	private static final String LOG_TAG = FichierHelper.class.getName();

	/**
	 * R�cupere tous les fichiers et dossiers d'un repertoire
	 * @param dir : repertoire 
	 * @return liste des fichiers et dossiers du repertoire
	 */
	public static ArrayList<File> GetFichierDuDossier(String dir) {
		ArrayList<File> list = new ArrayList<File>();
		final File pathDir = new File(dir);
		Log.d(LOG_TAG,"Directory : "+dir);
		
		// On liste tous les dossiers pr�sents dans le dir
		final File[] files = pathDir.listFiles();
		
		if (files != null) {
			
			Log.d(LOG_TAG,"Liste des fichiers :");
			for (File file : files){
				list.add(file);
				Log.d(LOG_TAG,"\t"+file.getName());
			}
		}
		return list;
	}


}
