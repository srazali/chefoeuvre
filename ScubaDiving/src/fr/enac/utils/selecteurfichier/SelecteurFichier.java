package fr.enac.utils.selecteurfichier;

import java.io.File;
import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.example.scubadiving.R;

import fr.enac.activities.PlongeurActivity;
import fr.enac.utils.DossiersApplication;
/**
 * Affiche la popup pour choisir le fichier � importer et pour naviguer entre les dossiers
 * @author Jonathan Tolle
 */
public class SelecteurFichier extends ListActivity {
	/**
	 * LOG TAG
	 */
	private final static String LOG_TAG = SelecteurFichier.class.getName();

	/**
	 * Repertoire précédent
	 */
	private String mCheminPrecedent;

	/**
	 * Fichier a envoyé
	 */
	private String fichierAEnvoye;

	//------------------------------------------------------------------//
	//                     Affichage de l'activité                      //
	//------------------------------------------------------------------//
	/**
	 * Création de l'activité
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.selecteurfichier_liste);

		// On créé le dossier s'il n'existe pas
		File dir = new File(DossiersApplication.DOSSIER_IMPORT);
		if(dir.exists() && dir.isDirectory()){
			Log.d(LOG_TAG,"Création du dossier : "+DossiersApplication.DOSSIER_IMPORT);			
			dir.mkdir();
		}
		mCheminPrecedent = dir.getParent();
		afficheFichierDansLaListe(DossiersApplication.DOSSIER_IMPORT);
	}

	/**
	 * Si l'utilisateur clic a l'exterieur alors on annule l'importation
	 */
	@Override
	protected void onStop(){
		super.onStop();
		// si on lève une exception alors on arrete l'activité et on renvoi ""
		if(fichierAEnvoye!=null && !fichierAEnvoye.isEmpty()){
			Intent intent = new Intent(this, PlongeurActivity.class);
			setResult(RESULT_CANCELED, intent);
			Log.d(LOG_TAG,"on stop");
		}
	}

	/**
	 * Affiche les fichiers et dossiers du chemin
	 * A modifier pour faire traverser l'exception jusqu'à l'activité
	 * @param chemin
	 */
	private void afficheFichierDansLaListe(String chemin) {

		// On récupère la liste des fichiers et dossiers
		ArrayList<SelecteurFichierObjetAdapter> files;

		try {			
			files = getFichierObjetAdapters(chemin);
			SelecteurFichierListeAdapter adapter = new SelecteurFichierListeAdapter(this,
					R.layout.selecteurfichier_objetliste,
					files);
			setListAdapter(adapter);			
		} catch (Exception e) {
			Log.d(LOG_TAG,"Exception : "+e.getMessage());
			this.finish();		
		}		
	}



	//------------------------------------------------------------------//
	//                          Evenements                              //
	//------------------------------------------------------------------//
	/**
	 * Gestion du click sur les évéments de la liste
	 */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {           
		super.onListItemClick(l, v, position, id);  

		SelecteurFichierObjetAdapter  fileObject = (SelecteurFichierObjetAdapter) l.getItemAtPosition(position);   
		File file = fileObject.getmFile();
		Log.d(LOG_TAG,"Click : \n  Position :"+position+"  \n  ListItem : [" +file.getName()+"]");

		// Si le premier �l�ment selectionn� est celui qui remonte dans la hi�rachie
		if(file.getName()==".."){					
			File parent = new File(mCheminPrecedent);	
			mCheminPrecedent = parent.getParent();
			afficheFichierDansLaListe(parent.getAbsolutePath());			
			Log.d(LOG_TAG,"Parent : "+parent.getAbsolutePath());

		}

		// Si c'est un dossier l'utilisateur peut naviguer dans les dossier
		if(file.isDirectory() && file.getName()!=".."){
			mCheminPrecedent = file.getParent();
			afficheFichierDansLaListe(file.getAbsolutePath());
		}

		// Si ce n'est pas un repertoire, alors on envoi le document CSV à PlongeurActivity
		if(!file.isDirectory()){	
			fichierAEnvoye = file.getAbsolutePath();
			Intent intent = new Intent(this, PlongeurActivity.class);
			intent.putExtra("fichier",fichierAEnvoye);
			setResult(RESULT_OK, intent);
			this.finish();
		}
	}

	//------------------------------------------------------------------//
	//                          Méthodes                                //
	//------------------------------------------------------------------//
	/**
	 * Recup�re les fichiers CSV et les dossiers
	 * @return liste de FichierObjetAdapter
	 */
	private ArrayList<SelecteurFichierObjetAdapter> getFichierObjetAdapters(String chemin) throws Exception{

		// si la carte SD n'est pas encore prete
		if(!Environment.getExternalStorageState().equals("mounted"))
			throw new Exception(getResources().getString(R.string.exc_externalStorageUnmounted));

		// initialisation des variables
		ArrayList<File> fichiers = FichierHelper.GetFichierDuDossier(chemin);
		ArrayList<SelecteurFichierObjetAdapter> fichiersFormates = new ArrayList<SelecteurFichierObjetAdapter>();
		Drawable ic_csv = getResources().getDrawable(R.drawable.ic_csv);
		Drawable ic_dossier = getResources().getDrawable(R.drawable.ic_dossier);

		// traitement des fichiers
		if(mCheminPrecedent!=null){
			fichiersFormates.add(new SelecteurFichierObjetAdapter(new File(".."), ic_dossier));
		}
		for(File fichier : fichiers){

			Log.d(LOG_TAG,"Fichier : "+fichier.getName());

			// pour les dossier, ont ajout l'icone
			if(fichier.isDirectory()){
				fichiersFormates.add(new SelecteurFichierObjetAdapter(fichier, ic_dossier));
			}

			// pour les fichiers CSV, ont ajout l'icone
			if(!fichier.isDirectory() && GetExtension(fichier.getAbsolutePath()).toLowerCase().equals("csv")){
				fichiersFormates.add(new SelecteurFichierObjetAdapter(fichier, ic_csv));
			}

		}
		return fichiersFormates;
	}

	/**
	 * Récupère l'extension du fichier
	 * @param fichier
	 * @return extension
	 */
	private static String GetExtension(String fichier)
	{       
		String ext = fichier.substring((fichier.lastIndexOf(".") + 1), fichier.length());
		return ext;
	}
}

