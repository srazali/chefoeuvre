package fr.enac.utils.selecteurfichier;

import java.io.File;

import android.graphics.drawable.Drawable;

/**
 * Classe utilis�e dans le FichierListeAdapter
 * @author Jonathan Tolle
 */
public class SelecteurFichierObjetAdapter {
	private File mFile;
	private Drawable mDrawable;		
	
	public SelecteurFichierObjetAdapter(File file, Drawable drawable) {
		super();
		this.mFile = file;
		this.mDrawable = drawable;
	}
	
	public File getmFile() {
		return mFile;
	}

	public Drawable getmDrawable() {
		return mDrawable;
	}	
}
