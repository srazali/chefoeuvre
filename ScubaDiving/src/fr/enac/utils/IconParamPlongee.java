package fr.enac.utils;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.scubadiving.R;

public class IconParamPlongee {

	private static Bitmap iconePDS;
	private static Bitmap iconeOxy;
	private static Bitmap iconeTDS;
	private static Bitmap iconeTelVHF;
	private static Bitmap iconeFicEv;
	private static Bitmap iconeBloG;
	private static Bitmap iconeRapPl;
	private static Bitmap iconeTabNot;
	private static Bitmap iconeTabDec;
	private static Bitmap iconeTalWal;
	private static Bitmap iconePavAlp;
	private static Bitmap iconeBouee;
	private static Bitmap iconeLigVie;
	private static Bitmap iconeLampEcla;
	private static Bitmap iconeCyalu;
	private static Bitmap iconeCorpsMort;
	private static Bitmap iconeGueuse;
	private static Bitmap iconeDefaut;

	/**
	 * Retourne l'icone en fonction du nom du parametre
	 * 
	 * @param 
	 * @return
	 */
	public static Bitmap getImageFromParamPlongee(Resources res, String nomParam) {
		if (nomParam.equals("Plan de secours")) {
			if (iconePDS == null) {
				iconePDS = BitmapFactory.decodeResource(res,
						R.drawable.ic_pds);
			}
			return iconePDS;
		} else if (nomParam.equals("Oxygenothérapie")) {
			if (iconeOxy == null) {
				iconeOxy = BitmapFactory.decodeResource(res,
						R.drawable.ic_oxygen);
			}
			return iconeOxy;
		} else if (nomParam.equals("Trousse de secours")) {
			if (iconeTDS == null) {
				iconeTDS = BitmapFactory.decodeResource(res,
						R.drawable.ic_trousse);
			}
			return iconeTDS;
		} else if (nomParam.equals("Téléphone/VHF")) {
			if (iconeTelVHF == null) {
				iconeTelVHF = BitmapFactory.decodeResource(res,
						R.drawable.ic_telvhf);
			}
			return iconeTelVHF;
		} else if (nomParam.equals("Fiches évaluation")) {
			if (iconeFicEv == null) {
				iconeFicEv = BitmapFactory.decodeResource(res,
						R.drawable.ic_ficeva);
			}
			return iconeFicEv;
		} else if (nomParam.equals("Bloc Gréée")) {
			if (iconeBloG == null) {
				iconeBloG = BitmapFactory.decodeResource(res,
						R.drawable.ic_blogre);
			}
			return iconeBloG;
		} else if (nomParam.equals("Rappel plongeurs")) {
			if (iconeRapPl == null) {
				iconeRapPl = BitmapFactory.decodeResource(res,
						R.drawable.ic_rapplo);
			}
			return iconeRapPl;
		} else if (nomParam.equals("Tab. notation")) {
			if (iconeTabNot == null) {
				iconeTabNot = BitmapFactory.decodeResource(res,
						R.drawable.ic_tabnot);
			}
			return iconeTabNot;
		} else if (nomParam.equals("Tab. décompression")) {
			if (iconeTabDec == null) {
				iconeTabDec = BitmapFactory.decodeResource(res,
						R.drawable.ic_tabdec);
			}
			return iconeTabDec;
		} else if (nomParam.equals("Talkie/Walkie")) {
			if (iconeTalWal == null) {
				iconeTalWal = BitmapFactory.decodeResource(res,
						R.drawable.ic_talkie);
			}
			return iconeTalWal;
		} else if (nomParam.equals("Pavillon Alpha")) {
			if (iconePavAlp == null) {
				iconePavAlp = BitmapFactory.decodeResource(res,
						R.drawable.ic_alpha);
			}
			return iconePavAlp;
		} else if (nomParam.equals("Bouée")) {
			if (iconeBouee == null) {
				iconeBouee = BitmapFactory.decodeResource(res,
						R.drawable.ic_bouee);
			}
			return iconeBouee;
		} else if (nomParam.equals("Ligne de vie")) {
			if (iconeLigVie == null) {
				iconeLigVie = BitmapFactory.decodeResource(res,
						R.drawable.ic_ligvie);
			}
			return iconeLigVie;
		} else if (nomParam.equals("Lampe à éclats")) {
			if (iconeLampEcla == null) {
				iconeLampEcla = BitmapFactory.decodeResource(res,
						R.drawable.ic_lampeclat);
			}
			return iconeLampEcla;
		} else if (nomParam.equals("Cyalumes")) {
			if (iconeCyalu == null) {
				iconeCyalu = BitmapFactory.decodeResource(res,
						R.drawable.ic_cyalume);
			}
			return iconeCyalu;
		} else if (nomParam.equals("Corps mort")) {
			if (iconeCorpsMort == null) {
				iconeCorpsMort = BitmapFactory.decodeResource(res,
						R.drawable.ic_cormor);
			}
			return iconeCorpsMort;
		} else if (nomParam.equals("Gueuse")) {
			if (iconeGueuse == null) {
				iconeGueuse = BitmapFactory.decodeResource(res,
						R.drawable.ic_cormor);
			}
			return iconeGueuse;
		} else {
			if (iconeDefaut == null) {
				iconeDefaut = BitmapFactory.decodeResource(res,
						R.drawable.ic_defaut);
			}
			return iconeDefaut;
		}
	}
}
