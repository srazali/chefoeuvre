package fr.enac.utils.tuletour;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import fr.enac.activities.TourActivity;

/**
 * Listener utiliser lorsqu'on clic(sélectionée) une tule dans un tour
 * @author Jonathan Tolle
 *
 */
public class TuleTourOnClickSelectionListener implements OnClickListener{
	private static final String LOG_TAG = TuleTourOnClickSelectionListener.class.getName();

	private TuleTour tule;

	public TuleTourOnClickSelectionListener(TuleTour tule) {
		this.tule = tule;
	}

	@Override
	public void onClick(View v) {
		switch(tule.getEtatCourant()){
		case ATTENTE:
			Log.d(LOG_TAG,"Clic de sélection : palanqueePosition "+tule.getPalanqueePosition()+" (tour n°"+(tule.getTourPosition()+1)+")");
			TourActivity.tuleSelectionnee = tule;
		case ENCOURS:
			break;
		case FINIE:
			break;
		}

	}
}
