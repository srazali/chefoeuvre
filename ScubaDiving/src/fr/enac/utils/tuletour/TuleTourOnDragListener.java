package fr.enac.utils.tuletour;

import fr.enac.activities.TourActivity;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.view.View.OnDragListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * Listener utiliser lorsqu'on drop une tule dans un tour
 * @author Jonathan Tolle
 *
 */
public class TuleTourOnDragListener implements OnDragListener{
	private static final String LOG_TAG = TuleTourOnDragListener.class.getName();

	private int tourPosition;
	private TourActivity activity;

	public TuleTourOnDragListener(int tourPosition, TourActivity activity) {
		this.tourPosition = tourPosition;
		this.activity = activity;
	}

	@Override
	public boolean onDrag(View v, DragEvent event) {
		int action = event.getAction();
		switch (action) {
		case DragEvent.ACTION_DRAG_STARTED:
			break;
		case DragEvent.ACTION_DRAG_ENTERED:
			Log.d(LOG_TAG, "Entered palanqueePosition "+TourActivity.tuleDragged.getPalanqueePosition()+" (tour n°"+(TourActivity.tuleDragged.getTourPosition()+1)+") dans tour n°"+(tourPosition+1));
			break;
		case DragEvent.ACTION_DRAG_EXITED:
			break;
		case DragEvent.ACTION_DROP:
			TuleTour tule = TourActivity.tuleDragged;
			if (TourActivity.tuleDragged != null) {
				Log.d(LOG_TAG, "Drop palanqueePosition "+tule.getPalanqueePosition()+" (tour n°"+(tule.getTourPosition()+1)+") dans tour n°"+(tourPosition+1));	
				activity.setTourToPalanquee(tule.getTourPosition(), tule.getPalanqueePosition(), tourPosition);
			}
			break;
		case DragEvent.ACTION_DRAG_ENDED:
			Log.d(LOG_TAG, "Drag ended");
			if(TourActivity.tuleDragged != null){ // évite aux autres dragListener de faire des updates en cascade
				activity.updateTuleTours();
				TourActivity.tuleDragged = null;
			}

		default:
			break;
		}
		return true;
	}

}
