package fr.enac.utils.tuletour;

/**
 * Interface des listeners
 * @author Jonathan Tolle
 */
public interface TuleTourUpdateListener {
	public void tuleTourUpdated(TuleTourUpdateEvent event);
}
