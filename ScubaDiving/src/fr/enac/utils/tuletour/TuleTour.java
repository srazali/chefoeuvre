package fr.enac.utils.tuletour;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.scubadiving.R;

import fr.enac.lii.CDS.TypePlongee;
import fr.enac.models.Palanquee;
import fr.enac.utils.tuleplongeur.TulePlongeurGridItemsUpdateEvent;
import fr.enac.utils.tuleplongeur.TulePlongeurGridItemsUpdateListener;

/**
 * Custom compound view pour afficher les palanquées dans la view tour
 * La custom view permet :
 * 	- de visualiser dans une palanquée est sélectionnée (pour le clic & clic entre les tours)
 *  - de visualiser la progression d'une palanquée (temps restant d'immersion)
 *  - de changer d'état entre : attente, immersion, fini
 *  - d'interagir en fonctions des états
 */
public class TuleTour extends RelativeLayout{
	private static int TIMER_MINUTE = 60000;
	private static final String LOG_TAG = TuleTour.class.getName();

	// ------------------------------------------------------------------//
	// Variables de classe //
	// ------------------------------------------------------------------//
	private final RelativeLayout attenteLayout;
	private final RelativeLayout immersionLayout;
	private final RelativeLayout finiLayout;

	/* FIXME : Revoir le xml de tuletour pour éviter la redondance des plongeurs pour chaque mode */
	private final TextView tv_attente_encadrant;
	private final TextView tv_attente_plongeur1;
	private final TextView tv_attente_plongeur2;
	private final TextView tv_attente_plongeur3;
	private final TextView tv_attente_plongeur4;
	private final TextView tv_attente_serrefile;	

	private final TextView tv_immersion_encadrant;
	private final TextView tv_immersion_plongeur1;
	//private final TextView tv_immersion_plongeur2;
	//private final TextView tv_immersion_plongeur3;
	//private final TextView tv_immersion_plongeur4;
	//private final TextView tv_immersion_serrefile;	

	private final TextView tv_fini_encadrant;
	private final TextView tv_fini_plongeur1;
	//private final TextView tv_fini_plongeur2;
	//private final TextView tv_fini_plongeur3;
	//private final TextView tv_fini_plongeur4;
	//private final TextView tv_fini_serrefile;	

	private final TextView tv_attente_nombrePlongeurs;
	private final TextView tv_attente_tempsImmersion;
	private final TextView tv_immersion_tempsRestant;
	private final TextView tv_fini_tempsFinal;
	private final ProgressBar pb_immersion_progressBar;

	private int tempsRestant; 		// en min - compte à rebours
	private int tempsImmersion;		// en min - temps initial
	private int tourPosition; 		// correspond à l'index de tour dans la liste getmPlongee().getmListTours()
	private int palanqueePosition; 	// correspond à l'index de palanquee dans la liste getmPlongee().getmListTours().get(tourPosition).getmListPalanquee()

	private TypePlongee typePlongee;

	private Palanquee.Etat etatCourant;

	/**
	 * Listeners : l'activité tour est listener de chaque TuleTour pour savoir quand
	 * la palanquée plonge ou non dans le but de mettre à jours la BDD et le model
	 */
	private ArrayList<TuleTourUpdateListener> listeners;
	private Timer timer;
	private final Handler mHandler;

	// ------------------------------------------------------------------//
	// Constructeurs //
	// ------------------------------------------------------------------//

	/**
	 * Constructeur : utilisé lors de la création via la programmation
	 * @param context
	 * @param plongeurs
	 * @param type
	 * @param tempsImmersion
	 */
	public TuleTour(Context context, int tourPosition, int palanqueePosition, String[] plongeurs, TypePlongee type, Palanquee.Etat etat, int tempsImmersion) {
		super(context);
		/* Récupération du layout général */
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		addView(inflater.inflate(R.layout.tule_tour, null));

		/* Création liste listener */
		listeners = new ArrayList<TuleTourUpdateListener>();		
		mHandler = new Handler();
		timer = new Timer();

		/* Récupération des vues */
		attenteLayout= (RelativeLayout) findViewById(R.id.tuletour_attente);	
		immersionLayout = (RelativeLayout) findViewById(R.id.tuletour_immersion);
		finiLayout = (RelativeLayout) findViewById(R.id.tuletour_fini);

		tv_attente_encadrant = ((TextView) findViewById(R.id.tuletour_attente_encadrant));
		tv_attente_plongeur1 = ((TextView) findViewById(R.id.tuletour_attente_plongeur1));
		tv_attente_plongeur2 = ((TextView) findViewById(R.id.tuletour_attente_plongeur2));
		tv_attente_plongeur3 = ((TextView) findViewById(R.id.tuletour_attente_plongeur3));
		tv_attente_plongeur4 = ((TextView) findViewById(R.id.tuletour_attente_plongeur4));
		tv_attente_serrefile = ((TextView) findViewById(R.id.tuletour_attente_serrefil));		

		tv_immersion_encadrant = ((TextView) findViewById(R.id.tuletour_immersion_encadrant));
		tv_immersion_plongeur1 = ((TextView) findViewById(R.id.tuletour_immersion_plongeur1));
		//tv_immersion_plongeur2 = ((TextView) findViewById(R.id.tuletour_immersion_plongeur2));
		//tv_immersion_plongeur3 = ((TextView) findViewById(R.id.tuletour_immersion_plongeur3));
		//tv_immersion_plongeur4 = ((TextView) findViewById(R.id.tuletour_immersion_plongeur4));
		//tv_immersion_serrefile = ((TextView) findViewById(R.id.tuletour_immersion_serrefil));	

		tv_fini_encadrant = ((TextView) findViewById(R.id.tuletour_fini_encadrant));
		tv_fini_plongeur1 = ((TextView) findViewById(R.id.tuletour_fini_plongeur1));
		//tv_fini_plongeur2 = ((TextView) findViewById(R.id.tuletour_fini_plongeur2));
		//tv_fini_plongeur3 = ((TextView) findViewById(R.id.tuletour_fini_plongeur3));
		//tv_fini_plongeur4 = ((TextView) findViewById(R.id.tuletour_fini_plongeur4));
		//tv_fini_serrefile = ((TextView) findViewById(R.id.tuletour_fini_serrefil));	

		tv_attente_nombrePlongeurs = ((TextView) findViewById(R.id.tuletour_attente_nombrePlongeurs));
		tv_attente_tempsImmersion = ((TextView) findViewById(R.id.tuletour_attente_tempsImmersion));
		tv_immersion_tempsRestant = ((TextView) findViewById(R.id.tuletour_immersion_tempsRestant));
		pb_immersion_progressBar = ((ProgressBar) findViewById(R.id.tuletour_immersion_progressBar));
		tv_fini_tempsFinal = ((TextView) findViewById(R.id.tuletour_fini_tempsFinal)); 	


		/* initialisation du temps de plongée */
		this.palanqueePosition = palanqueePosition;
		this.tourPosition = tourPosition;
		this.tempsImmersion = tempsImmersion;
		this.etatCourant = etat;

		/* Affectation du temps d'immersion */
		tv_attente_tempsImmersion.setText(tempsImmersion+"min");

		/* affectation des listeners */
		attenteLayout.setOnLongClickListener(new TuleTourOnLongClickListener(this));
		attenteLayout.setOnClickListener(new TuleTourOnClickSelectionListener(this));

		((ImageView) findViewById(R.id.imv_tour_play)).setOnTouchListener(new OnTouchListener() {

			// ------------------------------------------------------------------//
			// Evénement PLAY //
			// ------------------------------------------------------------------//	
			@Override
			public boolean onTouch(View v, MotionEvent event) {	
				Log.d("DEBUG", "On touch");
				switch (etatCourant) {
				case ATTENTE:					
					etatCourant = Palanquee.Etat.ENCOURS;
					actions(etatCourant);
					//Toast feedback = Toast.makeText(getContext(), getResources().getString(R.string.toast_tour_debutPlongee), Toast.LENGTH_SHORT);
					//feedback.show();
					// on notifie le changement de l'etat à tourActivity
					notifierListener();
					break;
				case ENCOURS:
					// Interdit
					break;
				case FINIE:
					// Interdit
					break;
				default:
					// Interdit
					break;
				}
				return true;
			}
		});

		((ImageView) findViewById(R.id.imv_tour_stop)).setOnTouchListener(new OnTouchListener() {

			// ------------------------------------------------------------------//
			// Evénement STOP //
			// ------------------------------------------------------------------//	
			@Override
			public boolean onTouch(View v, MotionEvent event) {			
				switch (etatCourant) {
				case ATTENTE:					
					// Interdit
					break;					
				case ENCOURS:			
					etatCourant = Palanquee.Etat.FINIE;
					actions(etatCourant);
					//Toast feedback = Toast.makeText(getContext(), getResources().getString(R.string.toast_tour_finPlongee), Toast.LENGTH_SHORT);
					//feedback.show();
					// on notifie le changement de l'etat à tourActivity
					notifierListener();
					break;
				case FINIE:
					// Interdit
					break;
				default:
					// Interdit
					break;
				}
				return true;
			}
		});

		((ImageView) findViewById(R.id.imv_tour_replay)).setOnTouchListener(new OnTouchListener() {

			// ------------------------------------------------------------------//
			// Evénement REPLAY //
			// ------------------------------------------------------------------//	
			@Override
			public boolean onTouch(View v, MotionEvent event) {			
				switch (etatCourant) {
				case ATTENTE:					
					// Interdit
					break;					
				case ENCOURS:
					// Interdit
					break;
				case FINIE:
					etatCourant = Palanquee.Etat.ATTENTE;
					actions(etatCourant);
					//Toast feedback = Toast.makeText(getContext(), getResources().getString(R.string.toast_tour_debutPlongee), Toast.LENGTH_SHORT);
					//feedback.show();

					// on notifie le changement de l'etat à tourActivity
					notifierListener();
					break;
				default:
					// Interdit
					break;
				}
				return true;
			}
		});

		/* Affectation des plongeurs et vérification des valeurs vide */
		setEncadrant((plongeurs[0] != null && !plongeurs[0].isEmpty() ? plongeurs[0] : ""));
		setPlongeur1((plongeurs[1] != null && !plongeurs[1].isEmpty() ? plongeurs[1] : ""));
		setPlongeur2((plongeurs[2] != null && !plongeurs[2].isEmpty() ? plongeurs[2] : ""));
		setPlongeur3((plongeurs[3] != null && !plongeurs[3].isEmpty() ? plongeurs[3] : ""));
		setPlongeur4((plongeurs[4] != null && !plongeurs[4].isEmpty() ? plongeurs[4] : ""));
		setSerrefile((plongeurs[5] != null && !plongeurs[5].isEmpty() ? plongeurs[5] : ""));
		setTypePlongee(type);

		/* Mise à jours du nombre de plongeurs présent dans la palanquée */
		miseAjourNbPlongeur();

		/* Démarrage de l'automate */
		actions(etatCourant);
	}

	// ------------------------------------------------------------------//
	// Logique d'interactions //
	// ------------------------------------------------------------------//
	/**
	 * Automate
	 * @param etat
	 */
	private void actions(Palanquee.Etat etat) {
		Log.d(LOG_TAG, "Etat : "+etat);
		switch (etat) {
		case ATTENTE:
			/* Initialisation du temps du timer */
			this.tempsRestant = tempsImmersion;
			/* On affiche le bon layout */
			attenteLayout.setVisibility(LinearLayout.VISIBLE);			
			immersionLayout.setVisibility(RelativeLayout.GONE);
			finiLayout.setVisibility(RelativeLayout.GONE);
			break;

		case ENCOURS:
			/* On affiche le bon layout */
			attenteLayout.setVisibility(LinearLayout.GONE);
			immersionLayout.setVisibility(RelativeLayout.VISIBLE);
			finiLayout.setVisibility(RelativeLayout.GONE);			

			// FIXME : trouver un moyen pour arreter le timer lorsqu'on est plus dans EN_COURS
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					decrementeTempsRestant();
				}
			}, 0, TIMER_MINUTE); 
			
			break;

		case FINIE:
			miseAjourTempsFinal(tempsImmersion-tempsRestant);
			/* On affiche le bon layout */
			attenteLayout.setVisibility(LinearLayout.GONE);
			immersionLayout.setVisibility(RelativeLayout.GONE);
			finiLayout.setVisibility(RelativeLayout.VISIBLE);			
			break;

		default:
			break;
		}
	}	

	// ------------------------------------------------------------------//
	// Getters & Setters //
	// ------------------------------------------------------------------//
	public Palanquee.Etat getEtatCourant() {
		return etatCourant;
	}

	public String getEncadrant() {
		return tv_attente_encadrant.getText().toString();
	}

	public String getPlongeur1() {
		return tv_attente_plongeur1.getText().toString();
	}

	public String getPlongeur2() {
		return tv_attente_plongeur2.getText().toString();
	}

	public String getPlongeur3() {
		return tv_attente_plongeur3.getText().toString();
	}

	public String getPlongeur4() {
		return tv_attente_plongeur4.getText().toString();
	}

	public String getSerrefile() {
		return tv_attente_serrefile.getText().toString();
	}

	public TypePlongee getTypePlongee() {
		return typePlongee;
	}

	public void setEncadrant(String encadrant) {
		tv_attente_encadrant.setText(encadrant);
		tv_immersion_encadrant.setText(encadrant);
		tv_fini_encadrant.setText(encadrant);
	}

	public void setPlongeur1(String plongeur1) {
		tv_attente_plongeur1.setText(plongeur1);
		tv_immersion_plongeur1.setText(plongeur1);
		tv_fini_plongeur1.setText(plongeur1);
	}

	public void setPlongeur2(String plongeur2) {
		tv_attente_plongeur2.setText(plongeur2);
		//tv_fini_plongeur2.setText(plongeur2);
	}

	public void setPlongeur3(String plongeur3) {
		tv_attente_plongeur3.setText(plongeur3);
		//tv_fini_plongeur3.setText(plongeur3);
	}

	public void setPlongeur4(String plongeur4) {
		tv_attente_plongeur4.setText(plongeur4);
		//tv_fini_plongeur4.setText(plongeur4);
	}

	public void setSerrefile(String serrefile) {
		tv_attente_serrefile.setText(serrefile);
		//tv_fini_serrefile.setText(serrefile);
	}

	public void setTypePlongee(TypePlongee typePlongee) {
		this.typePlongee = typePlongee;
		/* Style de la tuile pour la différence entre autonomie et encadré */
		if(typePlongee == TypePlongee.EXPLORATION) {
			immersionLayout.setBackground(getContext().getResources().getDrawable(R.drawable.tule_tour_autonomie));
			attenteLayout.setBackground(getContext().getResources().getDrawable(R.drawable.tule_tour_autonomie));
			tv_attente_encadrant.setVisibility(TextView.GONE);
			tv_fini_encadrant.setVisibility(TextView.GONE);
		}
		else {
			immersionLayout.setBackground(getContext().getResources().getDrawable(R.drawable.tule_tour_encadrement));
			attenteLayout.setBackground(getContext().getResources().getDrawable(R.drawable.tule_tour_encadrement));
			tv_attente_encadrant.setVisibility(TextView.VISIBLE);
			tv_fini_encadrant.setVisibility(TextView.VISIBLE);
		}
	}

	public int getTourPosition() {
		return tourPosition;
	}

	public int getPalanqueePosition() {
		return palanqueePosition;
	}

	public void setTourPosition(int tourPosition) {
		this.tourPosition = tourPosition;
	}

	public void setPalanqueePosition(int palanqueePosition) {
		this.palanqueePosition = palanqueePosition;
	}

	public int getTempsFinal(){
		return tempsRestant;
	}


	// ------------------------------------------------------------------//
	// Méthodes //
	// ------------------------------------------------------------------//	

	/*public String toString(){		
		return "[TULE TOUR : "+typePlongee.toString()+"]" +
				"\n\t encadrant : "+getEncadrant()+
				"\n\t plongeur1 : "+getPlongeur1()+
				"\n\t plongeur2 : "+getPlongeur2()+
				"\n\t plongeur3 : "+getPlongeur3()+
				"\n\t plongeur4 : "+getPlongeur4()+
				"\n\t serrefile : "+getSerrefile()+
				"[Temps restant : "+tempsRestant+"]";		
	}*/

	/**
	 * Récupère le nombre total de plongeurs dans la palanquée
	 * @return
	 */
	private int recupererNbPlongeur(){
		/* Incrémente la variable si le text n'est pas vide */
		int i = 0;
		i += (getEncadrant()!="" ? 1 : 0);
		i += (getPlongeur1()!="" ? 1 : 0);
		i += (getPlongeur2()!="" ? 1 : 0);
		i += (getPlongeur3()!="" ? 1 : 0);
		i += (getPlongeur4()!="" ? 1 : 0);
		i += (getSerrefile()!="" ? 1 : 0);
		return i;	
	}

	/**
	 * Met à jour le nombre de plongeur dans la palanquée
	 */
	private void miseAjourNbPlongeur() {
		tv_attente_nombrePlongeurs.setText(""+recupererNbPlongeur());		
	}

	/**
	 * Met à jour le temps final de la palanquée
	 */
	private void miseAjourTempsFinal(int temps) {
		tv_fini_tempsFinal.setText(" "+temps+" min");		
	}

	/**
	 * Utilisé par le timer
	 */
	public void decrementeTempsRestant() {
		tempsRestant--;
		mHandler.post(new Runnable() {
			
			@Override
			public void run() {
				if (tempsRestant < 0) {
					tv_immersion_tempsRestant.setText((tempsImmersion - tempsRestant)+"min");
				}
				else {
					tv_immersion_tempsRestant.setText((tempsRestant)+"min");
				}
			}
		});	
		notifierListener(); //mise à jour temps final
	}

	/**
	 * Notifie les listeners du changement des tules
	 */
	public void notifierListener() {
		for (TuleTourUpdateListener hl : listeners)
			hl.tuleTourUpdated(new TuleTourUpdateEvent(this));	
	}

	/**
	 * Ajoute les listeners utilisé pour fire 
	 * TuleGridPlongeurItemUpdateEvent
	 * à l'objet plongeurActivity
	 * @param toAdd
	 */
	public void addListener(TuleTourUpdateListener toAdd) {
		listeners.add(toAdd);
	}


}

