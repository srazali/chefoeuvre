package fr.enac.utils.tuletour;

import java.util.EventObject;

import fr.enac.models.Palanquee;

public class TuleTourUpdateEvent extends EventObject{

	/**
	 * Enleve le warning
	 */
	private static final long serialVersionUID = 1L;
	
	// ------------------------------------------------------------------//
	// Constructeurs //
	// ------------------------------------------------------------------//
	/**
	 * Constructeur par défaut
	 * 
	 * @param source
	 */
	public TuleTourUpdateEvent(Object source) {
		super(source);
	}
}
