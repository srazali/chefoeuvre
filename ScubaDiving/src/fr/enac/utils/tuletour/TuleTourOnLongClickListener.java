package fr.enac.utils.tuletour;

import fr.enac.activities.TourActivity;
import android.content.ClipData;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnLongClickListener;

/**
 * LongClickListener pour débuter le drag & drop
 * @author Jonathan Tolle
 */
public class TuleTourOnLongClickListener implements OnLongClickListener{
	private static final String LOG_TAG = TuleTourOnLongClickListener.class.getName();
	
	private TuleTour tule;

	public TuleTourOnLongClickListener(TuleTour tule) {
		this.tule = tule;
	}

	public TuleTour getTule() {
		return tule;
	}

	@Override
	public boolean onLongClick(View view) {
		ClipData data = ClipData.newPlainText("", "");
		DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
		view.startDrag(data, shadowBuilder, view, 0);
		view.setVisibility(View.INVISIBLE);
		TourActivity.tuleDragged = tule;
		return true;
	}

}
