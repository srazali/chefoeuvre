package fr.enac.utils.tuletour;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import fr.enac.activities.TourActivity;

/**
 * Listener utiliser lorsqu'on clic(déposée) une tule dans un tour
 * @author Jonathan Tolle
 *
 */
public class TuleTourOnClickDeposeListener implements OnTouchListener{
	private static final String LOG_TAG = TuleTourOnClickDeposeListener.class.getName();

	private int tourPosition;
	private TourActivity activity;

	public TuleTourOnClickDeposeListener(int tourPosition, TourActivity activity) {
		this.tourPosition = tourPosition;
		this.activity = activity;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (TourActivity.tuleSelectionnee != null) {
			TuleTour tule = TourActivity.tuleSelectionnee;			
			Log.d(LOG_TAG, "Clic déposé palanqueePosition "+tule.getPalanqueePosition()+" (tour n°"+(tule.getTourPosition()+1)+") dans tour n°"+(tourPosition+1));	
			activity.setTourToPalanquee(tule.getTourPosition(), tule.getPalanqueePosition(), tourPosition);
			activity.updateTuleTours();
			TourActivity.tuleSelectionnee = null;
		}
		return false;
	}

}
