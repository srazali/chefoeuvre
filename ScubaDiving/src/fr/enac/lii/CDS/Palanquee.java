/*
 * Copyright Sébastien LERICHE, 2014
 * 
 * sebastien.leriche@flabelline.com
 * 
 * Ce logiciel est un programme informatique servant à manipuler les concepts de
 * plongée sous-marine exprimés dans le Code du Sport.
 * 
 * Ce logiciel est régi par la licence CeCILL-B soumise au
 * droit français et respectant les principes de diffusion des logiciels libres.
 * Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les
 * conditions de la licence CeCILL-B telle que diffusée par le
 * CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie, de
 * modification et de redistribution accordés par cette licence, il n'est offert
 * aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une
 * responsabilité restreinte pèse sur l'auteur du programme, le titulaire des
 * droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard l'attention de l'utilisateur est attirée sur les risques associés
 * au chargement, à l'utilisation, à la modification et/ou au développement et à
 * la reproduction du logiciel par l'utilisateur étant donné sa spécificité de
 * logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve
 * donc à des développeurs et des professionnels avertis possédant des
 * connaissances informatiques approfondies. Les utilisateurs sont donc invités
 * à charger et tester l'adéquation du logiciel à leurs besoins dans des
 * conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs
 * données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes
 * conditions de sécurité.
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL-B, et que vous en avez
 * accepté les termes.
 */

/**
 * @author leriche
 */

package fr.enac.lii.CDS;

import java.io.Serializable;

/**
 * 
 * @author leriche
 * 
 */

public class Palanquee implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Plongeur encadrant;
	private Plongeur[] plongeur = new Plongeur[4];
	private Plongeur serreFile;
	private TypeGaz typeGaz;
	private TypePlongee typePlongee;
	private String justification;

	/**
	 * Construit une palanquée vide, par défaut à l'air et en exploration
	 */
	public Palanquee() {
		typeGaz = TypeGaz.AIR; // air par défaut
		typePlongee = TypePlongee.EXPLORATION; // explo
		justification = null;
	}

	/**
	 * Ajouter l'encadrant dans la palanquée (écrase éventuellement le
	 * précédant)
	 * 
	 * @param p
	 *            plongeur encadrant
	 */
	public void ajouterEncadrant(Plongeur p) {
		encadrant = p;
	}

	/**
	 * Ajouter le serre-file dans la palanquée (écrase éventuellement le
	 * précédant)
	 * 
	 * @param p
	 *            plongeur serre-file
	 */
	public void ajouterSerreFile(Plongeur p) {
		serreFile = p;
	}

	/**
	 * Ajouter un plongeur dans la palanquée (écrase éventuellement le
	 * précédant), sachant qu'il ne peut y avoir que 4 plonngeurs au plus. Pour
	 * une valeur de pos invalide, il n'y a aucune action.
	 * 
	 * @param p
	 *            le plongeur
	 * @param pos
	 *            son numéro dans la palanquée (entre 0 et 3)
	 */
	public void ajouterPlongeur(Plongeur p, int pos) {
		if (pos < 0 || pos > 3)
			return;
		plongeur[pos] = p;
	}

	/**
	 * Permet de comprendre pourquoi une palanquée n'est pas conforme au CDS
	 * 
	 * @return la chaine décrivant le problème ou bien null
	 */
	public String getJustification() {
		return justification;
	}

	/**
	 * Affiche la constitution de la palanquée et les informations associées
	 */
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		boolean ok = peutPlonger();
		s.append("Palanquée (" + nombrePlongeurs() + ") : [" + typeGaz + "/"
				+ typePlongee + "] peutPlonger=" + ok + " - profMax="
				+ profondeurMax() + "m\n"
				+ (ok ? "" : getJustification() + "\n"));
		s.append("Encadrant : " + encadrant + "\n");
		for (Plongeur p : plongeur) {
			s.append("Plongeur : " + p + "\n");
		}
		s.append("Serre-file : " + serreFile + "\n");
		return s.toString();
	}

	/**
	 * Change le type de plongée (explo/enseignement)
	 * 
	 * @param t
	 */
	public void setTypePlongee(TypePlongee t) {
		this.typePlongee = t;
	}
	
	/**
	 * Renvoie le type de plongee de la palanquee.
	 * 
	 * @return le type de plongee de la palanquee
	 * @author Sophien Razali
	 */
	public TypePlongee getTypePlongee() {
		return typePlongee;
	}

	/**
	 * Change le type de gaz pour la palaquée (air, nitrox, trimix)
	 * 
	 * @param t
	 */
	public void setTypeGaz(TypeGaz t) {
		this.typeGaz = t;
	}

	/**
	 * Renvoie le type de gaz utilis� par la palanquee.
	 * 
	 * @return le type de gaz utilis� par la palanquee
	 * @author Sophien Razali
	 */
	public TypeGaz getTypeGaz() {
		return typeGaz;
	}

	/**
	 * Compte le nombre de plongeurs dans la palanquée y compris encadrant et
	 * serre-file
	 * 
	 * @return le nombre est compris entre 0 et 6
	 */
	public int nombrePlongeurs() {
		int nb = 0;
		if (encadrant != null)
			nb++;
		for (Plongeur p : plongeur) {
			if (p != null)
				nb++;
		}
		if (serreFile != null)
			nb++;
		return nb;
	}

	/**
	 * Renvoie faux si la palanquée n'est pas constituée confirmément au CDS.
	 * Note : la valeur vraie ne garanti pas que la constitution soit
	 * conforme...
	 * 
	 * @return
	 */
	public boolean peutPlonger() {
		justification = null;

		if (!peutPlongerAir()) {
			return false;
		}
		if (typeGaz == TypeGaz.NITROX) {
			return peutPlongerNitrox();
		}
		if (typeGaz == TypeGaz.TRIMIX) {
			return peutPlongerTrimix();
		}
		return true;
	}

	private boolean peutPlongerAir() {
		int nb = nombrePlongeurs();

		// toujours au moins deux plongeurs
		if (nb < 2) {
			justification = "moins de 2 plongeurs";
			return false;
		}

		// en exploration, l'encadrant, si existant est au moins GP
		if (typePlongee == TypePlongee.EXPLORATION && encadrant != null
				&& (encadrant.getEncadrantAir() == null)) {
			justification = "encadrant non qualifié";
			return false;
		}

		// en enseignement, l'encadrant est présent et est au moins E1
		if (typePlongee == TypePlongee.ENSEIGNEMENT) {
			if (encadrant == null) {
				justification = "encadrant absent";
				return false;
			}
			if (encadrant.getEncadrantAir() == null
					|| encadrant.getEncadrantAir().compareTo(EncadrantAir.E1) < 0) {
				justification = "encadrant non qualifié";
				return false;
			}
		}

		// le serre file est au moins GP
		if (serreFile != null) {
			if (serreFile.getEncadrantAir() == null
					|| serreFile.getEncadrantAir() == EncadrantAir.E1) {
				justification = "serre-file < GP";
				return false;
			}
		}

		// en autonomie : pas d'encadrant, max 3 et pas de serre-file
		// de plus tous doivent avoir une aptitude d'autonome
		if (encadrant == null) {
			// pas de serreFile
			if (serreFile != null) {
				justification = "serre-file présent";
				return false;
			}
			// pas plus de 3
			if (nb > 3) {
				justification = "plus de 3 plongeurs";
				return false;
			}
			// tous autonomes
			for (Plongeur p : plongeur) {
				if (p == null)
					continue;
				if (p.getAptitudesAir()[1] == null) {
					justification = "plongeur " + p + " non autonome";
					return false;
				}
			}
		}

		// baptême => 1 seul plongeur + 1 encadrant E1 mini + plongée
		// enseignement
		for (Plongeur p : plongeur) {
			if (p == null)
				continue;
			if (p.getAptitudesAir()[0] == AptitudeAir.BAT) {
				typePlongee = TypePlongee.ENSEIGNEMENT; // obligatoire
				if (serreFile != null) { // pour décompter le serre-file du test
											// suivant
					nb = nb - 1;
				}
				if (nb != 2) {
					justification = "plus de 1 plongeur";
					return false;
				}
				if ( // encadrant != null
				encadrant.getEncadrantAir().compareTo(EncadrantAir.E1) < 0) {
					justification = "encadrant non qualifié";
					return false;
				}
			}
		}

		return true;
	}

	private boolean peutPlongerNitrox() {
		// en enseignement, l'encadrant est au moins E2 et PN-C
		if (typePlongee == TypePlongee.ENSEIGNEMENT) {
			if (encadrant.getEncadrantAir().compareTo(EncadrantAir.E2) < 0
					|| encadrant.getAptitudeNitrox() != AptitudeNitrox.PNC) {
				justification = "encadrant non qualifié";
				return false;
			}
		}

		// en exploration, l'encadrant, si existant est au moins GP et PN-C
		if (typePlongee == TypePlongee.EXPLORATION && encadrant != null) {
			if (encadrant.getEncadrantAir() == EncadrantAir.E1
					|| encadrant.getAptitudeNitrox() != AptitudeNitrox.PNC) {
				justification = "encadrant non qualifié";
				return false;
			}
		}

		// le serre file est au moins PN-C
		if (serreFile != null) {
			if (serreFile.getAptitudeNitrox() == null
					|| serreFile.getAptitudeNitrox() != AptitudeNitrox.PNC) {
				justification = "serre file n'est pas PN-C";
				return false;
			}
		}

		// en explo tout le monde est au moins PN
		if (typePlongee == TypePlongee.EXPLORATION) {
			for (Plongeur p : plongeur) {
				if (p == null)
					continue;
				if (p.getAptitudeNitrox() == null) {
					justification = "plongeur " + p + " n'est pas PN";
					return false;
				}
			}
		}
		return true;
	}

	private boolean peutPlongerTrimix() {
		// pas de serre-file en trimix
		if (serreFile != null) {
			justification = "plongeur en serre-file";
			return false;
		}

		// en enseignement, l'encadrant est présent et est au moins E3 et PTH-70
		if (typePlongee == TypePlongee.ENSEIGNEMENT) {
			if (encadrant.getEncadrantAir().compareTo(EncadrantAir.E3) < 0
					|| encadrant.getAptitudeTrimix() == null
					|| encadrant.getAptitudeTrimix().compareTo(
							AptitudeTrimix.PTH70) < 0) {
				justification = "encadrant non qualifié";
				return false;
			}
		}

		// en exploration, l'encadrant, si existant est au moins E3 et PTH-40
		if (typePlongee == TypePlongee.EXPLORATION && encadrant != null) {
			if (encadrant.getEncadrantAir().compareTo(EncadrantAir.E3) < 0
					|| encadrant.getAptitudeTrimix() == null) {
				justification = "encadrant non qualifié";
				return false;
			}
		}

		// en explo, tout le monde est au moins PE-40 et PTH-40
		if (typePlongee == TypePlongee.EXPLORATION) {
			for (Plongeur p : plongeur) {
				if (p == null)
					continue;
				if (p.getAptitudesAir()[0].compareTo(AptitudeAir.PE40) < 0
						|| p.getAptitudeTrimix() == null) {
					justification = "plongeur " + p + " non qualifié";
					return false;
				}
			}
		}

		// en formation, il faut être au moins PN-C
		if (typePlongee == TypePlongee.ENSEIGNEMENT) {
			for (Plongeur p : plongeur) {
				if (p == null)
					continue;
				if (p.getAptitudeNitrox() != AptitudeNitrox.PNC) {
					justification = "plongeur " + p + " non qualifié";
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Renvoie la profondeur maximale d'évolution de la palanquée, ou 0 si non
	 * conforme au CDS
	 * 
	 * @return profondeur en mètres
	 */
	public int profondeurMax() {
		if (!peutPlonger())
			return 0;

		// en autonomie : min des plongeurs, limitée au max selon le type de gaz
		if (encadrant == null) {
			int aux, min = typeGaz.getProfMax();
			for (Plongeur p : plongeur) {
				if (p == null)
					continue;
				aux = p.profondeurMax(typeGaz, TypePlongee.EXPLORATION, true);
				if (aux < min) {
					min = aux;
				}
			}
			return min;
		}

		// en enseignement : min des plongeurs et de l'encadrant
		if (typePlongee == TypePlongee.ENSEIGNEMENT) {
			int aux, min = typeGaz.getProfMax();
			if (typeGaz == TypeGaz.TRIMIX) { // cas particulier du trimix
				if (encadrant.getEncadrantAir().equals(EncadrantAir.E3)
						|| encadrant.getAptitudeTrimix().equals(
								AptitudeTrimix.PTH70)) {
					aux = 40; // E3 ou PTH70=>40m max
				} else {
					aux = 80; // E4 PTH120
				}
			} else { // nitrox et air
				aux = encadrant.getEncadrantAir().getProfEnseignement();
				// limite de 40m si serre-file
				if (serreFile != null) {
					if (aux > 40) {
						aux = 40;
					}
				}
			}
			// System.out.println("-" + encadrant + " / " + aux);
			if (aux < min) {
				min = aux;
			}
			for (Plongeur p : plongeur) {
				if (p == null)
					continue;
				aux = p.profondeurMax(typeGaz, TypePlongee.ENSEIGNEMENT, false);
				// System.out.println("-" + p + " / " + aux);
				if (aux < min) {
					min = aux;
				}
			}
			return min;
		}

		// plongée encadrée : min des plongeurs et de l'encadrant
		int aux, min = typeGaz.getProfMax();
		if (typeGaz == TypeGaz.TRIMIX) { // cas particulier du trimix
			if (encadrant.getEncadrantAir().equals(EncadrantAir.E3)) {
				aux = 40; // E3=>40m max
			} else if (encadrant.getAptitudeTrimix().equals(
					AptitudeTrimix.PTH70)) {
				aux = 70; // E4 PTH70
			} else {
				aux = 80; // E4 PTH120
			}
		} else {
			aux = encadrant.getEncadrantAir().getProfEncadrement();
			// limite de 40m si serre-file
			if (serreFile != null) {
				if (aux > 40) {
					aux = 40;
				}
			}
		}
		if (aux < min) {
			min = aux;
		}
		for (Plongeur p : plongeur) {
			if (p == null)
				continue;
			aux = p.profondeurMax(typeGaz, TypePlongee.EXPLORATION, false);
			if (aux < min) {
				min = aux;
			}
		}

		return min;
	}
	
	public Plongeur getEncadrant() {
		return encadrant;
	}
	
	public void enleverEncadrant(){
		encadrant = null;
	}
	
	public Plongeur getSerreFile() {
		return serreFile;
	}
	
	public void enleverSerreFile(){
		serreFile = null;
	}
	
	public Plongeur getPlongeur(int i) {
		return plongeur[i];
	}
	
	public Plongeur[] getPlongeur() {
		return plongeur;
	}
	
	public void enleverPlongeur(int i){
		plongeur[i] = null;
	}
}
