/*
 * Copyright Sébastien LERICHE, 2014
 * 
 * sebastien.leriche@flabelline.com
 * 
 * Ce logiciel est un programme informatique servant à manipuler les concepts de
 * plongée sous-marine exprimés dans le Code du Sport.
 * 
 * Ce logiciel est régi par la licence CeCILL-B soumise au
 * droit français et respectant les principes de diffusion des logiciels libres.
 * Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les
 * conditions de la licence CeCILL-B telle que diffusée par le
 * CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie, de
 * modification et de redistribution accordés par cette licence, il n'est offert
 * aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une
 * responsabilité restreinte pèse sur l'auteur du programme, le titulaire des
 * droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard l'attention de l'utilisateur est attirée sur les risques associés
 * au chargement, à l'utilisation, à la modification et/ou au développement et à
 * la reproduction du logiciel par l'utilisateur étant donné sa spécificité de
 * logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve
 * donc à des développeurs et des professionnels avertis possédant des
 * connaissances informatiques approfondies. Les utilisateurs sont donc invités
 * à charger et tester l'adéquation du logiciel à leurs besoins dans des
 * conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs
 * données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes
 * conditions de sécurité.
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL-B, et que vous en avez
 * accepté les termes.
 */

/**
 * @author leriche
 */

package fr.enac.lii.CDS;

public enum AptitudeAir {
	BAT(0, 6), DEB(6, 20), PE12(12, 20), PA12(12, 20, true), PE20(20, 40), PA20(
			20, 40, true), PE40(40, 60), PA40(40, 60, true), PE60(60, 60), PA60(
			60, 60, true);

	private int profExplo, profEnseignement;
	private boolean autonomie = false;

	private AptitudeAir(int profExplo, int profEnseignement) {
		this.profExplo = profExplo;
		this.profEnseignement = profEnseignement;
	}

	private AptitudeAir(int profExplo, int profEnseignement, boolean autonomie) {
		this.profExplo = profExplo;
		this.profEnseignement = profEnseignement;
		this.autonomie = autonomie;
	}

	public int getProfEnseignement() {
		return profEnseignement;
	}

	public int getProfExplo() {
		return profExplo;
	}

	public boolean getAutonomie() {
		return autonomie;
	}

	@Override
	public String toString() {
		if (PE12.compareTo(this) <= 0) {
			String name = name().toString();
			return name.substring(0, 2) + "-" + name.substring(2);
		} else {
			return name();
		}

	}

}
