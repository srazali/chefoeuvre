/*
 * Copyright Sébastien LERICHE, 2014
 * 
 * sebastien.leriche@flabelline.com
 * 
 * Ce logiciel est un programme informatique servant à manipuler les concepts de
 * plongée sous-marine exprimés dans le Code du Sport.
 * 
 * Ce logiciel est régi par la licence CeCILL-B soumise au
 * droit français et respectant les principes de diffusion des logiciels libres.
 * Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les
 * conditions de la licence CeCILL-B telle que diffusée par le
 * CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info".
 * 
 * En contrepartie de l'accessibilité au code source et des droits de copie, de
 * modification et de redistribution accordés par cette licence, il n'est offert
 * aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une
 * responsabilité restreinte pèse sur l'auteur du programme, le titulaire des
 * droits patrimoniaux et les concédants successifs.
 * 
 * A cet égard l'attention de l'utilisateur est attirée sur les risques associés
 * au chargement, à l'utilisation, à la modification et/ou au développement et à
 * la reproduction du logiciel par l'utilisateur étant donné sa spécificité de
 * logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve
 * donc à des développeurs et des professionnels avertis possédant des
 * connaissances informatiques approfondies. Les utilisateurs sont donc invités
 * à charger et tester l'adéquation du logiciel à leurs besoins dans des
 * conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs
 * données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes
 * conditions de sécurité.
 * 
 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL-B, et que vous en avez
 * accepté les termes.
 */

/**
 * @author leriche
 */

package fr.enac.lii.CDS;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.util.Log;

/**
 * @author leriche
 * 
 */
public class Plongeur implements Serializable, Comparable<Plongeur> {

	private static final long serialVersionUID = 1L;

	private String niveauPlongeurCDS = null;
	private boolean aptitudeComplementaire = false;
	private AptitudeAir[] aptitudeAir = new AptitudeAir[2]; // [PE, PA]
	private EncadrantAir encadrantAir = null;
	private AptitudeNitrox aptitudeNitrox = null;
	private AptitudeTrimix aptitudeTrimix = null;

	/**
	 * Par défaut, un plongeur aura l'aptitude BAT
	 */
	public Plongeur() {
		reset();
	}

	/**
	 * réinitialise le niveau de plongeur, pour permettre l'usage de plusieurs
	 * appels à spécifierNiveauCDS()
	 */
	private void reset() {
		niveauPlongeurCDS = null;
		aptitudeComplementaire = false;
		encadrantAir = null;
		aptitudeNitrox = null;
		aptitudeTrimix = null;
		// par défaut, un plongeur sera en baptême
		aptitudeAir[0] = AptitudeAir.BAT;
		aptitudeAir[1] = null;
	}

	/**
	 * 
	 * @return la liste des aptitudes air les plus élevées [PA,PE],
	 *         éventuellement nulles pour PE
	 */
	public AptitudeAir[] getAptitudesAir() {
		return aptitudeAir.clone();
	}

	/**
	 * 
	 * @return le niveau d'encadrant air le + élevé ou null
	 */
	public EncadrantAir getEncadrantAir() {
		return encadrantAir;
	}

	/**
	 * 
	 * @return l'aptitude nitrox la plus élevée ou null
	 */
	public AptitudeNitrox getAptitudeNitrox() {
		return aptitudeNitrox;
	}

	/**
	 * 
	 * @return le niveau code du sport du plongeur
	 */
	public String getNiveauPlongeurCDS() {
		return toString();
	}

	/**
	 * 
	 * @return l'aptitude trimix la plus élevée ou null
	 */
	public AptitudeTrimix getAptitudeTrimix() {
		return aptitudeTrimix;
	}

	/**
	 * Donne la profondeur maximale accesible règlementairement (CDS) au
	 * plongeur, en fonction de la nature du gaz utilisé, du type de plongée et
	 * de son éventuelle autonomie
	 * 
	 * @param typeGaz
	 *            air, nitrox ou trimix
	 * @param typePlongee
	 *            exploration ou enseignement
	 * @param autonomie
	 *            vrai si plongée en autonomie
	 * @return
	 */
	public int profondeurMax(TypeGaz typeGaz, TypePlongee typePlongee,
			boolean autonomie) {

		// cas du trimix : extension de la plongée à l'air, cf aptitudes
		if (typeGaz == TypeGaz.TRIMIX) {
			// enseignement
			if (typePlongee == TypePlongee.ENSEIGNEMENT) {
				if (aptitudeTrimix == null) {
					return 40; // aucune qualif trimix => limite 40m
				}
				return aptitudeTrimix.getProfEnseignement();
			}
			// explo ou encadrement
			return aptitudeTrimix.getProfExplo();

		}

		// cas du nitrox : PE explo en formation/encadrement, PA explo en
		// autonomie
		if (typeGaz == TypeGaz.NITROX) {
			if (autonomie) {
				return aptitudeAir[1].getProfExplo(); // PA
			}
			return aptitudeAir[0].getProfExplo(); // PE
		}

		// cas air limite = prérogatives air
		if (typePlongee == TypePlongee.ENSEIGNEMENT) {
			return aptitudeAir[0].getProfEnseignement();
		}
		if (autonomie) {
			return aptitudeAir[1].getProfExplo(); // PA
		}
		return aptitudeAir[0].getProfExplo();
	}

	/**
	 * Renvoie une chaîne représentant le niveau CDS et les aptitudes
	 * éventuelles
	 */
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();

		// si pas encadrant ou bien E1 ou E2, on cherche d'abord le niveau CDS,
		// par exemple P3 ou P5
		if ((encadrantAir == null || encadrantAir == EncadrantAir.E1 || encadrantAir == EncadrantAir.E2)
				&& niveauPlongeurCDS != null) {
			s.append(niveauPlongeurCDS);
		}

		// pour un N1, on laisse la possibilité d'avoir la compétence PA12
		if ("P1".equals(niveauPlongeurCDS) && aptitudeAir[1] != null) {
			s.append("+" + aptitudeAir[1]);
		}

		// on ajoute le niveau d'encadrant
		if (encadrantAir != null) {
			if (s.length() > 0) {
				s.append("+"); // P3+E1
			}
			s.append(encadrantAir);
		}

		// si pas de niveau CDS ni encadrant OU aptitude déclarée, on utilise
		// les aptitudes*/
		if (niveauPlongeurCDS == null && encadrantAir == null
				|| aptitudeComplementaire) {
			if (aptitudeComplementaire) {
				s.append("+");
			}
			// si on a PEx+PAx on ne marque que PAx
			if (aptitudeAir[1] != null
					&& aptitudeAir[1].ordinal() == aptitudeAir[0].ordinal() + 1) {
				s.append(aptitudeAir[1]);
			} else { // sinon on marque tout
				s.append(aptitudeAir[0]);
				if (aptitudeAir[1] != null) {
					s.append("+" + aptitudeAir[1]);
				}
			}
		}

		// soit on est trimix (et donc nitrox) soit on est nitrox (ou rien)
		if (aptitudeTrimix != null) {
			s.append(" " + aptitudeTrimix);
		} else if (aptitudeNitrox != null) {
			s.append(" " + aptitudeNitrox);
		}

		return s.toString();
	}

	/**
	 * Décompose les niveaux et aptitudes du plongeur pour en faire 3 groupes le
	 * groupe de niveau, le groupe des aptitudes et le groupe du mélange
	 * 
	 * @return Le tableau contenant les 3 groupes [niveau, aptitude, mélange]
	 */
	public static String[] toSpinner(String description) {
		String[] s3 = new String[3];
		description = description.replace("-", ""); // les enums ne contiennent
													// pas de caractère '-'
		for (String elem : description.split("[ \\+]")) {
			try {
				if (s3[0] == null) {
					s3[0] = Niveau.valueOf(elem).toString();
				} else {
					s3[0] += "+" + Niveau.valueOf(elem).toString();
				}
			} catch (IllegalArgumentException e) {
			}
			try {
				if (s3[1] == null) {
					s3[1] = AptitudeAir.valueOf(elem).toString();
				} else {
					s3[1] += "+" + AptitudeAir.valueOf(elem).toString();
				}
			} catch (IllegalArgumentException e) {
			}
			try {
				if (s3[2] == null) {
					s3[2] = AptitudeNitrox.valueOf(elem).toString();
				} else {
					s3[2] += "+" + AptitudeNitrox.valueOf(elem).toString();
				}
			} catch (IllegalArgumentException e) {
			}
			try {
				if (s3[2] == null) {
					s3[2] = AptitudeTrimix.valueOf(elem).toString();
				} else {
					s3[2] += "+" + AptitudeTrimix.valueOf(elem).toString();
				}
			} catch (IllegalArgumentException e) {
			}
		}

		return s3;
	}

	private void specifierAptitudeAir(AptitudeAir apt) {
		if (apt.getAutonomie()) {
			aptitudeAir[1] = apt;
			// PAx=>PEx si non déjà spécifié
			if (aptitudeAir[0] == AptitudeAir.BAT
					&& apt.compareTo(AptitudeAir.PE12) >= 0) {
				// PE est avant PA
				aptitudeAir[0] = AptitudeAir.values()[apt.ordinal() - 1];
			}
		} else {
			aptitudeAir[0] = apt;
		}
	}

	private void specifierEncadrantAir(EncadrantAir enc) {
		encadrantAir = enc;
		// par défaut, les E2+ sont PA60 et PE60
		if (enc.compareTo(EncadrantAir.E2) >= 0) {
			specifierAptitudeAir(AptitudeAir.PA60);
			specifierAptitudeAir(AptitudeAir.PE60);
		}
	}

	private void specifierAptitudeNitrox(AptitudeNitrox apt) {
		aptitudeNitrox = apt;
	}

	private void specifierAptitudeTrimix(AptitudeTrimix apt) {
		aptitudeTrimix = apt;
		// un plongeur trimix est PN-C
		specifierAptitudeNitrox(AptitudeNitrox.PNC);
	}

	/**
	 * Décrit sous forme de chaine de caractère le niveau et/ou les aptitudes
	 * telles que données dans le CDS. Par exemple : E4 PTH-120, GP+E1 PN-C,
	 * PA-12 PN...
	 * 
	 * @param niv
	 *            la chaine
	 */
	public void specifierNiveauCDS(String niv) {
		// réinitialise toutes les compétences/aptitudes
		reset();

		// séparateur : espace ou +
		for (String elem : niv.toUpperCase().trim().split("[ \\+]")) {
			try {
				specifierElementCDS(elem);
			} catch (UnknownElement e) {
				// System.err.println("Erreur niveau CDS : "+elem);
				//Log.d("CDS", "Erreur niveau CDS : " + elem);
			}
		}
	}

	private void specifierElementCDS(String niv) throws UnknownElement {
		//Log.d("CDS", "Plongeur CDS : " + niv);
		if (niv.equals("BAT")) {
			niveauPlongeurCDS = niv;
			specifierAptitudeAir(AptitudeAir.BAT);
			return;
		}

		if (niv.equals("DEB")) {
			niveauPlongeurCDS = niv;
			specifierAptitudeAir(AptitudeAir.DEB);
			return;
		}

		Pattern p = Pattern.compile("(P[EA])-([1246][20])");
		Matcher m = p.matcher(niv);
		if (m.matches()) {
			try {
				aptitudeComplementaire = true;
				specifierAptitudeAir(AptitudeAir.valueOf(m.group(1)
						+ m.group(2)));
			} catch (IllegalArgumentException e) {
				throw new UnknownElement("Erreur pattern : " + niv
						+ " ne semble pas être un élément du CDS");
			}
			return;
		}

		if (niv.equals("P1")) {
			niveauPlongeurCDS = niv;
			specifierAptitudeAir(AptitudeAir.PE20);
			return;
		}

		if (niv.equals("P2")) {
			niveauPlongeurCDS = niv;
			specifierAptitudeAir(AptitudeAir.PE40);
			specifierAptitudeAir(AptitudeAir.PA20);
			return;
		}

		if (niv.equals("P3") || niv.equals("P4") || niv.equals("GP")
				|| niv.equals("P5")) {
			niveauPlongeurCDS = niv;
			specifierAptitudeAir(AptitudeAir.PE60);
			specifierAptitudeAir(AptitudeAir.PA60);
			if (niv.equals("GP")) {
				specifierEncadrantAir(EncadrantAir.GP);
			}
			if (niv.equals("P4") || (niv.equals("P5"))) { // P5 = P4
				specifierEncadrantAir(EncadrantAir.P4);
			}
			return;
		}

		if (niv.equals("E1")) {
			specifierEncadrantAir(EncadrantAir.E1);
			return;
		}

		if (niv.equals("E2")) {
			specifierEncadrantAir(EncadrantAir.E2);
			return;
		}

		if (niv.equals("E3")) {
			specifierEncadrantAir(EncadrantAir.E3);
			return;
		}

		if (niv.equals("E4")) {
			specifierEncadrantAir(EncadrantAir.E4);
			return;
		}

		if (niv.equals("PN")) {
			specifierAptitudeNitrox(AptitudeNitrox.PN);
			return;
		}

		if (niv.equals("PN-C")) {
			specifierAptitudeNitrox(AptitudeNitrox.PNC);
			return;
		}

		if (niv.equals("PTH-40")) {
			specifierAptitudeTrimix(AptitudeTrimix.PTH40);
			return;
		}

		if (niv.equals("PTH-70")) {
			specifierAptitudeTrimix(AptitudeTrimix.PTH70);
			return;
		}

		if (niv.equals("PTH-120")) {
			specifierAptitudeTrimix(AptitudeTrimix.PTH120);
			return;
		}

		throw new UnknownElement("Erreur : " + niv
				+ " ne semble pas être un élément du CDS");
	}

	private int getValue() {
		int val = 0;
		int v1 = 0, v2 = 0;

		if (encadrantAir != null) {
			val += (encadrantAir.ordinal() + 1) * 100;
		}
		if (aptitudeAir[0] != null) {
			v1 = aptitudeAir[0].ordinal();
		}
		if (aptitudeAir[1] != null) {
			v2 = aptitudeAir[1].ordinal();
		}
		val += (Math.max(v1, v2) + 1) * 10;
		if (aptitudeNitrox != null) {
			val += (aptitudeNitrox.ordinal() + 1);
		}
		if (aptitudeTrimix != null) {
			val += (aptitudeTrimix.ordinal() + 1 + 2);
		}
		return val;
	}

	@Override
	public int compareTo(Plongeur arg0) {
		Integer val = this.getValue();
		Integer autre = arg0.getValue();
		return val.compareTo(autre);
	}
}
