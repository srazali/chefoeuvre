package fr.enac.lii.CDS;

/**
 * Recense tous les niveaux des plongeurs
 * (utilisé dans la sélection des niveaux sur l'interface graphique)
 * @author Jonathan Tolle
 */
public enum Niveau{
	P1,
	P2,
	P3,
	P4,
	GP,
	P5,
	E1,
	E2,
	E3,
	E4
}