package fr.enac.database;

import android.database.sqlite.SQLiteDatabase;

/**
 * Classe abstraite DAO de type générique T représentant un objet DAO.
 * Les objets DAO permettent aux développeurs d'accèder à la base de données
 * en fournissant des méthodes de lecture et de modification.
 * 
 * @author Sophien Razali
 * @see <a href="http://fr.openclassrooms.com/informatique/cours/apprenez-a-programmer-en-java/le-pattern-dao-1">Tutoriel suivi (OpenClassrooms)</a>
 * @param <T> type générique
 */
public abstract class DAO<T> {
	
	/**
	 * L'objet SQLiteDatabase permettant à l'objet DAO d'accèder
	 * à la base de données.
	 */
	protected SQLiteDatabase mDatabase = null;

	public DAO(SQLiteDatabase db) {
		this.mDatabase = db;
		this.mDatabase.setForeignKeyConstraintsEnabled(true);
	}
	
	/**
	 * Insert l'objet passé en paramètre dans la base de données.
	 * 
	 * @param object l'objet à insérer
	 * @return l'id de la ligne créée lors de l'insertion
	 */
	public abstract long insert(T object);
	
	/**
	 * Supprime l'objet passé en paramètre de la base de données.
	 * 
	 * @param object l'objet à supprimer
	 * @return true si l'objet a été supprimé, false sinon.
	 */
	public abstract boolean delete(T object);
	
	/**
	 * Update (met à jour) l'objet passé en paramètre dans la base de données.
	 * 
	 * @param object l'objet à éditer
	 * @return true si l'bjet a été édité, false sinon
	 */
	public abstract boolean update (T object);
	
	/**
	 * Sélectionne la ligne de la base de données respectant l'id passé en paramètre,
	 * et renvoie l'objet correspondant.
	 * 
	 * @param id l'id de la ligne
	 * @return l'objet correspondant
	 */
	public abstract T select(long id);
}
