package fr.enac.database;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import fr.enac.models.Palanquee;
import fr.enac.models.Plongee;
import fr.enac.models.PlongeeEtatEnum;
import fr.enac.models.PlongeeHoraireEnum;
import fr.enac.models.Plongeur;
import fr.enac.models.PlongeurTypeEnum;
import fr.enac.models.Tour;

/**
 * Classe PlongeeDAO représentant la relation entre
 * un objet Plongee et la base de données.
 * 
 * @author Sophien Razali
 *
 */
public class PlongeeDAO extends DAO<Plongee> {
	
	private static final String PLONGEE_UPDATE = 
			DatabaseHelper.PLONGEE_CLE + " = ?;";
	
	private static final String PLONGEE_DELETE =
			DatabaseHelper.PLONGEE_CLE + " = ?;";
	
	private static final String SELECT_TOUTES_LES_PLONGEES = 
			"SELECT * FROM " + DatabaseHelper.PLONGEE_NOM_TABLE;
	
	private static final String SELECT_PLONGEES_AVEC_ETAT =
			"SELECT * FROM " + DatabaseHelper.PLONGEE_NOM_TABLE +
			" WHERE " + DatabaseHelper.PLONGEE_ETAT + " = ?;";

	private static final String PLONGEE_SELECT_AVEC_ID = 
			SELECT_TOUTES_LES_PLONGEES + 
			" WHERE " + DatabaseHelper.PLONGEE_CLE + 
			" = ? ORDER BY " + DatabaseHelper.PLONGEE_DATE
			+ " DESC;";
	
	private static final String SELECT_TOUS_LES_DP =
			"SELECT " + DatabaseHelper.PLONGEE_DIRECTEUR +
			" FROM " + DatabaseHelper.PLONGEE_NOM_TABLE + ";";
	
	private static final String SELECT_TOUS_LES_LIEUX =
			"SELECT " + DatabaseHelper.PLONGEE_LIEU +
			" FROM " + DatabaseHelper.PLONGEE_NOM_TABLE + ";";

	public PlongeeDAO(SQLiteDatabase mDatabase) {
		super(mDatabase);
	}
	
	@Override
	public long insert(Plongee plongee) {
		ContentValues values = new ContentValues();
		if (plongee.getmEtat() != null)
			values.put(DatabaseHelper.PLONGEE_ETAT, plongee.getmEtat().name());
		values.put(DatabaseHelper.PLONGEE_DIRECTEUR, plongee.getmDirecteurPlongee());
		values.put(DatabaseHelper.PLONGEE_LIEU, plongee.getmLieu());
		values.put(DatabaseHelper.PLONGEE_NOTE, plongee.getmNote());
		values.put(DatabaseHelper.PLONGEE_DATE_CREATION, plongee.getmDateCreation());
		values.put(DatabaseHelper.PLONGEE_DATE, plongee.getmDatePlongee());
		if (plongee.getmHoraire() != null)
			values.put(DatabaseHelper.PLONGEE_HORAIRE, plongee.getmHoraire().name());
		values.put(DatabaseHelper.PLONGEE_DERNIERE_ACTIVITE, plongee.getmDerniereActivite());
		return mDatabase.insert(DatabaseHelper.PLONGEE_NOM_TABLE, null, values);
	}

	@Override
	public boolean delete(Plongee plongee) {
		return mDatabase.delete(DatabaseHelper.PLONGEE_NOM_TABLE, PLONGEE_DELETE, new String[] {String.valueOf(plongee.getmId())}) != 0;
	}

	@Override
	public boolean update(Plongee plongee) {
		ContentValues values = new ContentValues();
		if (plongee.getmEtat() != null)
			values.put(DatabaseHelper.PLONGEE_ETAT, plongee.getmEtat().name());
		values.put(DatabaseHelper.PLONGEE_DIRECTEUR, plongee.getmDirecteurPlongee());
		values.put(DatabaseHelper.PLONGEE_LIEU, plongee.getmLieu());
		values.put(DatabaseHelper.PLONGEE_NOTE, plongee.getmNote());
		values.put(DatabaseHelper.PLONGEE_DATE_CREATION, plongee.getmDateCreation());
		values.put(DatabaseHelper.PLONGEE_DATE, plongee.getmDatePlongee());
		if (plongee.getmHoraire() != null)
			values.put(DatabaseHelper.PLONGEE_HORAIRE, plongee.getmHoraire().name());
		values.put(DatabaseHelper.PLONGEE_DERNIERE_ACTIVITE, plongee.getmDerniereActivite());
		return mDatabase.update(DatabaseHelper.PLONGEE_NOM_TABLE, values, PLONGEE_UPDATE, new String[] {String.valueOf(plongee.getmId())}) != 0;
	}

	@Override
	public Plongee select(long id) {
		Cursor cursor = mDatabase.rawQuery(PLONGEE_SELECT_AVEC_ID, new String[] {String.valueOf(id)});
		Plongee p = null;
		while(cursor.moveToNext()) {
			p = new Plongee(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEE_CLE)),
					null,
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEE_DATE_CREATION)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEE_DATE)),
					null,
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_DIRECTEUR)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_LIEU)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_NOTE)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_DERNIERE_ACTIVITE)));
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_ETAT)) != null)
				p.setmEtat(Enum.valueOf(PlongeeEtatEnum.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_ETAT))));
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_HORAIRE)) != null)
				p.setmHoraire(Enum.valueOf(PlongeeHoraireEnum.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_HORAIRE))));
			
			// Sélection des paramètres de la plongée
			ParametrePlongeeDAO paramPlongDAO = new ParametrePlongeeDAO(mDatabase);
			p.setmListeParametres(paramPlongDAO.selectParametresPlongeeAvecIdPlongee(p.getmId()));
			
			// Sélection des tours de la plongée
			TourDAO tDAO = new TourDAO(mDatabase);
			p.setmListeTours(tDAO.selectToursAvecIdPlongee(p.getmId()));
			
			// Sélection des plongeurs participants à la plongée
			PlongeurDAO plongeurDAO = new PlongeurDAO(mDatabase);
			p.setmListePlongeurs(plongeurDAO.selectPlongeursAvecIdPlongee(p.getmId()));
			
			// Sélection des sécurité surface pour les tours de la plongée
			SecuriteSurfaceDAO secuSurfaceDAO = new SecuriteSurfaceDAO(mDatabase);
			for (Tour tour : p.getmListeTours()) {
				tour.setmListeSecuritesSurface(secuSurfaceDAO.selectSecuritesSurfaceAvecIdTour(tour.getmId()));
			}
			
			// Sélection des palanquées pour les tours de la plongée
			PalanqueeDAO palanqueeDAO = new PalanqueeDAO(mDatabase);
			for (Tour tour : p.getmListeTours()) {
				tour.setmListePalanquees(palanqueeDAO.selectPalanqueesAvecIdTour(tour.getmId()));
			}
			
			// Sélection des paramètres des palanquées pour les tours de la plongée
			ParametrePalanqueeDAO paramPalanqueeDAO = new ParametrePalanqueeDAO(mDatabase);
			for (Tour tour : p.getmListeTours()) {
				for (Palanquee palanquee : tour.getmListePalanquees()) {
					palanquee.setmListeParametresPalanquee(paramPalanqueeDAO.selectParametresPalanqueeAvecIdPalanquee(palanquee.getmId()));
				}
			}
			
			// Sélection des paramètres des palanquées pour les tours de la plongée			
			for (Tour tour : p.getmListeTours()) {
				for (Palanquee palanquee : tour.getmListePalanquees()) {
					int posPlongeur = 0;
					for (Plongeur plongeur : plongeurDAO.selectPlongeursAvecIdPalanquee(palanquee.getmId())) {
						if (plongeur.getmTypePlongeur() == PlongeurTypeEnum.ENCADRANT) {
							palanquee.ajouterEncadrant(plongeur);
						}
						else if (plongeur.getmTypePlongeur() == PlongeurTypeEnum.SERRE_FILE) {
							palanquee.ajouterSerreFile(plongeur);
						}
						else if (plongeur.getmTypePlongeur() == PlongeurTypeEnum.PLONGEUR) {
							palanquee.ajouterPlongeur(plongeur, posPlongeur);
							posPlongeur++;
						}
					}
				}
			}
		}
		return p;
	}
	
	public List<Plongee> selectPlongees() {
		Cursor cursor = mDatabase.rawQuery(SELECT_TOUTES_LES_PLONGEES, null);
		List<Plongee> listePlongees = new ArrayList<Plongee>();
		while(cursor.moveToNext()) {
			Plongee p = new Plongee(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEE_CLE)),
					null,
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEE_DATE_CREATION)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEE_DATE)),
					null,
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_DIRECTEUR)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_LIEU)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_NOTE)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_DERNIERE_ACTIVITE)));
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_ETAT)) != null)
				p.setmEtat(Enum.valueOf(PlongeeEtatEnum.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_ETAT))));
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_HORAIRE)) != null)
				p.setmHoraire(Enum.valueOf(PlongeeHoraireEnum.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_HORAIRE))));
			listePlongees.add(p);
		}
		return listePlongees;
	}
	
	public List<Plongee> selectPlongeesAvecEtat(PlongeeEtatEnum etat) {
		Cursor cursor = mDatabase.rawQuery(SELECT_PLONGEES_AVEC_ETAT, new String[] {etat.name()});
		List<Plongee> listePlongees = new ArrayList<Plongee>();
		while(cursor.moveToNext()) {
			Plongee p = new Plongee(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEE_CLE)),
					null,
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEE_DATE_CREATION)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEE_DATE)),
					null,
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_DIRECTEUR)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_LIEU)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_NOTE)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_DERNIERE_ACTIVITE)));
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_ETAT)) != null)
				p.setmEtat(Enum.valueOf(PlongeeEtatEnum.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_ETAT))));
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_HORAIRE)) != null)
				p.setmHoraire(Enum.valueOf(PlongeeHoraireEnum.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_HORAIRE))));
			
			listePlongees.add(p);
		}		
		return listePlongees;
	}
	
	public Set<String> selectTousLesDP() {
		Cursor cursor = mDatabase.rawQuery(SELECT_TOUS_LES_DP, null);
		Set<String> setDP = new HashSet<String>();
		while(cursor.moveToNext()) {
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_DIRECTEUR)) != null) {
				setDP.add(Normalizer.normalize(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_DIRECTEUR)), Normalizer.Form.NFD));
			}
		}
		return setDP;
	}

	public Set<String> selectTousLesLieux() {
		Cursor cursor = mDatabase.rawQuery(SELECT_TOUS_LES_LIEUX, null);
		Set<String> setDP = new HashSet<String>();
		while(cursor.moveToNext()) {
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_LIEU)) != null) {
				setDP.add(Normalizer.normalize(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEE_LIEU)), Normalizer.Form.NFD));
			}
		}
		return setDP;
	}
}
