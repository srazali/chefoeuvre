package fr.enac.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import fr.enac.models.SecuriteSurface;

/**
 * Classe SecuriteSurfaceDAO représentant la relation entre
 * un objet SecuriteSurface et la base de données.
 * 
 * @author Sophien Razali
 *
 */
public class SecuriteSurfaceDAO extends DAO<SecuriteSurface> {

	private static final String SECURITE_SURFACE_UPDATE = 
			DatabaseHelper.SECURITE_SURFACE_CLE + " = ?;";
	
	private static final String SECURITE_SURFACE_DELETE =
			DatabaseHelper.SECURITE_SURFACE_CLE + " = ?;";
	
	private static final String SELECT_TOUTES_LES_SECURITE_SURFACE = 
			"SELECT * FROM " + DatabaseHelper.SECURITE_SURFACE_NOM_TABLE;
	
	private static final String SECURITE_SURFACE_SELECT_AVEC_ID_TOUR = 
			"SELECT * FROM " + DatabaseHelper.SECURITE_SURFACE_NOM_TABLE + 
			" WHERE " + DatabaseHelper.SECURITE_SURFACE_CLE_TOUR + " = ?;";

	private static final String SECURITE_SURFACE_SELECT_AVEC_ID = 
			"SELECT * FROM " + DatabaseHelper.SECURITE_SURFACE_NOM_TABLE + 
			" WHERE " + DatabaseHelper.SECURITE_SURFACE_CLE + " = ?;";
	
	public SecuriteSurfaceDAO(SQLiteDatabase mDatabase) {
		super(mDatabase);
	}
	
	@Override
	public long insert(SecuriteSurface securiteSurface) {
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.SECURITE_SURFACE_NOM, securiteSurface.getmName());
		values.put(DatabaseHelper.SECURITE_SURFACE_CLE_TOUR, securiteSurface.getmIdTour());
		return mDatabase.insert(DatabaseHelper.SECURITE_SURFACE_NOM_TABLE, null, values);
	}

	@Override
	public boolean delete(SecuriteSurface securiteSurface) {
		return mDatabase.delete(DatabaseHelper.SECURITE_SURFACE_NOM_TABLE, SECURITE_SURFACE_DELETE, new String[] {String.valueOf(securiteSurface.getmId())}) != 0;
	}

	@Override
	public boolean update(SecuriteSurface securiteSurface) {
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.SECURITE_SURFACE_NOM, securiteSurface.getmName());
		values.put(DatabaseHelper.SECURITE_SURFACE_CLE_TOUR, securiteSurface.getmIdTour());
		return mDatabase.update(DatabaseHelper.SECURITE_SURFACE_NOM_TABLE, values, SECURITE_SURFACE_UPDATE, new String[] {String.valueOf(securiteSurface.getmId())}) != 0;
	}

	@Override
	public SecuriteSurface select(long id) {
		Cursor cursor = mDatabase.rawQuery(SECURITE_SURFACE_SELECT_AVEC_ID, new String[] {String.valueOf(id)});
		SecuriteSurface s = null;
		while(cursor.moveToNext()) {
			s = new SecuriteSurface(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.SECURITE_SURFACE_CLE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.SECURITE_SURFACE_CLE_TOUR)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.SECURITE_SURFACE_NOM)));
			
		}
		return s;
	}
	
	public List<SecuriteSurface> selectSecuritesSurface() {
		Cursor cursor = mDatabase.rawQuery(SELECT_TOUTES_LES_SECURITE_SURFACE, null);
		List<SecuriteSurface> listeSecuriteSurface = new ArrayList<SecuriteSurface>();
		while(cursor.moveToNext()) {
			SecuriteSurface s = new SecuriteSurface(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.SECURITE_SURFACE_CLE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.SECURITE_SURFACE_CLE_TOUR)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.SECURITE_SURFACE_NOM)));
			listeSecuriteSurface.add(s);
			
		}
		return listeSecuriteSurface;
	}
	
	public List<SecuriteSurface> selectSecuritesSurfaceAvecIdTour(long id) {
		Cursor cursor = mDatabase.rawQuery(SECURITE_SURFACE_SELECT_AVEC_ID_TOUR, new String[] {String.valueOf(id)});
		List<SecuriteSurface> listeSecuriteSurface = new ArrayList<SecuriteSurface>();
		while(cursor.moveToNext()) {
			SecuriteSurface s = new SecuriteSurface(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.SECURITE_SURFACE_CLE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.SECURITE_SURFACE_CLE_TOUR)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.SECURITE_SURFACE_NOM)));
			listeSecuriteSurface.add(s);
			
		}
		return listeSecuriteSurface;
	}
}
