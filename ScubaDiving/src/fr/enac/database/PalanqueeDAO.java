package fr.enac.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import fr.enac.lii.CDS.TypeGaz;
import fr.enac.lii.CDS.TypePlongee;
import fr.enac.models.Palanquee;

/**
 * Classe PalanqueeDAO représentant la relation entre
 * un objet Palanquee et la base de données.
 * 
 * @author Sophien Razali
 *
 */
public class PalanqueeDAO extends DAO<Palanquee> {
	private static final String PALANQUEE_UPDATE = 
			DatabaseHelper.PALANQUEE_CLE + " = ?;";
	
	private static final String PALANQUEE_DELETE =
			DatabaseHelper.PALANQUEE_CLE + " = ?;";
	
	private static final String SELECT_TOUTES_LES_PALANQUEES = 
			"SELECT * FROM " + DatabaseHelper.PALANQUEE_NOM_TABLE;
	
	private static final String PALANQUEE_SELECT_AVEC_ID_TOUR = 
			"SELECT * FROM " + DatabaseHelper.PALANQUEE_NOM_TABLE + 
			" WHERE " + DatabaseHelper.PALANQUEE_CLE_TOUR + " = ?;";

	private static final String PALANQUEE_SELECT_AVEC_ID = 
			"SELECT * FROM " + DatabaseHelper.PALANQUEE_NOM_TABLE + 
			" WHERE " + DatabaseHelper.PALANQUEE_CLE + " = ?;";
	
	public PalanqueeDAO(SQLiteDatabase mDatabase) {
		super(mDatabase);
	}
	
	@Override
	public long insert(Palanquee palanquee) {
		ContentValues values = new ContentValues();
		if (palanquee.getTypeGaz() != null)
			values.put(DatabaseHelper.PALANQUEE_TYPE_GAZ, palanquee.getTypeGaz().name());
		if (palanquee.getTypePlongee() != null)
			values.put(DatabaseHelper.PALANQUEE_TYPE_PLONGEE, palanquee.getTypePlongee().name());
		values.put(DatabaseHelper.PALANQUEE_CLE_TOUR, palanquee.getmIdTour());
		values.put(DatabaseHelper.PALANQUEE_ETAT, palanquee.getmEtat().getId());
		return mDatabase.insert(DatabaseHelper.PALANQUEE_NOM_TABLE, null, values);
	}

	@Override
	public boolean delete(Palanquee palanquee) {
		return mDatabase.delete(DatabaseHelper.PALANQUEE_NOM_TABLE, PALANQUEE_DELETE, new String[] {String.valueOf(palanquee.getmId())}) != 0;
	}

	@Override
	public boolean update(Palanquee palanquee) {
		ContentValues values = new ContentValues();
		if (palanquee.getTypeGaz() != null)
			values.put(DatabaseHelper.PALANQUEE_TYPE_GAZ, palanquee.getTypeGaz().name());
		if (palanquee.getTypePlongee() != null) {
			values.put(DatabaseHelper.PALANQUEE_TYPE_PLONGEE, palanquee.getTypePlongee().name());
			Log.e("UPDATE", palanquee.getTypePlongee().name());
		}
		values.put(DatabaseHelper.PALANQUEE_CLE_TOUR, palanquee.getmIdTour());
		values.put(DatabaseHelper.PALANQUEE_ETAT, palanquee.getmEtat().getId());
		return mDatabase.update(DatabaseHelper.PALANQUEE_NOM_TABLE, values, PALANQUEE_UPDATE, new String[] {String.valueOf(palanquee.getmId())}) != 0;
	}

	@Override
	public Palanquee select(long id) {
		Cursor cursor = mDatabase.rawQuery(PALANQUEE_SELECT_AVEC_ID, new String[] {String.valueOf(id)});
		Palanquee p = null;
		while(cursor.moveToNext()) {
			p = new Palanquee(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_CLE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_CLE_TOUR)));
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_TYPE_GAZ)) != null)
				p.setTypeGaz(Enum.valueOf(TypeGaz.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_TYPE_GAZ))));
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_TYPE_PLONGEE)) != null)
				p.setTypePlongee(Enum.valueOf(TypePlongee.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_TYPE_PLONGEE))));
			p.setmEtat(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_ETAT)));
		}
		return p;
	}
	
	public List<Palanquee> selectPalanquees() {
		Cursor cursor = mDatabase.rawQuery(SELECT_TOUTES_LES_PALANQUEES, null);
		List<Palanquee> listePalanquees = new ArrayList<Palanquee>();
		while(cursor.moveToNext()) {
			Palanquee p = new Palanquee(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_CLE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_CLE_TOUR)));
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_TYPE_GAZ)) != null)
				p.setTypeGaz(Enum.valueOf(TypeGaz.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_TYPE_GAZ))));
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_TYPE_PLONGEE)) != null)
				p.setTypePlongee(Enum.valueOf(TypePlongee.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_TYPE_PLONGEE))));
			p.setmEtat(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_ETAT)));
			listePalanquees.add(p);
		}
		return listePalanquees;
	}
	
	public List<Palanquee> selectPalanqueesAvecIdTour(long id) {
		Cursor cursor = mDatabase.rawQuery(PALANQUEE_SELECT_AVEC_ID_TOUR, new String[] {String.valueOf(id)});
		List<Palanquee> listePalanquees = new ArrayList<Palanquee>();
		while(cursor.moveToNext()) {
			Palanquee p = new Palanquee(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_CLE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_CLE_TOUR)));
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_TYPE_GAZ)) != null)
				p.setTypeGaz(Enum.valueOf(TypeGaz.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_TYPE_GAZ))));
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_TYPE_PLONGEE)) != null)
				p.setTypePlongee(Enum.valueOf(TypePlongee.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_TYPE_PLONGEE))));
			p.setmEtat(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PALANQUEE_ETAT)));
			listePalanquees.add(p);
		}
		return listePalanquees;
	}
	
}
