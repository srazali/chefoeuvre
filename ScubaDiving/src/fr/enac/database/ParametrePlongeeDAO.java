package fr.enac.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import fr.enac.models.ParametrePlongee;
import fr.enac.models.ParametrePlongeeCategorieEnum;

/**
 * Classe ParametrePlongeeDAO représentant la relation entre
 * un objet ParametrePlongee et la base de données.
 * 
 * @author Sophien Razali
 *
 */
public class ParametrePlongeeDAO extends DAO<ParametrePlongee> {
	private static final String PARAMETRE_PLONGEE_UPDATE = 
			DatabaseHelper.PARAMETRE_PLONGEE_CLE + " = ?;";
	
	private static final String PARAMETRE_PLONGEE_DELETE =
			DatabaseHelper.PARAMETRE_PLONGEE_CLE + " = ?;";
	
	private static final String SELECT_TOUS_LES_PARAMETRES_PLONGEE = 
			"SELECT * FROM " + DatabaseHelper.PARAMETRE_PLONGEE_NOM_TABLE;
	
	private static final String PARAMETRE_PLONGEE_SELECT_AVEC_ID_PLONGEE = 
			"SELECT * FROM " + DatabaseHelper.PARAMETRE_PLONGEE_NOM_TABLE + 
			" WHERE " + DatabaseHelper.PARAMETRE_PLONGEE_CLE_PLONGEE + " = ?;";

	private static final String PARAMETRE_PLONGEE_SELECT_AVEC_ID = 
			"SELECT * FROM " + DatabaseHelper.PARAMETRE_PLONGEE_NOM_TABLE + 
			" WHERE " + DatabaseHelper.PARAMETRE_PLONGEE_CLE + " = ?;";
	
	public ParametrePlongeeDAO(SQLiteDatabase mDatabase) {
		super(mDatabase);
	}
	
	@Override
	public long insert(ParametrePlongee param) {
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.PARAMETRE_PLONGEE_NOM, param.getmNom());
		values.put(DatabaseHelper.PARAMETRE_PLONGEE_ICONE, param.getmIconId());
		values.put(DatabaseHelper.PARAMETRE_PLONGEE_DETAILS, param.getmDetails());
		if (param.getmCategorie() != null)
			values.put(DatabaseHelper.PARAMETRE_PLONGEE_CATEGORIE, param.getmCategorie().name());
		values.put(DatabaseHelper.PARAMETRE_PLONGEE_CLE_PLONGEE, param.getmIdPlongee());
		return mDatabase.insert(DatabaseHelper.PARAMETRE_PLONGEE_NOM_TABLE, null, values);
	}

	@Override
	public boolean delete(ParametrePlongee param) {
		return mDatabase.delete(DatabaseHelper.PARAMETRE_PLONGEE_NOM_TABLE, PARAMETRE_PLONGEE_DELETE, new String[] {String.valueOf(param.getmId())}) != 0;
	}

	@Override
	public boolean update(ParametrePlongee param) {
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.PARAMETRE_PLONGEE_NOM, param.getmNom());
		values.put(DatabaseHelper.PARAMETRE_PLONGEE_ICONE, param.getmIconId());
		values.put(DatabaseHelper.PARAMETRE_PLONGEE_DETAILS, param.getmDetails());
		if (param.getmCategorie() != null)
			values.put(DatabaseHelper.PARAMETRE_PLONGEE_CATEGORIE, param.getmCategorie().name());
		values.put(DatabaseHelper.PARAMETRE_PLONGEE_CLE_PLONGEE, param.getmIdPlongee());
		return mDatabase.update(DatabaseHelper.PARAMETRE_PLONGEE_NOM_TABLE, values, PARAMETRE_PLONGEE_UPDATE, new String[] {String.valueOf(param.getmId())}) != 0;
	}

	@Override
	public ParametrePlongee select(long id) {
		Cursor cursor = mDatabase.rawQuery(PARAMETRE_PLONGEE_SELECT_AVEC_ID, new String[] {String.valueOf(id)});
		ParametrePlongee pp = null;
		while(cursor.moveToNext()) {
			pp = new ParametrePlongee(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_CLE)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_NOM)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_ICONE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_CLE_PLONGEE)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_DETAILS)),
					null);
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_CATEGORIE)) != null) 
					pp.setmCategorie(Enum.valueOf(ParametrePlongeeCategorieEnum.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_CATEGORIE))));
		}
		return pp;
	}
	
	public List<ParametrePlongee> selectParametresPlongee() {
		Cursor cursor = mDatabase.rawQuery(SELECT_TOUS_LES_PARAMETRES_PLONGEE, null);
		List<ParametrePlongee> listeParametresPlongee = new ArrayList<ParametrePlongee>();
		while(cursor.moveToNext()) {
			ParametrePlongee pp = new ParametrePlongee(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_CLE)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_NOM)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_ICONE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_CLE_PLONGEE)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_DETAILS)),
					null);
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_CATEGORIE)) != null) 
					pp.setmCategorie(Enum.valueOf(ParametrePlongeeCategorieEnum.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_CATEGORIE))));
			listeParametresPlongee.add(pp);
			
		}
		return listeParametresPlongee;
	}
	
	public List<ParametrePlongee> selectParametresPlongeeAvecIdPlongee(long id) {
		Cursor cursor = mDatabase.rawQuery(PARAMETRE_PLONGEE_SELECT_AVEC_ID_PLONGEE, new String[] {String.valueOf(id)});
		List<ParametrePlongee> listeParametresPlongee = new ArrayList<ParametrePlongee>();
		while(cursor.moveToNext()) {
			ParametrePlongee pp = new ParametrePlongee(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_CLE)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_NOM)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_ICONE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_CLE_PLONGEE)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_DETAILS)),
					null);
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_CATEGORIE)) != null) 
					pp.setmCategorie(Enum.valueOf(ParametrePlongeeCategorieEnum.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PLONGEE_CATEGORIE))));
			listeParametresPlongee.add(pp);
		}
		return listeParametresPlongee;
	}
}
