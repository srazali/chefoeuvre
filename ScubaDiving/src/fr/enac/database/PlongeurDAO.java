package fr.enac.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import fr.enac.models.Plongeur;
import fr.enac.models.PlongeurTypeEnum;

/**
 * Classe PlongeurDAO représentant la relation entre
 * un objet Plongeur et la base de données.
 * 
 * @author Sophien Razali
 *
 */
public class PlongeurDAO extends DAO<Plongeur> {

	private static final String PLONGEUR_UPDATE = 
			DatabaseHelper.PLONGEUR_CLE + " = ?;";
	
	private static final String PLONGEUR_DELETE =
			DatabaseHelper.PLONGEUR_CLE + " = ?;";
	
	private static final String PLONGEUR_ALL_DELETE_AVEC_ID_PLONGEE =
			DatabaseHelper.PLONGEUR_CLE_PLONGEE + " = ?;";
	
	private static final String SELECT_TOUS_LES_PLONGEURS = 
			"SELECT * FROM " + DatabaseHelper.PLONGEUR_NOM_TABLE;
	
	private static final String PLONGEUR_SELECT_AVEC_ID_PLONGEE = 
			"SELECT * FROM " + DatabaseHelper.PLONGEUR_NOM_TABLE + 
			" WHERE " + DatabaseHelper.PLONGEUR_CLE_PLONGEE + " = ?;";
	
	private static final String PLONGEUR_SELECT_AVEC_ID = 
			"SELECT * FROM " + DatabaseHelper.PLONGEUR_NOM_TABLE + 
			" WHERE " + DatabaseHelper.PLONGEUR_CLE + " = ?;";
	
	private static final String PLONGEUR_SELECT_AVEC_ID_PALANQUEE = 
			"SELECT * FROM " + DatabaseHelper.APPARTENANCE_PLONGEUR_PALANQUEE_NOM_TABLE + 
			" WHERE " + DatabaseHelper.APPARTENANCE_PLONGEUR_PALANQUEE_CLE_PALANQUEE + " = ?;";
	
	private static final String PLONGEUR_SELECT_AVEC_ID_PLONGEE_QUI_PARTICIPENT =
			"SELECT * FROM " + DatabaseHelper.APPARTENANCE_PLONGEUR_PALANQUEE_NOM_TABLE + 
			" WHERE " + DatabaseHelper.APPARTENANCE_PLONGEUR_PALANQUEE_CLE_PALANQUEE + " = ?" +
					"AND "+DatabaseHelper.PLONGEUR_PARTICIPE+"=1;";
	
	private static final String PLONGEUR_DELETE_DE_PALANQUEE =
			DatabaseHelper.APPARTENANCE_PLONGEUR_PALANQUEE_CLE_PLONGEUR + " = ?" +
			" AND " + DatabaseHelper.APPARTENANCE_PLONGEUR_PALANQUEE_CLE_PALANQUEE + " = ?;";
	
	public PlongeurDAO(SQLiteDatabase mDatabase) {
		super(mDatabase);
	}
	
	@Override
	public long insert(Plongeur plongeur) {		
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.PLONGEUR_NOM, plongeur.getmNom());
		values.put(DatabaseHelper.PLONGEUR_PRENOM, plongeur.getmPrenom());
		values.put(DatabaseHelper.PLONGEUR_CLE_PLONGEE, plongeur.getmIdPlongee());
		values.put(DatabaseHelper.PLONGEUR_PARTICIPE, plongeur.getmParticipe().getId());
		values.put(DatabaseHelper.PLONGEUR_CERTIFICAT, plongeur.getmCertificat().getId());
		values.put(DatabaseHelper.PLONGEUR_DIPLOME, plongeur.getmDiplome().getId());
		values.put(DatabaseHelper.PLONGEUR_LICENCE, plongeur.getmDiplome().getId());
		values.put(DatabaseHelper.PLONGEUR_NIVEAU_CDS, plongeur.toString());	
		return mDatabase.insert(DatabaseHelper.PLONGEUR_NOM_TABLE, null, values);
	}

	@Override
	public boolean delete(Plongeur plongeur) {
		return mDatabase.delete(DatabaseHelper.PLONGEUR_NOM_TABLE, PLONGEUR_DELETE, new String[] {String.valueOf(plongeur.getmId())}) != 0;
	}
	
	/**
	 * Supprime tous les plongeurs d'une plongée
	 * @param idPlongee
	 * @return
	 */
	public boolean deleteAllPlongeursByIdPlongee(long idPlongee) {
		return mDatabase.delete(DatabaseHelper.PLONGEUR_NOM_TABLE, PLONGEUR_ALL_DELETE_AVEC_ID_PLONGEE, new String[] {String.valueOf(idPlongee)}) != 0;
	}

	@Override
	public boolean update(Plongeur plongeur) {
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.PLONGEUR_NOM, plongeur.getmNom());
		values.put(DatabaseHelper.PLONGEUR_PRENOM, plongeur.getmPrenom());
		values.put(DatabaseHelper.PLONGEUR_CLE_PLONGEE, plongeur.getmIdPlongee());
		values.put(DatabaseHelper.PLONGEUR_PARTICIPE, plongeur.getmParticipe().getId());
		values.put(DatabaseHelper.PLONGEUR_CERTIFICAT, plongeur.getmCertificat().getId());
		values.put(DatabaseHelper.PLONGEUR_DIPLOME, plongeur.getmDiplome().getId());
		values.put(DatabaseHelper.PLONGEUR_LICENCE, plongeur.getmDiplome().getId());
		values.put(DatabaseHelper.PLONGEUR_NIVEAU_CDS, plongeur.toString());
		return mDatabase.update(DatabaseHelper.PLONGEUR_NOM_TABLE, values, PLONGEUR_UPDATE, new String[] {String.valueOf(plongeur.getmId())}) != 0;
	}

	@Override
	public Plongeur select(long id) {
		Cursor cursor = mDatabase.rawQuery(PLONGEUR_SELECT_AVEC_ID, new String[] {String.valueOf(id)});
		Plongeur p = null;
		while(cursor.moveToNext()) {
			p = new Plongeur(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_CLE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_CLE_PLONGEE)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_NOM)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_PRENOM)));
			p.setmParticipe(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_PARTICIPE)));
			p.setmCertificat(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_CERTIFICAT)));
			p.setmDiplome(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_DIPLOME)));
			p.setmLicence(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_LICENCE)));
			p.specifierNiveauCDS(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_NIVEAU_CDS)));
		}
		return p;
	}
	
	public List<Plongeur> selectPlongeurs() {
		Cursor cursor = mDatabase.rawQuery(SELECT_TOUS_LES_PLONGEURS, null);
		List<Plongeur> listePlongeurs = new ArrayList<Plongeur>();
		while(cursor.moveToNext()) {
			Plongeur p = new Plongeur(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_CLE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_CLE_PLONGEE)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_NOM)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_PRENOM)));
			p.setmParticipe(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_PARTICIPE)));
			p.setmCertificat(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_CERTIFICAT)));
			p.setmDiplome(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_DIPLOME)));
			p.setmLicence(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_LICENCE)));
			p.specifierNiveauCDS(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_NIVEAU_CDS)));
			listePlongeurs.add(p);
		}
		return listePlongeurs;
	}
	
	public List<Plongeur> selectPlongeursAvecIdPlongee(long id) {
		Cursor cursor = mDatabase.rawQuery(PLONGEUR_SELECT_AVEC_ID_PLONGEE, new String[] {String.valueOf(id)});
		List<Plongeur> listePlongeurs = new ArrayList<Plongeur>();
		while(cursor.moveToNext()) {
			Plongeur p = new Plongeur(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_CLE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_CLE_PLONGEE)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_NOM)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_PRENOM)));
			p.setmParticipe(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_PARTICIPE)));
			p.setmCertificat(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_CERTIFICAT)));
			p.setmDiplome(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_DIPLOME)));
			p.setmLicence(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_LICENCE)));
			p.specifierNiveauCDS(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_NIVEAU_CDS)));
			Log.e("PLONGEURDAO", "selectniveau = "+p.toString());
			listePlongeurs.add(p);
		}
		return listePlongeurs;
	}
	
	public List<Plongeur> selectPlongeursAvecIdPlongeeQuiParticipent(long id) {
		Cursor cursor = mDatabase.rawQuery(PLONGEUR_SELECT_AVEC_ID_PLONGEE_QUI_PARTICIPENT, new String[] {String.valueOf(id)});
		List<Plongeur> listePlongeurs = new ArrayList<Plongeur>();
		while(cursor.moveToNext()) {
			Plongeur p = new Plongeur(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_CLE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_CLE_PLONGEE)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_NOM)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_PRENOM)));
			p.setmParticipe(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_PARTICIPE)));
			p.setmCertificat(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_CERTIFICAT)));
			p.setmDiplome(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_DIPLOME)));
			p.setmLicence(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_LICENCE)));
			p.specifierNiveauCDS(cursor.getString(cursor.getColumnIndex(DatabaseHelper.PLONGEUR_NIVEAU_CDS)));
			listePlongeurs.add(p);
		}
		return listePlongeurs;
	}

	public long insertPlongeurDansPalanquee(Plongeur plongeur, long idPalanquee) {
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.APPARTENANCE_PLONGEUR_PALANQUEE_CLE_PALANQUEE, idPalanquee);
		values.put(DatabaseHelper.APPARTENANCE_PLONGEUR_PALANQUEE_CLE_PLONGEUR, plongeur.getmId());
		values.put(DatabaseHelper.APPARTENANCE_PLONGEUR_PALANQUEE_PLONGEUR_TYPE, plongeur.getmTypePlongeur().name());
		return mDatabase.insert(DatabaseHelper.APPARTENANCE_PLONGEUR_PALANQUEE_NOM_TABLE, null, values);
	}
	
	public boolean deletePlongeurDePalanquee(Plongeur plongeur, long idPalanquee) {
		return mDatabase.delete(DatabaseHelper.APPARTENANCE_PLONGEUR_PALANQUEE_NOM_TABLE, PLONGEUR_DELETE_DE_PALANQUEE, new String[] {String.valueOf(plongeur.getmId()), String.valueOf(idPalanquee)}) != 0;
		
	}
	
	public List<Plongeur> selectPlongeursAvecIdPalanquee(long idPalanquee) {
		Cursor cursor = mDatabase.rawQuery(PLONGEUR_SELECT_AVEC_ID_PALANQUEE, new String[] {String.valueOf(idPalanquee)});
		List<Plongeur> listePlongeurs = new ArrayList<Plongeur>();
		while(cursor.moveToNext()) {
			Plongeur p = select(cursor.getLong(cursor.getColumnIndex(DatabaseHelper.APPARTENANCE_PLONGEUR_PALANQUEE_CLE_PLONGEUR)));
			p.setmTypePlongeur(Enum.valueOf(PlongeurTypeEnum.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.APPARTENANCE_PLONGEUR_PALANQUEE_PLONGEUR_TYPE))));
			listePlongeurs.add(p);
		}
		return listePlongeurs;
	}
}
