package fr.enac.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Classe DatabaseHelper issue de SQLiteOpenHelper représentant
 * la base de données de l'application.
 * C'est dans cette classe qu'a lieu la création des tables.
 * 
 * @author Sophien Razali
 *
 */
public class DatabaseHelper extends SQLiteOpenHelper {

	private static DatabaseHelper mInstance = null;

	/**
	 * La version de la base de données.
	 * Si des modifications sont apportées à la structure de la base de données,
	 * il faudra modifier ce numéro (augmenter la valeur). Afin de déclencher
	 * une mise à jour de la base de données.
	 * 
	 * @see DatabaseHelper.onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	 */
	public final static int VERSION = 54;
	
	/**
	 * Le nom de la base de données.
	 */
	public final static String NOM = "database.db";

	/*--------------------- TABLE PLONGEE ---------------------*/
	
	public static final String PLONGEE_CLE = "_id";
	public static final String PLONGEE_ETAT = "etat";
	public static final String PLONGEE_DIRECTEUR = "directeur_plongee";
	public static final String PLONGEE_LIEU = "lieu";
	public static final String PLONGEE_NOTE = "note";
	public static final String PLONGEE_DATE_CREATION = "date_creation";
	public static final String PLONGEE_DATE = "date";
	public static final String PLONGEE_HORAIRE = "horaire";
	public static final String PLONGEE_DERNIERE_ACTIVITE = "derniere_activite";
	
	public static final String PLONGEE_NOM_TABLE = "Plongee";
	
	public static final String PLONGEE_TABLE_CREATE = "CREATE TABLE " + PLONGEE_NOM_TABLE + " ("
			+ PLONGEE_CLE + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ PLONGEE_ETAT + " TEXT, "
			+ PLONGEE_DIRECTEUR + " TEXT, "
			+ PLONGEE_LIEU + " TEXT, "
			+ PLONGEE_NOTE + " TEXT, "
			+ PLONGEE_DATE_CREATION + " INTEGER, "
			+ PLONGEE_DATE + " INTEGER, "
			+ PLONGEE_HORAIRE + " TEXT, "
			+ PLONGEE_DERNIERE_ACTIVITE + " TEXT);";
	
	public static final String PLONGEE_TABLE_DROP = "DROP TABLE IF EXISTS " + PLONGEE_NOM_TABLE + ";";
	
	/*--------------------- TABLE TOUR ---------------------*/

	public static final String TOUR_CLE = "_id";
	public static final String TOUR_POSITION = "position";
	public static final String TOUR_CLE_PLONGEE = "id_plongee";
	
	public static final String TOUR_NOM_TABLE = "Tour";
	
	public static final String TOUR_TABLE_CREATE = "CREATE TABLE " + TOUR_NOM_TABLE + " ("
			+ TOUR_CLE + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ TOUR_POSITION + " INTEGER NOT NULL, "
			+ TOUR_CLE_PLONGEE + " INTEGER NOT NULL, "
			+ "FOREIGN KEY(" + TOUR_CLE_PLONGEE + ") REFERENCES " + PLONGEE_NOM_TABLE + "(" + PLONGEE_CLE + ") ON DELETE CASCADE);";
	
	public static final String TOUR_TABLE_DROP = "DROP TABLE IF EXISTS " + TOUR_NOM_TABLE + ";";
	
	/*--------------------- TABLE SECURITE SURFACE ---------------------*/

	public static final String SECURITE_SURFACE_CLE = "_id";
	public static final String SECURITE_SURFACE_NOM = "nom";
	public static final String SECURITE_SURFACE_CLE_TOUR = "id_tour";
	
	public static final String SECURITE_SURFACE_NOM_TABLE = "Securite_Surface";
	
	public static final String SECURITE_SURFACE_TABLE_CREATE = "CREATE TABLE " + SECURITE_SURFACE_NOM_TABLE + " ("
			+ SECURITE_SURFACE_CLE + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ SECURITE_SURFACE_NOM + " TEXT NOT NULL, "
			+ SECURITE_SURFACE_CLE_TOUR + " INTEGER NOT NULL, "
			+ "FOREIGN KEY(" + SECURITE_SURFACE_CLE_TOUR + ") REFERENCES " + TOUR_NOM_TABLE + "(" + TOUR_CLE + ") ON DELETE CASCADE);";
	
	public static final String SECURITE_SURFACE_TABLE_DROP = "DROP TABLE IF EXISTS " + SECURITE_SURFACE_NOM_TABLE + ";";
	
	/*--------------------- TABLE PARAMETRE PLONGEE ---------------------*/
	
	public static final String PARAMETRE_PLONGEE_CLE = "_id";
	public static final String PARAMETRE_PLONGEE_NOM = "nom";
	public static final String PARAMETRE_PLONGEE_ICONE = "icone";
	public static final String PARAMETRE_PLONGEE_DETAILS = "details";
	public static final String PARAMETRE_PLONGEE_CATEGORIE = "categorie";
	public static final String PARAMETRE_PLONGEE_CLE_PLONGEE = "id_plongee";
	
	public static final String PARAMETRE_PLONGEE_NOM_TABLE = "Parametre_Plongee";
	
	public static final String PARAMETRE_PLONGEE_TABLE_CREATE = "CREATE TABLE " + PARAMETRE_PLONGEE_NOM_TABLE + " ("
			+ PARAMETRE_PLONGEE_CLE + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ PARAMETRE_PLONGEE_NOM + " TEXT NOT NULL, "
			+ PARAMETRE_PLONGEE_ICONE + " INTEGER, "
			+ PARAMETRE_PLONGEE_DETAILS + " TEXT, "
			+ PARAMETRE_PLONGEE_CATEGORIE + " TEXT, "
			+ PARAMETRE_PLONGEE_CLE_PLONGEE + " INTEGER NOT NULL, "
			+ "FOREIGN KEY(" + PARAMETRE_PLONGEE_CLE_PLONGEE + ") REFERENCES " + PLONGEE_NOM_TABLE + "(" + PLONGEE_CLE + ") ON DELETE CASCADE);";
	
	public static final String PARAMETRE_PLONGEE_TABLE_DROP = "DROP TABLE IF EXISTS " + PARAMETRE_PLONGEE_NOM_TABLE + ";";
	
	/*--------------------- TABLE PALANQUEE ---------------------*/

	public static final String PALANQUEE_CLE = "_id";
	public static final String PALANQUEE_TYPE_GAZ = "type_gaz";
	public static final String PALANQUEE_TYPE_PLONGEE = "type_plongee";
	public static final String PALANQUEE_ETAT = "etat";
	public static final String PALANQUEE_CLE_TOUR = "id_tour";
	
	public static final String PALANQUEE_NOM_TABLE = "Palanquee";
	
	public static final String PALANQUEE_TABLE_CREATE = "CREATE TABLE " + PALANQUEE_NOM_TABLE + " ("
			+ PALANQUEE_CLE + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ PALANQUEE_TYPE_GAZ + " TEXT, "
			+ PALANQUEE_TYPE_PLONGEE + " TEXT, "
			+ PALANQUEE_ETAT + " INTEGER, "
			+ PALANQUEE_CLE_TOUR + " INTEGER NOT NULL, "
			+ "FOREIGN KEY(" + PALANQUEE_CLE_TOUR + ") REFERENCES " + TOUR_NOM_TABLE + "(" + TOUR_CLE + ") ON DELETE CASCADE);";
	
	public static final String PALANQUEE_TABLE_DROP = "DROP TABLE IF EXISTS " + PALANQUEE_NOM_TABLE + ";";

	/*--------------------- TABLE PARAMETRE PALANQUEE ---------------------*/
	
	public static final String PARAMETRE_PALANQUEE_CLE = "_id";
	public static final String PARAMETRE_PALANQUEE_NOM = "nom";
	public static final String PARAMETRE_PALANQUEE_ICONE = "icone";
	public static final String PARAMETRE_PALANQUEE_VALEUR = "valeur";
	public static final String PARAMETRE_PALANQUEE_TYPE = "type";
	public static final String PARAMETRE_PALANQUEE_CLE_PALANQUEE = "id_palanquee";
	
	public static final String PARAMETRE_PALANQUEE_NOM_TABLE = "Parametre_Palanquee";
	
	public static final String PARAMETRE_PALANQUEE_TABLE_CREATE = "CREATE TABLE " + PARAMETRE_PALANQUEE_NOM_TABLE + " ("
			+ PARAMETRE_PALANQUEE_CLE + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ PARAMETRE_PALANQUEE_NOM + " TEXT NOT NULL, "
			+ PARAMETRE_PALANQUEE_ICONE + " INTEGER, "
			+ PARAMETRE_PALANQUEE_VALEUR + " TEXT, "
			+ PARAMETRE_PALANQUEE_TYPE + " TEXT, "
			+ PARAMETRE_PALANQUEE_CLE_PALANQUEE + " INTEGER NOT NULL, "
			+ "FOREIGN KEY(" + PARAMETRE_PALANQUEE_CLE_PALANQUEE + ") REFERENCES " + PALANQUEE_NOM_TABLE + "(" + PALANQUEE_CLE + ") ON DELETE CASCADE);";
	
	public static final String PARAMETRE_PALANQUEE_TABLE_DROP = "DROP TABLE IF EXISTS " + PARAMETRE_PALANQUEE_NOM_TABLE + ";";
	
	/*--------------------- TABLE PLONGEUR ---------------------*/

	public static final String PLONGEUR_CLE = "_id";
	public static final String PLONGEUR_NOM = "nom";
	public static final String PLONGEUR_PRENOM = "prenom";
	public static final String PLONGEUR_CLE_PLONGEE = "id_plongee";
	public static final String PLONGEUR_NIVEAU_CDS = "niveau_cds";
	public static final String PLONGEUR_PARTICIPE = "participe";
	public static final String PLONGEUR_CERTIFICAT = "certificat";
	public static final String PLONGEUR_DIPLOME = "diplome";
	public static final String PLONGEUR_LICENCE = "licence";
	
	public static final String PLONGEUR_NOM_TABLE = "Plongeur";
	
	public static final String PLONGEUR_TABLE_CREATE = "CREATE TABLE " + PLONGEUR_NOM_TABLE + " ("
			+ PLONGEUR_CLE + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ PLONGEUR_NOM + " TEXT NOT NULL, "
			+ PLONGEUR_PRENOM + " TEXT NOT NULL, "
			+ PLONGEUR_CLE_PLONGEE + " INTEGER NOT NULL, "
			+ PLONGEUR_PARTICIPE + " INTEGER NULL, "
			+ PLONGEUR_CERTIFICAT + " INTEGER NULL, "
			+ PLONGEUR_DIPLOME + " INTEGER NULL, "
			+ PLONGEUR_LICENCE + " INTEGER NULL, "
			+ PLONGEUR_NIVEAU_CDS + " TEXT NOT NULL, "
			+ "FOREIGN KEY(" + PLONGEUR_CLE_PLONGEE + ") REFERENCES " + PLONGEE_NOM_TABLE + "(" + PLONGEE_CLE + ") ON DELETE CASCADE);";
	
	public static final String PLONGEUR_TABLE_DROP = "DROP TABLE IF EXISTS " + PLONGEUR_NOM_TABLE + ";";
	
	/*--------------------- TABLE APPARTENANCE PLONGEUR PALANQUEE ---------------------*/

	public static final String APPARTENANCE_PLONGEUR_PALANQUEE_CLE = "_id";
	public static final String APPARTENANCE_PLONGEUR_PALANQUEE_CLE_PALANQUEE = "id_palanquee";
	public static final String APPARTENANCE_PLONGEUR_PALANQUEE_CLE_PLONGEUR = "id_plongeur";
	public static final String APPARTENANCE_PLONGEUR_PALANQUEE_PLONGEUR_TYPE = "type_plongeur";
	
	public static final String APPARTENANCE_PLONGEUR_PALANQUEE_NOM_TABLE = "Appartenance_plongeur_palanquee";
	
	public static final String APPARTENANCE_PLONGEUR_PALANQUEE_TABLE_CREATE = "CREATE TABLE " + APPARTENANCE_PLONGEUR_PALANQUEE_NOM_TABLE + " ("
			+ APPARTENANCE_PLONGEUR_PALANQUEE_CLE + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ APPARTENANCE_PLONGEUR_PALANQUEE_CLE_PALANQUEE + " INTEGER NOT NULL, "
			+ APPARTENANCE_PLONGEUR_PALANQUEE_CLE_PLONGEUR + " INTEGER NOT NULL, "
			+ APPARTENANCE_PLONGEUR_PALANQUEE_PLONGEUR_TYPE + " TEXT NOT NULL, "
			+ "FOREIGN KEY(" + APPARTENANCE_PLONGEUR_PALANQUEE_CLE_PALANQUEE + ") REFERENCES " + PALANQUEE_NOM_TABLE + "(" + PALANQUEE_CLE + ") ON DELETE CASCADE,"
			+ "FOREIGN KEY(" + APPARTENANCE_PLONGEUR_PALANQUEE_CLE_PLONGEUR + ") REFERENCES " + PLONGEUR_NOM_TABLE + "(" + PLONGEUR_CLE + ") ON DELETE CASCADE);";
	
	public static final String APPARTENANCE_PLONGEUR_PALANQUEE_TABLE_DROP = "DROP TABLE IF EXISTS " + APPARTENANCE_PLONGEUR_PALANQUEE_NOM_TABLE + ";";
	
	/*--------------------------------------------------------------*/

	/**
	 * Retourne l'instance de la classe si elle existe déjà,
	 * sinon créé une instance de la classe et la retourne.
	 * 
	 * @param context le contexte courant
	 * @return l'instance de la classe
	 */
	public static DatabaseHelper getInstance(Context context) {
		if (mInstance == null) {
			mInstance = new DatabaseHelper(context.getApplicationContext());
		}
		return mInstance;
	}
	
	private DatabaseHelper(Context context) {
		super(context, DatabaseHelper.NOM, null, DatabaseHelper.VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(PLONGEE_TABLE_CREATE);
		db.execSQL(TOUR_TABLE_CREATE);
		db.execSQL(SECURITE_SURFACE_TABLE_CREATE);
		db.execSQL(PARAMETRE_PLONGEE_TABLE_CREATE);
		db.execSQL(PALANQUEE_TABLE_CREATE);
		db.execSQL(PARAMETRE_PALANQUEE_TABLE_CREATE);
		db.execSQL(PLONGEUR_TABLE_CREATE);
		db.execSQL(APPARTENANCE_PLONGEUR_PALANQUEE_TABLE_CREATE);
	}
	
	/**
	 * Cette méthode est appelée lorsque la version définit plus haut, change.
	 * Attention cette méthodes supprimera toutes les tables (et leur contenu)
	 * avant de recréer la base de données.
	 * Voir la documentation de onUpgrade pour plus d'informations.
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(PLONGEE_TABLE_DROP);
		db.execSQL(TOUR_TABLE_DROP);
		db.execSQL(SECURITE_SURFACE_TABLE_DROP);
		db.execSQL(PARAMETRE_PLONGEE_TABLE_DROP);
		db.execSQL(PALANQUEE_TABLE_DROP);
		db.execSQL(PARAMETRE_PALANQUEE_TABLE_DROP);
		db.execSQL(PLONGEUR_TABLE_DROP);
		db.execSQL(APPARTENANCE_PLONGEUR_PALANQUEE_TABLE_DROP);
		onCreate(db);
	}

}
