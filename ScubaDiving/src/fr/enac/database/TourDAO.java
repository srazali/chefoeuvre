package fr.enac.database;

import java.util.ArrayList;
import java.util.List;

import fr.enac.models.Tour;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Classe TourDAO représentant la relation entre
 * un objet Tour et la base de données.
 * 
 * @author Sophien Razali
 *
 */
public class TourDAO extends DAO<Tour> {
	
	private static final String TOUR_UPDATE = 
			DatabaseHelper.TOUR_CLE + " = ?;";
	
	private static final String TOUR_DELETE =
			DatabaseHelper.TOUR_CLE + " = ?;";
	
	private static final String SELECT_TOUS_LES_TOURS = 
			"SELECT * FROM " + DatabaseHelper.TOUR_NOM_TABLE;
	
	private static final String TOUR_SELECT_AVEC_ID_PLONGEE = 
			"SELECT * FROM " + DatabaseHelper.TOUR_NOM_TABLE + 
			" WHERE " + DatabaseHelper.TOUR_CLE_PLONGEE + " = ?;";
	
	private static final String TOUR_SELECT_AVEC_ID_PLONGEE_ET_POSITION = 
			"SELECT * FROM " + DatabaseHelper.TOUR_NOM_TABLE + 
			" WHERE " + DatabaseHelper.TOUR_CLE_PLONGEE + " = ?" +
					" AND " + DatabaseHelper.TOUR_POSITION + " = ?;";

	private static final String TOUR_SELECT_AVEC_ID = 
			"SELECT * FROM " + DatabaseHelper.TOUR_NOM_TABLE + 
			" WHERE " + DatabaseHelper.TOUR_CLE + " = ?;";
	
	public TourDAO(SQLiteDatabase mDatabase) {
		super(mDatabase);
	}
	
	@Override
	public long insert(Tour tour) {
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.TOUR_POSITION, tour.getmPosition());
		values.put(DatabaseHelper.TOUR_CLE_PLONGEE, tour.getmIdPlongee());
		return mDatabase.insert(DatabaseHelper.TOUR_NOM_TABLE, null, values);
	}

	@Override
	public boolean delete(Tour tour) {
		return mDatabase.delete(DatabaseHelper.TOUR_NOM_TABLE, TOUR_DELETE, new String[] {String.valueOf(tour.getmId())}) != 0;
	}

	@Override
	public boolean update(Tour tour) {
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.TOUR_POSITION, tour.getmPosition());
		values.put(DatabaseHelper.TOUR_CLE_PLONGEE, tour.getmIdPlongee());
		return mDatabase.update(DatabaseHelper.TOUR_NOM_TABLE, values, TOUR_UPDATE, new String[] {String.valueOf(tour.getmId())}) != 0;
	}

	@Override
	public Tour select(long id) {
		Cursor cursor = mDatabase.rawQuery(TOUR_SELECT_AVEC_ID, new String[] {String.valueOf(id)});
		Tour t = null;
		while(cursor.moveToNext()) {
			t = new Tour(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TOUR_CLE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TOUR_CLE_PLONGEE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TOUR_POSITION)));
			
		}
		return t;
	}
	
	public List<Tour> selectTours() {
		Cursor cursor = mDatabase.rawQuery(SELECT_TOUS_LES_TOURS, null);
		List<Tour> listeTours = new ArrayList<Tour>();
		while(cursor.moveToNext()) {
			Tour t = new Tour(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TOUR_CLE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TOUR_CLE_PLONGEE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TOUR_POSITION)));
			listeTours.add(t);
			
		}
		return listeTours;
	}
	
	public List<Tour> selectToursAvecIdPlongee(long id) {
		Cursor cursor = mDatabase.rawQuery(TOUR_SELECT_AVEC_ID_PLONGEE, new String[] {String.valueOf(id)});
		List<Tour> listeTours = new ArrayList<Tour>();
		while(cursor.moveToNext()) {
			Tour t = new Tour(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TOUR_CLE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TOUR_CLE_PLONGEE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TOUR_POSITION)));
			listeTours.add(t);
			
		}
		return listeTours;
	}
	
	public List<Tour> selectToursAvecPositionEtIdPlongee(long id, long position) {
		Cursor cursor = mDatabase.rawQuery(TOUR_SELECT_AVEC_ID_PLONGEE_ET_POSITION, new String[] {String.valueOf(id), String.valueOf(position)});
		List<Tour> listeTours = new ArrayList<Tour>();
		while(cursor.moveToNext()) {
			Log.e("TOTO", "TOTO²");
			Tour t = new Tour(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TOUR_CLE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TOUR_CLE_PLONGEE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TOUR_POSITION)));
			listeTours.add(t);
			
		}
		return listeTours;
	}
}
