package fr.enac.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import fr.enac.models.ParametrePalanquee;
import fr.enac.models.ParametrePalanqueeTypeEnum;

/**
 * Classe ParametrePalanqueeDAO représentant la relation entre
 * un objet ParametrePalanquee et la base de données.
 * 
 * @author Sophien Razali
 *
 */
public class ParametrePalanqueeDAO extends DAO<ParametrePalanquee> {
	private static final String PARAMETRE_PALANQUEE_UPDATE = 
			DatabaseHelper.PARAMETRE_PALANQUEE_CLE + " = ?;";
	
	private static final String PARAMETRE_PALANQUEE_DELETE =
			DatabaseHelper.PARAMETRE_PALANQUEE_CLE + " = ?;";
	
	private static final String SELECT_TOUS_LES_PARAMETRES_PALANQUEE = 
			"SELECT * FROM " + DatabaseHelper.PARAMETRE_PALANQUEE_NOM_TABLE;
	
	private static final String PARAMETRE_PALANQUEE_SELECT_AVEC_ID_PALANQUEE = 
			"SELECT * FROM " + DatabaseHelper.PARAMETRE_PALANQUEE_NOM_TABLE + 
			" WHERE " + DatabaseHelper.PARAMETRE_PALANQUEE_CLE_PALANQUEE + " = ?" +
			" ORDER BY " + DatabaseHelper.PARAMETRE_PALANQUEE_CLE + ";";

	private static final String PARAMETRE_PALANQUEE_SELECT_AVEC_ID = 
			"SELECT * FROM " + DatabaseHelper.PARAMETRE_PALANQUEE_NOM_TABLE + 
			" WHERE " + DatabaseHelper.PARAMETRE_PALANQUEE_CLE + " = ?;";
	
	public ParametrePalanqueeDAO(SQLiteDatabase mDatabase) {
		super(mDatabase);
	}
	
	@Override
	public long insert(ParametrePalanquee param) {
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.PARAMETRE_PALANQUEE_NOM, param.getmNom());
		values.put(DatabaseHelper.PARAMETRE_PALANQUEE_ICONE, param.getmIconId());
		values.put(DatabaseHelper.PARAMETRE_PALANQUEE_VALEUR, param.getmValeur());
		if (param.getmType() != null)
			values.put(DatabaseHelper.PARAMETRE_PALANQUEE_TYPE, param.getmType().name());
		values.put(DatabaseHelper.PARAMETRE_PALANQUEE_CLE_PALANQUEE, param.getmIdPalanquee());
		return mDatabase.insert(DatabaseHelper.PARAMETRE_PALANQUEE_NOM_TABLE, null, values);
	}

	@Override
	public boolean delete(ParametrePalanquee param) {
		return mDatabase.delete(DatabaseHelper.PARAMETRE_PALANQUEE_NOM_TABLE, PARAMETRE_PALANQUEE_DELETE, new String[] {String.valueOf(param.getmId())}) != 0;
	}

	@Override
	public boolean update(ParametrePalanquee param) {
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.PARAMETRE_PALANQUEE_NOM, param.getmNom());
		values.put(DatabaseHelper.PARAMETRE_PALANQUEE_ICONE, param.getmIconId());
		values.put(DatabaseHelper.PARAMETRE_PALANQUEE_VALEUR, param.getmValeur());
		values.put(DatabaseHelper.PARAMETRE_PALANQUEE_CLE_PALANQUEE, param.getmIdPalanquee());
		return mDatabase.update(DatabaseHelper.PARAMETRE_PALANQUEE_NOM_TABLE, values, PARAMETRE_PALANQUEE_UPDATE, new String[] {String.valueOf(param.getmId())}) != 0;
	}

	@Override
	public ParametrePalanquee select(long id) {
		Cursor cursor = mDatabase.rawQuery(PARAMETRE_PALANQUEE_SELECT_AVEC_ID, new String[] {String.valueOf(id)});
		ParametrePalanquee pp = null;
		while(cursor.moveToNext()) {
			pp = new ParametrePalanquee(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_CLE)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_NOM)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_ICONE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_CLE_PALANQUEE)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_VALEUR)),
					null);
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_TYPE)) != null) 
				pp.setmType(Enum.valueOf(ParametrePalanqueeTypeEnum.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_TYPE))));
		}
		return pp;
	}
	
	public List<ParametrePalanquee> selectParametresPalanquee() {
		Cursor cursor = mDatabase.rawQuery(SELECT_TOUS_LES_PARAMETRES_PALANQUEE, null);
		List<ParametrePalanquee> listeParametresPalanquee = new ArrayList<ParametrePalanquee>();
		while(cursor.moveToNext()) {
			ParametrePalanquee pp = new ParametrePalanquee(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_CLE)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_NOM)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_ICONE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_CLE_PALANQUEE)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_VALEUR)),
					null);
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_TYPE)) != null) 
				pp.setmType(Enum.valueOf(ParametrePalanqueeTypeEnum.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_TYPE))));
			listeParametresPalanquee.add(pp);
		}
		return listeParametresPalanquee;
	}
	
	public List<ParametrePalanquee> selectParametresPalanqueeAvecIdPalanquee(long id) {
		Cursor cursor = mDatabase.rawQuery(PARAMETRE_PALANQUEE_SELECT_AVEC_ID_PALANQUEE, new String[] {String.valueOf(id)});
		List<ParametrePalanquee> listeParametresPalanquee = new ArrayList<ParametrePalanquee>();
		while(cursor.moveToNext()) {
			ParametrePalanquee pp = new ParametrePalanquee(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_CLE)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_NOM)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_ICONE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_CLE_PALANQUEE)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_VALEUR)),
					null);
			if (cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_TYPE)) != null) 
				pp.setmType(Enum.valueOf(ParametrePalanqueeTypeEnum.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARAMETRE_PALANQUEE_TYPE))));
			listeParametresPalanquee.add(pp);
		}
		return listeParametresPalanquee;
	}
}
