package fr.enac.activities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.scubadiving.R;

import fr.enac.constantes.ActivityOrderEnum;
import fr.enac.database.PlongeurDAO;
import fr.enac.models.Plongee;
import fr.enac.models.Plongeur;
import fr.enac.models.Plongeur.Papier;
import fr.enac.models.Plongeur.Participe;
import fr.enac.utils.IconNiveauPlongeur;
import fr.enac.utils.ImportCSV;
import fr.enac.utils.dialog.NiveauPlongeurDialog;
import fr.enac.utils.selecteurfichier.SelecteurFichier;
import fr.enac.utils.tuleplongeur.TulePlongeurGridAdapter;
import fr.enac.utils.tuleplongeur.TulePlongeurGridItemsUpdateEvent;
import fr.enac.utils.tuleplongeur.TulePlongeurGridItemsUpdateListener;
import fr.enac.utils.tuleplongeur.TulePlongeurObjetAdapter;
import fr.enac.utils.tuleplongeur.TulePlongeurObjetAdapter.TuleEtat;

/**
 * @author Tolle
 * @author Leriche
 * 
 */
public class PlongeurActivity extends BaseActivity implements TulePlongeurGridItemsUpdateListener {

	private static final String LOG_TAG = PlongeurActivity.class.getName();
	private static final String URL_WS = "http://www.adlm.org/ED69B392-DC9D-4A77-A7DC-B4F997662534.php?term=*";

	private static final int DURER_TOAST = 300;	
	private static final int SELECTEUR_FICHIER_ID = 1; //utilisé pour le startActivityForResult
	private static final int NIVEAU_PICKER_ID = 2; //utilisé pour le startActivityForResult


	// ------------------------------------------------------------------//
	// Variables de classes //
	// ------------------------------------------------------------------//
	private String fichierAImporte;

	/* Vues */
	private TulePlongeurGridAdapter mtuleGrilleAdapter;
	private ArrayList<TulePlongeurObjetAdapter> mtules;

	/* Base de données */
	private PlongeurDAO plongeurDAO;

	/**
	 * Modes de fonctionnenement de l'activité suivant les actions de
	 * l'utilisateur - utilisateur clic sur un plongeur : mode
	 * PLONGEUR_SELECTIONNE - utilisateur clic sur bouton ajouter :
	 * AJOUT_PLONGEUR - utilisateur n'a rien sélectionné et n'a pas ajouté de
	 * plongeur : AUCUN_PLONGEUR_SELECTIONNE - utilisateur à importer une liste
	 * de plongeurs : NOUVEAUX_PLONGEURS
	 */
	private enum Etat {
		PLONGEUR_SELECTIONNE,
		AJOUT_PLONGEUR,
		NOUVEAUX_PLONGEURS,
		SELECTEUR_FICHIER
	}

	private Etat etatCourant;

	/* Pour éviter d'avoir les champs nom et prénom null */
	private boolean nomEstVide = true;
	private boolean prenomEstVide = true;

	// ------------------------------------------------------------------//
	// Activité //
	// ------------------------------------------------------------------//

	/**
	 * Création de l'activité
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_plongeur);
		Log.d(LOG_TAG,"PlongeurActivity onCreate()");
		/* Initialisation des variables de classe */
		mtules = new ArrayList<TulePlongeurObjetAdapter>();

		/* Base de données */
		plongeurDAO = new PlongeurDAO(mDatabaseHelper.getWritableDatabase());

		/* Set de l'ordre */
		mOrdre = ActivityOrderEnum.PLONGEUR_ACTIVITY;

		/* Mise à jour de la table pour l'autocomplétion */
		if (!checkConnectivity()) {
			Log.d(LOG_TAG,"Autocomplétion : Pas de réseau -> cache");
			useCache();
		} else {
			Log.d(LOG_TAG,"Autocomplétion : Réseau ok -> Json");
			new JsonUpdater().execute(URL_WS);
		}
	}

	/**
	 * Fonction appelée à chaque fois qu'on retourne sur l'activité mais aussi à
	 * la première fois après le onCreate()
	 * 
	 * @see protected void onCreate(Bundle savedInstanceState);
	 */
	@Override
	protected void onStart() {
		super.onStart();
		Log.d(LOG_TAG, "PlongeurActivity onStart()");
		Log.d(LOG_TAG,"Plonegurs existants (total : "+ mPlongee.getmListePlongeurs().size() + " )");
		Log.d(LOG_TAG, "Plongee id : " + mPlongee.getmId());

		// Affichage des plongeurs
		initGrillePlongeurs();

		// état initial de l'automate
		etatCourant = Etat.AJOUT_PLONGEUR;

		// Effectue les actions en fonction de l'état
		actions(etatCourant);
	}

	/**
	 * Affiche la liste des plongeurs dans des tules
	 */
	private void initGrillePlongeurs() {
		// On récupère les plongeurs de la BDD
		createTulesDepuisListPlongeurs(mPlongee.getmListePlongeurs());
		Log.d(LOG_TAG,"Plonegurs existants (total : "+ mPlongee.getmListePlongeurs().size() + " )");

		// Création de la gridview personnalisée
		GridView mtuleGrille = (GridView) findViewById(R.id.gdv_plongeurs);
		mtuleGrilleAdapter = new TulePlongeurGridAdapter(this, R.layout.tule_objetgrid,	mtules);

		mtuleGrille.setAdapter(mtuleGrilleAdapter);
		mtuleGrille.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);

		// Abonnement aux events de la gridview
		mtuleGrilleAdapter.addListener(this);		
		

		// Validation des champs du formulaire nom et prénom.  But: éviter l'ajout de plongeur si un des deux champs n'a pas été remplis
		AutoCompleteTextView nom = (AutoCompleteTextView) findViewById(R.id.edt_plongeur_nom);
		nom.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d(LOG_TAG, "Nom text changed :"+s);
				if(s.length()==0)
					nomEstVide = true;
				else
					nomEstVide = false;	

				if(nomEstVide && prenomEstVide)
					activerLargeBouton(R.id.btn_plongeur_valider, false);
				else
					activerLargeBouton(R.id.btn_plongeur_valider, true);

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}

			@Override
			public void afterTextChanged(Editable s) {}
		});

		AutoCompleteTextView prenom = (AutoCompleteTextView) findViewById(R.id.edt_plongeur_prenom);
		prenom.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Log.d(LOG_TAG, "Prénom text changed :"+s);
				if(s.length()==0)
					prenomEstVide = true;
				else
					prenomEstVide = false;		

				if(nomEstVide && prenomEstVide)
					activerLargeBouton(R.id.btn_plongeur_valider, false);
				else
					activerLargeBouton(R.id.btn_plongeur_valider, true);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}

			@Override
			public void afterTextChanged(Editable s) {}
		});

	}

	/**
	 * Met à jour le nombre de plongeurs sélectionnés
	 */
	private void afficherNbPlongeursSelectionnes() {
		TextView nbTotalPlongeur = (TextView) findViewById(R.id.tv_plongeur_nbTotalPlongeurs);
		TextView nbPlongeursValides = (TextView) findViewById(R.id.tv_plongeur_nbPlongeursValides);
		nbTotalPlongeur.setText(String.valueOf(mtules.size()));
		nbPlongeursValides.setText(String.valueOf(getNbTulesValides()));
	}

	// ------------------------------------------------------------------//
	// Getters & Setters //
	// ------------------------------------------------------------------//

	@Override
	public Plongee getmPlongee() {
		return mPlongee;
	}

	// ------------------------------------------------------------------//
	// Logique d'interactions //
	// ------------------------------------------------------------------//
	/**
	 * Gère l'affichage selon l'état de l'activité
	 */
	private void actions(Etat etat) {
		Log.d(LOG_TAG, "PlongeurActivity Etat :" + etatCourant.toString());
		TulePlongeurObjetAdapter tuleSelectionnee;

		switch (etat) {
		case AJOUT_PLONGEUR:
			// on déselectionne le tule selectionné (s'il ya)
			tuleSelectionnee = getTuleSelectionnee();
			if (tuleSelectionnee != null) {
				tuleSelectionnee.deselectionne();
				// on notifie la grille de déselectionner la tule
				mtuleGrilleAdapter.notifyDataSetChanged();
			}
			// on change le titre pour prévenir l'utilisateur qu'il peut ajouter un utilisateur
			changeTitreFormulaire(getResources().getString(R.string.tv_plongeur_ajouterPlongeur));			
			// on remet les champs du formulaire a zéro
			viderFormulaire();
			// on enable le formulaire
			activerFormulaire(true);
			// on désactive le bouton d'ajout
			activerLargeBouton(R.id.btn_plongeur_ajouterUnPlongeur, false);
			// on désactive le bouton d'ajouter (valider formulaire) pour éviter d'ajout un plongeur null
			nomEstVide = true;
			prenomEstVide = true;
			activerLargeBouton(R.id.btn_plongeur_valider, false);
			// on remplace le text du bouton par "ajouter"
			changeTextBouton(R.id.btn_plongeur_valider,(String) getResources().getString(R.string.btn_plongeur_ajouter));
			// Affichage du nombre de plongeurs sélectionnés
			afficherNbPlongeursSelectionnes();
			break;

		case PLONGEUR_SELECTIONNE:
			// On récupère la position du plongeur
			int positionPlongeur = mtules.indexOf(getTuleSelectionnee());
			// on change le titre pour prévenir l'utilisateur qu'il peut ajouter un utilisateur
			changeTitreFormulaire(getResources().getString(R.string.tv_plongeur_details));
			// on remet les champs du formulaire a zéro
			viderFormulaire();
			// on enable le formulaire
			activerFormulaire(true);
			// on active le bouton d'ajout de plongeur
			activerLargeBouton(R.id.btn_plongeur_ajouterUnPlongeur, true);
			// on remplace le text du bouton par "modifier"
			changeTextBouton(R.id.btn_plongeur_valider,	(String) getResources().getString(R.string.btn_plongeur_modifier));
			// on rempli le formulaire par les informations du plongeur sélectionné
			remplirFormulaire(mPlongee.getmListePlongeurs().get(positionPlongeur));
			// Affichage du nombre de plongeurs sélectionnés
			afficherNbPlongeursSelectionnes();
			break;

		case NOUVEAUX_PLONGEURS:
			// on supprime les plongeurs précédents
			mPlongee.getmListePlongeurs().clear();
			// on récupère la liste de plongeurs depuis le fichier CSV
			updateListPlongeursDepuisCSV(fichierAImporte);
			// on créé les tules de plongeurs
			createTulesDepuisListPlongeurs(mPlongee.getmListePlongeurs());
			// on notifie la grille du changement des tules
			mtuleGrilleAdapter.notifyDataSetChanged();
			// Affichage du nombre de plongeurs sélectionnés
			afficherNbPlongeursSelectionnes();
			break;

		case SELECTEUR_FICHIER:
			Intent selecteurFichier = new Intent(this, SelecteurFichier.class);
			startActivityForResult(selecteurFichier, SELECTEUR_FICHIER_ID);
			break;
		default:
			break;
		}
	}


	/**
	 * 
	 * Evénement récupérer lors de la fin des activity SELECTEUR FICHIER
	 */

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// feedback
		Toast feedback;

		// SELECTEUR FICHIER
		if (requestCode == SELECTEUR_FICHIER_ID) {
			if (resultCode == RESULT_OK) {
				fichierAImporte = data.getStringExtra("fichier");
				etatCourant = Etat.NOUVEAUX_PLONGEURS;
				actions(etatCourant);
				feedback = Toast.makeText(this, getResources().getString(R.string.toast_plongeur_importOK), DURER_TOAST);
				feedback.show();
			}

			if (resultCode == RESULT_CANCELED) {
				etatCourant = Etat.AJOUT_PLONGEUR;
				actions(etatCourant);
				feedback = Toast.makeText(this,	getResources().getString(R.string.toast_plongeur_importKO), DURER_TOAST);
				feedback.show();
			}
		}
	}


	/**
	 * Affiche la popup de selection des fichiers
	 * 
	 * @param v
	 */
	public void clicAfficherSelecteurDeFichier(View v) {
		switch (etatCourant) {
		/*case AUCUN_PLONGEUR_SELECTIONNE:
			etatCourant = Etat.SELECTEUR_FICHIER;
			actions(etatCourant);
			break;*/
		case AJOUT_PLONGEUR:
			etatCourant = Etat.SELECTEUR_FICHIER;
			actions(etatCourant);
			break;
		case PLONGEUR_SELECTIONNE:
			etatCourant = Etat.SELECTEUR_FICHIER;
			actions(etatCourant);
			break;
		case NOUVEAUX_PLONGEURS:
			etatCourant = Etat.SELECTEUR_FICHIER;
			actions(etatCourant);
			break;
		case SELECTEUR_FICHIER:
			// interdit
			break;
		default:
			// interdit
			break;
		}
	}

	/**
	 * Affiche le niveau picker
	 * 
	 * @param v
	 */
	public void clicAfficherNiveauDialog(View v) {
		String[] params;
		DialogFragment newFragment;
		switch (etatCourant) {
		case AJOUT_PLONGEUR:
			params = getNiveauPickerFormulaire();			
			newFragment = NiveauPlongeurDialog.newInstance(params);
			newFragment.show(getFragmentManager(), "Selection des qualifications du plongeur");
			break;
		case PLONGEUR_SELECTIONNE:
			params = getNiveauPickerFormulaire();			
			newFragment = NiveauPlongeurDialog.newInstance(params);
			newFragment.show(getFragmentManager(), "Selection des qualifications du plongeur");
			break;
		case NOUVEAUX_PLONGEURS:
			// interdit
			break;
		case SELECTEUR_FICHIER:
			// interdit
			break;
		default:
			// interdit
			break;
		}
	}

	/**
	 * Evenement du bouton Ajouter un plongeur
	 * 
	 * @param v
	 */
	public void clicAjouterPlongeur(View v) {
		switch (etatCourant) {
		case AJOUT_PLONGEUR:
			// interdit
			break;
		case PLONGEUR_SELECTIONNE:
			etatCourant = Etat.AJOUT_PLONGEUR;
			actions(etatCourant);
			break;
		case NOUVEAUX_PLONGEURS:
			etatCourant = Etat.AJOUT_PLONGEUR;
			actions(etatCourant);
			break;
		case SELECTEUR_FICHIER:
			// interdit
			break;
		default:
			// interdit
			break;
		}
	}

	/**
	 * Gère les évenements envoyés par la gridview a travers l'interface
	 * TuleGridPlongeurItemsUpdateListener
	 */
	@Override
	public void tulesUpdated(TulePlongeurGridItemsUpdateEvent event) {
		Log.d(LOG_TAG,"Evenement tules updated");
		switch (etatCourant) {
		/*case AUCUN_PLONGEUR_SELECTIONNE:
			mtules = event.getTules();
			etatCourant = getEtatDepuisTulesEvent(event);
			plongeurParticipeOuNon();
			actions(etatCourant);
			break;*/
		case AJOUT_PLONGEUR:
			mtules = event.getTules();
			etatCourant = getEtatDepuisTulesEvent(event);
			actions(etatCourant);
			break;
		case PLONGEUR_SELECTIONNE:			
			mtules = event.getTules();
			etatCourant = getEtatDepuisTulesEvent(event);			
			plongeurParticipeOuNon();
			actions(etatCourant);
			break;
		case NOUVEAUX_PLONGEURS:
			mtules = event.getTules();
			etatCourant = getEtatDepuisTulesEvent(event);
			actions(etatCourant);
			break;
		case SELECTEUR_FICHIER:
			// interdit
			break;
		default:
			// interdit
			break;
		}
	}

	/**
	 * Evenement appelé lors du click pour valider l'ajout ou la modification
	 * @param v
	 */
	public void clicValiderFormulaire(View v) {
		Log.d(LOG_TAG,"Evenement clic valider");
		/* Récupération des champs */
		AutoCompleteTextView nom = (AutoCompleteTextView) findViewById(R.id.edt_plongeur_nom);
		AutoCompleteTextView prenom = (AutoCompleteTextView) findViewById(R.id.edt_plongeur_prenom);
		CheckBox certificat = (CheckBox) findViewById(R.id.cbx_plongeur_certificat);
		CheckBox diplome = (CheckBox) findViewById(R.id.cbx_plongeur_diplome);
		CheckBox licence = (CheckBox) findViewById(R.id.cbx_plongeur_licence);
		String[] niveau = getNiveauPickerFormulaire();

		switch (etatCourant) {
		case AJOUT_PLONGEUR:		
			/* Ajout du plongeur dans la bdd */
			creationPlongeur(getmPlongee().getmId(),
					nom.getText().toString(),
					prenom.getText().toString(),
					niveau[0]+" "+niveau[1]+" "+niveau[2],
					Participe.OUI,
					Plongeur.convertBooleanToPapier(certificat.isChecked()),
					Plongeur.convertBooleanToPapier(diplome.isChecked()),
					Plongeur.convertBooleanToPapier(licence.isChecked()));

			Plongeur nouveau = mPlongee.getmListePlongeurs().get(mPlongee.getmListePlongeurs().size()-1);
			/* Ajout de la tule */
			mtules.add(new TulePlongeurObjetAdapter(TuleEtat.SELECTIONNE_ET_VALIDE,
					IconNiveauPlongeur.getBitmapFromNiveau(getResources(), nouveau.getNiveauPlongeurCDS()),
					formatNomPourTule(nouveau.getmNom(), nouveau.getmPrenom()),
					nouveau.getNiveauPlongeurCDS(),
					nouveau.getmId()));

			/* On notifie le changement à la grille */
			mtuleGrilleAdapter.notifyDataSetChanged();

			/* On change l'état de l'automate */
			etatCourant = Etat.PLONGEUR_SELECTIONNE;
			actions(etatCourant);
			break;

		case PLONGEUR_SELECTIONNE:

			// On récupère la position du plongeur
			TulePlongeurObjetAdapter tuleSelected = getTuleSelectionnee();
			int positionPlongeur = mtules.indexOf(tuleSelected);

			/* Mise à jour du plongeur */
			Plongeur p = mPlongee.getmListePlongeurs().get(positionPlongeur);
			p.setmNom(nom.getText().toString());
			p.setmPrenom(prenom.getText().toString());

			p.specifierNiveauCDS(niveau[0]+" "+niveau[1]+" "+niveau[2]);
			p.setmParticipe(Participe.OUI);
			p.setmCertificat(Plongeur.convertBooleanToPapier(certificat.isChecked()));
			p.setmDiplome(Plongeur.convertBooleanToPapier(diplome.isChecked()));
			p.setmLicence(Plongeur.convertBooleanToPapier(licence.isChecked()));

			Log.d(LOG_TAG, "Niveau rentré: "+niveau[0]+" "+niveau[1]+" "+niveau[2]);
			Log.d(LOG_TAG, "Niveau apres du plongeur: "+p.toString());

			/* Mise à jour de la bdd */
			plongeurDAO.update(p);
			Log.d(LOG_TAG, "BDD update du plongeur : " + p.getmId() + ", "
					+ p.getmNom() + " " + p.getmPrenom() 
					+ "\n\t [ Niveau : "+ p.toString() 
					+ "\n\t Participe : "+p.getmParticipe().toString()
					+ "\n\t Certificat : "+p.getmCertificat().toString()
					+ "\n\t Diplome : "+p.getmCertificat().toString()
					+ "\n\t Licence : "+p.getmCertificat().toString()
					+ "\n\t ] à la plongee (" + p.getmIdPlongee() + ")");

			/* Mise à jour de la tuile */
			tuleSelected.setEtatCourant(TuleEtat.SELECTIONNE_ET_VALIDE);
			tuleSelected.setMicone(IconNiveauPlongeur.getBitmapFromNiveau(getResources(),p.getNiveauPlongeurCDS())); 
			tuleSelected.setMnom(formatNomPourTule(p.getmNom(), p.getmPrenom()));
			tuleSelected.setMniveau(p.getNiveauPlongeurCDS());

			/* On notifie le changement à la grille */
			mtuleGrilleAdapter.notifyDataSetChanged();

			/* On change l'état de l'automate */
			etatCourant = Etat.PLONGEUR_SELECTIONNE;
			actions(etatCourant);
			break;
		case SELECTEUR_FICHIER:
			// interdit
			break;
		case NOUVEAUX_PLONGEURS:
			// interdit
			break;
		default:
			// interdit
			break;
		}
	}

	/**
	 * Return l'état en fonction de la liste de tules envoyé par l'event
	 * 
	 * @param event
	 * @return
	 */
	private Etat getEtatDepuisTulesEvent(TulePlongeurGridItemsUpdateEvent event) {
		TulePlongeurObjetAdapter tuleSelected = getTuleSelectionnee();
		if (tuleSelected == null) {						
			return Etat.AJOUT_PLONGEUR;
		} else { // si une tule est sélectionnée
			return Etat.PLONGEUR_SELECTIONNE;
		}
	}

	/**
	 * Regarde en fonction de l'état de la tuile
	 * si le plongeur particpe ou non à la plongée
	 */
	private void plongeurParticipeOuNon(){
		/* Pas optimiser car on update lors 
		 * du NON_SELECTIONNE_ET_VALIDE au SELECTIONNE_ET_VALIDE
		 */
		if(etatCourant == Etat.PLONGEUR_SELECTIONNE){ 
			TulePlongeurObjetAdapter tuleSelected = getTuleSelectionnee();
			int positionPlongeur = mtules.indexOf(tuleSelected);
			Log.d(LOG_TAG, "Etat du plongeur "+tuleSelected.getEtatCourant());

			/* Ne participe pas à la plongée*/
			if(tuleSelected.getEtatCourant() == TuleEtat.SELECTIONNE_ET_NON_VALIDE){			

				/* Mise à jour du plongeur */
				Plongeur p = mPlongee.getmListePlongeurs().get(positionPlongeur);
				p.setmParticipe(Participe.NON);

				Log.d(LOG_TAG, "Le plongeur "+p.getmNom()+" ne participe pas à la plongée");
				/* Mise à jour de la bdd */
				plongeurDAO.update(p);
			}

			/* Participe à la plongée */
			if(tuleSelected.getEtatCourant() == TuleEtat.SELECTIONNE_ET_VALIDE){

				/* Mise à jour du plongeur */
				Plongeur p = mPlongee.getmListePlongeurs().get(positionPlongeur);
				p.setmParticipe(Participe.OUI);

				Log.d(LOG_TAG, "Le plongeur "+p.getmNom()+" participe à la plongée");
				/* Mise à jour de la bdd */
				plongeurDAO.update(p);


			}				
		}
	}

	// ------------------------------------------------------------------//
	// Fonctions vers la BDD //
	// ------------------------------------------------------------------//
	/**
	 * Création d'un plongeur en base de données
	 * 
	 * @param idPlongee
	 * @param nom
	 * @param prenom
	 * @param niveau
	 * @return plongeur : PlongeurModel
	 */
	private void creationPlongeur(long idPlongee, String nom,
			String prenom, String niveau, Participe participe,
			Papier certificat, Papier diplome, Papier licence) {
		/*
		 * Création du model
		 */
		Plongeur plongeur = new Plongeur(idPlongee, nom, prenom);
		plongeur.setmParticipe(participe);
		plongeur.setmCertificat(certificat);
		plongeur.setmDiplome(diplome);
		plongeur.setmLicence(licence);
		plongeur.specifierNiveauCDS(niveau);

		/*
		 * Enregistrement en BDD
		 */
		long id = plongeurDAO.insert(plongeur);
		/* Affectation de l'id */
		plongeur.setmId(id);
		Log.d(LOG_TAG, "BDD ajout du plongeur : " + plongeur.getmId() + ", "
				+ plongeur.getmNom() + " " + plongeur.getmPrenom() 
				+ "\n\t [ \n\tNiveau : "+ plongeur.toString() 
				+ "\n\t Participe : "+plongeur.getmParticipe().toString()
				+ "\n\t Certificat : "+plongeur.getmCertificat().toString()
				+ "\n\t Diplome : "+plongeur.getmCertificat().toString()
				+ "\n\t Licence : "+plongeur.getmCertificat().toString()
				+ "\n\t ] \nà la plongee (" + plongeur.getmIdPlongee() + ")");

		/* Ajout a la liste */
		mPlongee.getmListePlongeurs().add(plongeur);
	}

	// ------------------------------------------------------------------//
	// Fonctions //
	// ------------------------------------------------------------------//
	/**
	 * Import le fichier CSV
	 */
	private void updateListPlongeursDepuisCSV(String fichierAImporte) {
		mPlongee.getmListePlongeurs().clear();
		/* Suppression des plongeurs dans la BDD */
		plongeurDAO.deleteAllPlongeursByIdPlongee(mPlongee.getmId());
		Log.d(LOG_TAG, "Importation du fichier : " + fichierAImporte);
		try {

			/* On lit le fichier csv */
			List<String> lignes = ImportCSV.readFile(ImportCSV
					.getResource(fichierAImporte));

			/* Enregistrer les lignes en BDD */
			for (String ligne : lignes) {
				String[] contenu = ligne.split(";");

				/* on ajoute que si la ligne a bien 3 ou plus de champs (les
				 champs supplémentaires seront ignorés) */
				if (contenu.length >= 3) {

					/* Ajout du plongeur */
					creationPlongeur(mPlongee.getmId(),
							contenu[0],
							contenu[1],
							contenu[2],
							Participe.OUI,
							Plongeur.convertBooleanToPapier(false),
							Plongeur.convertBooleanToPapier(false),
							Plongeur.convertBooleanToPapier(false));

				}
			}
		} catch (IOException e) {
			Toast toast = Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_SHORT);
			toast.show();
		}
	}

	/**
	 * Format les plongeurs en tules
	 * 
	 * @return
	 */
	private void createTulesDepuisListPlongeurs(List<Plongeur> plongeurs) {
		mtules.clear();
		if (plongeurs.size() > 0) {
			for (Plongeur p : plongeurs) {
				Bitmap icone = IconNiveauPlongeur.getBitmapFromNiveau(getResources(),p.toString());
				Log.d(LOG_TAG, "Création de la tule : " + p.getmId() + ", "
						+ p.getmNom() + " " + p.getmPrenom() + " ["
						+ p.toString() + "] à la plongee ("
						+ p.getmIdPlongee() + ")");	

				// si le plongeur est validé
				if (p.getmParticipe() == Participe.OUI) {
					mtules.add(new TulePlongeurObjetAdapter(
							TuleEtat.NON_SELECTIONNE_ET_VALIDE, icone,
							formatNomPourTule(p.getmNom(), p.getmPrenom()), 
							p.toString(),
							p.getmId()));

				} else { // plongeur déselectionné
					mtules.add(new TulePlongeurObjetAdapter(
							TuleEtat.NON_SELECTIONNE_ET_NON_VALIDE, icone,
							formatNomPourTule(p.getmNom(), p.getmPrenom()), 
							p.toString(),
							p.getmId()));
				}
			}
		}
	}

	/**
	 * Retourne le nombre de tules validés par l'utilisateur
	 * 
	 * @return nb tules valides
	 */
	private int getNbTulesValides() {
		int i = 0;
		for (TulePlongeurObjetAdapter t : mtules) {

			// si une tule est valide alors on incrémente i
			if (t.getEtatCourant() == TuleEtat.NON_SELECTIONNE_ET_VALIDE
					|| t.getEtatCourant() == TuleEtat.SELECTIONNE_ET_VALIDE)
				i++;
		}
		return i;
	}

	/**
	 * 
	 * @return la tule sélectionnée
	 */
	public TulePlongeurObjetAdapter getTuleSelectionnee() {
		for (TulePlongeurObjetAdapter t : mtules) {

			// si une tule est sélectionnée alors on return true
			if (t.getEtatCourant() == TuleEtat.SELECTIONNE_ET_NON_VALIDE
					|| t.getEtatCourant() == TuleEtat.SELECTIONNE_ET_VALIDE)
				return t;
		}
		return null;
	}

	// ------------------------------------------------------------------//
	// Fonctions du Formulaire //
	// ------------------------------------------------------------------//
	/**
	 * Active / Désactive les champs du formulaire "Détails"
	 */
	private void activerFormulaire(boolean bool) {
		ImageView icone = (ImageView) findViewById(R.id.img_plongeur_icone);
		AutoCompleteTextView nom = (AutoCompleteTextView) findViewById(R.id.edt_plongeur_nom);
		AutoCompleteTextView prenom = (AutoCompleteTextView) findViewById(R.id.edt_plongeur_prenom);
		CheckBox certificat = (CheckBox) findViewById(R.id.cbx_plongeur_certificat);
		CheckBox diplome = (CheckBox) findViewById(R.id.cbx_plongeur_diplome);
		CheckBox licence = (CheckBox) findViewById(R.id.cbx_plongeur_licence);

		icone.setEnabled(bool);
		nom.setEnabled(bool);
		prenom.setEnabled(bool);
		certificat.setEnabled(bool);
		diplome.setEnabled(bool);
		licence.setEnabled(bool);

		// active ou non le bouton
		activerLargeBouton(R.id.btn_plongeur_valider, bool);

		// active ou non le niveau picker
		activerNiveauPicker(bool);
	}

	/**
	 * Remplis le formulaire avec les informations du plongeur 
	 */
	private void remplirFormulaire(Plongeur plongeur) {
		ImageView icone = (ImageView) findViewById(R.id.img_plongeur_icone);
		AutoCompleteTextView nom = (AutoCompleteTextView) findViewById(R.id.edt_plongeur_nom);
		AutoCompleteTextView prenom = (AutoCompleteTextView) findViewById(R.id.edt_plongeur_prenom);
		CheckBox certificat = (CheckBox) findViewById(R.id.cbx_plongeur_certificat);
		CheckBox diplome = (CheckBox) findViewById(R.id.cbx_plongeur_diplome);
		CheckBox licence = (CheckBox) findViewById(R.id.cbx_plongeur_licence);

		icone.setImageBitmap(IconNiveauPlongeur.getDarkBitmapFromNiveau(getResources(), plongeur.toString()));
		nom.setText(plongeur.getmNom());
		prenom.setText(plongeur.getmPrenom());

		String[] niveauxToSpinner = Plongeur.toSpinner(plongeur.toString());
		changeNiveauPickerFormulaire(niveauxToSpinner[0], niveauxToSpinner[1], niveauxToSpinner[2]);

		certificat.setChecked(Plongeur.convertPapierToBoolean(plongeur.getmCertificat()));
		diplome.setChecked(Plongeur.convertPapierToBoolean(plongeur.getmDiplome()));
		licence.setChecked(Plongeur.convertPapierToBoolean(plongeur.getmLicence()));

		/* On met à jour ces variables pour la gestion des noms et prénoms vide qui désactive le bouton valider du formulaire */
		nomEstVide = plongeur.getmNom().isEmpty() ? true : false;
		prenomEstVide = plongeur.getmPrenom().isEmpty() ? true : false;
	}

	/**
	 * Vide tous les champs du formulaire
	 */
	private void viderFormulaire() {
		ImageView icone = (ImageView) findViewById(R.id.img_plongeur_icone);
		AutoCompleteTextView nom = (AutoCompleteTextView) findViewById(R.id.edt_plongeur_nom);
		AutoCompleteTextView prenom = (AutoCompleteTextView) findViewById(R.id.edt_plongeur_prenom);
		CheckBox certificat = (CheckBox) findViewById(R.id.cbx_plongeur_certificat);
		CheckBox diplome = (CheckBox) findViewById(R.id.cbx_plongeur_diplome);
		CheckBox licence = (CheckBox) findViewById(R.id.cbx_plongeur_licence);

		icone.setImageDrawable(getResources().getDrawable(R.drawable.ic_plongeur_deb_dark));
		nom.setText("");
		prenom.setText("");

		changeNiveauPickerFormulaire(
				(String) getResources().getString(R.string.tv_plongeur_details_niveau),
				(String) getResources().getString(R.string.tv_plongeur_details_aptitude),
				(String) getResources().getString(R.string.tv_plongeur_details_melange));

		certificat.setChecked(false);
		diplome.setChecked(false);
		licence.setChecked(false);
	}

	/**
	 * Récupère les string affichés dans les niveaux, aptitudes, mélanges
	 * @return
	 */
	private String[] getNiveauPickerFormulaire(){
		TextView niveau = (TextView) findViewById(R.id.tv_plongeur_niveau);
		TextView aptitude = (TextView) findViewById(R.id.tv_plongeur_aptitude);
		TextView melange = (TextView) findViewById(R.id.tv_plongeur_complement);

		return new String[]{ 
				niveau.getText().toString(),
				aptitude.getText().toString(),
				melange.getText().toString()
		};
	}

	/**
	 * Change le text du niveau picker
	 * (utilisé par le NiveauPlongeurDialog)
	 * @param niveau
	 * @param aptitude
	 * @param melange
	 */
	public void changeNiveauPickerFormulaire(String niveau, String aptitude,String melange) {
		TextView niveauPickerNiveau = (TextView) findViewById(R.id.tv_plongeur_niveau);
		TextView niveauPickerAptitude = (TextView) findViewById(R.id.tv_plongeur_aptitude);
		TextView niveauPickerComplement = (TextView) findViewById(R.id.tv_plongeur_complement);

		// si string empty alors on met les string par défaut
		niveau = (niveau==null || niveau.isEmpty() || niveau == "0" || niveau =="null") ? getResources().getString(R.string.tv_plongeur_details_niveau) : niveau;
		aptitude = (aptitude==null || aptitude.isEmpty() || aptitude == "0" || aptitude =="null") ? getResources().getString(R.string.tv_plongeur_details_aptitude) : aptitude;
		melange = (melange==null || melange.isEmpty() || melange == "0" || melange =="null") ? getResources().getString(R.string.tv_plongeur_details_melange) : melange;

		// affichage des niveaux
		niveauPickerNiveau.setText(niveau);
		niveauPickerAptitude.setText(aptitude);
		niveauPickerComplement.setText(melange);
	}

	/**
	 * Change le label du bouton
	 * 
	 * @param text
	 */
	private void changeTextBouton(int resourcesId, String text) {
		Button bouton = (Button) findViewById(resourcesId);
		bouton.setText(text);
	}

	/**
	 * Change le titre du formulaire pour notifier à l'utilisateur
	 * s'il modifie un plongeur ou s'il en ajoute un
	 */
	private void changeTitreFormulaire(String text) {
		TextView formulaire = (TextView) findViewById(R.id.tv_plongeur_formulaire_titre);
		formulaire.setText(text);		
	}


	/**
	 * Active ou déactive le bouton
	 * 
	 * @param resourcesId
	 * @param bool
	 */
	private void activerLargeBouton(int resourcesId, boolean bool) {
		Button bouton = (Button) findViewById(resourcesId);
		if (!bool) {
			bouton.setBackground(getResources().getDrawable(R.drawable.large_bouton_disable));
		} else {
			bouton.setBackground(getResources().getDrawable(R.drawable.large_bouton_normal));
		}
		bouton.setEnabled(bool);
	}

	/**
	 * Active ou déactive le niveau picker
	 * 
	 * @param resourcesId
	 * @param bool
	 */
	private void activerNiveauPicker(boolean bool) {
		TextView niveauPickerNiveau = (TextView) findViewById(R.id.tv_plongeur_niveau);
		TextView niveauPickerAptitude = (TextView) findViewById(R.id.tv_plongeur_aptitude);
		TextView niveauPickerComplement = (TextView) findViewById(R.id.tv_plongeur_complement);
		LinearLayout niveauPicker = (LinearLayout) findViewById(R.id.llh_plongeur_niveauPicker);

		if (!bool) {
			Drawable d = getResources().getDrawable(R.drawable.niveaupickeur_item_disable);
			niveauPickerNiveau.setBackground(d);
			niveauPickerAptitude.setBackground(d);
			niveauPickerComplement.setBackground(d);
		} else {
			Drawable d = getResources().getDrawable(R.drawable.niveaupickeur_item_normal);
			niveauPickerNiveau.setBackground(d);
			niveauPickerAptitude.setBackground(d);
			niveauPickerComplement.setBackground(d);
		}
		niveauPicker.setEnabled(bool);
	}

	// ------------------------------------------------------------------//
	// Barre du bas pour les changements d'activité //
	// ------------------------------------------------------------------//

	public void activitePrecedente(View v) {
		getActionBar().setSelectedNavigationItem(mOrdre.getIndex() - 1);
	}

	public void activiteSuivante(View v) {
		getActionBar().setSelectedNavigationItem(mOrdre.getIndex() + 1);
	}

	// ------------------------------------------------------------------//
	// Autocompletion
	// ------------------------------------------------------------------//

	HashMap<String, InfosPlongeur> infosPlongeurMap = new HashMap<String, InfosPlongeur>();

	private void updateAutoComplete(String result) {
		String[] name = null;

		try {
			JSONArray jsonArray = new JSONArray(result);
			name = new String[jsonArray.length()];
			String fn, ln, niv, melange;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				fn = jsonObject.getString("firstname");
				ln = jsonObject.getString("lastname");
				niv = jsonObject.getString("airLevel");
				melange = jsonObject.getString("mixLevel");
				name[i] = stripAccents(fn + " " + ln);
				infosPlongeurMap.put(name[i], new InfosPlongeur(ln, fn, niv,
						melange));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, name);
		AutoCompleteTextView textViewN = (AutoCompleteTextView) findViewById(R.id.edt_plongeur_nom);
		AutoCompleteTextView textViewP = (AutoCompleteTextView) findViewById(R.id.edt_plongeur_prenom);
		OnItemClickListener cl = new ClickListener();
		textViewN.setAdapter(adapter);
		textViewN.setOnItemClickListener(cl);
		textViewP.setAdapter(adapter);
		textViewP.setOnItemClickListener(cl);
	}

	private class ClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			InfosPlongeur p = infosPlongeurMap
					.get(arg0.getItemAtPosition(arg2));
			AutoCompleteTextView textViewN = (AutoCompleteTextView) findViewById(R.id.edt_plongeur_nom);
			AutoCompleteTextView textViewP = (AutoCompleteTextView) findViewById(R.id.edt_plongeur_prenom);
			textViewN.setText(p.getNom());
			textViewP.setText(p.getPrenom());
			changeNiveauPickerFormulaire(p.getNiveau(), null, p.getMelange());
			//cache le clavier
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(textViewN.getWindowToken(), 0);
			//imm.hideSoftInputFromWindow(textViewP.getWindowToken(), 0);
		}
	}

	private boolean checkConnectivity() {
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		return (networkInfo != null && networkInfo.isConnected());
	}

	// Uses AsyncTask to create a task away from the main UI thread.
	private class JsonUpdater extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... urls) {

			// params comes from the execute() call: params[0] is the url.
			try {
				return downloadUrl(urls[0]);
			} catch (IOException e) {
				return "ERROR";
			}
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			if (!"ERROR".equals(result)) {
				saveCache(result);
				updateAutoComplete(result);
			} else {
				Log.d(LOG_TAG,"Erreur chargement Json");
				useCache();
			}
		}
	}

	private void saveCache(String result) {
		File cacheDir = this.getCacheDir();
		File jsonFile = new File(cacheDir, "plongeurs.data");
		FileWriter fout = null;
		try {
			fout = new FileWriter(jsonFile);
			fout.write(result);
			fout.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void useCache() {
		File cacheDir = this.getCacheDir();
		File jsonFile = new File(cacheDir, "plongeurs.data");
		BufferedReader fread = null;
		try {
			fread = new BufferedReader(new FileReader(jsonFile));
			updateAutoComplete(fread.readLine());
			fread.close();
		} catch (IOException e) {
			Log.d(LOG_TAG,"Impossible d'utiliser le cache");
			e.printStackTrace();
		}
	}

	public static String stripAccents(String input) {
		// SDK level 9
		return Normalizer.normalize(input.replace("-", " "),
				Normalizer.Form.NFD).replaceAll(
						"\\p{InCombiningDiacriticalMarks}+", "");
	}

	// Given a URL, establishes an HttpUrlConnection and retrieves
	// the web page content as a InputStream, which it returns as
	// a string.
	private String downloadUrl(String myurl) throws IOException {
		InputStream is = null;


		try {
			URL url = new URL(myurl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(10000 /* milliseconds */);
			conn.setConnectTimeout(15000 /* milliseconds */);
			conn.setRequestMethod("GET");
			conn.setDoInput(true);
			// Starts the query
			conn.connect();
			int response = conn.getResponseCode();
			if (response < 200 || response > 205) {
				Log.d(LOG_TAG, "(downloadURL) response code="
						+ response);
				throw new IOException();
			}
			is = conn.getInputStream();

			// Convert the InputStream into a string
			String contentAsString = stream2String(is);
			return contentAsString;

			// Makes sure that the InputStream is closed after the app is
			// finished using it.
		} finally {
			if (is != null) {
				is.close();
			}
		}
	}

	// Reads an InputStream and converts it to a String.
	private String stream2String(InputStream stream) throws IOException,
	UnsupportedEncodingException {
		BufferedReader reader = null;
		reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
		return reader.readLine();
	}

	private class InfosPlongeur {
		private String nom;
		private String prenom;
		private String niveau;
		private String melange;

		public String getNom() {
			return nom;
		}

		public String getPrenom() {
			return prenom;
		}

		public String getNiveau() {
			return niveau;
		}

		public String getMelange() {
			return melange;
		}

		public InfosPlongeur(String nom, String prenom, String niveau,
				String melange) {
			super();
			this.nom = nom;
			this.prenom = prenom;
			this.niveau = niveau;
			this.melange = melange;
		}

	}

}
