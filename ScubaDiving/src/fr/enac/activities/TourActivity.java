package fr.enac.activities;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.scubadiving.R;

import fr.enac.constantes.ActivityOrderEnum;
import fr.enac.database.PalanqueeDAO;
import fr.enac.database.ParametrePalanqueeDAO;
import fr.enac.database.SecuriteSurfaceDAO;
import fr.enac.lii.CDS.TypePlongee;
import fr.enac.models.Palanquee;
import fr.enac.models.ParametrePalanquee;
import fr.enac.models.ParametrePalanqueeTypeEnum;
import fr.enac.models.Plongee;
import fr.enac.models.Plongeur;
import fr.enac.models.SecuriteSurface;
import fr.enac.models.Tour;
import fr.enac.utils.TempsFormat;
import fr.enac.utils.tuletour.TuleTour;
import fr.enac.utils.tuletour.TuleTourOnClickDeposeListener;
import fr.enac.utils.tuletour.TuleTourOnDragListener;
import fr.enac.utils.tuletour.TuleTourUpdateEvent;
import fr.enac.utils.tuletour.TuleTourUpdateListener;

/**
 * Activité pour gérer les palanquées dans des tours
 * @author Jonathan Tolle
 */
public class TourActivity extends BaseActivity implements TuleTourUpdateListener{

	private static final String LOG_TAG = TourActivity.class.getName();

	public static TuleTour tuleDragged = null; // Utilisé par le TuleTourOnLongClickListener
	public static TuleTour tuleSelectionnee = null; // Utilisé par le TuleTourOnClicDeposeListener
	private static int tourSelectionnee = 0; // Utilisé pour enregistrer la securité surface (n° - 1)

	// ------------------------------------------------------------------//
	// Variables de classes //
	// ------------------------------------------------------------------//
	private ArrayList<TuleTour> mTuleTours = new ArrayList<TuleTour>();	

	private LinearLayout llh_tour1;
	private LinearLayout llh_tour2;
	private LinearLayout llh_tour3;

	/* Base de données */
	private PalanqueeDAO palanqueeDAO;
	private SecuriteSurfaceDAO secuDAO;
	private ParametrePalanqueeDAO paramDAO;

	/**
	 * Automate de l'activité Tour
	 * INIT: aucun objet n'est sélectionné par l'utilisateur
	 * PALANQUEE_SELECTIONNE : lorsqu'un utilisateur clic sur une palanquée
	 * SECURITE_SURFACE : lorsqu'un utilisateur clic sur les icones pour modifier la sécurité surface
	 */
	private enum Etat{
		INIT,		
	};
	private Etat etatCourant;

	private PalanqueeDAO pDAO;

	


	// ------------------------------------------------------------------//
	// Activité //
	// ------------------------------------------------------------------//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tour);		

		/* Base de données */
		pDAO = new PalanqueeDAO(mDatabaseHelper.getWritableDatabase());
		paramDAO = new ParametrePalanqueeDAO(mDatabaseHelper.getWritableDatabase());
		secuDAO = new SecuriteSurfaceDAO(mDatabaseHelper.getWritableDatabase());
		palanqueeDAO = new PalanqueeDAO(mDatabaseHelper.getReadableDatabase());

		/* Pour le spinner */
		mOrdre = ActivityOrderEnum.TOUR_ACTIVITY;
	}

	@Override
	protected void onStart() {
		super.onStart();

		Log.d(LOG_TAG,"onStart (plongee ID :"+mPlongee.getmId()+")");

		etatCourant = Etat.INIT;
		actions(etatCourant);
	}

	/**
	 * Initialise les palanquées dans les tours
	 */
	private void initComponents() {		

		/* Récupération des vues des tours */
		llh_tour1 = (LinearLayout) findViewById(R.id.llh_tour_tour1);
		llh_tour2 = (LinearLayout) findViewById(R.id.llh_tour_tour2);
		llh_tour3 = (LinearLayout) findViewById(R.id.llh_tour_tour3);

		/* Ajout des listener */
		HorizontalScrollView hsv_tour1 = ((HorizontalScrollView) findViewById(R.id.llh_tour_dragtour1));
		HorizontalScrollView hsv_tour2 = ((HorizontalScrollView) findViewById(R.id.llh_tour_dragtour2));
		HorizontalScrollView hsv_tour3 = ((HorizontalScrollView) findViewById(R.id.llh_tour_dragtour3));
		/* Drag & Drop */
		hsv_tour1.setOnDragListener(new TuleTourOnDragListener(0, this));
		hsv_tour2.setOnDragListener(new TuleTourOnDragListener(1, this));
		hsv_tour3.setOnDragListener(new TuleTourOnDragListener(2, this));
		/* Clic & clic */
		hsv_tour1.setOnTouchListener(new TuleTourOnClickDeposeListener(0, this));
		hsv_tour2.setOnTouchListener(new TuleTourOnClickDeposeListener(1, this));
		hsv_tour3.setOnTouchListener(new TuleTourOnClickDeposeListener(2, this));

		if(mPlongee.getmListeTours() != null && mPlongee.getmListeTours().size()==0){
			/* aucune palanquée n'a été créée */
			Toast.makeText(this, "Attention, vous devez d'abord créer une ou plusieur palanquées", Toast.LENGTH_LONG).show();
		}
		else{
			/* On parcours tous les tours déjà initialisés */
			List<Tour> tours = mPlongee.getmListeTours();
			Log.d(LOG_TAG,"Nombre de tours existants :"+tours.size());

			int tourPosition=0;			
			for(Tour tour:tours){

				/* On parcours toutes les palanquées du tour */
				List<Palanquee> palanquees = tour.getmListePalanquees();			
				if(palanquees.size()>0){
					int palanqueePosition = 0;					
					Log.d(LOG_TAG,"[Tour n°"+(tourPosition+1)+"] Nombre de palanquées existantes :"+palanquees.size());

					/* On affecte les palanquées des tours au layout */
					for(Palanquee palanquee: palanquees){
						afficherPalanquee(palanquee, tourPosition, palanqueePosition);
						palanqueePosition++;						
					}
				}
				tourPosition++;
			}
		}				
	}

	/**
	 * Affiche les tules de palanquée dans les tours
	 */
	private void afficherPalanquee(Palanquee palanquee, int tourPosition, int palanqueePosition){		
		/* Récupération des plongeurs de la palanquée */
		Plongeur encadrant = (Plongeur) palanquee.getEncadrant();		
		Plongeur plongeur1 = (Plongeur) palanquee.getPlongeur()[0];
		Plongeur plongeur2 = (Plongeur) palanquee.getPlongeur()[1];
		Plongeur plongeur3 = (Plongeur) palanquee.getPlongeur()[2];
		Plongeur plongeur4 = (Plongeur) palanquee.getPlongeur()[3];
		Plongeur serrefile = (Plongeur) palanquee.getSerreFile();

		String[] plongeurs = new String[6]; //max plongeurs dans palanquée

		plongeurs[0] = (encadrant != null ? formatNomPourTule(encadrant.getmNom(), encadrant.getmPrenom()) :"");
		plongeurs[1] = (plongeur1 != null ? formatNomPourTule(plongeur1.getmNom(), plongeur1.getmPrenom()) :"");
		plongeurs[2] = (plongeur2 != null ? formatNomPourTule(plongeur2.getmNom(), plongeur2.getmPrenom()) :"");
		plongeurs[3] = (plongeur3 != null ? formatNomPourTule(plongeur3.getmNom(), plongeur3.getmPrenom()) :"");
		plongeurs[4] = (plongeur4 != null ? formatNomPourTule(plongeur4.getmNom(), plongeur4.getmPrenom()) :"");
		plongeurs[5] = (serrefile != null ? formatNomPourTule(serrefile.getmNom(), serrefile.getmPrenom()) :"");

		/* Création du paramètre de plongée Temps réel si le paramètre n'existe pas */
		/*if (palanquee.getmListeParametresPalanquee().size()<3 || palanquee.getmListeParametresPalanquee().get(3) == null) { //FIXME : chercher le paramètre réel de temps, plutôt que de supposer que ce sera le 3ème
			ParametrePalanquee tempsReel = new ParametrePalanquee(-1, "temps réel",  R.drawable.p_p_temps_p, palanquee.getmId(), "0 min", ParametrePalanqueeTypeEnum.REEL); //FIXME String a mettre dans les ressources
			tempsReel.setmId(paramDAO.insert(tempsReel)); 
			palanquee.getmListeParametresPalanquee().add(tempsReel);				
		} */
		
		/* Récupération du temps de plongée */
		int temps = 0;
		try {
			if(palanquee.getmEtat() == Palanquee.Etat.ATTENTE){ //Si la palanquée est en attente alors on récupère le temps prévu
				Log.e(LOG_TAG,"je passe");
				for (ParametrePalanquee pp : palanquee.getmListeParametresPalanquee()) {
					if (pp.getmNom().equals(PalanqueeActivity.NOM_PARAMETRE_TEMPS_PREVU)) {
						temps = (int) TempsFormat.convertStringTimeToMinute(pp.getmValeur());
						Log.e(LOG_TAG, pp.getmValeur());
					}
				}
			}
			else{ // si la palanquée est finie ou en cours de plongée alors on récupère le temps réel qui est mis automatiquement à jour par le compteur
				//temps = TempsFormat.convertStringTimeToMinute("1h");
				//temps =  convertStringTimeToMinute(palanquee.getmListeParametresPalanquee().get(3).getmValeur());
				for (ParametrePalanquee pp : palanquee.getmListeParametresPalanquee()) {
					if (pp.getmNom().equals(PalanqueeActivity.NOM_PARAMETRE_TEMPS_REEL)) {
						temps = (int) TempsFormat.convertStringTimeToMinute(pp.getmValeur());
						Log.e(LOG_TAG, pp.getmValeur());
					}
				}
			}
		}
		catch (NumberFormatException e){
			Log.d(LOG_TAG,"Erreur temps : "+e.getMessage());
		}

		/* Création de la tule tour */
		TypePlongee type = (encadrant != null ? TypePlongee.ENSEIGNEMENT : TypePlongee.EXPLORATION);
		TuleTour tule = new TuleTour(this, 
				tourPosition,
				palanqueePosition,
				plongeurs,									 
				type,
				palanquee.getmEtat(),
				temps); 
		
		/* Ajout du handler tuleTourUpdated */
		tule.addListener(this);

		/* Ajout à la liste */
		mTuleTours.add(tule);		

		//Log.d(LOG_TAG,"Ajout de la tule tour:"+ tule.toString());

		/* Affectation de la tule au tour correspondant */
		if(tourPosition==0){
			llh_tour1.addView(tule);
		}			
		else if(tourPosition==1){
			llh_tour2.addView(tule);
		}
		else if(tourPosition==2){
			llh_tour3.addView(tule);
		}			
		else
			Log.d(LOG_TAG,"Position tour non reconnue");
	}
	// ------------------------------------------------------------------//
	// Logique d'interactions //
	// ------------------------------------------------------------------//
	/**
	 * Gère l'affichage selon l'état de l'activité
	 */
	private void actions(Etat etat) {
		switch (etat) {

		case INIT:
			initComponents();			
			remplirFormulaire();
			break;

		default:
			// interdit
			break;
		}
	}

	/**
	 * Evenement des boutons pour définir la sécurité surface
	 * @param view : bouton
	 */
	public void afficherSecuSurface(View view){
		switch (etatCourant) {

		case INIT:
			//on regarde l'id de la view pour connaitre le tour
			String idName = getResources().getResourceEntryName(view.getId());
			if(idName.equals("btn_tour_secu1")){
				tourSelectionnee = 0;
			}
			else if(idName.equals("btn_tour_secu2")){
				tourSelectionnee = 1;
			}
			else{
				tourSelectionnee = 2;
			}
			//on vide puis remplis le formulaire
			viderFormulaire();		
			remplirFormulaire();
			break;

		default:
			// interdit
			break;
		}
	}

	/**
	 * Enregistre les securités surfaces
	 * @param view
	 */
	public void clicValiderFormulaire(View view){
		switch (etatCourant) {

		case INIT:
			// récupération des nom 
			AutoCompleteTextView act_secu1 = (AutoCompleteTextView) findViewById(R.id.actv_tour_secu1);
			AutoCompleteTextView act_secu2 = (AutoCompleteTextView) findViewById(R.id.actv_tour_secu2);
			AutoCompleteTextView act_secu3 = (AutoCompleteTextView) findViewById(R.id.actv_tour_secu3);
			// enregistrement en BDD
			enregistrerSecu(0, act_secu1.getText().toString());
			enregistrerSecu(1, act_secu2.getText().toString());
			enregistrerSecu(2, act_secu3.getText().toString());
			// feedback utilisateur
			Toast feedback = Toast.makeText(this, getResources().getString(R.string.toast_tour_validerOK), Toast.LENGTH_SHORT);
			feedback.show();
			break;

		default:
			// interdit
			break;
		}
	}

	//copiée depuis PalanqueeActivity
	private void ajouterParametre(String nom, String value, long icone, ParametrePalanqueeTypeEnum type, Palanquee palanquee) {
		ParametrePalanquee pp = new ParametrePalanquee(-1, nom, icone,
				palanquee.getmId(), value, type);
		pp.setmId(paramDAO.insert(pp));
		palanquee.ajouterParametre(pp);
		pDAO.update(palanquee);
	}
	
	/**
	 * Met à jour l'état des tuleTour dans la BDD et le model
	 * TourActivity écoute chaque tuleTour
	 */
	@Override
	public void tuleTourUpdated(TuleTourUpdateEvent event) {
		
		switch (etatCourant) {		
		case INIT:
			TuleTour tule = (TuleTour) event.getSource();			
			
			/* Récupération de la palanquée */
			Palanquee palanquee = getmPlongee().getmListeTours().get(tule.getTourPosition()).getmListePalanquees().get(tule.getPalanqueePosition());
			/* Modification dans le model */
			palanquee.setmEtat(tule.getEtatCourant());
			/* Modification de l'état courant dans la BDD */
			palanqueeDAO.update(palanquee);			
			
			/* Création du paramètre de plongée Temps réel si le paramètre n'existe pas */
			if (palanquee.getmListeParametresPalanquee().size()<4 || palanquee.getmListeParametresPalanquee().get(3) == null) { //FIXME : chercher le paramètre réel de temps, plutôt que de supposer que ce sera le 3ème
				ParametrePalanquee tempsReel = new ParametrePalanquee(-1, PalanqueeActivity.NOM_PARAMETRE_TEMPS_REEL,  R.drawable.p_p_temps_p, palanquee.getmId(), "0 min", ParametrePalanqueeTypeEnum.REEL); 
				tempsReel.setmId(paramDAO.insert(tempsReel)); 
				palanquee.getmListeParametresPalanquee().add(tempsReel);				
			}
			
			/* Récupération temps réel */
			for (ParametrePalanquee pp : palanquee.getmListeParametresPalanquee()) {
				if (pp.getmNom().equals(PalanqueeActivity.NOM_PARAMETRE_TEMPS_PREVU)) {
					pp.setmValeur(tule.getTempsFinal()+"min");			
					paramDAO.update(pp);		
				}
			}	
			break;
		default:
			// interdit
			break;
		}

	}
	// ------------------------------------------------------------------//
	// Getters & Setters //
	// ------------------------------------------------------------------//

	@Override
	public Plongee getmPlongee() {
		return mPlongee;
	}

	// ------------------------------------------------------------------//
	// Fonctions vers la BDD //
	// ------------------------------------------------------------------//
	/**
	 * Change la palanquee de tour
	 * @param tourPosition
	 * @param idPalanquee
	 */
	public void setTourToPalanquee(int ancienTour, int anciennePosition, int nouveauTour) {
		/* Récupération de la palanquée */
		Palanquee palanquee = getmPlongee().getmListeTours().get(ancienTour).getmListePalanquees().get(anciennePosition);

		/* On l'enleve de la liste */
		getmPlongee().getmListeTours().get(ancienTour).getmListePalanquees().remove(anciennePosition);

		/* On rajoute à la liste du nouveau tour */
		getmPlongee().getmListeTours().get(nouveauTour).getmListePalanquees().add(palanquee);

		/* Changement en BDD */
		palanquee.setmIdTour(getmPlongee().getmListeTours().get(nouveauTour).getmId());
		palanqueeDAO.update(palanquee);
	}

	/**
	 * Enregistre les nouveaux nom de la sécurité surface
	 * @param indexSecu : index dans la liste
	 * @param nouveauNom : string du AutoCompleteView
	 */
	private void enregistrerSecu(int indexSecu, String nouveauNom){

		Log.d(LOG_TAG, "Secu tour n°"+tourSelectionnee);

		List<SecuriteSurface> secus = getmPlongee().getmListeTours().get(tourSelectionnee).getmListeSecuritesSurface(); 

		// on regarde s'il existe déjà un securité surface défini a cet index 
		SecuriteSurface secu = (secus.size() > indexSecu) ? secus.get(indexSecu) : null;

		// si le securité surface n'existait pas alors on insert en BDD
		if(secu == null){
			secu = new SecuriteSurface(-1,  getmPlongee().getmListeTours().get(tourSelectionnee).getmId(), nouveauNom);
			long id = secuDAO.insert(secu);
			secu.setmId(id);
			secus.add(indexSecu, secu);
		}

		// sinon on update
		else{
			secu.setmName(nouveauNom);
			secuDAO.update(secu);
		}
	}

	// ------------------------------------------------------------------//
	// Fonctions du Formulaire //
	// ------------------------------------------------------------------//
	/**
	 * Remplis le formulaire avec le nom & prénom du secu
	 */
	private void remplirFormulaire() {

		List<SecuriteSurface> secus = getmPlongee().getmListeTours().get(tourSelectionnee).getmListeSecuritesSurface();	

		AutoCompleteTextView act_secu1 = (AutoCompleteTextView) findViewById(R.id.actv_tour_secu1);
		AutoCompleteTextView act_secu2 = (AutoCompleteTextView) findViewById(R.id.actv_tour_secu2);
		AutoCompleteTextView act_secu3 = (AutoCompleteTextView) findViewById(R.id.actv_tour_secu3);

		String secu1 = (secus.size() >= 1 ? secus.get(0).getmName(): "");
		String secu2 = (secus.size() >= 2 ? secus.get(1).getmName(): "");
		String secu3 = (secus.size() >= 3 ? secus.get(2).getmName(): "");

		act_secu1.setText(secu1);
		act_secu2.setText(secu2);
		act_secu3.setText(secu3);
	}

	/**
	 * Vide tous les champs du formulaire
	 */
	private void viderFormulaire() {
		AutoCompleteTextView act_secu1 = (AutoCompleteTextView) findViewById(R.id.actv_tour_secu1);
		AutoCompleteTextView act_secu2 = (AutoCompleteTextView) findViewById(R.id.actv_tour_secu2);
		AutoCompleteTextView act_secu3 = (AutoCompleteTextView) findViewById(R.id.actv_tour_secu3);

		act_secu1.setText("");
		act_secu2.setText("");
		act_secu3.setText("");
	}
	
	

	// ------------------------------------------------------------------//
	// Fonctions publiques//
	// ------------------------------------------------------------------//
	/**
	 * Met à jour les tules de tous les tours
	 */
	public void updateTuleTours() {
		llh_tour1.removeAllViews();
		llh_tour2.removeAllViews();
		llh_tour3.removeAllViews();
		mTuleTours.clear();
		initComponents();		
	}

	// ------------------------------------------------------------------//
	// Barre du bas pour les changements d'activité //
	// ------------------------------------------------------------------//
	public void activitePrecedente(View v) {
		getActionBar().setSelectedNavigationItem(mOrdre.getIndex() - 1);
	}

	public void activiteSuivante(View v) {
		//TODO rien a faire
	}


}
