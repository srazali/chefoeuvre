package fr.enac.activities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.scubadiving.R;

import fr.enac.database.DatabaseHelper;
import fr.enac.database.PlongeeDAO;
import fr.enac.models.Palanquee;
import fr.enac.models.ParametrePalanquee;
import fr.enac.models.Plongee;
import fr.enac.models.PlongeeEtatEnum;
import fr.enac.models.Tour;
import fr.enac.utils.DossiersApplication;
import fr.enac.utils.HtmlGenerateurExportation;
import fr.enac.utils.dialog.DupliquerPlongeeDialog;
import fr.enac.utils.dialog.PlongeeEnCoursDialog;

/**
 * TODO: Commenter ici
 * 
 * @author Sophien Razali
 *
 */
public class AccueilActivity extends Activity {
	
	/* Base de données */
	private DatabaseHelper databaseHelper;
	private PlongeeDAO plongeeDAO;

	/* Plongee */
	private Plongee mPlongeeEnCours;
	
	/* Vues */
	private Button btn_accueil_demarrer;
	private Button btn_accueil_fermerPlongee;
	private Button btn_accueil_dupliquerPlongee;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_accueil);
		
		/* Génération des dossiers nécessaires à l'application */
		DossiersApplication.genererDossiers();

		/* Récupération des vues */
		btn_accueil_demarrer = (Button) findViewById(R.id.btn_accueil_demarrer);
		btn_accueil_fermerPlongee = (Button) findViewById(R.id.btn_accueil_fermerPlongee);
		btn_accueil_dupliquerPlongee = (Button) findViewById(R.id.btn_accueil_dupliquerPlongee);
		
		/* Initialisation de la base de données */
		databaseHelper = DatabaseHelper.getInstance(this);
		plongeeDAO = new PlongeeDAO(databaseHelper.getWritableDatabase());
		
		/* Verification qu'il existe (ou non) de plongées avec l'état "EN_COURS" dans la base de données */
		mPlongeeEnCours = checkPlongeeEnCours();
		if (mPlongeeEnCours != null) {
			afficherAlertPlongeeEnCours();
		}
		
		if (plongeeDAO.selectPlongees().isEmpty()) {
			btn_accueil_dupliquerPlongee.setEnabled(false);
			btn_accueil_dupliquerPlongee.setAlpha(0.5f);
		}
	}

	/**
	 * Cette méthode vérfie s'il y a une plongée "EN_COUR" en base
	 * de données, et change le texte d'un bouton en conséquence.
	 * 
	 */
	@Override
	protected void onStart() {
		super.onStart();
		
		mPlongeeEnCours = checkPlongeeEnCours();
		
		if (mPlongeeEnCours != null) {
			btn_accueil_demarrer.setText(getResources().getString(R.string.btn_accueil_continuerPlongeeEnCours));
			btn_accueil_fermerPlongee.setEnabled(true);
			btn_accueil_fermerPlongee.setAlpha(1.0f);
			btn_accueil_dupliquerPlongee.setEnabled(false);
			btn_accueil_dupliquerPlongee.setAlpha(0.5f);
		}
		else {
			btn_accueil_demarrer.setText(getResources().getString(R.string.btn_accueil_nouvellePlongee));
			btn_accueil_fermerPlongee.setEnabled(false);
			btn_accueil_fermerPlongee.setAlpha(0.5f);
			btn_accueil_dupliquerPlongee.setEnabled(true);
			btn_accueil_dupliquerPlongee.setAlpha(1.0f);
		}
		
		if (plongeeDAO.selectPlongees().isEmpty()) {
			btn_accueil_dupliquerPlongee.setEnabled(false);
			btn_accueil_dupliquerPlongee.setAlpha(0.5f);
		}
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		/* Suppression du bouton de terminaison de la plongée */
		menu.removeItem(R.id.action_terminerPlongee);
		menu.removeItem(R.id.action_note);
		return true;
	}
	
	/**
	 * Listener de l'actionBar lorsqu'un item est sélectionné.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
			case R.id.action_planSecours:
				afficherPlanDeSecours();
				return true;
			case R.id.action_about:
				afficherAbout();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	//------------------ Méthodes privées ------------------/
	
	/**
	 * Cette méthode renvoie la plongée "EN_COURS" se trouvant
	 * en base de données, ou null s'il n'y en a aucunes.
	 * 
	 * @return la plongée "EN_COURS" ou null s'il n'y en a pas
	 */
	private Plongee checkPlongeeEnCours() {
		if (plongeeDAO.selectPlongeesAvecEtat(PlongeeEtatEnum.EN_COURS).size() > 0) {
			return plongeeDAO.select(plongeeDAO.selectPlongeesAvecEtat(PlongeeEtatEnum.EN_COURS).get(0).getmId());
		}
		return null;
	}

	/**
	 * Cette méthode est appelée lorsque une plongée en cours est détectée en base
	 * de données. Elle affiche une AlertDialog à l'utilisateur lui permettant 
	 * de fermer la plongée ou bien de la continuer.
	 * 
	 * @see PlongeeEnCoursDialog
	 */
	void afficherAlertPlongeeEnCours() {
	    DialogFragment newFragment = PlongeeEnCoursDialog.newInstance(mPlongeeEnCours);
	    newFragment.show(getFragmentManager(), "Plongee en cours");
	}
	
	public void afficherDialogDupliquerPlongee(View v) {
		DupliquerPlongeeDialog dialog = new DupliquerPlongeeDialog();
		dialog.show(getFragmentManager(), "Duplication plongée");
	}

	/**
	 * Cette méthode envoie un intent contenant la plongée en cours
	 * afin de continuer celle-ci au travers des autres activités.
	 */
	public void continuerPlongeeEnCours() {
		Intent intent;
		try {
			Log.e("ACCUEIL", "derniere activitée " + mPlongeeEnCours.getmDerniereActivite());
			intent = new Intent(this, Class.forName(mPlongeeEnCours.getmDerniereActivite()));
		} catch (ClassNotFoundException e) {
			Log.e("ACCUEIL", "Class not found");
			intent = new Intent(this, PlongeeActivity.class);
		}
		intent.putExtra(getResources().getString(R.string.intent_clePlongeeObjet), mPlongeeEnCours);
		startActivity(intent);
	}
	
	/**
	 * Cette méthode modifie l'état de la plongée à "TERMINEE",
	 * et la modifie en base de données.
	 */
	public void fermerPlongeeEnCours(View v) {
		mPlongeeEnCours.setmEtat(PlongeeEtatEnum.TERMINEE);
		if (plongeeDAO.update(mPlongeeEnCours)) {
			try {
				HtmlGenerateurExportation.exporterPlongee(mPlongeeEnCours);
				Toast.makeText(this, getResources().getString(R.string.export_reussi), Toast.LENGTH_LONG).show();
			} catch (IOException e) {
				e.printStackTrace();
			}
			btn_accueil_demarrer.setText(getResources().getString(R.string.btn_accueil_nouvellePlongee));
			mPlongeeEnCours = null;
			btn_accueil_fermerPlongee.setEnabled(false);
			btn_accueil_fermerPlongee.setAlpha(0.5f);
			btn_accueil_dupliquerPlongee.setEnabled(true);
			btn_accueil_dupliquerPlongee.setAlpha(1.0f);
		}
	}
	
	/**
	 * Cette méthode créé une nouvelle plongée, l'enregistre en base
	 * de données, et lance l'activité suivante (ici PlongeeActivity).
	 * 
	 * @param v la vue associée à l'événement
	 */
	public void demmarrerPlongee(View v) {
		if (mPlongeeEnCours != null) {
			continuerPlongeeEnCours();
		}
		else {
			Plongee nouvellePlongee = new Plongee(-1, PlongeeEtatEnum.EN_COURS, Calendar.getInstance().getTimeInMillis(), 0, null, "", "", "", "");
			nouvellePlongee.setmId(plongeeDAO.insert(nouvellePlongee));
			Intent intent = new Intent(this, PlongeeActivity.class);
			intent.putExtra(getResources().getString(R.string.intent_clePlongeeObjet), nouvellePlongee);
			startActivity(intent);
		}
	}

	public void dupliquerPlongee(Plongee plongeeADupliquer) {
		plongeeADupliquer.setmId(-1);
		plongeeADupliquer.setmDatePlongee(0);
		plongeeADupliquer.setmDateCreation(Calendar.getInstance().getTimeInMillis());
		plongeeADupliquer.setmEtat(PlongeeEtatEnum.EN_COURS);
		for (Tour t : plongeeADupliquer.getmListeTours()) {
			for (Palanquee p : t.getmListePalanquees()) {
				p.setmListeParametresPalanquee(new ArrayList<ParametrePalanquee>());
			}
		}
		mPlongeeEnCours = plongeeADupliquer;
		mPlongeeEnCours.setmId(plongeeDAO.insert(mPlongeeEnCours));
		Intent intent = new Intent(this, PlongeeActivity.class);
		intent.putExtra(getResources().getString(R.string.intent_clePlongeeObjet), mPlongeeEnCours);
		startActivity(intent);
	}
	
	/**
	 * Cette méthode démarre l'activité contenant le
	 * "A propos" de l'application.
	 */
	private void afficherAbout() {
		startActivity(new Intent(this, AboutActivity.class));
	}

	/**
	 * Cette méthode chercher le fichier plan de secours.pdf
	 * et l'affiche 
	 */
	private void afficherPlanDeSecours() {
		File file = new File(DossiersApplication.DOSSIER_SECOURS+"/Plan de secours.pdf"); // mettre le nom du fichier PDF if (file.exists()) { 

		if(!file.exists()) {
			Toast.makeText(this, getResources().getString(R.string.toast_baseactivity_pdfmanquant), Toast.LENGTH_SHORT).show(); 
		}
		else {
			Uri path = Uri.fromFile(file); 
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(path, "application/pdf"); 
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
			try { 
				startActivity(intent);
			}
			catch (ActivityNotFoundException e) { 
				Toast.makeText(this, getResources().getString(R.string.toast_baseactivity_pdfreadermanquant), Toast.LENGTH_SHORT).show(); 
			}
		}
	}
}
