package fr.enac.activities;

import java.io.File;
import java.io.IOException;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.scubadiving.R;

import fr.enac.actionbars.NavigationSpinner;
import fr.enac.constantes.ActivityOrderEnum;
import fr.enac.database.DatabaseHelper;
import fr.enac.database.PlongeeDAO;
import fr.enac.models.Plongee;
import fr.enac.models.PlongeeEtatEnum;
import fr.enac.utils.DossiersApplication;
import fr.enac.utils.HtmlGenerateurExportation;
import fr.enac.utils.dialog.NoteDialog;

/**
 * Cette classe représente les attributs et méthodes
 * communes aux activités qui en héritent.
 * 
 * @author Sophien Razali
 *
 */
public abstract class BaseActivity extends Activity{

	private static final String LOG_TAG = BaseActivity.class.getName();

	/**
	 * Le spinner de l'actionbar.
	 */
	protected NavigationSpinner mSpinner;

	/**
	 * La base de donnée.
	 */
	protected DatabaseHelper mDatabaseHelper;

	/**
	 * L'objet plongée, transmis d'activité en activité.
	 */
	protected Plongee mPlongee; 

	/**
	 * L'ordre, définit l'index, la position de l'activité.
	 * @see ActivityOrderEnum
	 * @see NavigationSpinner
	 * 
	 */
	protected ActivityOrderEnum mOrdre;

	/**
	 * Cette méthode recupère l'instance de la base de données
	 * dans l'objet mDatabaseHelper et rajoute le spinner à
	 * l'actionBar de l'activité.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(this.toString(), "onCreate() appelée");
		
		/* Initialisation de la base de données */
		mDatabaseHelper = DatabaseHelper.getInstance(this);

		/* Ajout du spinner à l'actionbar */
		mSpinner = setActionBar(this, R.layout.activity_plongeur, getActionBar());
	}

	/**
	 * Cette méthode récupère l'objet plongée dans l'intent appelant
	 * et reset le spinner pour qu'il ait le bon index.
	 */
	@Override
	protected void onStart() {
		super.onStart();
		Log.e(this.toString(), "onStart() appelée");
		
		/* Récupération de l'objet plongée */
		mPlongee = (Plongee) getIntent().getSerializableExtra(getResources().getString(R.string.intent_clePlongeeObjet));
		PlongeeDAO plongeeDAO = new PlongeeDAO(mDatabaseHelper.getWritableDatabase());
		mPlongee.setmDerniereActivite(getClass().getName());
		plongeeDAO.update(mPlongee);
		
		/* Le navigation spinner peut maintenant recevoir les évènements */
		mSpinner.setActivityInitialise(true);

		/* Reset du selected item du spinner de navigation */
		getActionBar().setSelectedNavigationItem(mOrdre.getIndex());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/**
	 * Cette méthode est appelée lorsque l'activité va être quittée.
	 * Elle détruit l'instance de l'activité courante afin qu'il n'y ait pas
	 * deux instances de la même activité.
	 */
	@Override
	protected void onStop() {
		super.onStop();
		Log.e(this.toString(), "onStop() appelée");
		this.finish();
	}

	/**
	 * Listener de l'actionBar lorsqu'un item est sélectionné.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_note:
				afficherNotesDialog();
				return true;
			case R.id.action_planSecours:
				afficherPlanDeSecours();
				return true;
			case R.id.action_about:
				afficherAbout();
				return true;
			case R.id.action_terminerPlongee:
				mPlongee = getmPlongee();
				mPlongee.setmEtat(PlongeeEtatEnum.TERMINEE);
				PlongeeDAO plongeeDAO = new PlongeeDAO(mDatabaseHelper.getWritableDatabase());
				if (plongeeDAO.update(mPlongee)) {
					try {
						HtmlGenerateurExportation.exporterPlongee(mPlongee);
						Toast.makeText(this, getResources().getString(R.string.export_reussi), Toast.LENGTH_LONG).show();
						this.finish();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	
	// ------------------------------------------------------------------//
	// Méthodes abstraites //
	// ------------------------------------------------------------------//
	
	/**
	 * Listener ouvrant l'activité sencée précéder l'activité courante.
	 * 
	 * @param v la vue associée à l'évènement
	 */
	public abstract void activitePrecedente(View v);

	/**
	 * Listener ouvrant l'activité sencée suivre l'activité courante.
	 * 
	 * @param v la vue associée à l'évènement
	 */
	public abstract void activiteSuivante(View v);

	/**
	 * Retourne l'objet plongee.
	 */
	public abstract Plongee getmPlongee();
	
	
	// ------------------------------------------------------------------//
	// Méthodes privées //
	// ------------------------------------------------------------------//

	/**
	 * Ajout la navigation entre les activités.
	 * 
	 * @param context le context de l'application
	 * @param layout le layout relié au context
	 * @param actionBar l'actionBar
	 * @return le navigation spinner
	 */
	private NavigationSpinner setActionBar(Context context, int layout, ActionBar actionBar){ 
		NavigationSpinner _spinner = new NavigationSpinner(context, layout, actionBar);
		_spinner.afficherSpinner();		
		return _spinner;
	}

	/**
	 * Cette méthode affiche l'activité about.
	 */
	private void afficherAbout() {
		startActivity(new Intent(this, AboutActivity.class));
	}
	
	/**
	 * Cette méthode affiches le notes de la plongée dans une
	 * dialog contenant un champ de texte éditable.
	 */
	private void afficherNotesDialog() {
		DialogFragment newFragment = NoteDialog.newInstance(mPlongee);
		newFragment.show(getFragmentManager(), "Notes");
	}

	/**
	 * Cette méthode chercher le fichier plan de secours.pdf
	 * et l'affiche 
	 */
	private void afficherPlanDeSecours() {
		File file = new File(DossiersApplication.DOSSIER_SECOURS+"/Plan de secours.pdf"); // mettre le nom du fichier PDF if (file.exists()) { 

		if(!file.exists()) {
			Toast.makeText(this, getResources().getString(R.string.toast_baseactivity_pdfmanquant), Toast.LENGTH_SHORT).show(); 
		}
		else {
			Uri path = Uri.fromFile(file); 
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(path, "application/pdf"); 
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
			try { 
				startActivity(intent);
			}
			catch (ActivityNotFoundException e) { 
				Toast.makeText(this, getResources().getString(R.string.toast_baseactivity_pdfreadermanquant), Toast.LENGTH_SHORT).show(); 
			}
		}
	}
	
	// ------------------------------------------------------------------//
	// Méthodes utilisées dans plusieurs activités //
	// ------------------------------------------------------------------//
	/**
	 * Format le nom et prénom pour l'affichage dans les tules
	 * @param nom
	 * @param prenom
	 * @return
	 */
	public String formatNomPourTule(String nom, String prenom) {
		//s'il n'y a que le nom de rentrer (exemple : TAV) on retourne le nom entier
		if((nom != null && !nom.isEmpty()) && (prenom == null || prenom.isEmpty()))
			return nom;
		//s'il n'y a que le prenom de rentrer (exemple : TAV aussi) on retourne le prenom entier
		else if( (nom == null || nom.isEmpty()) && (prenom != null && !prenom.isEmpty()))
			return prenom;
		//si le nom et prenom ne sont pas null
		else{
			return new StringBuilder(nom.charAt(0) + ". " + prenom).toString();
		}
	}
	
	// ------------------------------------------------------------------//
	// Méthodes non utilisées //
	// ------------------------------------------------------------------//
	
	/**
	 * Méthode non utilisée.
	 */
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		if(intent == null){
			Log.e(LOG_TAG, "OnNewIntent = null");
		}
		else if(intent.getSerializableExtra(getResources().getString(R.string.intent_clePlongeeObjet)) != null) {
			setIntent(intent);
		}
	}
}
