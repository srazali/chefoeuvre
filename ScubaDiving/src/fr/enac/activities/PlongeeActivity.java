package fr.enac.activities;

import java.io.IOException;
import java.text.Normalizer;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.json.JSONException;

import android.app.AlarmManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.scubadiving.R;
import com.survivingwithandroid.weatherapp.JSONWeatherParser;
import com.survivingwithandroid.weatherapp.WeatherHttpClient;
import com.survivingwithandroid.weatherapp.model.Weather;

import fr.enac.constantes.ActivityOrderEnum;
import fr.enac.database.PlongeeDAO;
import fr.enac.models.Plongee;
import fr.enac.models.PlongeeHoraireEnum;
import fr.enac.utils.ImportCSV;

/**
 * L'activité PlongeeActivity permet à l'utilisateur de saisir des informations
 * concernant la plongée. Elle affiche également les conditions météo à
 * l'utilisateur les plus proches de sa position actuelle.
 * 
 * @author Sophien Razali
 * @author Sébastien Leriche
 * 
 */
public class PlongeeActivity extends BaseActivity {

	private static final String LOG_TAG = PlongeeActivity.class.getName();

	/* Vues */
	private AutoCompleteTextView etv_infoPlongeeDP;
	private AutoCompleteTextView etv_infoPlongeeLieu;
	private DatePicker dpck_infoPlongeeDate;
	private RadioGroup rdg_infoPlongeeHoraire;

	/* Vues associées à la météo */
	private TextView tv_meteoCity;
	private TextView tv_meteoDescriptionGenerale;
	private TextView tv_meteoTemperature;
	private TextView tv_meteoPression;
	private TextView tv_meteoVitesseVent;
	private TextView tv_meteoDirectionVent;
	private TextView tv_meteoHumidite;
	private ImageView iv_meteoCiel;

	/* Base de données */
	private PlongeeDAO mPlongeeDAO;

	/**
	 * Cette méthode récupère les vues, et met à jour les informations météo.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_plongee);

		/* Récupération des vues */
		etv_infoPlongeeDP = (AutoCompleteTextView) findViewById(R.id.etv_infoPlongeeDP);
		etv_infoPlongeeLieu = (AutoCompleteTextView) findViewById(R.id.etv_infoPlongeeLieu);
		dpck_infoPlongeeDate = (DatePicker) findViewById(R.id.dpck_infoPlongeeDate);
		rdg_infoPlongeeHoraire = (RadioGroup) findViewById(R.id.rdg_infoPlongeeHoraire);

		/* Récupération des vues météo */
		tv_meteoCity = (TextView) findViewById(R.id.cityText);
		tv_meteoDescriptionGenerale = (TextView) findViewById(R.id.condDescr);
		tv_meteoTemperature = (TextView) findViewById(R.id.temp);
		tv_meteoHumidite = (TextView) findViewById(R.id.hum);
		tv_meteoPression = (TextView) findViewById(R.id.press);
		tv_meteoVitesseVent = (TextView) findViewById(R.id.windSpeed);
		tv_meteoDirectionVent = (TextView) findViewById(R.id.windDeg);
		iv_meteoCiel = (ImageView) findViewById(R.id.condIcon);

		/* Set de l'ordre */
		mOrdre = ActivityOrderEnum.PLONGEE_ACTIVITY;

		/* Base de données */
		mPlongeeDAO = new PlongeeDAO(mDatabaseHelper.getWritableDatabase());
	}

	/**
	 * Cette methode récupère les informations de l'objet plongée et remplit les
	 * champs de texte avec ces informations. Elle rajoute également
	 * l'autocomplétion pour certains champs.
	 */
	@Override
	protected void onStart() {
		super.onStart();

		/*
		 * Autocomplétion pour les directeurs de plongée, depuis ceux présents
		 * en bdd
		 */
		Set<String> req = mPlongeeDAO.selectTousLesDP();
		ajouterDPCSV(req);
		if (req != null) {
			String[] arrayDP = new String[req.size()];
			arrayDP = req.toArray(arrayDP);
			etv_infoPlongeeDP.setAdapter(new ArrayAdapter<String>(this,
					android.R.layout.simple_dropdown_item_1line, arrayDP));
			etv_infoPlongeeDP.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
						long arg3) {
					//cache le clavier
					InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(etv_infoPlongeeDP.getWindowToken(), 0);
				}
			});
		}

		/* Autocomplétion pour les lieux, depuis ceux présents en bdd */
		req = mPlongeeDAO.selectTousLesLieux();
		ajouterLieuxCSV(req);
		if (req != null) {
			String[] arrayLieux = new String[req.size()];
			arrayLieux = req.toArray(arrayLieux);
			etv_infoPlongeeLieu.setAdapter(new ArrayAdapter<String>(this,
					android.R.layout.simple_dropdown_item_1line, arrayLieux));
			etv_infoPlongeeLieu.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
						long arg3) {
					//cache le clavier
					InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(etv_infoPlongeeLieu.getWindowToken(), 0);
				}
			});
		}

		if (mPlongee != null) {
			/* Edition de champs pour correspondre à la plongée */
			etv_infoPlongeeDP.setText(mPlongee.getmDirecteurPlongee());
			etv_infoPlongeeLieu.setText(mPlongee.getmLieu());
			if (mPlongee.getmDatePlongee() != 0) {
				Calendar c = Calendar.getInstance();
				c.setTimeInMillis(mPlongee.getmDatePlongee());
				dpck_infoPlongeeDate.updateDate(c.get(Calendar.YEAR),
						c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
			}
			if (mPlongee.getmHoraire() != null) {
				if (mPlongee.getmHoraire().equals(PlongeeHoraireEnum.MATIN)) {
					((RadioButton) findViewById(R.id.rad_infoPlongeeMatin))
							.setChecked(true);
				} else if (mPlongee.getmHoraire().equals(
						PlongeeHoraireEnum.APRES_MIDI)) {
					((RadioButton) findViewById(R.id.rad_infoPlongeeApresMidi))
							.setChecked(true);
				} else if (mPlongee.getmHoraire().equals(
						PlongeeHoraireEnum.SOIR)) {
					((RadioButton) findViewById(R.id.rad_infoPlongeeSoir))
							.setChecked(true);
				}
			}
		} else {
			Log.e(LOG_TAG, "OBJET PLONGEE NULL");
		}

		/* Mise a jour des informations météo */
		updateMTO();
	}

	/**
	 * Cette méthode est appelée lorsque l'utilisateur est sur le point de
	 * quitter l'activité. Elle edite l'objet plongée et l'update dans la base
	 * de données.
	 */
	@Override
	protected void onPause() {
		super.onPause();
		/* Récupération des données entrées par l'utilisateur */
		updatePlongeeAvecLesChamps();

		/* Enregistrement en base de données de la plongée modifiée */
		mPlongeeDAO.update(mPlongee);
	}

	// ------------------------------------------------------------------//
	// Méthodes héritées //
	// ------------------------------------------------------------------//

	/**
	 * Cette méthode ferme et détruit l'instance de cette activité. Comme les
	 * instances de toutes les activités sont détruites dans la méthode onStop,
	 * cette méthode a pour effet de revenir à l'activité d'accueil.
	 * 
	 */
	@Override
	public void activitePrecedente(View v) {
		this.finish();
	}

	@Override
	public void activiteSuivante(View v) {
		/*
		 * Récupération des données entrées par l'utilisateur et edition de
		 * l'objet plongee
		 */
		updatePlongeeAvecLesChamps();
		getActionBar().setSelectedNavigationItem(mOrdre.getIndex() + 1);
	}

	/**
	 * Cette méthode récupère les données entrées par l'utilisateur et edite
	 * l'objet plongee, puis le retourne.
	 */
	@Override
	public Plongee getmPlongee() {
		updatePlongeeAvecLesChamps();
		return mPlongee;
	}

	// ------------------------------------------------------------------//
	// Méthodes privées //
	// ------------------------------------------------------------------//

	/**
	 * Cette méthode récupère les données entrées par l'utilisateur et edit
	 * l'objet plongee de l'activité avec les données récupérées.
	 */
	private void updatePlongeeAvecLesChamps() {
		mPlongee.setmDirecteurPlongee(etv_infoPlongeeDP.getText().toString());
		mPlongee.setmLieu(etv_infoPlongeeLieu.getText().toString());

		Calendar c = Calendar.getInstance();
		c.set(dpck_infoPlongeeDate.getYear(), dpck_infoPlongeeDate.getMonth(),
				dpck_infoPlongeeDate.getDayOfMonth());
		mPlongee.setmDatePlongee(c.getTimeInMillis());

		RadioButton horaireRadioButton = (RadioButton) findViewById(rdg_infoPlongeeHoraire
				.getCheckedRadioButtonId());
		if (horaireRadioButton.getText().equals(
				getResources().getString(R.string.rad_info_plongee_matin))) {
			mPlongee.setmHoraire(PlongeeHoraireEnum.MATIN);
		} else if (horaireRadioButton.getText().equals(
				getResources().getString(R.string.rad_info_plongee_apres_midi))) {
			mPlongee.setmHoraire(PlongeeHoraireEnum.APRES_MIDI);
		} else if (horaireRadioButton.getText().equals(
				getResources().getString(R.string.rad_info_plongee_soir))) {
			mPlongee.setmHoraire(PlongeeHoraireEnum.SOIR);
		}
	}

	// ------------------------------------------------------------------//
	// Méthodes météo //
	// ------------------------------------------------------------------//

	private void updateMTO() {
		FindCityTask t = new FindCityTask();
		t.execute();
	}

	private boolean checkConnectivity() {
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		return (networkInfo != null && networkInfo.isConnected());
	}

	private class FindCityTask extends AsyncTask<String, Void, String> {
		ProgressDialog pgrd_Chargement;

		public FindCityTask() {
			pgrd_Chargement = new ProgressDialog(PlongeeActivity.this);
			pgrd_Chargement.setTitle("Téléchargement Météo");
			pgrd_Chargement
					.setMessage("Récupération des informations météo en cours...");
		}

		@Override
		protected String doInBackground(String... params) {
			Location bestResult = null;
			float bestAccuracy = Float.MAX_VALUE;
			long bestTime = Long.MIN_VALUE;
			long minTime = AlarmManager.INTERVAL_FIFTEEN_MINUTES;

			// Acquire a reference to the system Location Manager
			LocationManager locationManager = (LocationManager) PlongeeActivity.this
					.getSystemService(Context.LOCATION_SERVICE);

			// Find best last known location
			List<String> matchingProviders = locationManager.getAllProviders();
			// matchingProviders.add( LocationManager.NETWORK_PROVIDER);
			// Or use LocationManager.GPS_PROVIDER
			for (String provider : matchingProviders) {
				Location location = locationManager
						.getLastKnownLocation(provider);
				// Log.d(LOG_TAG, provider + " -> " + location);
				if (location != null) {
					float accuracy = location.getAccuracy();
					long time = location.getTime();

					if ((time > minTime && accuracy < bestAccuracy)) {
						bestResult = location;
						bestAccuracy = accuracy;
						bestTime = time;
					} else if (time < minTime
							&& bestAccuracy == Float.MAX_VALUE
							&& time > bestTime) {
						bestResult = location;
						bestTime = time;
					}
				}
			}

			if (bestResult == null)
				return null;

			/*----------to get City-Name from coordinates ------------- */
			String cityName = null;
			String country = null;
			Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
			List<Address> addresses;
			try {
				addresses = gcd.getFromLocation(bestResult.getLatitude(),
						bestResult.getLongitude(), 1);
				if (addresses != null) {
					// System.out.println(addresses.get(0).getLocality());
					cityName = addresses.get(0).getLocality();
					country = addresses.get(0).getAddressLine(2);
				} else
					return null;
			} catch (IOException e) {
				// e.printStackTrace();
				return null;
			}
			return cityName + "-" + country;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pgrd_Chargement.show();
		}

		@Override
		protected void onPostExecute(String city) {
			super.onPostExecute(city);

			if (pgrd_Chargement != null) {
				pgrd_Chargement.dismiss();
			} else {
				Log.e(LOG_TAG, "pd est null...");
			}

			if (city == null)
				city = "Banyuls-FR";
			tv_meteoCity.setText(city);

			if (checkConnectivity()) {
				JSONWeatherTask task = new JSONWeatherTask();
				task.execute(new String[] { city });
			} else {
				tv_meteoDescriptionGenerale
						.setText("Pas de réseau,\npas de météo !");
			}
		}
	}

	private class JSONWeatherTask extends AsyncTask<String, Void, Weather> {

		@Override
		protected Weather doInBackground(String... params) {
			Weather weather = new Weather();
			String data = ((new WeatherHttpClient()).getWeatherData(params[0]));
			if (data == null) {
				// @author : Jonathan
				// Tu ne peux pas afficher data si data est null
				// source : Log.d("Erreur téléchargement MTO", data);
				// a mon avis tu voulais afficher ça :
				Log.d(LOG_TAG, "Erreur téléchargement MTO");
				return null;
			}
			// Log.d("JSON", data);
			try {
				weather = JSONWeatherParser.getWeather(data);
				// Let's retrieve the icon
				weather.iconData = ((new WeatherHttpClient())
						.getImage(weather.currentCondition.getIcon()));
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return weather;

		}

		@Override
		protected void onPostExecute(Weather weather) {
			super.onPostExecute(weather);

			if (weather == null) {
				return;
			}

			if (weather.iconData != null && weather.iconData.length > 0) {
				Bitmap img = BitmapFactory.decodeByteArray(weather.iconData, 0,
						weather.iconData.length);
				iv_meteoCiel.setImageBitmap(img);
			}

			tv_meteoCity.setText(weather.location.getCity() + ", "
					+ weather.location.getCountry());
			tv_meteoDescriptionGenerale.setText(weather.currentCondition
					.getDescr());
			tv_meteoTemperature.setText(""
					+ Math.round(weather.temperature.getTemp()) + "°C ["
					+ Math.round(weather.temperature.getMinTemp()) + "-"
					+ Math.round(weather.temperature.getMaxTemp()) + "]");
			tv_meteoHumidite.setText(""
					+ Math.round(weather.currentCondition.getHumidity()) + "%");
			tv_meteoPression.setText(""
					+ Math.round(weather.currentCondition.getPressure())
					+ " hPa");
			tv_meteoVitesseVent.setText(""
					+ Math.round(weather.wind.getSpeed()) + " km/h");
			tv_meteoDirectionVent.setText(windDirection(Math.round(weather.wind.getDeg())));
		}
		
	}

	private static String windDirection(Integer a) {
		final String[] dir = {"N","NE","E","SE","S","SO","O","NO"};
		int index = ((a+45/2)%360)/45;
		if (index>=0 && index<dir.length) {
			return dir[index]+" ("+a+" °)";
		} return a+" °";
	}
	
	// --- méthode CSV ----

	public static String stripAccents(String input) {
		// SDK level 9
		return Normalizer.normalize(input.replace("-", " "),
				Normalizer.Form.NFD).replaceAll(
				"\\p{InCombiningDiacriticalMarks}+", "");
	}

	/**
	 * Rajoute les noms des DP depuis un fichier CSV les noms sont sans accents
	 * 
	 * @param req
	 *            l'ensemble de noms
	 */
	public void ajouterDPCSV(Set<String> req) {
		List<String> list = ImportCSV.readCSVfromAssets(this, "dp.csv");
		for (String s : list) {
			s = stripAccents(s);
			String[] split = s.split(";");
			split[0] = split[0].toLowerCase();
			split[0] = Character.toUpperCase(split[0].charAt(0))
					+ split[0].substring(1);
			split[1] = split[1].toUpperCase();
			req.add(s.replace(";", " ")); // ne duplique pas les éléments
											// existants
		}
	}

	/**
	 * Rajoute les noms de sites existants dans le fichier CSV
	 * @param req l'ensemble de sites
	 */
	public void ajouterLieuxCSV(Set<String> req) {
		List<String> list = ImportCSV.readCSVfromAssets(this, "sites.csv");
		for (String s : list) {
			req.add(s.replace(";", ", ")); // ne duplique pas les éléments
											// existants
		}
	}

}