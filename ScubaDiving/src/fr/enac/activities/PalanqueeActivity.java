package fr.enac.activities;

import java.util.ArrayList;
import java.util.List;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.ToggleButton;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;

import com.example.scubadiving.R;

import fr.enac.constantes.ActivityOrderEnum;
import fr.enac.database.PalanqueeDAO;
import fr.enac.database.ParametrePalanqueeDAO;
import fr.enac.database.PlongeurDAO;
import fr.enac.database.TourDAO;
import fr.enac.lii.CDS.TypeGaz;
import fr.enac.lii.CDS.TypePlongee;
import fr.enac.models.Palanquee;
import fr.enac.models.ParametrePalanquee;
import fr.enac.models.ParametrePalanqueeTypeEnum;
import fr.enac.models.Plongee;
import fr.enac.models.Plongeur;
import fr.enac.models.Plongeur.Participe;
import fr.enac.models.PlongeurTypeEnum;
import fr.enac.models.Tour;
import fr.enac.utils.IconNiveauPlongeur;
import fr.enac.utils.dragndrop.PalanqueeOnLongClickTouchListener;
import fr.enac.utils.tule.ParametrePalanqueeGridAdapter;
import fr.enac.utils.tule.TuleGridAdapterForDrag;
import fr.enac.utils.tuleplongeur.TulePlongeurGridAdapter;
import fr.enac.utils.tuleplongeur.TulePlongeurObjetAdapter;
import fr.enac.utils.tuleplongeur.TulePlongeurObjetAdapter.TuleEtat;

public class PalanqueeActivity extends BaseActivity {

	// private static final String LOG_TAG = PalanqueeActivity.class.getName();

	public static long idDragPlongeur = -1;
	
	/* Nom utilisé pour la récupération du temps prévu de la palanquée */
	public static final String NOM_PARAMETRE_TEMPS_PREVU = "temps prévu";
	public static final String NOM_PARAMETRE_TEMPS_REEL = "temps réel";
	
	public static ParametrePalanquee selectedParametre = null;

	private TabHost tabHostPlongeRePlonge;

	private GridView mtuleGrillePlonge;
	private GridView mtuleGrilleRePlonge;

	private LinearLayout lPalanqeeBox1;
	private LinearLayout lPalanqeeBox2;
	private LinearLayout lPalanqeeBox3;
	private LinearLayout lPalanqeeBox4;
	private LinearLayout lPalanqeeBox5;
	private LinearLayout lPalanqeeBox6;

	private int curseurPalanquee = 0;

	private ArrayList<TulePlongeurObjetAdapter> mtulesPlonge = new ArrayList<TulePlongeurObjetAdapter>();
	private ArrayList<TulePlongeurObjetAdapter> mtulesRePlonge = new ArrayList<TulePlongeurObjetAdapter>();

	private TulePlongeurGridAdapter mtuleGrilleAdapter;

	private PalanqueeDAO pDAO;
	private ParametrePalanqueeDAO paraDAO;
	private PlongeurDAO plDAO;
	private TourDAO tDAO;

	private GridView gridParamettre;
	ArrayList<ParametrePalanquee> listParamettre = new ArrayList<ParametrePalanquee>();
	
	private ToggleButton tgl_palanqueeTypeAir;
	private ToggleButton tgl_palanqueeTypePlongee;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_palanquee);

		// init instance de la bdd
		pDAO = new PalanqueeDAO(mDatabaseHelper.getWritableDatabase());
		plDAO = new PlongeurDAO(mDatabaseHelper.getWritableDatabase());
		tDAO = new TourDAO(mDatabaseHelper.getWritableDatabase());
		paraDAO = new ParametrePalanqueeDAO(mDatabaseHelper.getWritableDatabase());

		// setup tabHost
		tabHostPlongeRePlonge = (TabHost) findViewById(R.id.tabHost);
		tabHostPlongeRePlonge.setup();

		// setup tab Plonge
		TabSpec spec1 = tabHostPlongeRePlonge.newTabSpec("Plonge");
		spec1.setContent(R.id.tabPlonge);
		spec1.setIndicator("Plonge");

		// setup tab RePlonge
		TabSpec spec2 = tabHostPlongeRePlonge.newTabSpec("RePlonge");
		spec2.setIndicator("RePlonge");
		spec2.setContent(R.id.tabRePlonge);

		// ajout des tab au tabHost
		tabHostPlongeRePlonge.addTab(spec1);
		tabHostPlongeRePlonge.addTab(spec2);
		
		/* Récupération des toggle buttons */
		tgl_palanqueeTypeAir = (ToggleButton) findViewById(R.id.tgl_palanqueeTypeAir);
		tgl_palanqueeTypePlongee = (ToggleButton) findViewById(R.id.tgl_palanqueeTypePlongee);
		tgl_palanqueeTypeAir.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					// Palanquée à l'air
					Log.e("TOGGLE", "Palanquée à l'air");
					getListPalanquee().get(curseurPalanquee).setTypeGaz(TypeGaz.AIR);
				}
				else {
					// Palanquée au nitrox
					Log.e("TOGGLE", "Palanquée au nitrox");
					getListPalanquee().get(curseurPalanquee).setTypeGaz(TypeGaz.NITROX);
				}
				pDAO.update(getListPalanquee().get(curseurPalanquee));
			}
		});
		tgl_palanqueeTypePlongee.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					// Palanquée à l'air
					Log.e("TOGGLE", "Palanquée enseignement");
					getListPalanquee().get(curseurPalanquee).setTypePlongee(TypePlongee.ENSEIGNEMENT);
				}
				else {
					// Palanquée au nitrox
					Log.e("TOGGLE", "Palanquée exploration");
					getListPalanquee().get(curseurPalanquee).setTypePlongee(TypePlongee.EXPLORATION);
				}
				pDAO.update(getListPalanquee().get(curseurPalanquee));
			}
		});

		/* Set de l'ordre */
		mOrdre = ActivityOrderEnum.PALANQUEE_ACTIVITY;
	}

	@Override
	protected void onStart() {
		super.onStart();
		curseurPalanquee = 0;
		/*
		 * TODO: Ici il faut que tu fasses en sortes de récupérer les palanquées
		 * se trouvant dans l'objet plongee et des les afficher
		 */

		// ajout une palanquée si y a pas, et aussi ajout d'un tour si il
		// n'existe pas
		initListPalanquee();

		// init des listes des tules
		initmtulesPlonge();
		initmtulesRePlonge();

		// init des bouton
		initBoutonDefilement();

		// Affichage de la liste des plongeurs qui plongent
		mtuleGrillePlonge = (GridView) findViewById(R.id.gridViewPlonge);
		updateGrillePlongeurs(mtulesPlonge, mtuleGrillePlonge);

		// Affichage de la liste des plongeurs qui replonge
		mtuleGrilleRePlonge = (GridView) findViewById(R.id.gridViewRePlonge);
		updateGrillePlongeurs(mtulesRePlonge, mtuleGrilleRePlonge);

		mtuleGrillePlonge.setOnDragListener(new GridOnDragListener());

		// init des affichages des paramettre de palanquee
		initParametrePalanquee();

		initBoxListner();
		activerBoutonSuivant();
		activerBoutonPrecedant();
		updateListePalanquee();
		updateTitrePalanquee();
		refreshParametre();
		updateImagePeutPlonger();
	}

	private void initBoxListner() {
		// Affichage des plongeurs de la palanquee
		lPalanqeeBox1 = (LinearLayout) findViewById(R.id.lPalanqeeBox1);
		lPalanqeeBox2 = (LinearLayout) findViewById(R.id.lPalanqeeBox2);
		lPalanqeeBox3 = (LinearLayout) findViewById(R.id.lPalanqeeBox3);
		lPalanqeeBox4 = (LinearLayout) findViewById(R.id.lPalanqeeBox4);
		lPalanqeeBox5 = (LinearLayout) findViewById(R.id.lPalanqeeBox5);
		lPalanqeeBox6 = (LinearLayout) findViewById(R.id.lPalanqeeBox6);

		// ajout des drag listner aux vues Box
		lPalanqeeBox1.setOnDragListener(new BoxOnDragListener(1));
		lPalanqeeBox2.setOnDragListener(new BoxOnDragListener(2));
		lPalanqeeBox3.setOnDragListener(new BoxOnDragListener(3));
		lPalanqeeBox4.setOnDragListener(new BoxOnDragListener(4));
		lPalanqeeBox5.setOnDragListener(new BoxOnDragListener(5));
		lPalanqeeBox6.setOnDragListener(new BoxOnDragListener(6));

		// ajout des click listner aux vues Box
		lPalanqeeBox1.setOnClickListener(new BoxClickListener(1));
		lPalanqeeBox2.setOnClickListener(new BoxClickListener(2));
		lPalanqeeBox3.setOnClickListener(new BoxClickListener(3));
		lPalanqeeBox4.setOnClickListener(new BoxClickListener(4));
		lPalanqeeBox5.setOnClickListener(new BoxClickListener(5));
		lPalanqeeBox6.setOnClickListener(new BoxClickListener(6));
	}

	public static ParametrePalanquee getSelectedParametre() {
		return selectedParametre;
	}

	public static void setSelectedParametre(ParametrePalanquee selectedParametre) {
		PalanqueeActivity.selectedParametre = selectedParametre;
	}

	private void ajouterParametre(String nom, String value, long icone,
			ParametrePalanqueeTypeEnum type, Palanquee palanquee) {
		ParametrePalanquee pp = new ParametrePalanquee(-1, nom, icone,
				palanquee.getmId(), value, type);
		pp.setmId(paraDAO.insert(pp));
		palanquee.ajouterParametre(pp);
		pDAO.update(palanquee);
	}

	private void supprimerParametre(ParametrePalanquee pp, Palanquee palanquee) {
		paraDAO.delete(pp);
		palanquee.supprimerParametre(pp);
		pDAO.update(palanquee);
	}

	private void refreshParametre() {
		listParamettre.clear();
		listParamettre.addAll(((ArrayList<ParametrePalanquee>) paraDAO.selectParametresPalanqueeAvecIdPalanquee(getListPalanquee().get(curseurPalanquee).getmId())));

		ParametrePalanqueeGridAdapter param = new ParametrePalanqueeGridAdapter(
				this, R.layout.tule_parametre_palanquee_prevu, listParamettre);
		gridParamettre.setAdapter(param);

		EditText etNom = (EditText) findViewById(R.id.et_palanquee_nom);
		EditText etValue = (EditText) findViewById(R.id.et_palanquee_value);
		etNom.setText("");
		etValue.setText("");
		setSelectedParametre(null);
	}

	private void initParametrePalanquee() {
		// // Debut de gestion des paramettre de sécurité
		gridParamettre = (GridView) findViewById(R.id.gv_palanquee_parametre_prevu);
		listParamettre.clear();

		Button btnValider = (Button) findViewById(R.id.btn_palanquee_valider);
		btnValider.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (getSelectedParametre() != null) {
					EditText etNom = (EditText) findViewById(R.id.et_palanquee_nom);
					if (etNom.getText().length() > 0) {
						getSelectedParametre().setmNom(etNom.getText().toString());
					} else {
						Toast.makeText(getApplicationContext(),	"Veuillez indiquer un nom pour le paramètre", Toast.LENGTH_LONG).show();
					}

					EditText etValue = (EditText) findViewById(R.id.et_palanquee_value);
					if (etValue.getText().length() > 0) {
						getSelectedParametre().setmValeur(etValue.getText().toString());
					} else {
						Toast.makeText(getApplicationContext(), "Veuillez indiquer une valeur pour le paramètre", Toast.LENGTH_LONG).show();
					}
					paraDAO.update(getSelectedParametre());
					getListPalanquee().get(curseurPalanquee).setmListeParametresPalanquee(paraDAO.selectParametresPalanqueeAvecIdPalanquee(getListPalanquee().get(curseurPalanquee).getmId()));
					setSelectedParametre(null);
					refreshParametre();
				} else {
					boolean error = false;
					EditText etNom = (EditText) findViewById(R.id.et_palanquee_nom);
					if (etNom.getText().length() <= 0) {
						Toast.makeText(getApplicationContext(),	"Veuillez indiquer un nom pour le paramètre", Toast.LENGTH_LONG).show();
						error = true;
					}
					EditText etValue = (EditText) findViewById(R.id.et_palanquee_value);
					if (etValue.getText().length() <= 0) {
						Toast.makeText(getApplicationContext(),	"Veuillez indiquer une valeur pour le paramètre", Toast.LENGTH_LONG).show();
						error = true;
					}
					if (!error) {
						ajouterParametre(etNom.getText().toString(),
										 etValue.getText().toString(),
										 R.drawable.ic_launcher,
										 ParametrePalanqueeTypeEnum.NEUTRE,
										 getListPalanquee().get(curseurPalanquee));
						setSelectedParametre(null);
						refreshParametre();
					}
				}

			}
		});

		Button btnSupprimer = (Button) findViewById(R.id.btn_palanquee_supprimer);
		btnSupprimer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (getSelectedParametre() != null) {
					Palanquee p = getListPalanquee().get(curseurPalanquee);
					boolean peutEtreSupprimer = true;
					for (int i = 0; i < 5; i++) {
						if (p.getmListeParametresPalanquee().get(i).getmId() == getSelectedParametre()
								.getmId()) {
							peutEtreSupprimer = false;
						}
					}
					if (peutEtreSupprimer) {
						supprimerParametre(getSelectedParametre(), p);
						setSelectedParametre(null);
						refreshParametre();
					} else {
						Toast.makeText(getApplicationContext(),
								"Ce paramètre ne peut pas être supprimer",
								Toast.LENGTH_LONG).show();
					}
				} else {
					Toast.makeText(getApplicationContext(),
							"Veuillez sélectionner sur un paramètre",
							Toast.LENGTH_LONG).show();
				}
			}
		});

		Button btnCreer = (Button) findViewById(R.id.btn_palanquee_creer);
		btnCreer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setSelectedParametre(null);
				EditText etNom = (EditText) findViewById(R.id.et_palanquee_nom);
				EditText etValeur = (EditText) findViewById(R.id.et_palanquee_value);
				etNom.setText("");
				etValeur.setText("");
				etNom.requestFocus();

			}
		});

		refreshParametre();
	}

	private void updateAll() {
		initmtulesPlonge();
		initmtulesRePlonge();
		updateGrillePlongeurs(mtulesPlonge, mtuleGrillePlonge);
		updateGrillePlongeurs(mtulesRePlonge, mtuleGrilleRePlonge);
		updateListePalanquee();
		activerBoutonSuivant();
		updateParametreProfondeurMax();
		initBoxListner();
		updateImagePeutPlonger();
	}

	private void updateParametreProfondeurMax() {
		Palanquee p = getListPalanquee().get(curseurPalanquee);
		if (p.profondeurMax() != 0) {
			p.getmListeParametresPalanquee().get(0).setmValeur(String.valueOf(p.profondeurMax()));
			paraDAO.update(p.getmListeParametresPalanquee().get(0));
		}
		refreshParametre();
	}

	/**
	 * Affiche la liste des plongeurs dans des tules
	 */
	private void updateGrillePlongeurs(
			ArrayList<TulePlongeurObjetAdapter> mtules, GridView mtuleGrille) {
		mtuleGrilleAdapter = new TuleGridAdapterForDrag(this,
				R.layout.tule_objetgrid, mtules);
		mtuleGrille.setAdapter(mtuleGrilleAdapter);

	}

	/**
	 * Affiche la liste des plongeurs dans des tules
	 */
	//TODO : factoriser les morceaux de codes, faire une méthode
	private void updateListePalanquee() {

		TextView tv = (TextView) findViewById(R.id.tvIdPalanqueeNomPlongeur1);
		Plongeur encadrant = ((Plongeur) getListPalanquee().get(
				curseurPalanquee).getEncadrant());
		if (encadrant != null) {
			tv.setText(encadrant.getNiveauPlongeurCDS() + "- "
					+ encadrant.getmNom() + " " + encadrant.getmPrenom());
			((LinearLayout) findViewById(R.id.lPalanqeeBoxContent1))
					.setOnLongClickListener(new PalanqueeOnLongClickTouchListener(
							((Plongeur) getListPalanquee()
									.get(curseurPalanquee).getEncadrant())
									.getmId()));
			((LinearLayout) findViewById(R.id.lPalanqeeBoxContent1))
					.setVisibility(View.VISIBLE);
		} else {
			tv.setText("");
			((LinearLayout) findViewById(R.id.lPalanqeeBoxContent1))
					.setVisibility(View.INVISIBLE);
		}

		TextView tv2 = (TextView) findViewById(R.id.tvIdPalanqueeNomPlongeur2);
		Plongeur p = (Plongeur) getListPalanquee().get(curseurPalanquee).getPlongeur(0);
		if (p != null) {
			tv2.setText(p.getNiveauPlongeurCDS()+"- "+ p.getmNom() + " "+ p.getmPrenom());
			((LinearLayout) findViewById(R.id.lPalanqeeBoxContent2))
					.setOnLongClickListener(new PalanqueeOnLongClickTouchListener(
							((Plongeur) getListPalanquee()
									.get(curseurPalanquee).getPlongeur(0))
									.getmId()));
			((LinearLayout) findViewById(R.id.lPalanqeeBoxContent2))
					.setVisibility(View.VISIBLE);
		} else {
			tv2.setText("");
			((LinearLayout) findViewById(R.id.lPalanqeeBoxContent2))
					.setVisibility(View.INVISIBLE);
		}

		TextView tv3 = (TextView) findViewById(R.id.tvIdPalanqueeNomPlongeur3);
		p = (Plongeur) getListPalanquee().get(curseurPalanquee).getPlongeur(1);
		if (p != null) {
			tv3.setText(p.getNiveauPlongeurCDS()+"- "+ p.getmNom() + " "+ p.getmPrenom());
			((LinearLayout) findViewById(R.id.lPalanqeeBoxContent3))
					.setOnLongClickListener(new PalanqueeOnLongClickTouchListener(
							((Plongeur) getListPalanquee()
									.get(curseurPalanquee).getPlongeur(1))
									.getmId()));
			((LinearLayout) findViewById(R.id.lPalanqeeBoxContent3))
					.setVisibility(View.VISIBLE);
		} else {
			tv3.setText("");
			((LinearLayout) findViewById(R.id.lPalanqeeBoxContent3))
					.setVisibility(View.INVISIBLE);
		}

		TextView tv4 = (TextView) findViewById(R.id.tvIdPalanqueeNomPlongeur4);
		p = (Plongeur) getListPalanquee().get(curseurPalanquee).getPlongeur(2);
		if (p != null) {
			tv4.setText(p.getNiveauPlongeurCDS()+"- "+ p.getmNom() + " "+ p.getmPrenom());
			((LinearLayout) findViewById(R.id.lPalanqeeBoxContent4))
					.setOnLongClickListener(new PalanqueeOnLongClickTouchListener(
							((Plongeur) getListPalanquee()
									.get(curseurPalanquee).getPlongeur(2))
									.getmId()));
			((LinearLayout) findViewById(R.id.lPalanqeeBoxContent4))
					.setVisibility(View.VISIBLE);
		} else {
			tv4.setText("");
			((LinearLayout) findViewById(R.id.lPalanqeeBoxContent4))
					.setVisibility(View.INVISIBLE);
		}

		TextView tv5 = (TextView) findViewById(R.id.tvIdPalanqueeNomPlongeur5);
		p = (Plongeur) getListPalanquee().get(curseurPalanquee).getPlongeur(3);
		if (p != null) {
			tv5.setText(p.getNiveauPlongeurCDS()+"- "+ p.getmNom() + " "+ p.getmPrenom());
			((LinearLayout) findViewById(R.id.lPalanqeeBoxContent5))
					.setOnLongClickListener(new PalanqueeOnLongClickTouchListener(
							((Plongeur) getListPalanquee()
									.get(curseurPalanquee).getPlongeur(3))
									.getmId()));
			((LinearLayout) findViewById(R.id.lPalanqeeBoxContent5))
					.setVisibility(View.VISIBLE);
		} else {
			tv5.setText("");
			((LinearLayout) findViewById(R.id.lPalanqeeBoxContent5))
					.setVisibility(View.INVISIBLE);
		}

		TextView tv6 = (TextView) findViewById(R.id.tvIdPalanqueeNomPlongeur6);
		p = (Plongeur) getListPalanquee().get(curseurPalanquee).getSerreFile();
		if (p != null) {
			tv6.setText(p.getNiveauPlongeurCDS()+"- "+ p.getmNom() + " "+ p.getmPrenom());
			((LinearLayout) findViewById(R.id.lPalanqeeBoxContent6))
					.setOnLongClickListener(new PalanqueeOnLongClickTouchListener(
							((Plongeur) getListPalanquee()
									.get(curseurPalanquee).getSerreFile())
									.getmId()));
			((LinearLayout) findViewById(R.id.lPalanqeeBoxContent6))
					.setVisibility(View.VISIBLE);
		} else {
			tv6.setText("");
			((LinearLayout) findViewById(R.id.lPalanqeeBoxContent6))
					.setVisibility(View.INVISIBLE);
		}
		
		if (getListPalanquee().get(curseurPalanquee).getTypeGaz().equals(TypeGaz.AIR)) {
			tgl_palanqueeTypeAir.setChecked(true);
		}
		else {
			tgl_palanqueeTypeAir.setChecked(false);
		}
		
		if (getListPalanquee().get(curseurPalanquee).getTypePlongee().equals(TypePlongee.ENSEIGNEMENT)) {
			tgl_palanqueeTypePlongee.setChecked(true);
		}
		else {
			tgl_palanqueeTypePlongee.setChecked(false);
		}
	}

	private void initmtulesPlonge() {
		mtulesPlonge.clear();
		for (Plongeur p : getListPlongeurQuiPlonge()) {
			mtulesPlonge.add(new TulePlongeurObjetAdapter(
					TuleEtat.NON_SELECTIONNE_ET_VALIDE, IconNiveauPlongeur
							.getBitmapFromNiveau(this.getResources(),
									p.getNiveauPlongeurCDS()), p.getmNom()
							+ " " + p.getmPrenom(), p.getNiveauPlongeurCDS(), p
							.getmId()));
		}
	}

	private void initmtulesRePlonge() {
		mtulesRePlonge.clear();
		for (Plongeur p : getListPlongeurQuiRePlonge()) {
			mtulesRePlonge.add(new TulePlongeurObjetAdapter(
					TuleEtat.NON_SELECTIONNE_ET_VALIDE, IconNiveauPlongeur
							.getBitmapFromNiveau(this.getResources(),
									p.getNiveauPlongeurCDS()), p.getmNom()
							+ " " + p.getmPrenom(), p.getNiveauPlongeurCDS(), p
							.getmId()));
		}
	}

	/**
	 * Renvoie true si la palanquée est vide, ie sans encadrant ni plongeur ni
	 * serre-file
	 */
	private boolean isVide(Palanquee p) {
		if (p.getEncadrant() != null)
			return false;
		for (fr.enac.lii.CDS.Plongeur pl : p.getPlongeur()) {
			if (pl != null)
				return false;
		}
		if (p.getSerreFile() != null)
			return false;
		return true;
	}

	private void activerBoutonSuivant() {
		ImageButton ib = (ImageButton) findViewById(R.id.imageButton3);
		Palanquee p = getListPalanquee().get(curseurPalanquee);
		if (!isVide(p)) {
			ib.setVisibility(ImageButton.VISIBLE);
		} else {
			ib.setVisibility(ImageButton.INVISIBLE);
		}
	}

	private void activerBoutonPrecedant() {
		ImageButton ib = (ImageButton) findViewById(R.id.imageButton2);
		if (curseurPalanquee > 0) {
			ib.setVisibility(ImageButton.VISIBLE);
		} else {
			ib.setVisibility(ImageButton.INVISIBLE);
		}
	}

	private void initBoutonDefilement() {
		ImageButton ibSuivant = (ImageButton) findViewById(R.id.imageButton3);
		ibSuivant.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (curseurPalanquee == getListPalanquee().size() - 1) {
					ajoutPalanquee();
				}
				curseurPalanquee++;

				activerBoutonSuivant();
				activerBoutonPrecedant();
				updateListePalanquee();
				updateTitrePalanquee();
				refreshParametre();
				updateImagePeutPlonger();
			}
		});

		ImageButton ibPrecedant = (ImageButton) findViewById(R.id.imageButton2);
		ibPrecedant.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (curseurPalanquee > 0) {
					curseurPalanquee--;
				}

				activerBoutonSuivant();
				activerBoutonPrecedant();
				updateListePalanquee();
				updateTitrePalanquee();
				refreshParametre();
				updateImagePeutPlonger();
			}
		});

		activerBoutonSuivant();
		activerBoutonPrecedant();
		updateTitrePalanquee();
	}

	// activation ou desactivation du
	@Override
	public void activitePrecedente(View v) {
		getActionBar().setSelectedNavigationItem(mOrdre.getIndex() - 1);
	}

	// activation ou desactivation des boutons de défilement
	@Override
	public void activiteSuivante(View v) {
		getActionBar().setSelectedNavigationItem(mOrdre.getIndex() + 1);
	}

	@Override
	public Plongee getmPlongee() {
		return mPlongee;
	}

	// retourn la liste des plongeur à partir du model
	private List<Plongeur> getListPlongeurQuiPlonge() {
		List<Plongeur> listPlonge = new ArrayList<Plongeur>();
		List<Plongeur> listRePlonge = getListPlongeurQuiRePlonge();
		for (Plongeur p : mPlongee.getmListePlongeurs()) {
			if (p.getmParticipe().equals(Participe.OUI)) {
				listPlonge.add(p);
			}
			for (Plongeur pR : listRePlonge) {
				if (pR.getmId() == p.getmId()) {
					listPlonge.remove(p);
				}
			}
		}

		return listPlonge;
	}

	// retourn la liste des plongeur qui replonge à partir du model
	private List<Plongeur> getListPlongeurQuiRePlonge() {
		List<Plongeur> listReplonge = new ArrayList<Plongeur>();
		for (Tour t : mPlongee.getmListeTours()) {

			for (Palanquee p : t.getmListePalanquees()) {

				if (p.getEncadrant() != null
						&& ((Plongeur) p.getEncadrant()).getmParticipe()
								.equals(Participe.OUI)) {
					if (!listReplonge.contains(p.getEncadrant())) {
						listReplonge.add((Plongeur) p.getEncadrant());
					}
				}
				if (p.getSerreFile() != null
						&& ((Plongeur) p.getSerreFile()).getmParticipe()
								.equals(Participe.OUI)) {
					if (!listReplonge.contains(p.getSerreFile())) {
						listReplonge.add((Plongeur) p.getSerreFile());
					}
				}
				for (fr.enac.lii.CDS.Plongeur pl : p.getPlongeur()) {
					if (pl != null
							&& ((Plongeur) pl).getmParticipe().equals(
									Participe.OUI)) {
						if (!listReplonge.contains(pl)) {
							listReplonge.add((Plongeur) pl);
						}
					}
				}
			}
		}

		return listReplonge;
	}

	private List<Palanquee> getListPalanquee() {
		List<Palanquee> listPalanquee = new ArrayList<Palanquee>();
		for (Tour t : mPlongee.getmListeTours()) {
			listPalanquee.addAll(t.getmListePalanquees());
		}
		return listPalanquee;
	}

	private void ajoutPalanquee() {
		Log.e("PALANQUE", "Ajout");
		Palanquee p = new Palanquee(-1, mPlongee.getmListeTours().get(0)
				.getmId());
		p.setmId(pDAO.insert(p));
		mPlongee.getmListeTours().get(0).ajouterPalanquee(p);
		tDAO.update(mPlongee.getmListeTours().get(0));

		ajouterParametre("profondeur", "20 m", R.drawable.p_p_pronfdeur_p,
				ParametrePalanqueeTypeEnum.PREVU, p);
		/*
		 * ajouterParametre("profondeur", "40 m", R.drawable.p_p_pronfdeur_p,
		 * ParametrePalanqueeTypeEnum.REEL, p);
		 */
		ajouterParametre(NOM_PARAMETRE_TEMPS_PREVU, "40", R.drawable.p_p_temps_p,
				ParametrePalanqueeTypeEnum.PREVU, p);
		/*
		 * ajouterParametre("temps", "1h50", R.drawable.p_p_temps_p,
		 * ParametrePalanqueeTypeEnum.REEL, p);
		 */
		/*
		 * ajouterParametre("Palier", "2", R.drawable.p_p_temps_p,
		 * ParametrePalanqueeTypeEnum.NEUTRE, p);
		 */
	}

	private void initTour() {
		if (mPlongee.getmListeTours().isEmpty()) {
			if (tDAO.selectToursAvecPositionEtIdPlongee(mPlongee.getmId(), 1)
					.isEmpty()) {
				Tour t1 = new Tour(-1, mPlongee.getmId(), 1);
				t1.setmId(tDAO.insert(t1));
			}

			if (tDAO.selectToursAvecPositionEtIdPlongee(mPlongee.getmId(), 2)
					.isEmpty()) {
				Tour t2 = new Tour(-1, mPlongee.getmId(), 2);
				t2.setmId(tDAO.insert(t2));
			}

			if (tDAO.selectToursAvecPositionEtIdPlongee(mPlongee.getmId(), 3)
					.isEmpty()) {
				Tour t3 = new Tour(-1, mPlongee.getmId(), 3);
				t3.setmId(tDAO.insert(t3));
			}
		}
		mPlongee.ajouterTour(tDAO.selectToursAvecPositionEtIdPlongee(
				mPlongee.getmId(), 1).get(0));
		mPlongee.ajouterTour(tDAO.selectToursAvecPositionEtIdPlongee(
				mPlongee.getmId(), 2).get(0));
		mPlongee.ajouterTour(tDAO.selectToursAvecPositionEtIdPlongee(
				mPlongee.getmId(), 3).get(0));

	}

	private void initListPalanquee() {
		initTour();
		if (mPlongee.getmListeTours().get(0).getmListePalanquees().isEmpty()) {
			ajoutPalanquee();
		}
	}

	private Plongeur getPlongeurById(long id, List<Plongeur> l) {
		for (Plongeur p : l) {
			if (p.getmId() == id) {
				return p;
			}
		}
		return null;
	}

	private void plongeurFromPlongeToPalanquee(long id, int position) {
		if (!getListPalanquee().get(curseurPalanquee).contientPlongeur(
				getPlongeurById(id, getListPlongeurQuiPlonge()))
				&& getPlongeurById(id, getListPlongeurQuiPlonge()) != null) {
			switch (position) {
			case 1:
				if (getListPalanquee().get(curseurPalanquee).getEncadrant() != null) {
					plDAO.deletePlongeurDePalanquee(
							(Plongeur) getListPalanquee().get(curseurPalanquee)
									.getEncadrant(),
							getListPalanquee().get(curseurPalanquee).getmId());
				}
				getPlongeurById(id, getListPlongeurQuiPlonge())
						.setmTypePlongeur(PlongeurTypeEnum.ENCADRANT);
				plDAO.insertPlongeurDansPalanquee(
						getPlongeurById(id, getListPlongeurQuiPlonge()),
						getListPalanquee().get(curseurPalanquee).getmId());
				getListPalanquee().get(curseurPalanquee).ajouterEncadrant(
						getPlongeurById(id, getListPlongeurQuiPlonge()));
				break;
			case 2:
				if (getListPalanquee().get(curseurPalanquee).getPlongeur(0) != null) {
					plDAO.deletePlongeurDePalanquee(
							(Plongeur) getListPalanquee().get(curseurPalanquee)
									.getPlongeur(0),
							getListPalanquee().get(curseurPalanquee).getmId());
				}
				getPlongeurById(id, getListPlongeurQuiPlonge())
						.setmTypePlongeur(PlongeurTypeEnum.PLONGEUR);
				plDAO.insertPlongeurDansPalanquee(
						getPlongeurById(id, getListPlongeurQuiPlonge()),
						getListPalanquee().get(curseurPalanquee).getmId());
				getListPalanquee().get(curseurPalanquee).ajouterPlongeur(
						getPlongeurById(id, getListPlongeurQuiPlonge()), 0);
				break;
			case 3:
				if (getListPalanquee().get(curseurPalanquee).getPlongeur(1) != null) {
					plDAO.deletePlongeurDePalanquee(
							(Plongeur) getListPalanquee().get(curseurPalanquee)
									.getPlongeur(1),
							getListPalanquee().get(curseurPalanquee).getmId());
				}
				getPlongeurById(id, getListPlongeurQuiPlonge())
						.setmTypePlongeur(PlongeurTypeEnum.PLONGEUR);
				plDAO.insertPlongeurDansPalanquee(
						getPlongeurById(id, getListPlongeurQuiPlonge()),
						getListPalanquee().get(curseurPalanquee).getmId());
				getListPalanquee().get(curseurPalanquee).ajouterPlongeur(
						getPlongeurById(id, getListPlongeurQuiPlonge()), 1);
				break;
			case 4:
				if (getListPalanquee().get(curseurPalanquee).getPlongeur(2) != null) {
					plDAO.deletePlongeurDePalanquee(
							(Plongeur) getListPalanquee().get(curseurPalanquee)
									.getPlongeur(2),
							getListPalanquee().get(curseurPalanquee).getmId());
				}
				getPlongeurById(id, getListPlongeurQuiPlonge())
						.setmTypePlongeur(PlongeurTypeEnum.PLONGEUR);
				plDAO.insertPlongeurDansPalanquee(
						getPlongeurById(id, getListPlongeurQuiPlonge()),
						getListPalanquee().get(curseurPalanquee).getmId());
				getListPalanquee().get(curseurPalanquee).ajouterPlongeur(
						getPlongeurById(id, getListPlongeurQuiPlonge()), 2);
				break;
			case 5:
				if (getListPalanquee().get(curseurPalanquee).getPlongeur(3) != null) {
					plDAO.deletePlongeurDePalanquee(
							(Plongeur) getListPalanquee().get(curseurPalanquee)
									.getPlongeur(3),
							getListPalanquee().get(curseurPalanquee).getmId());
				}
				getPlongeurById(id, getListPlongeurQuiPlonge())
						.setmTypePlongeur(PlongeurTypeEnum.PLONGEUR);
				plDAO.insertPlongeurDansPalanquee(
						getPlongeurById(id, getListPlongeurQuiPlonge()),
						getListPalanquee().get(curseurPalanquee).getmId());
				getListPalanquee().get(curseurPalanquee).ajouterPlongeur(
						getPlongeurById(id, getListPlongeurQuiPlonge()), 3);
				break;
			case 6:
				if (getListPalanquee().get(curseurPalanquee).getSerreFile() != null) {
					plDAO.deletePlongeurDePalanquee(
							(Plongeur) getListPalanquee().get(curseurPalanquee)
									.getSerreFile(),
							getListPalanquee().get(curseurPalanquee).getmId());
				}
				getPlongeurById(id, getListPlongeurQuiPlonge())
						.setmTypePlongeur(PlongeurTypeEnum.SERRE_FILE);
				plDAO.insertPlongeurDansPalanquee(
						getPlongeurById(id, getListPlongeurQuiPlonge()),
						getListPalanquee().get(curseurPalanquee).getmId());
				getListPalanquee().get(curseurPalanquee).ajouterSerreFile(
						getPlongeurById(id, getListPlongeurQuiPlonge()));
				break;
			}
			pDAO.update(getListPalanquee().get(curseurPalanquee));
		}
	}

	private void plongeurFromRePlongeToPalanquee(long id, int position) {
		if (!getListPalanquee().get(curseurPalanquee).contientPlongeur(
				getPlongeurById(id, getListPlongeurQuiRePlonge()))
				&& getPlongeurById(id, getListPlongeurQuiRePlonge()) != null) {
			switch (position) {
			case 1:
				if (getListPalanquee().get(curseurPalanquee).getEncadrant() != null) {
					plDAO.deletePlongeurDePalanquee(
							(Plongeur) getListPalanquee().get(curseurPalanquee)
									.getEncadrant(),
							getListPalanquee().get(curseurPalanquee).getmId());
				}
				getPlongeurById(id, getListPlongeurQuiRePlonge())
						.setmTypePlongeur(PlongeurTypeEnum.ENCADRANT);
				plDAO.insertPlongeurDansPalanquee(
						getPlongeurById(id, getListPlongeurQuiRePlonge()),
						getListPalanquee().get(curseurPalanquee).getmId());
				getListPalanquee().get(curseurPalanquee).ajouterEncadrant(
						getPlongeurById(id, getListPlongeurQuiRePlonge()));
				break;
			case 2:
				if (getListPalanquee().get(curseurPalanquee).getPlongeur(0) != null) {
					plDAO.deletePlongeurDePalanquee(
							(Plongeur) getListPalanquee().get(curseurPalanquee)
									.getPlongeur(0),
							getListPalanquee().get(curseurPalanquee).getmId());
				}
				getPlongeurById(id, getListPlongeurQuiRePlonge())
						.setmTypePlongeur(PlongeurTypeEnum.PLONGEUR);
				plDAO.insertPlongeurDansPalanquee(
						getPlongeurById(id, getListPlongeurQuiRePlonge()),
						getListPalanquee().get(curseurPalanquee).getmId());
				getListPalanquee().get(curseurPalanquee).ajouterPlongeur(
						getPlongeurById(id, getListPlongeurQuiRePlonge()), 0);
				break;
			case 3:
				if (getListPalanquee().get(curseurPalanquee).getPlongeur(1) != null) {
					plDAO.deletePlongeurDePalanquee(
							(Plongeur) getListPalanquee().get(curseurPalanquee)
									.getPlongeur(1),
							getListPalanquee().get(curseurPalanquee).getmId());
				}
				getPlongeurById(id, getListPlongeurQuiRePlonge())
						.setmTypePlongeur(PlongeurTypeEnum.PLONGEUR);
				plDAO.insertPlongeurDansPalanquee(
						getPlongeurById(id, getListPlongeurQuiRePlonge()),
						getListPalanquee().get(curseurPalanquee).getmId());
				getListPalanquee().get(curseurPalanquee).ajouterPlongeur(
						getPlongeurById(id, getListPlongeurQuiRePlonge()), 1);
				break;
			case 4:
				if (getListPalanquee().get(curseurPalanquee).getPlongeur(2) != null) {
					plDAO.deletePlongeurDePalanquee(
							(Plongeur) getListPalanquee().get(curseurPalanquee)
									.getPlongeur(2),
							getListPalanquee().get(curseurPalanquee).getmId());
				}
				getPlongeurById(id, getListPlongeurQuiRePlonge())
						.setmTypePlongeur(PlongeurTypeEnum.PLONGEUR);
				plDAO.insertPlongeurDansPalanquee(
						getPlongeurById(id, getListPlongeurQuiRePlonge()),
						getListPalanquee().get(curseurPalanquee).getmId());
				getListPalanquee().get(curseurPalanquee).ajouterPlongeur(
						getPlongeurById(id, getListPlongeurQuiRePlonge()), 2);
				break;
			case 5:
				if (getListPalanquee().get(curseurPalanquee).getPlongeur(3) != null) {
					plDAO.deletePlongeurDePalanquee(
							(Plongeur) getListPalanquee().get(curseurPalanquee)
									.getPlongeur(3),
							getListPalanquee().get(curseurPalanquee).getmId());
				}
				getPlongeurById(id, getListPlongeurQuiRePlonge())
						.setmTypePlongeur(PlongeurTypeEnum.PLONGEUR);
				plDAO.insertPlongeurDansPalanquee(
						getPlongeurById(id, getListPlongeurQuiRePlonge()),
						getListPalanquee().get(curseurPalanquee).getmId());
				getListPalanquee().get(curseurPalanquee).ajouterPlongeur(
						getPlongeurById(id, getListPlongeurQuiRePlonge()), 3);
				break;
			case 6:
				if (getListPalanquee().get(curseurPalanquee).getSerreFile() != null) {
					plDAO.deletePlongeurDePalanquee(
							(Plongeur) getListPalanquee().get(curseurPalanquee)
									.getSerreFile(),
							getListPalanquee().get(curseurPalanquee).getmId());
				}
				getPlongeurById(id, getListPlongeurQuiRePlonge())
						.setmTypePlongeur(PlongeurTypeEnum.SERRE_FILE);
				plDAO.insertPlongeurDansPalanquee(
						getPlongeurById(id, getListPlongeurQuiRePlonge()),
						getListPalanquee().get(curseurPalanquee).getmId());
				getListPalanquee().get(curseurPalanquee).ajouterSerreFile(
						getPlongeurById(id, getListPlongeurQuiRePlonge()));
				break;
			}
			pDAO.update(getListPalanquee().get(curseurPalanquee));
		}
	}

	private void plongeurFromPalanquee(long id) {
		if (((Plongeur) getListPalanquee().get(curseurPalanquee).getEncadrant()) != null) {
			if (((Plongeur) getListPalanquee().get(curseurPalanquee)
					.getEncadrant()).getmId() == id) {
				plDAO.deletePlongeurDePalanquee((Plongeur) getListPalanquee()
						.get(curseurPalanquee).getEncadrant(),
						getListPalanquee().get(curseurPalanquee).getmId());
				getListPalanquee().get(curseurPalanquee).enleverEncadrant();
			}
		}
		if (((Plongeur) getListPalanquee().get(curseurPalanquee).getPlongeur(0)) != null) {
			if (((Plongeur) getListPalanquee().get(curseurPalanquee)
					.getPlongeur(0)).getmId() == id) {
				plDAO.deletePlongeurDePalanquee((Plongeur) getListPalanquee()
						.get(curseurPalanquee).getPlongeur(0),
						getListPalanquee().get(curseurPalanquee).getmId());
				getListPalanquee().get(curseurPalanquee).enleverPlongeur(0);
			}
		}
		if (((Plongeur) getListPalanquee().get(curseurPalanquee).getPlongeur(1)) != null) {
			if (((Plongeur) getListPalanquee().get(curseurPalanquee)
					.getPlongeur(1)).getmId() == id) {
				plDAO.deletePlongeurDePalanquee((Plongeur) getListPalanquee()
						.get(curseurPalanquee).getPlongeur(1),
						getListPalanquee().get(curseurPalanquee).getmId());
				getListPalanquee().get(curseurPalanquee).enleverPlongeur(1);
			}
		}
		if (((Plongeur) getListPalanquee().get(curseurPalanquee).getPlongeur(2)) != null) {
			if (((Plongeur) getListPalanquee().get(curseurPalanquee)
					.getPlongeur(2)).getmId() == id) {
				plDAO.deletePlongeurDePalanquee((Plongeur) getListPalanquee()
						.get(curseurPalanquee).getPlongeur(2),
						getListPalanquee().get(curseurPalanquee).getmId());
				getListPalanquee().get(curseurPalanquee).enleverPlongeur(2);
			}
		}
		if (((Plongeur) getListPalanquee().get(curseurPalanquee).getPlongeur(3)) != null) {
			if (((Plongeur) getListPalanquee().get(curseurPalanquee)
					.getPlongeur(3)).getmId() == id) {
				plDAO.deletePlongeurDePalanquee((Plongeur) getListPalanquee()
						.get(curseurPalanquee).getPlongeur(3),
						getListPalanquee().get(curseurPalanquee).getmId());
				getListPalanquee().get(curseurPalanquee).enleverPlongeur(3);
			}
		}
		if (((Plongeur) getListPalanquee().get(curseurPalanquee).getSerreFile()) != null) {
			if (((Plongeur) getListPalanquee().get(curseurPalanquee)
					.getSerreFile()).getmId() == id) {
				plDAO.deletePlongeurDePalanquee((Plongeur) getListPalanquee()
						.get(curseurPalanquee).getSerreFile(),
						getListPalanquee().get(curseurPalanquee).getmId());
				getListPalanquee().get(curseurPalanquee).enleverSerreFile();
			}
		}
		pDAO.update(getListPalanquee().get(curseurPalanquee));
	}

	private class BoxOnDragListener implements OnDragListener {

		int positionBox;

		public BoxOnDragListener(int positionBox) {
			this.positionBox = positionBox;
		}

		@Override
		public boolean onDrag(View v, DragEvent event) {
			int action = event.getAction();
			switch (action) {
			case DragEvent.ACTION_DRAG_STARTED:

				break;
			case DragEvent.ACTION_DRAG_ENTERED:
				break;
			case DragEvent.ACTION_DRAG_EXITED:
				break;
			case DragEvent.ACTION_DROP:
				if (idDragPlongeur > -1) {
					if (tabHostPlongeRePlonge.getCurrentTab() == 0) {
						plongeurFromPlongeToPalanquee(idDragPlongeur,
								positionBox);
					}
					if (tabHostPlongeRePlonge.getCurrentTab() == 1) {
						plongeurFromRePlongeToPalanquee(idDragPlongeur,
								positionBox);
					}
				}
				break;
			case DragEvent.ACTION_DRAG_ENDED:
				idDragPlongeur = -1;
				updateAll();
			default:
				break;
			}
			return true;
		}

	}

	private class GridOnDragListener implements OnDragListener {

		@Override
		public boolean onDrag(View v, DragEvent event) {
			int action = event.getAction();
			switch (action) {
			case DragEvent.ACTION_DRAG_STARTED:

				break;
			case DragEvent.ACTION_DRAG_ENTERED:
				break;
			case DragEvent.ACTION_DRAG_EXITED:
				break;
			case DragEvent.ACTION_DROP:
				if (idDragPlongeur > -1) {
					plongeurFromPalanquee(idDragPlongeur);
				}
				break;
			case DragEvent.ACTION_DRAG_ENDED:
				idDragPlongeur = -1;
				updateAll();
			default:
				break;
			}
			return true;
		}

	}

	private class BoxClickListener implements OnClickListener {

		int positionBox;

		public BoxClickListener(int positionBox) {
			this.positionBox = positionBox;
		}

		@Override
		public void onClick(View v) {
			System.out.println("xxxxxxxxxxxxxxxxxxxxxx :" + positionBox);
			if (idDragPlongeur > -1) {
				if (tabHostPlongeRePlonge.getCurrentTab() == 0) {
					plongeurFromPlongeToPalanquee(idDragPlongeur, positionBox);
				}
				if (tabHostPlongeRePlonge.getCurrentTab() == 1) {
					plongeurFromRePlongeToPalanquee(idDragPlongeur, positionBox);
				}
			}
			idDragPlongeur = -1;
			updateAll();
		}

	}

	private void updateTitrePalanquee() {
		TextView tv = (TextView) findViewById(R.id.tvIdPalanqueePalanquee);
		tv.setText("Palanquée n°" + (curseurPalanquee + 1) + " / "
				+ getListPalanquee().size());
	}

	private void updateImagePeutPlonger() {
		ImageView im = (ImageView) findViewById(R.id.iv_peut_plonger);
		if (getListPalanquee().get(curseurPalanquee).peutPlonger()) {
			im.setImageBitmap(BitmapFactory.decodeResource(getResources(),
					R.drawable.rond_vert));
		} else {
			im.setImageBitmap(BitmapFactory.decodeResource(getResources(),
					R.drawable.rond_rouge));
		}
	}
}
