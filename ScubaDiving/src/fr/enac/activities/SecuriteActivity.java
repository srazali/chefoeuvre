package fr.enac.activities;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.scubadiving.R;

import fr.enac.actionbars.NavigationSpinner;
import fr.enac.constantes.ActivityOrderEnum;
import fr.enac.database.DatabaseHelper;
import fr.enac.database.ParametrePlongeeDAO;
import fr.enac.database.PlongeeDAO;
import fr.enac.models.ParametrePlongee;
import fr.enac.models.ParametrePlongeeCategorieEnum;
import fr.enac.models.Plongee;
import fr.enac.utils.IconParamPlongee;
import fr.enac.utils.tuleparametreplongee.TuleParamPlongeeGridAdapter;
import fr.enac.utils.tuleparametreplongee.TuleParamPlongeeGridItemsUpdateEvent;
import fr.enac.utils.tuleparametreplongee.TuleParamPlongeeGridItemsUpdateListener;
import fr.enac.utils.tuleparametreplongee.TuleParamPlongeeGridOnItemClickListener;
import fr.enac.utils.tuleparametreplongee.TuleParamPlongeeObjetAdapter;
import fr.enac.utils.tuleparametreplongee.TuleParamPlongeeObjetAdapter.TuleParamPlongeeEtat;

public class SecuriteActivity extends BaseActivity implements
		TuleParamPlongeeGridItemsUpdateListener {

	private static final String LOG_TAG = SecuriteActivity.class.getName();
	private NavigationSpinner spinner;

	// Variables relatifs � la grille des parametres "Mat�riel de securit�"
	private GridView mtuleGrilleMateriel;
	private ArrayList<TuleParamPlongeeObjetAdapter> mtulesMateriel;
	private TuleParamPlongeeGridAdapter mtuleGrilleAdapterMateriel;

	// Variables relatifs � la grille des parametres "Signalisation"
	private GridView mtuleGrilleSignal;
	private ArrayList<TuleParamPlongeeObjetAdapter> mtulesSignal;
	private TuleParamPlongeeGridAdapter mtuleGrilleAdapterSignal;

	/* Plongée */
	private ArrayList<ParametrePlongee> listParamPlongeeBase;

	// Variable de la page
	// ---------------------- Layout
	private LinearLayout llv_detail_paramPlongee;

	// ---------------------- Text
	private EditText edt_nomParamPlongee;
	private Spinner spin_catParamPlongee;
	private EditText edt_detailsParamPlongee;

	// ---------------------- Boutons
	private Button btn_creer_paramPlongee;
	private Button btn_modifier_paramPlongee;
	private ImageButton btn_supprimer_paramPlongee;

	// Variables de retour sur erreur
	// ----------------------- Modification
	private String tempNomParam = "";
	private String tempDetailsParam = "";
	private ParametrePlongeeCategorieEnum tempCat = null;
	private Handler mHandler_cancel_modifParamPlongee;

	private int delay = 3000;

	// ----------------------- Suppression
	private Handler mHandler_cancel_supprParamPlongee;
	private TuleParamPlongeeObjetAdapter tuleSupprime;
	private int catTuleSupprime = -1;

	// Image
	private Bitmap imageCorbeille;
	private Bitmap imageBack;
	private Bitmap imageParam;

	/* Pour éviter d'avoir les champs nom et prénom null */
	private boolean nomEstVide;

	// BDD

	/* Récuperation de la base de donn�es */
	private DatabaseHelper db;

	// Tables utilisées
	private ParametrePlongeeDAO paramPlongeeDAO;
	private PlongeeDAO plongeeDAO;

	private enum EtatSecurite {
		AUCUN_PARAMETRE_SELECTIONNE, PARAMETRE_SELECTIONNE, MODE_CREATION, MODE_APRES_SUPPRESSION, MODE_APRES_MODIFICATION
	}

	private EtatSecurite etatCourant;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_securite);

		initComponents();
		mOrdre = ActivityOrderEnum.SECURITE_ACTIVITY;
	}

	@Override
	protected void onStart() {
		super.onStart();

		creerParametresBase();

		// creerBDD();

		majParametresPlongeeBase();
		initGrilleParametresMateriel();

		initGrilleParametresSignal();

		initHandlers();

		etatCourant = EtatSecurite.MODE_CREATION;
		actions(etatCourant);
	}

	// ------------------------------------------------------------------//
	// Fonctions d'initialisation //
	// ------------------------------------------------------------------//
	private void initComponents() {

		// Remplissage des parametres de plong�e

		mtulesMateriel = new ArrayList<TuleParamPlongeeObjetAdapter>();
		mtulesSignal = new ArrayList<TuleParamPlongeeObjetAdapter>();

		llv_detail_paramPlongee = (LinearLayout) findViewById(R.id.llv_rightSidebarre);
		btn_creer_paramPlongee = (Button) findViewById(R.id.btn_securite_creer_param);
		btn_modifier_paramPlongee = (Button) findViewById(R.id.btn_securite_modifier_param);
		btn_supprimer_paramPlongee = (ImageButton) findViewById(R.id.btn_securite_supprimer_param);

		edt_nomParamPlongee = (EditText) findViewById(R.id.edt_securite_nom_param_plongee);
		spin_catParamPlongee = (Spinner) findViewById(R.id.spinner_securite_cat_parametre);
		edt_detailsParamPlongee = (EditText) findViewById(R.id.edt_securite_details_parametre);

		mHandler_cancel_modifParamPlongee = new Handler();
		mHandler_cancel_supprParamPlongee = new Handler();
		tuleSupprime = null;

		imageCorbeille = BitmapFactory.decodeResource(getResources(),
				R.drawable.ic_corbeille);
		imageBack = BitmapFactory.decodeResource(getResources(),
				R.drawable.ic_back);
		imageParam = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.ic_imagedefault);

		db = DatabaseHelper.getInstance(this);
		paramPlongeeDAO = new ParametrePlongeeDAO(db.getWritableDatabase());
		plongeeDAO = new PlongeeDAO(db.getWritableDatabase());

		listParamPlongeeBase = new ArrayList<ParametrePlongee>();

		nomEstVide = false;
	}

	private void creerParametresBase() {
		long idPlongee = mPlongee.getmId();
		listParamPlongeeBase.clear();

		// Création parametres
		listParamPlongeeBase.add(new ParametrePlongee(-1, "Plan de secours", 0,
				idPlongee, "Plan de secours des ADLM",
				ParametrePlongeeCategorieEnum.MATERIEL_SECURITE));
		listParamPlongeeBase.add(new ParametrePlongee(-1, "Oxygenothérapie", 0,
				idPlongee, "",
				ParametrePlongeeCategorieEnum.MATERIEL_SECURITE));
		listParamPlongeeBase.add(new ParametrePlongee(-1, "Trousse de secours",
				0, idPlongee, "Malette de premiers secours",
				ParametrePlongeeCategorieEnum.MATERIEL_SECURITE));
		listParamPlongeeBase.add(new ParametrePlongee(-1, "Téléphone/VHF", 0,
				idPlongee, "Communication exterieur",
				ParametrePlongeeCategorieEnum.MATERIEL_SECURITE));
		listParamPlongeeBase.add(new ParametrePlongee(-1, "Fiches évaluation",
				0, idPlongee, "",
				ParametrePlongeeCategorieEnum.MATERIEL_SECURITE));
		listParamPlongeeBase.add(new ParametrePlongee(-1, "Bloc Gréée", 0,
				idPlongee, "",
				ParametrePlongeeCategorieEnum.MATERIEL_SECURITE));
		listParamPlongeeBase.add(new ParametrePlongee(-1, "Rappel plongeurs",
				0, idPlongee, "",
				ParametrePlongeeCategorieEnum.MATERIEL_SECURITE));
		listParamPlongeeBase.add(new ParametrePlongee(-1, "Tab. notation", 0,
				idPlongee, "",
				ParametrePlongeeCategorieEnum.MATERIEL_SECURITE));
		listParamPlongeeBase.add(new ParametrePlongee(-1, "Tab. décompression",
				0, idPlongee, "",
				ParametrePlongeeCategorieEnum.MATERIEL_SECURITE));

		listParamPlongeeBase.add(new ParametrePlongee(-1, "Talkie/Walkie", 0,
				idPlongee, "Communication sécu.surface",
				ParametrePlongeeCategorieEnum.SIGNALISATION));
		listParamPlongeeBase.add(new ParametrePlongee(-1, "Pavillon Alpha", 0,
				idPlongee, "",
				ParametrePlongeeCategorieEnum.SIGNALISATION));
		listParamPlongeeBase.add(new ParametrePlongee(-1, "Bouée", 0,
				idPlongee, "",
				ParametrePlongeeCategorieEnum.SIGNALISATION));
		listParamPlongeeBase.add(new ParametrePlongee(-1, "Ligne de vie", 0,
				idPlongee, "",
				ParametrePlongeeCategorieEnum.SIGNALISATION));
		listParamPlongeeBase.add(new ParametrePlongee(-1, "Lampe à éclats", 0,
				idPlongee, "",
				ParametrePlongeeCategorieEnum.SIGNALISATION));
		listParamPlongeeBase.add(new ParametrePlongee(-1, "Cyalumes", 0,
				idPlongee, "",
				ParametrePlongeeCategorieEnum.SIGNALISATION));
		listParamPlongeeBase.add(new ParametrePlongee(-1, "Corps mort", 0,
				idPlongee, "",
				ParametrePlongeeCategorieEnum.SIGNALISATION));
		listParamPlongeeBase.add(new ParametrePlongee(-1, "Gueuse", 0,
				idPlongee, "",
				ParametrePlongeeCategorieEnum.SIGNALISATION));

	}

	/**
	 * Affiche la liste des parametres de plongée de type "Matériel de sécurit�"
	 * dans des tules
	 */
	private void initGrilleParametresMateriel() {

		mtulesMateriel.clear();
		createTulesMateriel(mPlongee.getmListeParametres(), true);
		createTulesMateriel(listParamPlongeeBase, false);

		mtuleGrilleMateriel = (GridView) findViewById(R.id.gridview_cat_materiel);
		mtuleGrilleAdapterMateriel = new TuleParamPlongeeGridAdapter(this,
				R.layout.tule_parametre_objetgrid, mtulesMateriel);

		mtuleGrilleMateriel.setAdapter(mtuleGrilleAdapterMateriel);
		mtuleGrilleMateriel.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);

	}

	private void createTulesMateriel(
			List<ParametrePlongee> selectParametresPlongee, boolean isParamOk) {
		TuleParamPlongeeEtat etatTule;
		if (isParamOk) {
			etatTule = TuleParamPlongeeEtat.NON_SELECTIONNE_ET_VALIDE;
		} else {
			etatTule = TuleParamPlongeeEtat.NON_SELECTIONNE_ET_NON_VALIDE;
		}
		for (ParametrePlongee pp : selectParametresPlongee) {
			if (pp.getmCategorie() == ParametrePlongeeCategorieEnum.MATERIEL_SECURITE) {
				mtulesMateriel.add(new TuleParamPlongeeObjetAdapter(etatTule,
						IconParamPlongee.getImageFromParamPlongee(getResources(), pp.getmNom()), pp.getmNom(), pp.getmDetails(), pp
								.getmCategorie()));
			}
		}
	}

	/**
	 * Affiche la liste des parametres de plong�e de type "Signalisation" dans
	 * des tules
	 */
	private void initGrilleParametresSignal() {

		mtulesSignal.clear();
		createTulesSignal(mPlongee.getmListeParametres(), true);
		createTulesSignal(listParamPlongeeBase, false);

		mtuleGrilleSignal = (GridView) findViewById(R.id.gridview_cat_signal);
		mtuleGrilleAdapterSignal = new TuleParamPlongeeGridAdapter(this,
				R.layout.tule_parametre_objetgrid, mtulesSignal);

		mtuleGrilleSignal.setAdapter(mtuleGrilleAdapterSignal);
		mtuleGrilleSignal.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);

	}

	private void createTulesSignal(
			List<ParametrePlongee> selectParametresPlongee, boolean isParamOk) {
		TuleParamPlongeeEtat etatTule;
		if (isParamOk) {
			etatTule = TuleParamPlongeeEtat.NON_SELECTIONNE_ET_VALIDE;
		} else {
			etatTule = TuleParamPlongeeEtat.NON_SELECTIONNE_ET_NON_VALIDE;
		}
		for (ParametrePlongee pp : selectParametresPlongee) {
			if (pp.getmCategorie() == ParametrePlongeeCategorieEnum.SIGNALISATION) {
				mtulesSignal.add(new TuleParamPlongeeObjetAdapter(etatTule,
						IconParamPlongee.getImageFromParamPlongee(getResources(), pp.getmNom()), pp.getmNom(), pp.getmDetails(), pp
								.getmCategorie()));
			}
		}
	}

	private void majParametresPlongeeBase() {
		ArrayList<ParametrePlongee> listMAJ = new ArrayList<ParametrePlongee>();
		for (ParametrePlongee ppPlongee : mPlongee.getmListeParametres()) {
			/*
			 * for(ParametrePlongee ppListBase : listParamPlongeeBase){
			 * if(!(listMAJ.contains(ppListBase))){
			 * if(!(ppListBase.getmNom().equals(ppPlongee.getmNom()))){
			 * if(!(ppListBase
			 * .getmCategorie().toString().equals(ppPlongee.getmCategorie
			 * ().toString()))){ listMAJ.add(ppListBase); } } } }
			 */
			listParamPlongeeBase.remove(ppPlongee);
		}
		affichBases("MAJ PARAM");
	}

	private void initHandlers() {

		// Création du handler de la grille de Matériel de la gridview
		TuleParamPlongeeGridOnItemClickListener handlerGrilleMateriel = new TuleParamPlongeeGridOnItemClickListener(
				mtulesMateriel, mtuleGrilleAdapterMateriel, mtulesSignal,
				mtuleGrilleAdapterSignal);
		// Abonnement aux events de la gridview de Matériel
		handlerGrilleMateriel.addListener(this);
		// Affectation du handler à la gridview de Matériel
		mtuleGrilleMateriel.setOnItemClickListener(handlerGrilleMateriel);

		// Création du handler de la grille de Signalisation de la gridview
		TuleParamPlongeeGridOnItemClickListener handlerGrilleSignal = new TuleParamPlongeeGridOnItemClickListener(
				mtulesSignal, mtuleGrilleAdapterSignal, mtulesMateriel,
				mtuleGrilleAdapterMateriel);
		// Abonnement aux events de la gridview de Signalisation
		handlerGrilleSignal.addListener(this);
		// Affectation du handler à la gridview de Signalisation
		mtuleGrilleSignal.setOnItemClickListener(handlerGrilleSignal);

		// Listener sur l'editView permettant de ne pas ajouter un parametre
		// avec un nom vide
		EditText nomParam = (EditText) findViewById(R.id.edt_securite_nom_param_plongee);
		nomParam.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.length() == 0)
					nomEstVide = true;
				else
					nomEstVide = false;

				if (nomEstVide)
					activerLargeBouton(R.id.btn_securite_modifier_param, false);
				else
					activerLargeBouton(R.id.btn_securite_modifier_param, true);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
	}

	// ------------------------------------------------------------------//
	// Logique d'interactions //
	// ------------------------------------------------------------------//
	/**
	 * Gère l'affichage selon l'état de l'activité
	 */
	private void actions(EtatSecurite etat) {
		TuleParamPlongeeObjetAdapter tuleSelectionnee;

		switch (etat) {
		case AUCUN_PARAMETRE_SELECTIONNE:
			// on déselectionne le tule selectionné (s'il ya)
			tuleSelectionnee = getTuleSelectionnee();
			if (tuleSelectionnee != null) {
				tuleSelectionnee.deselectionne();
				// on notifie la grille de déselectionner la tule
				mtuleGrilleAdapterMateriel.notifyDataSetChanged();
				mtuleGrilleAdapterSignal.notifyDataSetChanged();
			}
			btn_supprimer_paramPlongee.setImageBitmap(imageCorbeille);
			// on disable le formulaire
			activerFormulaire(false);
			// on remet les champs du formulaire a zéro
			resetFormulaire();
			// on active le bouton "Ajouter un plongeur"
			activerLargeBouton(R.id.btn_securite_creer_param, true);
			// on remet les textes des boutons à jour.
			changeTextBouton(
					R.id.btn_securite_modifier_param,
					(String) getResources().getString(
							R.string.btn_securite_modifier));
			changeTextBouton(
					R.id.btn_securite_creer_param,
					(String) getResources().getString(
							R.string.btn_securite_creer));
			mHandler_cancel_modifParamPlongee
					.removeCallbacks(mUpdateButtonModif);
			mHandler_cancel_supprParamPlongee
					.removeCallbacks(mUpdateButtonSuppr);
			break;

		case MODE_CREATION:

			// on déselectionne le tule selectionné (s'il ya)
			tuleSelectionnee = getTuleSelectionnee();
			if (tuleSelectionnee != null) {
				tuleSelectionnee.deselectionne();
				// on notifie la grille du déselectionnement du tule
				mtuleGrilleAdapterMateriel.notifyDataSetChanged();
				mtuleGrilleAdapterSignal.notifyDataSetChanged();
			}
			// on remet les champs du formulaire a zéro
			resetFormulaire();
			// on enable le formulaire
			activerFormulaire(true);
			// on remplace le text du bouton par "ajouter"
			changeTextBouton(
					R.id.btn_securite_modifier_param,
					(String) getResources().getString(
							R.string.btn_securite_creer));
			// On desactive quand meme le bouton d'ajout pour ne pas permettre
			// l'ajout vide
			activerLargeBouton(R.id.btn_securite_creer_param, false);
			activerLargeBouton(R.id.btn_securite_modifier_param, false);
			activerImageBouton(R.id.btn_securite_supprimer_param, false);
			break;

		case PARAMETRE_SELECTIONNE:
			// On récupère le parametre à partir du tule selectionné
			ParametrePlongee ppSelect = getParamDepuisTuleSelec(getTuleSelectionnee());
			System.out.println("DEBUG PP : " + ppSelect.getmNom() + " : "
					+ ppSelect.getmDetails());
			// on remet les champs du formulaire a zéro
			resetFormulaire();
			// on enable le formulaire
			activerFormulaire(true);
			// on active le bouton "Ajouter un plongeur"
			activerLargeBouton(R.id.btn_securite_modifier_param, true);
			activerLargeBouton(R.id.btn_securite_creer_param, true);
			btn_supprimer_paramPlongee.setImageBitmap(imageCorbeille);
			// on remplace le text du bouton par "modifier"
			changeTextBouton(
					R.id.btn_securite_modifier_param,
					(String) getResources().getString(
							R.string.btn_securite_modifier));
			// on rempli le formulaire par les informations du plongeur
			// sélectionné
			remplirFormulaire(ppSelect);
			mHandler_cancel_modifParamPlongee
					.removeCallbacks(mUpdateButtonModif);
			mHandler_cancel_supprParamPlongee
					.removeCallbacks(mUpdateButtonSuppr);
			break;

		case MODE_APRES_MODIFICATION:
			// on remplace le text du bouton par "annuler"
			changeTextBouton(
					R.id.btn_securite_modifier_param,
					(String) getResources().getString(
							R.string.btn_securite_annuler));
			// on deactive les 2 autres boutons
			activerLargeBouton(R.id.btn_securite_creer_param, false);
			activerImageBouton(R.id.btn_securite_supprimer_param, false);
			btn_supprimer_paramPlongee.setEnabled(false);

			mHandler_cancel_modifParamPlongee
					.removeCallbacks(mUpdateButtonModif);
			mHandler_cancel_modifParamPlongee.postDelayed(mUpdateButtonModif,
					delay);
			break;

		case MODE_APRES_SUPPRESSION:
			// on remet les champs du formulaire a zéro
			resetFormulaire();

			btn_supprimer_paramPlongee.setImageBitmap(imageBack);
			activerLargeBouton(R.id.btn_securite_creer_param, false);
			activerLargeBouton(R.id.btn_securite_modifier_param, false);

			mHandler_cancel_supprParamPlongee
					.removeCallbacks(mUpdateButtonSuppr);
			mHandler_cancel_supprParamPlongee.postDelayed(mUpdateButtonSuppr,
					delay);
			break;
		default:
			break;
		}
	}

	@Override
	public void tulesUpdated(TuleParamPlongeeGridItemsUpdateEvent event) {
		switch (etatCourant) {
		case AUCUN_PARAMETRE_SELECTIONNE:
			etatCourant = getEtatDepuisTulesEvent(event);
			actions(etatCourant);
			if (etatCourant == EtatSecurite.PARAMETRE_SELECTIONNE) {
				gestionParam();
			}
			break;

		case MODE_CREATION:
			etatCourant = getEtatDepuisTulesEvent(event);
			actions(etatCourant);
			if (etatCourant == EtatSecurite.PARAMETRE_SELECTIONNE) {
				gestionParam();
			}
			break;

		case PARAMETRE_SELECTIONNE:
			etatCourant = getEtatDepuisTulesEvent(event);
			actions(etatCourant);
			if (etatCourant == EtatSecurite.PARAMETRE_SELECTIONNE) {
				gestionParam();
			}
			break;

		case MODE_APRES_MODIFICATION:
			etatCourant = getEtatDepuisTulesEvent(event);
			actions(etatCourant);
			if (etatCourant == EtatSecurite.PARAMETRE_SELECTIONNE) {
				gestionParam();
			}
			break;

		case MODE_APRES_SUPPRESSION:
			suppressionTuleBDD();
			etatCourant = getEtatDepuisTulesEvent(event);
			actions(etatCourant);
			if (etatCourant == EtatSecurite.PARAMETRE_SELECTIONNE) {
				gestionParam();
			}
			break;

		default:
			break;
		}
	}

	private void gestionParam() {
		// Puisque l'état est en parametre selectionne, on va checker si la tule
		// est valide ou non
		TuleParamPlongeeObjetAdapter tuleSel = getTuleSelectionnee();
		// On recupere le parametre plongee du tule selectionne
		ParametrePlongee ppInsert = getParamDepuisTuleSelec(tuleSel);

		if (tuleSel.getEtatCourant() == TuleParamPlongeeEtat.SELECTIONNE_ET_VALIDE) {
			if (!(mPlongee.getmListeParametres().contains(ppInsert))) {
				ppInsert.setmId(paramPlongeeDAO.insert(ppInsert));
				mPlongee.getmListeParametres().add(ppInsert);
			}
		} else if (tuleSel.getEtatCourant() == TuleParamPlongeeEtat.SELECTIONNE_ET_NON_VALIDE) {

			boolean test = mPlongee.getmListeParametres().remove(ppInsert);
			paramPlongeeDAO.delete(ppInsert);
		}
		affichBases("Selection Param");
	}

	/**
	 * Return l'état en fonction de la liste de tules envoyé par l'event
	 * 
	 * @param event
	 * @return
	 */
	private EtatSecurite getEtatDepuisTulesEvent(
			TuleParamPlongeeGridItemsUpdateEvent event) {
		TuleParamPlongeeObjetAdapter tuleSelected = getTuleSelectionnee();

		if (tuleSelected == null) {
			return EtatSecurite.MODE_CREATION;
		} else { // si une tule est sélectionnée
			System.out.println("DEBUG : " + tuleSelected.getNom() + " : "
					+ tuleSelected.getMdetails());
			return EtatSecurite.PARAMETRE_SELECTIONNE;
		}
	}

	private ParametrePlongee getParamDepuisTuleSelec(
			TuleParamPlongeeObjetAdapter tuleSelect) {
		ParametrePlongee retour = null;
		ArrayList<ParametrePlongee> listParamPlongee = listParamPlongeeBase;
		for (ParametrePlongee pp : mPlongee.getmListeParametres()) {
			listParamPlongee.add(pp);
		}
		for (ParametrePlongee pp : listParamPlongee) {
			if ((pp.getmNom().equals(tuleSelect.getNom()))
					&& (pp.getmCategorie().toString().equals(tuleSelect
							.getMcategorie().toString()))) {
				retour = pp;
			}
		}

		return retour;
	}

	public void clicCreerParametrePlongee(View v) {
		switch (etatCourant) {
		case AUCUN_PARAMETRE_SELECTIONNE:
			etatCourant = EtatSecurite.MODE_CREATION;
			actions(etatCourant);
			break;

		case MODE_CREATION:
			etatCourant = EtatSecurite.AUCUN_PARAMETRE_SELECTIONNE;
			actions(etatCourant);
			break;

		case PARAMETRE_SELECTIONNE:
			etatCourant = EtatSecurite.MODE_CREATION;
			actions(etatCourant);
			break;

		case MODE_APRES_MODIFICATION:
			// Interdit
			break;

		case MODE_APRES_SUPPRESSION:
			// Interdit
			break;
		default:
			break;
		}
	}

	public void clicModifierParametrePlongee(View v) {

		switch (etatCourant) {
		case AUCUN_PARAMETRE_SELECTIONNE:
			// Interdit
			break;

		case MODE_CREATION:
			// Création d'un parametre
			creerParametre();
			etatCourant = EtatSecurite.MODE_CREATION;
			actions(etatCourant);
			break;

		case PARAMETRE_SELECTIONNE:
			// Modification d'un parametre
			enregistrerTempInfosModif();
			etatCourant = EtatSecurite.MODE_APRES_MODIFICATION;
			actions(etatCourant);
			break;

		case MODE_APRES_MODIFICATION:
			// Retour sur modification
			retourModifParam();
			etatCourant = EtatSecurite.PARAMETRE_SELECTIONNE;
			actions(etatCourant);
			break;

		case MODE_APRES_SUPPRESSION:
			// Interdit
			break;
		default:
			break;
		}
	}

	private void retourModifParam() {

		for (TuleParamPlongeeObjetAdapter t : mtulesMateriel) {
			if ((t.getEtatCourant() == TuleParamPlongeeEtat.SELECTIONNE_ET_NON_VALIDE)
					|| (t.getEtatCourant() == TuleParamPlongeeEtat.SELECTIONNE_ET_VALIDE)) {
				t.setMnom(tempNomParam);
				t.setMdetails(tempDetailsParam);
			}
		}

		for (TuleParamPlongeeObjetAdapter t : mtulesSignal) {
			if ((t.getEtatCourant() == TuleParamPlongeeEtat.SELECTIONNE_ET_NON_VALIDE)
					|| (t.getEtatCourant() == TuleParamPlongeeEtat.SELECTIONNE_ET_VALIDE)) {
				t.setMnom(tempNomParam);
				t.setMdetails(tempDetailsParam);
			}
		}

		mtuleGrilleAdapterMateriel.notifyDataSetChanged();
		mtuleGrilleAdapterSignal.notifyDataSetChanged();

		mHandler_cancel_modifParamPlongee.removeCallbacks(mUpdateButtonModif);
	}

	private void creerParametre() {
		ParametrePlongeeCategorieEnum catParamCreer = ParametrePlongeeCategorieEnum.MATERIEL_SECURITE;
		switch (spin_catParamPlongee.getSelectedItemPosition()) {
		case 0:
			catParamCreer = ParametrePlongeeCategorieEnum.MATERIEL_SECURITE;
			mtulesMateriel.add(new TuleParamPlongeeObjetAdapter(
					TuleParamPlongeeEtat.NON_SELECTIONNE_ET_VALIDE, IconParamPlongee.getImageFromParamPlongee(getResources(), edt_nomParamPlongee.getText().toString()),
					edt_nomParamPlongee.getText().toString(),
					edt_detailsParamPlongee.getText().toString(),
					ParametrePlongeeCategorieEnum.MATERIEL_SECURITE));
			break;

		case 1:
			catParamCreer = ParametrePlongeeCategorieEnum.SIGNALISATION;
			mtulesSignal.add(new TuleParamPlongeeObjetAdapter(
					TuleParamPlongeeEtat.NON_SELECTIONNE_ET_VALIDE, IconParamPlongee.getImageFromParamPlongee(getResources(), edt_nomParamPlongee.getText().toString()),
					edt_nomParamPlongee.getText().toString(),
					edt_detailsParamPlongee.getText().toString(),
					ParametrePlongeeCategorieEnum.SIGNALISATION));
			break;
		default:
		}

		ParametrePlongee ppInsert = new ParametrePlongee(-1,
				edt_nomParamPlongee.getText().toString(), 0, mPlongee.getmId(),
				edt_detailsParamPlongee.getText().toString(), catParamCreer);

		ppInsert.setmId(paramPlongeeDAO.insert(ppInsert));

		mPlongee.getmListeParametres().add(ppInsert);

		listParamPlongeeBase.add(ppInsert);

		mtuleGrilleAdapterSignal.notifyDataSetChanged();
		mtuleGrilleAdapterMateriel.notifyDataSetChanged();

		affichBases("CREER");
	}

	private void enregistrerTempInfosModif() {
		ParametrePlongee ppSelect = getParamDepuisTuleSelec(getTuleSelectionnee());
		TuleParamPlongeeObjetAdapter tuleAModifier = null;
		TuleParamPlongeeObjetAdapter tuleModifie = null;
		for (TuleParamPlongeeObjetAdapter t : mtulesMateriel) {
			if ((t.getEtatCourant() == TuleParamPlongeeEtat.SELECTIONNE_ET_NON_VALIDE)
					|| (t.getEtatCourant() == TuleParamPlongeeEtat.SELECTIONNE_ET_VALIDE)) {
				tempNomParam = t.getNom();
				tempCat = t.getMcategorie();
				tempDetailsParam = t.getMdetails();
				tuleAModifier = t;
				tuleModifie = t;
			}
		}

		for (TuleParamPlongeeObjetAdapter t : mtulesSignal) {
			if ((t.getEtatCourant() == TuleParamPlongeeEtat.SELECTIONNE_ET_NON_VALIDE)
					|| (t.getEtatCourant() == TuleParamPlongeeEtat.SELECTIONNE_ET_VALIDE)) {
				tempNomParam = t.getNom();
				tempCat = t.getMcategorie();
				tempDetailsParam = t.getMdetails();
				tuleAModifier = t;
				tuleModifie = t;
			}
		}
		
		tuleModifie.setMnom(edt_nomParamPlongee.getText().toString());
		if (spin_catParamPlongee.getSelectedItemId() == 0) {
			tuleModifie.setMcategorie(ParametrePlongeeCategorieEnum.MATERIEL_SECURITE);
		} else {
			tuleModifie.setMcategorie(ParametrePlongeeCategorieEnum.SIGNALISATION);
		}
		tuleModifie.setMdetails(edt_detailsParamPlongee.getText().toString());
		
		if(tuleAModifier.getMcategorie() == ParametrePlongeeCategorieEnum.MATERIEL_SECURITE){
			mtulesSignal.remove(tuleAModifier);
			mtulesMateriel.add(tuleModifie);
		} else {
			mtulesMateriel.remove(tuleAModifier);
			mtulesSignal.add(tuleModifie);
		}
		mtuleGrilleAdapterMateriel.notifyDataSetChanged();
		mtuleGrilleAdapterSignal.notifyDataSetChanged();
		
		listParamPlongeeBase.remove(ppSelect);

		ppSelect.setmNom(edt_nomParamPlongee.getText().toString());
		if (spin_catParamPlongee.getSelectedItemId() == 0) {
			ppSelect.setmCategorie(ParametrePlongeeCategorieEnum.MATERIEL_SECURITE);
		} else {
			ppSelect.setmCategorie(ParametrePlongeeCategorieEnum.SIGNALISATION);
		}
		ppSelect.setmDetails(edt_detailsParamPlongee.getText().toString());
		listParamPlongeeBase.add(ppSelect);

	}

	public void clicSupprimerParametrePlongee(View v) {

		switch (etatCourant) {
		case AUCUN_PARAMETRE_SELECTIONNE:
			// Interdit
			break;

		case MODE_CREATION:
			// Interdit
			break;

		case PARAMETRE_SELECTIONNE:
			// Vers mode Suppression en attente
			gestionSuppressionTule();
			etatCourant = EtatSecurite.MODE_APRES_SUPPRESSION;
			actions(etatCourant);
			break;

		case MODE_APRES_MODIFICATION:
			// Interdit
			break;

		case MODE_APRES_SUPPRESSION:
			// Retour sur suppression
			retourSuppression();
			etatCourant = EtatSecurite.PARAMETRE_SELECTIONNE;
			actions(etatCourant);
			break;
		default:
			break;
		}
	}

	private void retourSuppression() {
		switch (catTuleSupprime) {
		case -1:
			// Pas de suppression
			break;
		case 0:
			mtulesMateriel.add(tuleSupprime);
			break;
		case 1:
			mtulesSignal.add(tuleSupprime);
			break;
		default:
			break;
		}
		tuleSupprime.setEtatCourant(TuleParamPlongeeEtat.SELECTIONNE_ET_VALIDE);
		mtuleGrilleAdapterMateriel.notifyDataSetChanged();
		mtuleGrilleAdapterSignal.notifyDataSetChanged();
		mHandler_cancel_supprParamPlongee.removeCallbacks(mUpdateButtonSuppr);
		tuleSupprime = null;
		catTuleSupprime = -1;
	}

	private void gestionSuppressionTule() {
		for (TuleParamPlongeeObjetAdapter t : mtulesMateriel) {
			if ((t.getEtatCourant() == TuleParamPlongeeEtat.SELECTIONNE_ET_NON_VALIDE)
					|| (t.getEtatCourant() == TuleParamPlongeeEtat.SELECTIONNE_ET_VALIDE)) {
				tuleSupprime = t;
				catTuleSupprime = 0;
			}
		}

		for (TuleParamPlongeeObjetAdapter t : mtulesSignal) {
			if ((t.getEtatCourant() == TuleParamPlongeeEtat.SELECTIONNE_ET_NON_VALIDE)
					|| (t.getEtatCourant() == TuleParamPlongeeEtat.SELECTIONNE_ET_VALIDE)) {
				tuleSupprime = t;
				catTuleSupprime = 1;
			}
		}

		switch (catTuleSupprime) {
		case -1:
			// Pas de suppression
			break;
		case 0:
			mtulesMateriel.remove(tuleSupprime);
			btn_supprimer_paramPlongee.setImageBitmap(imageBack);
			break;
		case 1:
			mtulesSignal.remove(tuleSupprime);
			break;
		default:
			break;
		}
		mtuleGrilleAdapterMateriel.notifyDataSetChanged();
		mtuleGrilleAdapterSignal.notifyDataSetChanged();
	}

	private Runnable mUpdateButtonModif = new Runnable() {
		public void run() {
			switch (etatCourant) {
			case AUCUN_PARAMETRE_SELECTIONNE:
				break;

			case MODE_CREATION:
				break;

			case PARAMETRE_SELECTIONNE:
				majTuleBDD();
				etatCourant = EtatSecurite.PARAMETRE_SELECTIONNE;
				actions(etatCourant);
				break;

			case MODE_APRES_MODIFICATION:
				// Modification dans la BDD
				majTuleBDD();
				etatCourant = EtatSecurite.PARAMETRE_SELECTIONNE;
				actions(etatCourant);
				break;

			case MODE_APRES_SUPPRESSION:
				// interdit
				break;
			default:
				break;
			}
		}
	};

	private void majTuleBDD() {
		/*
		 * ParametrePlongee pp = getParamTempDepuisTuleSelec();
		 * listParamPlongeeBase.remove(pp);
		 * 
		 * 
		 * 
		 * System.out.println("PARAM NOM :" + pp.getmNom());
		 * System.out.println("PARAM CAT :" + pp.getmCategorie().toString());
		 * System.out.println("PARAM DET :" + pp.getmDetails());
		 * pp.setmNom(getTuleSelectionnee().getNom());
		 * pp.setmCategorie(getTuleSelectionnee().getMcategorie());
		 * pp.setmDetails(getTuleSelectionnee().getMdetails());
		 */

		ParametrePlongee pp = getParamDepuisTuleSelec(getTuleSelectionnee());
		boolean test = mPlongee.getmListeParametres().remove(pp);
		listParamPlongeeBase.add(pp);
		if (test) {
			mPlongee.getmListeParametres().add(pp);
		}

		paramPlongeeDAO.update(pp);

		affichBases("MAJ");
	}

	private Runnable mUpdateButtonSuppr = new Runnable() {
		public void run() {
			switch (etatCourant) {
			case AUCUN_PARAMETRE_SELECTIONNE:
				etatCourant = EtatSecurite.AUCUN_PARAMETRE_SELECTIONNE;
				suppressionTuleBDD();
				actions(etatCourant);
				break;

			case MODE_CREATION:
				// Interdit
				break;

			case PARAMETRE_SELECTIONNE:
				etatCourant = EtatSecurite.PARAMETRE_SELECTIONNE;
				suppressionTuleBDD();
				actions(etatCourant);
				break;

			case MODE_APRES_MODIFICATION:
				// Interdit
				break;

			case MODE_APRES_SUPPRESSION:
				etatCourant = EtatSecurite.AUCUN_PARAMETRE_SELECTIONNE;
				suppressionTuleBDD();
				actions(etatCourant);
				break;
			default:
				break;
			}
		}

	};

	private void suppressionTuleBDD() {
		ParametrePlongee ppSupprime = getParamDepuisTuleSelec(tuleSupprime);
		if (mPlongee.getmListeParametres().contains(ppSupprime)) {
			mPlongee.getmListeParametres().remove(ppSupprime);
			paramPlongeeDAO.delete(ppSupprime);
		} else {
			listParamPlongeeBase.remove(ppSupprime);
		}

		affichBases("Suppr");
	}

	private void affichBases(String string) {
		affichParamListPlongee(string);
		affichParammPlongee(string);
		affichParamBDD(string);
	}

	private void affichParamListPlongee(String string) {

		Log.i(LOG_TAG, "----------------- AFFICH " + string
				+ " listParamPlongee -------------------");
		// Affich BDD
		ArrayList<ParametrePlongee> listParamPlongee = listParamPlongeeBase;

		for (ParametrePlongee pp : listParamPlongee) {
			Log.i(LOG_TAG, "AFFICH " + string + " listParamPlongee ; Nom  : "
					+ pp.getmNom() + "  ;  Details : " + pp.getmDetails());
		}
	}

	private void affichParammPlongee(String string) {

		Log.i(LOG_TAG, "----------------- AFFICH " + string
				+ " mPlongee -------------------");
		// Affich mPlongee
		ArrayList<ParametrePlongee> listParamPlongee = (ArrayList<ParametrePlongee>) mPlongee
				.getmListeParametres();

		for (ParametrePlongee pp : listParamPlongee) {
			Log.i(LOG_TAG,
					"AFFICH " + string + " mPLONGEE ; Nom  : " + pp.getmNom()
							+ "  ;  Details : " + pp.getmDetails());
		}
	}

	private void affichParamBDD(String string) {

		Log.i(LOG_TAG, "----------------- AFFICH " + string
				+ " BDD -------------------");
		// Affich BDD
		ArrayList<ParametrePlongee> listParamPlongee = (ArrayList<ParametrePlongee>) paramPlongeeDAO
				.selectParametresPlongeeAvecIdPlongee(mPlongee.getmId());

		for (ParametrePlongee pp : listParamPlongee) {
			Log.i(LOG_TAG,
					"AFFICH " + string + " BDD ; Nom  : " + pp.getmNom()
							+ "  ;  Details : " + pp.getmDetails()
							+ "  ;  ID : " + pp.getmId());
		}
	}

	/**
	 * Ajout la navigation entre les activit�s
	 * 
	 * @param context
	 * @param layout
	 * @param actionBar
	 * @return
	 */
	private NavigationSpinner setActionBar(Context context, int layout,
			ActionBar actionBar) {
		NavigationSpinner _spinner = new NavigationSpinner(context, layout,
				actionBar);
		_spinner.afficherSpinner();
		return _spinner;
	}

	/**
	 * 
	 * @return la tule sélectionnée
	 */
	public TuleParamPlongeeObjetAdapter getTuleSelectionnee() {
		TuleParamPlongeeObjetAdapter retour = null;
		for (TuleParamPlongeeObjetAdapter t : mtulesMateriel) {
			// si une tule est sélectionnée alors on return true
			if (t.getEtatCourant() == TuleParamPlongeeEtat.SELECTIONNE_ET_NON_VALIDE
					|| t.getEtatCourant() == TuleParamPlongeeEtat.SELECTIONNE_ET_VALIDE) {
				retour = t;
			}
		}
		for (TuleParamPlongeeObjetAdapter t : mtulesSignal) {
			// si une tule est sélectionnée alors on return true
			if (t.getEtatCourant() == TuleParamPlongeeEtat.SELECTIONNE_ET_NON_VALIDE
					|| t.getEtatCourant() == TuleParamPlongeeEtat.SELECTIONNE_ET_VALIDE) {
				retour = t;
			}
		}
		return retour;
	}

	// ------------------------------------------------------------------//
	// Fonctions du Formulaire //
	// ------------------------------------------------------------------//
	/**
	 * Active / Désactive les champs du formulaire "Détails"
	 */
	private void activerFormulaire(boolean bool) {
		ImageView icone = (ImageView) findViewById(R.id.iv_securite_icone_param_plongee);
		EditText nom = (EditText) findViewById(R.id.edt_securite_nom_param_plongee);
		Spinner categorie = (Spinner) findViewById(R.id.spinner_securite_cat_parametre);
		EditText details = (EditText) findViewById(R.id.edt_securite_details_parametre);

		icone.setEnabled(bool);
		nom.setEnabled(bool);
		categorie.setEnabled(bool);
		details.setEnabled(bool);

		// active ou non le bouton
		activerLargeBouton(R.id.btn_securite_modifier_param, bool);
		activerImageBouton(R.id.btn_securite_supprimer_param, bool);
	}

	/**
	 * Remplis le formulaire avec les informations du plongeur NB :A factoriser
	 */
	private void remplirFormulaire(ParametrePlongee paramPlongee) {
		ImageView icone = (ImageView) findViewById(R.id.iv_securite_icone_param_plongee);
		EditText nom = (EditText) findViewById(R.id.edt_securite_nom_param_plongee);
		Spinner categorie = (Spinner) findViewById(R.id.spinner_securite_cat_parametre);
		EditText details = (EditText) findViewById(R.id.edt_securite_details_parametre);

		icone.setImageBitmap(getTuleSelectionnee().getIcone());
		nom.setText(paramPlongee.getmNom());
		switch (paramPlongee.getmCategorie()) {
		case MATERIEL_SECURITE:
			categorie.setSelection(0);
			break;
		case SIGNALISATION:
			categorie.setSelection(1);
			break;
		default:

		}
		details.setText(paramPlongee.getmDetails());
	}

	/**
	 * Vide tous les champs du formulaire
	 */
	private void resetFormulaire() {
		ImageView icone = (ImageView) findViewById(R.id.iv_securite_icone_param_plongee);
		EditText nom = (EditText) findViewById(R.id.edt_securite_nom_param_plongee);
		Spinner categorie = (Spinner) findViewById(R.id.spinner_securite_cat_parametre);
		EditText details = (EditText) findViewById(R.id.edt_securite_details_parametre);

		icone.setImageDrawable(getResources().getDrawable(R.drawable.ic_defaut));
		nom.setText("");
		categorie.setSelection(0);
		details.setText("");
	}

	/**
	 * Change le label du bouton
	 * 
	 * @param text
	 */
	private void changeTextBouton(int resourcesId, String text) {
		Button bouton = (Button) findViewById(resourcesId);
		bouton.setText(text);
	}

	/**
	 * Active ou désactive le bouton
	 * 
	 * @param resourcesId
	 * @param bool
	 */
	private void activerLargeBouton(int resourcesId, boolean bool) {
		Button bouton = (Button) findViewById(resourcesId);
		if (!bool) {
			bouton.setBackground(getResources().getDrawable(
					R.drawable.large_bouton_disable));
		} else {
			bouton.setBackground(getResources().getDrawable(
					R.drawable.large_bouton_normal));
		}
		bouton.setEnabled(bool);
	}

	/**
	 * Active ou désactive le bouton
	 * 
	 * @param resourcesId
	 * @param bool
	 */
	private void activerImageBouton(int resourcesId, boolean bool) {
		ImageButton bouton = (ImageButton) findViewById(resourcesId);
		if (!bool) {
			bouton.setBackground(getResources().getDrawable(
					R.drawable.large_bouton_disable));
		} else {
			bouton.setBackground(getResources().getDrawable(
					R.drawable.large_bouton_normal));
		}
		bouton.setEnabled(bool);
	}

	public void activitePrecedente(View v) {
		getActionBar().setSelectedNavigationItem(mOrdre.getIndex() - 1);
	}

	public void activiteSuivante(View v) {
		getActionBar().setSelectedNavigationItem(mOrdre.getIndex() + 1);
	}

	@Override
	public Plongee getmPlongee() {
		return mPlongee;
	}
}
