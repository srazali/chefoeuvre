package fr.enac.actionbars;

import java.util.List;

import com.example.scubadiving.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

/**
 * Utilisé par le spinner pour afficher la liste déroulante et mettre en forme les données
 * @author Jonathan Tolle
 */
public class NavigationListeAdapter extends ArrayAdapter<NavigationObjetAdapter> implements SpinnerAdapter {

	private Context mcontext;
	private List<NavigationObjetAdapter> mMyItems;
	
	/**
	 * Constructeur
	 * @param context
	 * @param resource
	 * @param myItems
	 */
	public NavigationListeAdapter(Context context, int resource, List<NavigationObjetAdapter> myItems) {
		super(context, resource, myItems);
		this.mcontext = context;
		this.mMyItems = myItems;


	}

	/**
	 * Utils� pour afficher le spinner d�roulant
	 */
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.navigation_spinner, null);
		}
		NavigationObjetAdapter item = mMyItems.get(position);

		if (item != null) {
			ImageView image = (ImageView) view.findViewById(R.id.imv_icone);
			TextView titre = (TextView) view.findViewById(R.id.tv_activity);
			image.setImageDrawable(item.getmDrawable());
			titre.setText(item.getmActivity());
		}

		return view;
	}

	/**
	 * Utilis� pour afficher le top de la liste d�roulante
	 */
	@Override
	public View getView(int position, View convertView,
			ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.navigation_topspinner, null);

		TextView text = (TextView) view.findViewById(R.id.tv_nom);		
		text.setText(mcontext.getResources().getString(R.string.acb_menuNavigation));
		return text;
	}



}
