package fr.enac.actionbars;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;

import com.example.scubadiving.R;

import fr.enac.activities.BaseActivity;
import fr.enac.activities.PalanqueeActivity;
import fr.enac.activities.PlongeeActivity;
import fr.enac.activities.PlongeurActivity;
import fr.enac.activities.SecuriteActivity;
import fr.enac.activities.TourActivity;
import fr.enac.models.Plongee;

/**
 * Cette classe représentent le navigation spinner qui
 * sert à naviguer entre les activités dans l'actionbar.
 * 
 * @author Jonathan Tolle
 */
public class NavigationSpinner implements OnNavigationListener{

	private static final String LOG_TAG = "SPINNER";
	
	/* Informations sur l'activité reliée au navigation spinner */
	private Context mContext;
	private ActionBar mActionBar;
	private int mLayout;

	/* Données */
	private List<NavigationObjetAdapter> mListeItems;
	private NavigationListeAdapter mAdapter;
	private boolean isActivityInitialisee;
	
	/* Objets */
	private int mCurrentItem;

	/**
	 * Constructeur.
	 * 
	 * @param context
	 * @param layout
	 * @param actionBar
	 */
	public NavigationSpinner(Context context, int layout, ActionBar actionBar) {
		this.mContext = context;
		this.mActionBar = actionBar;
		this.mLayout = layout;
		this.isActivityInitialisee = false;
		this.mListeItems = new ArrayList<NavigationObjetAdapter>();
		
		String currentActivity = context.getClass().getName();
		if(currentActivity.equals(PlongeeActivity.class.getName()))
			mCurrentItem = 0;
		else if(currentActivity.equals(PlongeurActivity.class.getName()))
			mCurrentItem = 1;
		else if(currentActivity.equals(PalanqueeActivity.class.getName()))
			mCurrentItem = 2;
		else if(currentActivity.equals(SecuriteActivity.class.getName()))
			mCurrentItem = 3;
		else if(currentActivity.equals(TourActivity.class.getName()))
			mCurrentItem = 4;
	}
	
	/**
	 * Cette méthode peuple la liste des données, les affecte au
	 * navigation spinner et ajoute celui si à l'action barre.
	 */
	public void afficherSpinner() {
		Resources resources = mContext.getResources();
		// Ajout des liens entre les activit�s (mettre les liens dans le res.STRING)
		mListeItems.add(new NavigationObjetAdapter(resources.getDrawable(R.drawable.ic_action_next_item),
				resources.getString(R.string.acb_plongeeActivity)));
		mListeItems.add(new NavigationObjetAdapter(resources.getDrawable(R.drawable.ic_action_next_item),
				resources.getString(R.string.acb_plongeurActivity)));
		mListeItems.add(new NavigationObjetAdapter(resources.getDrawable(R.drawable.ic_action_next_item),
				resources.getString(R.string.acb_palanqueeActivity)));
		mListeItems.add(new NavigationObjetAdapter(resources.getDrawable(R.drawable.ic_action_next_item),
				resources.getString(R.string.acb_securiteActivity)));
		mListeItems.add(new NavigationObjetAdapter(resources.getDrawable(R.drawable.ic_action_next_item),
				resources.getString(R.string.acb_tourActivity)));

		// Ajout de l'adapater 
		mAdapter = new NavigationListeAdapter(mContext, mLayout , mListeItems);
		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		mActionBar.setListNavigationCallbacks(mAdapter, this);		
	}

	/**
	 * Listener.
	 */
	@Override
	public boolean onNavigationItemSelected(int position, long itemId) {
		if(isActivityInitialise() && position != mCurrentItem){
			mCurrentItem = position;
			changeActivity(position);
		}
		return true;
	}

	/**
	 * Cette méthode change l'activité en fonction de l'item sélectionné.
	 * Un intent est créé afin de changer d'activité, la plongee à transmettre
	 * est passée dans l'intent.
	 * 
	 * @param position
	 */
	public void changeActivity(int position) {
		Intent intent;
		Plongee plongeeATransmettre = ((BaseActivity) mContext).getmPlongee();	
		
		switch (position) {
			case 0:
				if (!mContext.getClass().getName().equals(PlongeeActivity.class.getName())) {
					intent= new Intent(mContext, PlongeeActivity.class);
					intent.putExtra(mContext.getResources().getString(R.string.intent_clePlongeeObjet), plongeeATransmettre);
					Log.e(LOG_TAG, "Change Activity : "+PlongeeActivity.class.getName());
					mContext.startActivity(intent);
				}
				break;
			case 1:
				if (!mContext.getClass().getName().equals(PlongeurActivity.class.getName())) {
					intent = new Intent(mContext, PlongeurActivity.class);
					intent.putExtra(mContext.getResources().getString(R.string.intent_clePlongeeObjet), plongeeATransmettre);
					Log.e(LOG_TAG, "Change Activity : "+PlongeurActivity.class.getName());
					mContext.startActivity(intent);
				}
				break;
			case 2:
				if (!mContext.getClass().getName().equals(PalanqueeActivity.class.getName())) {
					intent = new Intent(mContext, PalanqueeActivity.class);
					intent.putExtra(mContext.getResources().getString(R.string.intent_clePlongeeObjet), plongeeATransmettre);
					Log.e(LOG_TAG, "Change Activity : "+PalanqueeActivity.class.getName());
					mContext.startActivity(intent);
				}
				break;
			case 3:
				if (!mContext.getClass().getName().equals(SecuriteActivity.class.getName())) {
					intent = new Intent(mContext, SecuriteActivity.class);
					intent.putExtra(mContext.getResources().getString(R.string.intent_clePlongeeObjet), plongeeATransmettre);
					Log.e(LOG_TAG, "Change Activity : "+SecuriteActivity.class.getName());
					mContext.startActivity(intent);
				}
				break;
			case 4:
				if (!mContext.getClass().getName().equals(TourActivity.class.getName())) {
					intent = new Intent(mContext, TourActivity.class);
					intent.putExtra(mContext.getResources().getString(R.string.intent_clePlongeeObjet), plongeeATransmettre);
					Log.e(LOG_TAG, "Change Activity : "+TourActivity.class.getName());
					mContext.startActivity(intent);
				}
				break;
			default:
				break;
		}
	}

	//***** GETTERS AND SETTERS *****
	
	public boolean isActivityInitialise() {
		return isActivityInitialisee;
	}

	public void setActivityInitialise(boolean isActivityInitialisee) {
		this.isActivityInitialisee = isActivityInitialisee;
	}

}
