package fr.enac.actionbars;

import android.graphics.drawable.Drawable;

/**
 * Stock les objets du navigation spinner
 * @author Jonathan Tolle
 */
public class NavigationObjetAdapter {

	private Drawable mDrawable;
	private String mActivity;

	/**
	 * Constructor
	 * @param mDrawable
	 * @param mActivity
	 */
	public NavigationObjetAdapter(Drawable mDrawable, String mActivity) {
		super();
		this.mDrawable = mDrawable;
		this.mActivity = mActivity;
	}

	/*
	 * Getters et Setters
	 */

	public Drawable getmDrawable() {
		return mDrawable;
	}

	public String getmActivity() {
		return mActivity;
	}

	public void setmDrawable(Drawable mDrawable) {
		this.mDrawable = mDrawable;
	}

	public void setmActivity(String mActivity) {
		this.mActivity = mActivity;
	}
}
