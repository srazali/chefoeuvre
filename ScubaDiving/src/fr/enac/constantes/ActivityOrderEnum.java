package fr.enac.constantes;

public enum ActivityOrderEnum {
	PLONGEE_ACTIVITY,
	PLONGEUR_ACTIVITY,
	PALANQUEE_ACTIVITY,
	SECURITE_ACTIVITY,
	TOUR_ACTIVITY;
	
	public int getIndex() {
		return ordinal();
	}
}
