package com.survivingwithandroid.weatherapp;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import android.util.Log;

public class WeatherHttpClient {

	private static String APPID = "APPID=50a0e0cdefe1333197f6c99acdd8284f";
	private static String BASE_URL = "http://api.openweathermap.org/data/2.5/weather?lang=fr&units=metric&"
			+ APPID + "&q=";
	private static String IMG_URL = "http://api.openweathermap.org/img/w/";

	public String getWeatherData(String location) {
		try {
			return downloadUrl(BASE_URL + location);
		} catch (IOException e) {
			//e.printStackTrace();
			return null;
		}
	}

	public byte[] getImage(String code) {
		HttpURLConnection con = null;
		InputStream is = null;
		try {
			con = (HttpURLConnection) (new URL(IMG_URL + code + ".png"))
					.openConnection();
			con.setRequestMethod("GET");
			con.setReadTimeout(10000 /* milliseconds */);
			con.setConnectTimeout(15000 /* milliseconds */);
			con.setDoInput(true);
			con.connect();

			// Let's read the response
			is = con.getInputStream();
			byte[] buffer = new byte[1024];
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			while (is.read(buffer) != -1)
				baos.write(buffer);

			return baos.toByteArray();
		} catch (Throwable t) {
			//t.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (Throwable t) {
			}
			try {
				con.disconnect();
			} catch (Throwable t) {
			}
		}

		return null;

	}

	// Given a URL, establishes an HttpUrlConnection and retrieves
	// the web page content as a InputStream, which it returns as
	// a string.
	private String downloadUrl(String myurl) throws IOException {
		InputStream is = null;
		//Log.d("Download", myurl);
		try {
			URL url = new URL(myurl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(10000 /* milliseconds */);
			conn.setConnectTimeout(15000 /* milliseconds */);
			conn.setRequestMethod("GET");
			conn.setDoInput(true);
			// Starts the query
			conn.connect();
			int response = conn.getResponseCode();
			if (response < 200 || response > 205) {
				Log.d(this.getClass().toString(),
						"(downloadURL) response code=" + response);
				throw new IOException();
			}
			is = conn.getInputStream();

			// Convert the InputStream into a string
			String contentAsString = stream2String(is);
			return contentAsString;

			// Makes sure that the InputStream is closed after the app is
			// finished using it.
		} finally {
			if (is != null) {
				is.close();
			}
		}
	}

	// Reads an InputStream and converts it to a String.
	private String stream2String(InputStream stream) throws IOException,
			UnsupportedEncodingException {
		BufferedReader reader = null;
		reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
		return reader.readLine();
	}

}
